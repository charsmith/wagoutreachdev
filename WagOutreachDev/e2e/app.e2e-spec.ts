import { Walgreens.UIPage } from './app.po';

describe('walgreens.ui App', () => {
  let page: Walgreens.UIPage;

  beforeEach(() => {
    page = new Walgreens.UIPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
