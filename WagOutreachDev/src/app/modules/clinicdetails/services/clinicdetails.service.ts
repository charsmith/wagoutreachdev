import { Injectable } from '@angular/core';
import { ClinicDetails } from '../../../models/clinic-details';
import { Http, Response, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from "@angular/router";
import { ClinicDetailsJSON } from '../../../JSON/ClinicDetails';
import { environment } from '../../../../environments/environment';
import { Util } from '../../../utility/util';

@Injectable()
export class ClinicdetailsService {
  private formData: ClinicDetails = new ClinicDetails();
  constructor(private http: Http, private router: Router) {
    this.formData= ClinicDetailsJSON;
   }
  getCoClinicDetails() :Observable<any>{
    return Observable.of(ClinicDetailsJSON);
  }

  confirmClinicDetails(clinicDetails: any): Observable<any> {
    var url = environment.API_URL + environment.RESOURCES.CONFIRM_CLINIC_DETAILS;
   
    return this.http.post(url,clinicDetails,Util.getRequestHeaders()).catch((err) => Util.handleError(err, this.router));
}

}
