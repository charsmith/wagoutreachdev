import { TestBed, inject } from '@angular/core/testing';

import { ClinicdetailsService } from './clinicdetails.service';

describe('ClinicdetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClinicdetailsService]
    });
  });

  it('should be created', inject([ClinicdetailsService], (service: ClinicdetailsService) => {
    expect(service).toBeTruthy();
  }));
});
