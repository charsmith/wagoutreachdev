import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReusableModule } from '../../modules/common/components/reusable.module';
import { NgPipesModule } from 'ngx-pipes';
import { CalendarModule } from 'primeng/calendar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GrowlModule, MessagesModule, MessageModule, AutoCompleteModule } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { ConfirmationService } from 'primeng/components/common/api';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InputMaskModule, AccordionModule, KeyFilterModule, FieldsetModule, DialogModule, DataTableModule, TabViewModule, DataListModule, ConfirmDialogModule } from 'primeng/primeng';
import { KeyFilter, KEYFILTER_VALIDATOR } from 'primeng/primeng';
import { CharityProgramClinicDetailsComponent } from './components/charity-program-clinic-details/charity-program-clinic-details.component';
import { CoClinicDetailsComponent } from './components/co-clinic-details/co-clinic-details.component';
import { CorporateClinicDetailsComponent } from './components/corporate-clinic-details/corporate-clinic-details.component';
import { LocalClinicDetailsComponent } from './components/local-clinic-details/local-clinic-details.component';
import { ClinicdetailsService } from './services/clinicdetails.service';
import { CommonclinicdetailsComponent } from './components/commonclinicdetails.component';
import { OutreachProgramService } from '../contractaggreement/services/outreach-program.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgPipesModule,
    ReusableModule,
    CalendarModule,
    BrowserAnimationsModule,
    GrowlModule,
    MessagesModule,
    MessageModule,
    ConfirmDialogModule,
    DialogModule,
    KeyFilterModule,
    ReusableModule,
    AccordionModule,
    AutoCompleteModule,

  ],
  declarations: [CharityProgramClinicDetailsComponent, CoClinicDetailsComponent, CorporateClinicDetailsComponent,
    LocalClinicDetailsComponent,
    CommonclinicdetailsComponent],
  providers: [ClinicdetailsService, OutreachProgramService, ConfirmationService]// MessageService, ConfirmationService

})
export class ClinicDetailsModule { }
