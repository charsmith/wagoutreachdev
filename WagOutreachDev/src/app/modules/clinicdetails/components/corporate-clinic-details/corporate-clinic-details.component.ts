import {
  NgModule,
  Component,
  OnInit,
  ViewChild,
  Injectable,
  ViewContainerRef
} from '@angular/core';

import { CommonModule } from '@angular/common';

import {
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray,
  ReactiveFormsModule
} from '@angular/forms';

import { APP_ROUTES } from './../../../../routes/app.routes';
import { ClinicDetails, LocationDetail } from '../../../../models/clinic-details';
import { ClinicdetailsService } from '../../services/clinicdetails.service';
import { OutReachProgramType } from '../../../../models/OutReachPrograms';

@Component({
  selector: 'app-corporate-clinic-details',
  templateUrl: './corporate-clinic-details.component.html',
  styleUrls: ['./corporate-clinic-details.component.css']
})
export class CorporateClinicDetailsComponent implements OnInit {

  
  
  showHints:boolean = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff"))=='yes')?false:true;
  pageName:string = "localclinicprogramdetails";

  
  clinicDetailsForm: FormGroup;
  coClinicDetails: ClinicDetails = new ClinicDetails();
  OutReachProgramType: string;
  defaultDate: Date = new Date('Mon Jan 1 1900 00:00:00');
  maxDateValue: Date = new Date('Mon Apr 30 2018 23:59:59');
  minDateValue: Date = new Date('Mon May 01 2017 00:00:00');
  constructor(private formBuilder: FormBuilder, private coClinicdetailsService: ClinicdetailsService) {
  }

  ngOnInit() {
    this.OutReachProgramType = OutReachProgramType.communityoutreach;
    this.coClinicdetailsService.getCoClinicDetails().subscribe(data => {
      this.coClinicDetails = data
    });
    this.defaultDate = new Date();
    this.minDateValue = new Date();
    let today = new Date();
    this.maxDateValue = new Date(today.getFullYear() + 5, today.getMonth(), today.getDate() - 1);
    this.bindControls();
  }

  bindControls() {

    this.clinicDetailsForm = this.formBuilder.group({
      // store information
      storeID: this.formBuilder.control(this.coClinicDetails.StoreUserDetails.storeID, Validators.required),
     
      // Billing & Vaccine Information
      naClinicPlanId: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.naClinicPlanId, Validators.required),
      naClinicGroupId: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.naClinicGroupId, Validators.required),
      recipientId: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.recipientId, Validators.required),
      // ImmunizationDetails:this.initImmunization(),
      naClinicAddlComments: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.naClinicAddlComments, Validators.required),
      // Clinic location info
      // locationDetails:this.initClinicLocations(),
      //pharmacist info
      pharmacistName: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.pharmacistName, Validators.required),
      pharmacistPhone: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.pharmacistPhone, Validators.required),
      totalHours: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.totalHours, Validators.required),
      feedbackNotes: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.feedbackNotes)
    });

  }

}
