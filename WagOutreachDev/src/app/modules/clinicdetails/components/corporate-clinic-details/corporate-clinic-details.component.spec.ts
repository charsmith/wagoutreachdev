import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateClinicDetailsComponent } from './corporate-clinic-details.component';

describe('CorporateClinicDetailsComponent', () => {
  let component: CorporateClinicDetailsComponent;
  let fixture: ComponentFixture<CorporateClinicDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateClinicDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateClinicDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
