import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonclinicdetailsComponent } from './commonclinicdetails.component';

describe('CommonclinicdetailsComponent', () => {
  let component: CommonclinicdetailsComponent;
  let fixture: ComponentFixture<CommonclinicdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonclinicdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonclinicdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
