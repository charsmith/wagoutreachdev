import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalClinicDetailsComponent } from './local-clinic-details.component';

describe('LocalClinicDetailsComponent', () => {
  let component: LocalClinicDetailsComponent;
  let fixture: ComponentFixture<LocalClinicDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalClinicDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalClinicDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
