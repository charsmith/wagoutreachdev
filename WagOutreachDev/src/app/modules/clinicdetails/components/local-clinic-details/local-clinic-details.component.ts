import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ClinicdetailsService } from '../../services/clinicdetails.service';
import { ClinicDetails, LocationDetail } from '../../../../models/clinic-details';
//import { Utility } from '../../../common/utility';
import { CommonclinicdetailsComponent } from '../commonclinicdetails.component';
import { viewParentEl } from '@angular/core/src/view/util';
import { APP_ROUTES } from './../../../../routes/app.routes';
import { Router } from '@angular/router';
import { states } from '../../../../JSON/States';
import { Util } from '../../../../utility/util';
import { TimeToDatePipe } from '../../../contractaggreement/pipes/time-to-date.pipe';

@Component({
  selector: 'app-local-clinic-details',
  templateUrl: './local-clinic-details.component.html',
  styleUrls: ['./local-clinic-details.component.css'],
  providers: [TimeToDatePipe],
  outputs: ['fileUploaded']
})
export class LocalClinicDetailsComponent implements OnInit {

  showHints:boolean = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff"))=='yes')?false:true;
  pageName:string = "localclinicprogramdetails";

  clinicDetailsForm: FormGroup;
  coClinicDetails: ClinicDetails = new ClinicDetails();
  @Output() clinicSubmit = new EventEmitter<number>();
  validate: boolean;
  states: any[];
  dlgDispCOViewAgreement: boolean=false;
  defaultDate: Date = new Date('Mon Jan 1 1900 00:00:00');
  maxDateValue: Date = new Date('Mon Apr 30 2018 23:59:59');
  minDateValue: Date = new Date('Mon May 01 2017 00:00:00');
  clinicLocation: number;
  deleteAgreement : boolean = false;
  storeV:boolean=false;
  distV:boolean=false;
  ariaV:boolean=false;
  constructor(private formBuilder: FormBuilder, private coClinicdetailsService: ClinicdetailsService,
    private _timeToDatePipe: TimeToDatePipe, private utility: Util, private router: Router) {
  }

  ngOnInit() {
    this.coClinicdetailsService.getCoClinicDetails().subscribe(data => {
      this.coClinicDetails = data
    });

    this.clinicLocation = 1;
    this.defaultDate = new Date();
    this.minDateValue = new Date();
    let today = new Date();
    this.maxDateValue = new Date(today.getFullYear() + 5, today.getMonth(), today.getDate() - 1);
    this.states = states;
    this.bindControls();
  }

  dlgCOViewAgreement(){
    this.dlgDispCOViewAgreement=!this.dlgDispCOViewAgreement;
  }

  onChange(reassignVal)
 {
   debugger;
   if(reassignVal==1){
  this.storeV=true;
  this.distV=false;
  this.ariaV=false;
}
if(reassignVal==2){
  this.distV=true;
  this.storeV=false;
  this.ariaV=false;
}
if(reassignVal==3){
  this.ariaV=true;
  this.storeV=false;
  this.distV=false;
}
if(reassignVal==4){
  this.ariaV=false;
  this.storeV=false;
  this.distV=false;
}
 }
  closePop(){
    this.dlgDispCOViewAgreement=false;
  }
  bindControls() {
    this.clinicDetailsForm = this.formBuilder.group({
      // store information
      storeID: this.formBuilder.control(this.coClinicDetails.StoreUserDetails.storeID, Validators.required),
      // Business Information
      firstName: this.formBuilder.control(this.coClinicDetails.BusinessInformation.firstName, Validators.required),
      phone: this.formBuilder.control(this.coClinicDetails.BusinessInformation.phone, Validators.required),
      lastName: this.formBuilder.control(this.coClinicDetails.BusinessInformation.lastName, Validators.required),
      email: this.formBuilder.control(this.coClinicDetails.BusinessInformation.businessContactEmail, null),
      jobTitle: this.formBuilder.control(this.coClinicDetails.BusinessInformation.jobTitle, null),
      // Billing & Vaccine Information
      naClinicPlanId: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.naClinicPlanId, Validators.required),
      naClinicGroupId: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.naClinicGroupId, Validators.required),
      recipientId: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.recipientId, Validators.required),
      ImmunizationDetails: this.initImmunization(),
      naClinicAddlComments: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.naClinicAddlComments, null),
      //Clinic Contact Information
      locationDetails: this.initClinicLocations(),
      //pharmacist info
      pharmacistName: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.pharmacistName, Validators.required),
      pharmacistPhone: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.pharmacistPhone, Validators.required),
      totalHours: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.totalHours, Validators.required),
      feedbackNotes: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.feedbackNotes)
    });
  }
  initClinicLocations() {
    let start_time: Date;
    let end_time: Date;
    let loc_array: any[] = [];
    this.coClinicDetails.LocationDetails.forEach(loc => {
      this.clinicLocation = this.utility.colLetterToNum(loc.naClinicLocation);
      this.clinicLocation++
      loc_array.push(
        this.formBuilder.group(
          {
            naClinicLocation: this.formBuilder.control(loc.naClinicLocation, null),
            naClinicAddress1: this.formBuilder.control(loc.naClinicAddress1, Validators.required),
            naClinicAddress2: this.formBuilder.control(loc.naClinicAddress2),
            clinicDate: this.formBuilder.control(loc.clinicDate, Validators.required),
            naClinicStartTime: this.formBuilder.control(this._timeToDatePipe.transform(loc.clinicDate, loc.naClinicStartTime), Validators.required),
            naClinicEndTime: this.formBuilder.control(this._timeToDatePipe.transform(loc.clinicDate, loc.naClinicEndTime), Validators.required),
            naContactFirstName: this.formBuilder.control(loc.naContactFirstName, Validators.required),
            naContactLastName: this.formBuilder.control(loc.naContactLastName, Validators.required),
            naClinicContactPhone: this.formBuilder.control(loc.naClinicContactPhone, Validators.required),
            naContactEmail: this.formBuilder.control(loc.naContactEmail, Validators.required),
            naClinicCity: this.formBuilder.control(loc.naClinicCity, Validators.required),
            naClinicState: this.formBuilder.control(loc.naClinicState, Validators.required),
            naClinicZip: this.formBuilder.control(loc.naClinicZip, Validators.required),
            confirmedClientName: this.formBuilder.control(loc.confirmedClientName, null),
            previousContact: this.formBuilder.control('', null),
            previousLocation: this.formBuilder.control('', null),
            immunizations: this.AddInitialdummyClinicImmunizations()
          }
        )

      )
    });

    return this.formBuilder.array(loc_array);

  }
  AddInitialdummyClinicImmunizations() {
    let imz_array: any[] = [];
    this.coClinicDetails.LocationDetails[0].ImmunizationDetails.forEach(imz => {
      imz_array.push(
        this.formBuilder.group(
          {
            pk: this.formBuilder.control(imz.pk),
            estimatedQuantity: this.formBuilder.control(imz.estimatedQuantity, Validators.required),
            immunizationName: this.formBuilder.control(imz.immunizationName),
            paymentTypeName: this.formBuilder.control(imz.paymentTypeName),
            paymentTypeId: this.formBuilder.control(imz.paymentTypeID)
          }
        )
      )
    });

    return this.formBuilder.array(imz_array);

  }
  removeLocation(i: number) {
    const control = <FormArray>this.clinicDetailsForm.controls['locationDetails'];
    control.removeAt(i);
    this.clinicLocation--;
    for (let index = 1; index < control.length + 1; index++) {
      let element = control.controls[index - 1];
      if (index <= this.clinicLocation) {
        element.get('naClinicLocation').setValue(this.utility.numberToLetters(index));
      }
    };
  }
  initImmunization() {
    let imz_array: any[] = [];
    this.coClinicDetails.LocationDetails[0].ImmunizationDetails.forEach(imz => imz_array.push(
      this.formBuilder.group(
        {
          pk: this.formBuilder.control(imz.pk),
          estimatedQuantity: this.formBuilder.control(imz.estimatedQuantity, Validators.required),
          immunizationName: this.formBuilder.control(imz.immunizationName),
          paymentTypeName: this.formBuilder.control(imz.paymentTypeName),
          paymentTypeId: this.formBuilder.control(imz.paymentTypeID),
          price: this.formBuilder.control(imz.price),
          totalImmAdministered: this.formBuilder.control(imz.totalImmAdministered)
        }
      )
    ));
    return this.formBuilder.array(imz_array);
  }
  addLocation(location) {
    const control = <FormArray>this.clinicDetailsForm.controls['locationDetails'];
    const addrCtrl = this.initAddClinicLocation(location);
    control.push(addrCtrl);
  }
  initAddClinicLocation(location: LocationDetail) {
    if (location === undefined) {
      location = new LocationDetail();
    }
    location.naClinicLocation = this.utility.numberToLetters(this.clinicLocation);
    this.clinicLocation++;
    return (this.formBuilder.group(
      {
        naClinicLocation: this.formBuilder.control(location.naClinicLocation),
        naClinicAddress1: this.formBuilder.control(location.naClinicAddress1, Validators.required),
        naClinicAddress2: this.formBuilder.control(location.naClinicAddress2, null),
        clinicDate: this.formBuilder.control(location.clinicDate, Validators.required),
        naClinicStartTime: this.formBuilder.control(location.naClinicStartTime, Validators.required),
        naClinicEndTime: this.formBuilder.control(location.naClinicEndTime, Validators.required),
        naContactFirstName: this.formBuilder.control(location.naContactFirstName, Validators.required),
        naContactLastName: this.formBuilder.control(location.naContactLastName, Validators.required),
        naClinicContactPhone: this.formBuilder.control(location.naClinicContactPhone, Validators.required),
        naContactEmail: this.formBuilder.control(location.naContactEmail, Validators.required),
        naClinicCity: this.formBuilder.control(location.naClinicCity, Validators.required),
        naClinicState: this.formBuilder.control(location.naClinicState, Validators.required),
        naClinicZip: this.formBuilder.control(location.naClinicZip, Validators.required),
        confirmedClientName: this.formBuilder.control(location.confirmedClientName, null),
        previousContact: this.formBuilder.control('', null),
        previousLocation: this.formBuilder.control('', null),
        immunizations: this.AddAddlClinicImmunizations()
      }
    ));
  }
  AddAddlClinicImmunizations() {
    let imz_array: any[] = [];
    this.coClinicDetails.LocationDetails[0].ImmunizationDetails.forEach(imz => imz_array.push(
      this.formBuilder.group(
        {
          pk: this.formBuilder.control(imz.pk),
          estimatedQuantity: this.formBuilder.control('', Validators.required),
          immunizationName: this.formBuilder.control(imz.immunizationName),
          paymentTypeName: this.formBuilder.control(imz.paymentTypeName),
          paymentTypeId: this.formBuilder.control(imz.paymentTypeID),
        }
      )
    ));
    return this.formBuilder.array(imz_array);

  }



  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  displayFieldCssForArray(field: string, index: number) {
    let return_value = this.isValidArrayField(field, index);
    return {
      'has-error': return_value,
      'has-feedback': return_value
    };
  }

  displayFieldCssForLocImm(field: string, i: number, im: number) {
    const control = <FormArray>this.clinicDetailsForm.controls['locationDetails'];
    const immArray = <FormArray>control.controls[i].get('immunizations');
    const estQty = immArray.controls[im].get('estimatedQuantity');
    let return_val = !estQty.valid && estQty.touched;
    return {
      'has-error': return_val,
      'has-feedback': return_val
    };
  }

  isFieldValid(field: string) {
    if (this.clinicDetailsForm.get(field) != null)
      return !this.clinicDetailsForm.get(field).valid && this.clinicDetailsForm.get(field).touched;
  }

  isValidArrayField(fields: string, index: number) {
    let return_value = false;
    Object.keys(this.clinicDetailsForm.controls).forEach(field => {
      const control = this.clinicDetailsForm.get(field);
      if (control instanceof FormArray) {
        if (control.controls[index] !== undefined) {
          if (control.controls[index].get(fields) != null) {
            return_value = !control.controls[index].get(fields).valid && control.controls[index].get(fields).touched;
          }
        }
      }
    });
    return return_value;
  }

  isFormValid(): boolean {
    let frmar: FormArray = this.clinicDetailsForm.get('locationDetails') as FormArray;
    for (let index = 0; index < frmar.length; index++) {
      const element = frmar.controls[index] as FormGroup;
      this.utility.validateAllFormFields(element);
    }
    return this.clinicDetailsForm.valid;
  }
  confirmClinic() {
    if (this.clinicDetailsForm.valid) {
      this.clinicSubmit.emit(2);
      window.scrollTo(0, 0);
    }
    else {
      alert("Mandatory Fields are not entered. Please fill mandatory fields!");
      this.utility.validateAllFormFields(this.clinicDetailsForm);
    }
  }
  onlyNumbers(event: any) {
    Util.onlyNumbers(event);
  }
  completeClinic() {
    let control = <FormArray>this.clinicDetailsForm.controls['ImmunizationDetails'];

    for (let index = 0; index < control.length; index++) {
      control.controls[index].get('totalImmAdministered').setValidators([Validators.required]);
      control.controls[index].get('totalImmAdministered').updateValueAndValidity();     
      this.utility.validateAllFormFields(<FormGroup>control.controls[index]);
      if (!control.controls[index].valid) {       
        return;
      }    
    };
  
    // this.validate = true;
    this.clinicSubmit.emit(3);
    window.scrollTo(0, 0);

  }

 
  deleteAgreementData() {
    window.scrollTo(0,0);
    this.deleteAgreement = true;
  }
  cancelClinic() {
    this.clinicSubmit.emit(-1);
    window.scrollTo(0, 0);
  }
  
  copyPreviousLocation(formIndex: number, checked: boolean) {
    if (!checked) {
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicAddress1').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicAddress2').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicCity').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicState').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicZip').setValue('');
    }
    else {
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicAddress1').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicAddress1').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicAddress2').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicAddress2').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicCity').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicCity').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicState').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicState').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicZip').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicZip').value);
    }
  }

  copyPreviousContactInfo(formIndex: number, checked: boolean) {    
    if (!checked) {
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactFirstName').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactLastName').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactEmail').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicContactPhone').setValue('');
    }
    else {
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactFirstName').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naContactFirstName').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactLastName').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naContactLastName').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactEmail').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naContactEmail').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicContactPhone').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicContactPhone').value);
    }
  }

}
