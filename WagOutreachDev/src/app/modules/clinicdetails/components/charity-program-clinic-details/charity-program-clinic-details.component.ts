import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-charity-program-clinic-details',
  templateUrl: './charity-program-clinic-details.component.html',
  styleUrls: ['./charity-program-clinic-details.component.css']
})
export class CharityProgramClinicDetailsComponent implements OnInit {

  
  showHints:boolean = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff"))=='yes')?false:true;
  pageName:string = "localclinicprogramdetails";


  constructor() { }

  ngOnInit() {
  }

}
