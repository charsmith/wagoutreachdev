import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharityProgramClinicDetailsComponent } from './charity-program-clinic-details.component';

describe('CharityProgramClinicDetailsComponent', () => {
  let component: CharityProgramClinicDetailsComponent;
  let fixture: ComponentFixture<CharityProgramClinicDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharityProgramClinicDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharityProgramClinicDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
