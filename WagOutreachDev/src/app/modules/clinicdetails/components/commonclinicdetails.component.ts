import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-commonclinicdetails',
  templateUrl: './commonclinicdetails.component.html',
  styleUrls: ['./commonclinicdetails.component.css']
})
export class CommonclinicdetailsComponent implements OnInit {

  showHints:boolean = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff"))=='yes')?false:true;
  pageName:string = "localclinicprogramdetails";

  
  step: number = 1;
  clinicstatus : string = 'Completed Clinic';
  clickedPath='LocalClinicProgramDetails';

  constructor( private router: Router) {
    if(router.url.search('LocalClinicProgramDetails') > 0){
      this.clickedPath='LocalClinicProgramDetails';
    }
    else if(router.url.search('CommunityOutreachProgramDetails') > 0){
      this.clickedPath='CommunityOutreachProgramDetails';
    }
    else if(router.url.search('CorporateClinicProgramDetails') > 0){
      this.clickedPath='CorporateClinicProgramDetails';
    }
   }

  ngOnInit() {
  }
  GoToStep(step: number) {
  if(step == -1)
  {
    step=3;
    this.clinicstatus = 'Cancelled Clinic';
  }
    this.step = step;
    return;
}
}
