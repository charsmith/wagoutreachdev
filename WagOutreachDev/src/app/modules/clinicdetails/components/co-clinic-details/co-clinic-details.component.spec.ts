import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoClinicDetailsComponent } from './co-clinic-details.component';

describe('CoClinicDetailsComponent', () => {
  let component: CoClinicDetailsComponent;
  let fixture: ComponentFixture<CoClinicDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoClinicDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoClinicDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
