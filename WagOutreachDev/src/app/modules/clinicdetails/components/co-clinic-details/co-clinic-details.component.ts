import {
  NgModule,
  Component,
  OnInit,
  ViewChild,
  Injectable,
  ViewContainerRef,
  Output,
  EventEmitter
} from '@angular/core';

import { CommonModule } from '@angular/common';

import {
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray,
  ReactiveFormsModule
} from '@angular/forms';

import { APP_ROUTES } from './../../../../routes/app.routes';
import { ClinicDetails, LocationDetail } from '../../../../models/clinic-details';
import { ClinicdetailsService } from '../../services/clinicdetails.service';
import { OutReachProgramType } from '../../../../models/OutReachPrograms';
//import { Utility } from '../../../common/utility';
import { debug } from 'util';
import { Router } from '@angular/router';
import { states } from '../../../../JSON/States';
import { COOutReachType } from '../../../../JSON/COOutReachType';
import { Util } from '../../../../utility/util';
import { TimeToDatePipe } from '../../../contractaggreement/pipes/time-to-date.pipe';



@Component({
  selector: 'app-co-clinic-details',
  templateUrl: './co-clinic-details.component.html',
  styleUrls: ['./co-clinic-details.component.css'],
  providers: [TimeToDatePipe]
})

export class CoClinicDetailsComponent implements OnInit {
  clinicDetailsForm: FormGroup;
  coClinicDetails: ClinicDetails = new ClinicDetails();
  clinicDetails: ClinicDetails = new ClinicDetails();
  //clinicDetailsData: ClinicDetailsData = new ClinicDetailsData();
  defaultStartTime: Date = new Date('Mon Jan 1 1900 00:00:00')
  defaultDate: Date = new Date('Mon Jan 1 1900 00:00:00');
  maxDateValue: Date = new Date('Mon Apr 30 2018 23:59:59');
  minDateValue: Date = new Date('Mon May 01 2017 00:00:00');
  states: any[];
  coOutReachTypes:any[];
  @Output()
  clinicSubmit = new EventEmitter<number>();
  validate: boolean;
  clinicLocation: number;
  constructor(private formBuilder: FormBuilder, private coClinicdetailsService: ClinicdetailsService,
    private _timeToDatePipe: TimeToDatePipe, private utility: Util,private router: Router) { }

  ngOnInit() {
    this.coClinicdetailsService.getCoClinicDetails().subscribe(data => {
      this.coClinicDetails = data
    });
    
    this.clinicLocation = 1;
    this.defaultDate = new Date();
    this.minDateValue = new Date();
    let today = new Date();
    this.maxDateValue = new Date(today.getFullYear() + 5, today.getMonth(), today.getDate() - 1);
    this.states = states;
    this.coOutReachTypes = COOutReachType;
    this.bindControls();
  }

  bindControls() {
   this.clinicDetailsForm = this.formBuilder.group({
      // store information
      storeID: this.formBuilder.control(this.coClinicDetails.StoreUserDetails.storeID, Validators.required),
      // Business Information
      firstName: this.formBuilder.control(this.coClinicDetails.BusinessInformation.firstName, Validators.required),
      phone: this.formBuilder.control(this.coClinicDetails.BusinessInformation.phone, Validators.required),
      lastName: this.formBuilder.control(this.coClinicDetails.BusinessInformation.lastName, Validators.required),
      email: this.formBuilder.control(this.coClinicDetails.BusinessInformation.businessContactEmail, null),
      jobTitle: this.formBuilder.control(this.coClinicDetails.BusinessInformation.jobTitle, null),
      // Billing & Vaccine Information
      naClinicPlanId: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.naClinicPlanId, Validators.required),
      naClinicGroupId: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.naClinicGroupId, Validators.required),
      recipientId: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.recipientId, Validators.required),
      ImmunizationDetails: this.initImmunization(),
      naClinicAddlComments: this.formBuilder.control(this.coClinicDetails.BillingVaccineInformation.naClinicAddlComments, null),
      //Clinic Contact Information
      locationDetails: this.initClinicLocations(),
      //pharmacist info
      pharmacistName: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.pharmacistName, Validators.required),
      pharmacistPhone: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.pharmacistPhone, Validators.required),
      totalHours: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.totalHours, Validators.required),
      feedbackNotes: this.formBuilder.control(this.coClinicDetails.PharmacistPostClinicInformation.feedbackNotes)
    });
  }
  initClinicLocations() {
    let start_time: Date;
    let end_time: Date;
    let loc_array: any[] = [];
    this.coClinicDetails.LocationDetails.forEach(loc => {
      this.clinicLocation= this.utility.colLetterToNum( loc.naClinicLocation );
      this.clinicLocation++
      loc_array.push(
        this.formBuilder.group(
          {
            naClinicLocation: this.formBuilder.control(loc.naClinicLocation, null),
            naClinicAddress1: this.formBuilder.control(loc.naClinicAddress1, Validators.required),
            naClinicAddress2: this.formBuilder.control(loc.naClinicAddress2),
            clinicDate: this.formBuilder.control(loc.clinicDate, Validators.required),
            naClinicStartTime: this.formBuilder.control(this._timeToDatePipe.transform(loc.clinicDate, loc.naClinicStartTime), Validators.required),
            naClinicEndTime: this.formBuilder.control(this._timeToDatePipe.transform(loc.clinicDate, loc.naClinicEndTime), Validators.required),
            naContactFirstName: this.formBuilder.control(loc.naContactFirstName, Validators.required),
            naContactLastName: this.formBuilder.control(loc.naContactLastName, Validators.required),
            naClinicContactPhone: this.formBuilder.control(loc.naClinicContactPhone, Validators.required),
            naContactEmail: this.formBuilder.control(loc.naContactEmail, Validators.required),
            naClinicCity: this.formBuilder.control(loc.naClinicCity, Validators.required),
            naClinicState: this.formBuilder.control(loc.naClinicState, Validators.required),
            naClinicZip: this.formBuilder.control(loc.naClinicZip, Validators.required),
            confirmedClientName: this.formBuilder.control(loc.confirmedClientName, null),
            coOutreachTypeId: this.formBuilder.control(loc.coOutreachTypeId, Validators.required),
            previousContact: this.formBuilder.control('', null),
            previousLocation: this.formBuilder.control('', null),
            coOutreachTypeDesc: this.formBuilder.control({ value: loc.coOutreachTypeDesc, disabled: loc.coOutreachTypeId !== '4' }, Validators.required),
            immunizations: this.AddInitialdummyClinicImmunizations()
          }
        )
      )
    });

    return this.formBuilder.array(loc_array);
  }

  removeLocation(i: number) {
    let control = <FormArray>this.clinicDetailsForm.controls['locationDetails'];
    control.removeAt(i);
    this.clinicLocation--;
    for (let index = 1; index < control.length+1; index++) {
      let element = control.controls[index-1];
      if(index <= this.clinicLocation){
        element.get('naClinicLocation').setValue(this.utility.numberToLetters(index));
      }
    };
    
  }

  onOtherRadioClicked(OtherRadio: any, cliLocIndex: number) {
    const control = <FormArray>this.clinicDetailsForm.controls['locationDetails'];
    if (OtherRadio === '4') {
      control.controls[cliLocIndex].get('coOutreachTypeDesc').enable();
    }
    else {
      control.controls[cliLocIndex].get('coOutreachTypeDesc').disable(); // we need to do this to get rid of validations of this input
    }
  }

  initImmunization() {
    let imz_array: any[] = [];
    this.coClinicDetails.LocationDetails[0].ImmunizationDetails.forEach(imz => {
      //let immz[] = imz.ImmunizationDetails;
      imz_array.push(

        this.formBuilder.group(
          {
            pk: this.formBuilder.control(imz.pk),
            estimatedQuantity: this.formBuilder.control(imz.estimatedQuantity, Validators.required),
            immunizationName: this.formBuilder.control(imz.immunizationName),
            paymentTypeName: this.formBuilder.control(imz.paymentTypeName),
            paymentTypeId: this.formBuilder.control(imz.paymentTypeID),
            price: this.formBuilder.control(imz.price),
            totalImmAdministered: this.formBuilder.control(imz.totalImmAdministered)
          }
        )
      )
    }
    );
    return this.formBuilder.array(imz_array);
  }

  AddInitialdummyClinicImmunizations() {

    let imz_array: any[] = [];
    this.coClinicDetails.LocationDetails[0].ImmunizationDetails.forEach(imz => {
      imz_array.push(
        this.formBuilder.group(
          {
            pk: this.formBuilder.control(imz.pk),
            estimatedQuantity: this.formBuilder.control(imz.estimatedQuantity, Validators.required),
            immunizationName: this.formBuilder.control(imz.immunizationName),
            paymentTypeName: this.formBuilder.control(imz.paymentTypeName),
            paymentTypeId: this.formBuilder.control(imz.paymentTypeID),
          }
        )
      )
    });

    return this.formBuilder.array(imz_array);
  }

  addLocation(location) {
    const control = <FormArray>this.clinicDetailsForm.controls['locationDetails'];
    const addrCtrl = this.initAddClinicLocation(location);
    control.push(addrCtrl);
  }
  initAddClinicLocation(location: LocationDetail) {
    if (location === undefined) {
      location = new LocationDetail();
    }
    location.naClinicLocation = this.utility.numberToLetters(this.clinicLocation);
    this.clinicLocation++;
    return (this.formBuilder.group(
      {
        naClinicLocation: this.formBuilder.control(location.naClinicLocation),
        naClinicAddress1: this.formBuilder.control(location.naClinicAddress1, Validators.required),
        naClinicAddress2: this.formBuilder.control(location.naClinicAddress2, null),
        clinicDate: this.formBuilder.control(location.clinicDate, Validators.required),
        naClinicStartTime: this.formBuilder.control(location.naClinicStartTime, Validators.required),
        naClinicEndTime: this.formBuilder.control(location.naClinicEndTime, Validators.required),
        naContactFirstName: this.formBuilder.control(location.naContactFirstName, Validators.required),
        naContactLastName: this.formBuilder.control(location.naContactLastName, Validators.required),
        naClinicContactPhone: this.formBuilder.control(location.naClinicContactPhone, Validators.required),
        naContactEmail: this.formBuilder.control(location.naContactEmail, Validators.required),
        naClinicCity: this.formBuilder.control(location.naClinicCity, Validators.required),
        naClinicState: this.formBuilder.control(location.naClinicState, Validators.required),
        naClinicZip: this.formBuilder.control(location.naClinicZip, Validators.required),
        confirmedClientName: this.formBuilder.control(location.confirmedClientName, null),
        previousContact: this.formBuilder.control('', null),
        previousLocation: this.formBuilder.control('', null),
        coOutreachTypeId: this.formBuilder.control(location.coOutreachTypeId, Validators.required),
        coOutreachTypeDesc: this.formBuilder.control({ value: location.coOutreachTypeDesc, disabled: location.coOutreachTypeId !== '4' }, Validators.required),
        immunizations: this.AddAddlClinicImmunizations()
      }
    ));
  }
  AddAddlClinicImmunizations() {
    let imz_array: any[] = [];
    this.coClinicDetails.LocationDetails[0].ImmunizationDetails.forEach(imz => imz_array.push(
      this.formBuilder.group(
        {
          pk: this.formBuilder.control(imz.pk),
          estimatedQuantity: this.formBuilder.control('', Validators.required),
          immunizationName: this.formBuilder.control(imz.immunizationName),
          paymentTypeName: this.formBuilder.control(imz.paymentTypeName),
          paymentTypeId: this.formBuilder.control(imz.paymentTypeID),
        }
      )
    ));

    return this.formBuilder.array(imz_array);

  }
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  displayFieldCssForArray(field: string, index: number) {
    let return_value = this.isValidArrayField(field, index);
    return {
      'has-error': return_value,
      'has-feedback': return_value
    };
  }

  displayFieldCssForLocImm(field: string, i: number, im: number) {
    const control = <FormArray>this.clinicDetailsForm.controls['locationDetails'];
    const immArray = <FormArray>control.controls[i].get('immunizations');
    const estQty = immArray.controls[im].get('estimatedQuantity');
    let return_val = !estQty.valid && estQty.touched;
    return {
      'has-error': return_val,
      'has-feedback': return_val
    };
  }

  displayFieldCssOutReachType(field: string, i: number) {
    try {
      const control = <FormArray>this.clinicDetailsForm.controls['locationDetails'];
      const outReach = control.controls[i].get(field);
      let return_val = !outReach.valid && outReach.touched;
      return {
        'has-error': return_val,
        'has-feedback': return_val
      };

    } catch (e) {
      console.log(e);
    }
  }

  isFieldValid(field: string) {
    if (this.clinicDetailsForm.get(field) != null)
      return !this.clinicDetailsForm.get(field).valid && this.clinicDetailsForm.get(field).touched;
  }

  isValidArrayField(fields: string, index: number) {
    let return_value = false;
    Object.keys(this.clinicDetailsForm.controls).forEach(field => {
      const control = this.clinicDetailsForm.get(field);
      if (control instanceof FormArray) {
        if (control.controls[index] !== undefined) {
          if (control.controls[index].get(fields) != null) {
            return_value = !control.controls[index].get(fields).valid && control.controls[index].get(fields).touched;
          }
        }
      }
    });
    return return_value;
  }

  clinicConfirm(clinicDetailsForm: any) {
    this.clinicDetails.BusinessInformation.firstName = clinicDetailsForm.firstName;
    this.clinicDetails.BusinessInformation.lastName = clinicDetailsForm.lastName;
    this.clinicDetails.BusinessInformation.phone = clinicDetailsForm.phone;
    this.clinicDetails.BusinessInformation.jobTitle = clinicDetailsForm.jobTitle;
    this.clinicDetails.BusinessInformation.businessContactEmail = clinicDetailsForm.email;
    this.clinicDetails.BillingVaccineInformation.naClinicAddlComments = clinicDetailsForm.naClinicAddlComments;
    this.clinicDetails.BillingVaccineInformation.naClinicGroupId = clinicDetailsForm.naClinicGroupId;
    this.clinicDetails.BillingVaccineInformation.naClinicPlanId = clinicDetailsForm.naClinicPlanId;
    this.clinicDetails.BillingVaccineInformation.recipientId = clinicDetailsForm.recipientId
    this.clinicDetails.StoreUserDetails.storeID = clinicDetailsForm.storeID;
    this.clinicDetails.PharmacistPostClinicInformation.pharmacistName = clinicDetailsForm.pharmacistName;
    this.clinicDetails.PharmacistPostClinicInformation.pharmacistPhone = clinicDetailsForm.pharmacistPhone;
    this.clinicDetails.PharmacistPostClinicInformation.totalHours = clinicDetailsForm.totalHours;


    for (var i = 0; i < clinicDetailsForm.locationDetails.length; i++) {
      this.clinicDetails.LocationDetails.push(clinicDetailsForm.locationDetails[i]);
    }


    this.coClinicdetailsService.confirmClinicDetails(this.clinicDetails).subscribe((data: any) => {
      if (data.status == "200") {
        this.router.navigate(['/communityoutreach/storehome']);
      }
      else
      {
        alert("Issue with updating the details");
      }
    });
  }

  clinicComplete(clinicDetailsForm: any) {

  }
  isFormValid(): boolean {
    let frmar: FormArray = this.clinicDetailsForm.get('locationDetails') as FormArray;
    for (let index = 0; index < frmar.length; index++) {
      const element = frmar.controls[index] as FormGroup;
      this.utility.validateAllFormFields(element);
    }
    return this.clinicDetailsForm.valid;
  }
  confirmClinic(clinicDetailsForm: any) {
    if (this.clinicDetailsForm.valid) {
      this.clinicSubmit.emit(2);
      window.scrollTo(0, 0);
    }
    else {
      alert("Mandatory Fields are not entered. Please fill mandatory fields!");
      this.utility.validateAllFormFields(this.clinicDetailsForm);
    }
  }
  onlyNumbers(event: any) {
    Util.onlyNumbers(event);
  }

  completeClinic() {
    let control = <FormArray>this.clinicDetailsForm.controls['ImmunizationDetails'];

    for (let index = 0; index < control.length; index++) {
      control.controls[index].get('totalImmAdministered').setValidators([Validators.required]);
      control.controls[index].get('totalImmAdministered').updateValueAndValidity();     
      this.utility.validateAllFormFields(<FormGroup>control.controls[index]);
      if (!control.controls[index].valid) {       
        return;
      }    
    };
    this.clinicSubmit.emit(3);
    window.scrollTo(0, 0);

  }

  cancelClinic() {
    this.clinicSubmit.emit(-1);
    window.scrollTo(0, 0);
  }

  
  copyPreviousLocation(formIndex: number, checked: boolean) {
    if (!checked) {
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicAddress1').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicAddress2').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicCity').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicState').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicZip').setValue('');
    }
    else {
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicAddress1').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicAddress1').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicAddress2').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicAddress2').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicCity').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicCity').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicState').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicState').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicZip').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicZip').value);
    }
  }

  copyPreviousContactInfo(formIndex: number, checked: boolean) {    
    if (!checked) {
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactFirstName').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactLastName').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactEmail').setValue('');
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicContactPhone').setValue('');
    }
    else {
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactFirstName').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naContactFirstName').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactLastName').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naContactLastName').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naContactEmail').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naContactEmail').value);
      this.clinicDetailsForm.controls.locationDetails['controls'][formIndex].get('naClinicContactPhone').setValue(this.clinicDetailsForm.controls.locationDetails['controls'][0].get('naClinicContactPhone').value);
    }
  }
}
