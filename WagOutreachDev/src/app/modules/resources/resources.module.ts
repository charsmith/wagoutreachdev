import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogModule } from 'primeng/dialog';

import { ImmunizationComponent } from './components/immunization/immunization.component';
import { SenioroutreachComponent } from './components/senioroutreach/senioroutreach.component';
import { TrainingvideosComponent } from './components/trainingvideos/trainingvideos.component';




@NgModule({
  imports: [
    CommonModule,
    DialogModule
  ],
  declarations: [ImmunizationComponent, SenioroutreachComponent, TrainingvideosComponent]
})
export class ResourcesModule { }
