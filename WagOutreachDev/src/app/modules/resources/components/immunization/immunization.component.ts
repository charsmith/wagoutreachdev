import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-immunization',
  templateUrl: './immunization.component.html',
  styleUrls: ['./immunization.component.css']
})
export class ImmunizationComponent implements OnInit {
  displayDownload:boolean=false;
  constructor() { }

  ngOnInit() {
  }

  scrollDownSample(){
    window.document.getElementById('smapleCallScript').scrollIntoView();
  }


  showDownloadDialog()
  {
    this.displayDownload=true;
  }
}
