import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingvideosComponent } from './trainingvideos.component';

describe('TrainingvideosComponent', () => {
  let component: TrainingvideosComponent;
  let fixture: ComponentFixture<TrainingvideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingvideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingvideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
