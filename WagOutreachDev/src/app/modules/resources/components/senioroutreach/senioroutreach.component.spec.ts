import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenioroutreachComponent } from './senioroutreach.component';

describe('SenioroutreachComponent', () => {
  let component: SenioroutreachComponent;
  let fixture: ComponentFixture<SenioroutreachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenioroutreachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenioroutreachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
