import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-senioroutreach',
  templateUrl: './senioroutreach.component.html',
  styleUrls: ['./senioroutreach.component.css']
})
export class SenioroutreachComponent implements OnInit {
  displayDownload:boolean=false;
  selectedIndex:number;
  selectedResourcesFiles:any=[];
   walgreensResources : any[] = 
[{
    "resourceName": "The ABCD's of Medicare",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "The ABCD's of Medicare Short Version",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017ShortVersion.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017ShortVersion_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2017_18/MedicareABCD_OpenEnrollmentEdition2017ShortVersion.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Alzheimers & Dementia",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Alzheimers%20and%20Dementia08252014.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Alzheimers%20and%20Dementia08252014_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Alzheimers%20and%20Dementia08252014.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Blood Pressure and Cholesterol Management",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Blood%20Pressure%20and%20Cholesterol%2008262014C.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Blood%20Pressure%20and%20Cholesterol%2008262014C_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Blood%20Pressure%20and%20Cholesterol%2008262014C.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Cough and Cold in the Elderly",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Cough%20and%20Cold%20Elderly.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Cough%20and%20Cold%20Elderly_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Cough%20and%20Cold%20Elderly.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Cost Saving Medications",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Cost%20Saving%20Medications.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Cost%20Saving%20Medications_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Cost%20Saving%20Medications.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Depression in Eldery",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Depression%20in%20elderly%20SLIDES.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Depression%20in%20elderly%20SCRIPT.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Depression%20in%20elderly.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Diabetes Management Basics",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Diabetes%20management%20basics%2007292014C.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Diabetes%20management%20basics%2007292014C_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Diabetes%20management%20basics%2007292014C.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Drug Interactions",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Drug%20Interactions%2008262014B.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Drug%20Interactions%2008262014B_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Drug%20Interactions%2008262014B.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Eating for Healthy Aging",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Eating%20for%20Healthy%20Aging.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": ""
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Eating%20for%20Healthy%20Aging.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Exercise and Physical Activity",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Exercise%20Physical%20Activity.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Exercise%20Physical%20Activity_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Exercise%20Physical%20Activity.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Fall Prevention",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2017_18/FallPrevention03292017.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2017_18/FallPrevention03292017SpeakersNotes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2017_18/FallPrevention03292017.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "General Senior Issues",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/General%20Senior%20Issues.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/General%20Senior%20Issues_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/General%20Senior%20Issues.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Hypertension",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Hypertension%2009082104.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Hypertension%2009082104_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Hypertension%2009082104.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Immunizations",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Immunizations.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Immunizations_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Immunizations.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Medication Adherence",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Medication%20Adherence%2009022014.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Medication%20Safety%2009022014_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Medication%20Safety%2009022014.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Medication Safety",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Medication%20Safety%2009022014.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Medication%20Safety%2009022014_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Medication%20Safety%2009022014.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Nutrition-Exercise",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Nutrition%20and%20Exercise%2008262014C.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Nutrition%20and%20Exercise%2008262014C_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Nutrition%20and%20Exercise%2008262014C.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Osteoarthritis",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Osteoarthritis%20Presentation.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": ""
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Osteoarthritis%20Presentation.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Osteoporosis Prevention and Management",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Osteoporosis%2020140730C.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Osteoporosis%2020140730C_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Osteoporosis%2020140730C.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Parkinson's Disease",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Parkinsons%20Disease.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Parkinsons%20Disease_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Parkinsons%20Disease.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Photo Kiosk",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/presentation_kiosks.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": ""
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/presentation_kiosks.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Poison Prevention",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/PoisonPrevention.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/PoisonPrevention_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/PoisonPrevention.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Quit Smoking Now",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/CommunityEducationQuitSmokingNow.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": ""
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/CommunityEducationQuitSmokingNow.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Recommended Adult Immunizations",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2016_17/RecommendedAdultImmunizations.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": ""
    }, {
     "resourceType": "pptx",
     "resourcePath": ""
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Safe Medication Disposal",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2016_17/safemeddisposal_3252016.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2016_17/safemeddisposalwithnotes_3252016.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2016_17/safemeddisposal_3252016.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Skin Care for Seniors",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Skin%20Care%20for%20Seniors.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": ""
    }, {
     "resourceType": "pptx",
     "resourcePath": ""
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Stress Relief in Older Adults",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Stress%20Reduction%20in%20Elderly.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Stress%20Reduction%20in%20Elderly_notes.pdf"
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2015_16/Stress%20Reduction%20in%20Elderly.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Tips to Avoid Being Scammed",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Scams%20on%20Customers.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": ""
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Scams%20on%20Customers.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Walgreens Mobile App: Features To Make Your Life Easier",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2016_17/Walgreens%20App.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": ""
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2016_17/Walgreens%20App.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   },
{
    "resourceName": "Vitamins, Herbals & Supplements",
    "resourceFiles": [{
     "resourceType": "pdf",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Vitamins_Herbals_Supplements%2020140730C.pdf"
    }, {
     "resourceType": "pdfNotes",
     "resourcePath": ""
    }, {
     "resourceType": "pptx",
     "resourcePath": "http://wagbeta.tribunedirect.com/controls/ResourceFileHandler.ashx?Path=2014_15/Vitamins_Herbals_Supplements%2020140730C.pptx"
    }, {
     "resourceType": "excel",
     "resourcePath": ""
    }]
   }
]
  constructor() { }

  ngOnInit() {
    this.displayDownload = false;
  }
  showDownloadDialog(index)
  {
    this.selectedResourcesFiles=this.walgreensResources[index].resourceFiles;
    this.displayDownload=true;

  }
  dispSelectedResource(selected_type){
    

  }
}
