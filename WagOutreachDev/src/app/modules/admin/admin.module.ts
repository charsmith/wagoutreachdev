import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from '../admin/components/user-profile/user-profile.component';
import { EditUserComponent } from '../admin/components/user-profile/edit-user/edit-user.component';
import { AddUserComponent } from '../admin/components/user-profile/add-user/add-user.component';
import { AlertComponent } from '../../alert/alert.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableModule, DialogModule, ConfirmDialogModule, AutoCompleteModule, ProgressSpinnerModule } from 'primeng/primeng';
import { UserProfileService } from '../admin/services/user-profile.service';
import { RouterModule } from '@angular/router';
import { AlertService } from '../../alert.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReusableModule } from '../common/components/reusable.module';
import { ClickOutsideModule } from 'ng4-click-outside';

import { AccordionModule, CalendarModule } from 'primeng/primeng';
import { StoreModule } from '../store/store.module';

@NgModule({
  imports: [
    
    CommonModule,
    DialogModule,
    ClickOutsideModule,
    NgbModule.forRoot(),
    ConfirmDialogModule,
    FormsModule,
    DataTableModule,
    AutoCompleteModule,
    RouterModule,
    ReusableModule,
    StoreModule,
    AccordionModule,
    DataTableModule,
    CalendarModule,
    DialogModule,
    ReactiveFormsModule,    
    ProgressSpinnerModule
  ],
  declarations: [UserProfileComponent, EditUserComponent, AddUserComponent],
  providers: [UserProfileService]
})
export class AdminModule { }
