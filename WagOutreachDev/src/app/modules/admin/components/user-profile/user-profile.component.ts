import { Component,Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import {UserProfileService} from '../../services/user-profile.service';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [ ConfirmationService],
})
export class UserProfileComponent{
  title = 'Welcome to my world';
  isEdit : boolean = false;
  getData: string;
  private list : any[] = [];
  private headers : any[] = [];
  loading: boolean;
  date: string;
  list1:any[]= [];
  ddlist: string;
  searchString: string;
  RoleName : string = '';  
  cities:any[] = [];
  autoComplete : string;
  user_role : string;
  display: boolean = false;
  filteredCountriesSingle: any[];
  countries: any[];
  search_string: string;
  constructor(private router: Router, private _http:Http, private _userservice:UserProfileService, private confirmationService: ConfirmationService) { 
}
autocompleteSearch(event) {
  this.GetUserProfileData(this.autoComplete);
  
}
autocomplete(query, countries: any[]):any[] {
  //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
  let filtered : any[] = [];
  for(let i = 0; i < countries.length; i++) {
      let country = countries[i];
      if(country.username.toLowerCase().indexOf(query.toLowerCase()) == 0) {
          filtered.push(country);
      }
  }
  return filtered;
}
  ngOnInit() {
    this.GetUserInfo();
   //this.GetUserInfo(this.RoleName);
    this.GetUserRoles();
  }
  onChange(RoleName)
  {
    this._userservice.GetUserByRole(RoleName).subscribe(res => this.getData = JSON.parse(res)); 
  }
  clearSearch() {
    this.autoComplete = '';
    this.GetUserProfileData(this.autoComplete);
  }
  select(event)
{
  this.GetUserProfileData(event.username);
}
public GetUserProfileData(search_string)
{
  this._userservice.GetUserInfoBySearch(this.user_role, search_string).subscribe(res => this.getData = JSON.parse(res.json())); 
}
//   public GetUserInfo(RoleName) : void{ 
//     this.loading = true;
//     setTimeout(() => { 
//       // this._apiservice.GetUserInfo("http://localhost:49940/api/values/GetUserInformation")
//       // .subscribe(
//       //   data => ("Admin")
//       // );
//   this._userservice.GetUserByRole(this.RoleName).subscribe(res => this.getData = JSON.parse(res));  
//     this.loading = false;
//   }, 300);
// }
public GetUserInfo() : void{ 

   this._userservice.GetUserInformation().subscribe((data) => {
    this.getData = data;
     });
 }
public GetUserRoles(){ 
   this._userservice.GetUserRoles().subscribe(res => this.ddlist = JSON.parse(res));   
}
 EditUser(user: any) {
      // this.isEdit = true;
      this.router.navigateByUrl('/edit_user/'+user.pk);
    }
 DeleteUser(user: any){
       this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this action?',
            accept: () => {
                //Actual logic to perform a confirmation
            }
        });
    }
       
   
    
}


