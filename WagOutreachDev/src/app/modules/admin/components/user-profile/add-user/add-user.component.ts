import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '../../../services/user-profile.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  ddlist : string;
  assignments : string;
  RoleName : string;
  getData : string;
  districtNumber : string;
  districtName : string;
  cities:any[];
  hideElement: boolean = true;
  hideElement1: boolean = true;
  assignUserRole : string;
  emailField: string;
  disable: boolean = false;
  defaultName : string;
  user : any;
  constructor(private _userservice:UserProfileService) { }

  ngOnInit() {
    this.GetUserRoles();
    this.hideElement = true;
  }
  public GetUserRoles(): void{   
    this._userservice.GetUserRoles().subscribe(res => this.ddlist = JSON.parse((res))); 
    //this._userservice.GetUserRoles().subscribe(res => this.ddlist = JSON.parse((res))); 
 }

 onChange(assignedLocation)
 {
  this.emailField ="";
  this.disable = false;
   this.assignUserRole = assignedLocation.toLowerCase( );
   if(assignedLocation!=null && assignedLocation!="")
    {
    this._userservice.GetAvailableLocations(assignedLocation).subscribe(res => this.assignments = JSON.parse(res)); 
    this.hideElement = false;
    }
    else
    {
   this.hideElement = true;
    }
 }
 onAssignmentChange(selectedDistrict)
 {
     this.emailField ="";
     this.disable = false;
    
   if (this.assignUserRole == "district manager" && selectedDistrict!=null )
   {
         this.emailField = "d" + selectedDistrict + ".dm@walgreens.com";
         this.disable = true;
   }
    else if (this.assignUserRole == "pharmacy manager" && selectedDistrict!=null)
    this.emailField  = "rxm." + selectedDistrict + "@store.walgreens.com";
     else if (this.assignUserRole == "district manager" && selectedDistrict!=null)
     this.emailField  = "d" + selectedDistrict + ".dm@walgreens.com";

 }
}
