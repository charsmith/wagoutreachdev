import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {UserProfileService} from '../../../services/user-profile.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
 userID: number;
 user: any;
 userRoleItems : any[];
 selectedUserRole : any;
  constructor(private route: ActivatedRoute, private _userservice:UserProfileService) {
   }

  ngOnInit() {
      this._userservice.GetUserRoles().subscribe(res => this.userRoleItems = JSON.parse((res))); 
      this.route.params.subscribe((params) => {
            this.userID = params['id'];
            this._userservice.GetUserById(this.userID).subscribe(users => {
                this.user = JSON.parse(users)[0];
            });
        });
  }
   onSelect(userId) { 
    this.selectedUserRole = null;
    for (var i = 0; i < this.userRoleItems.length; i++)
    {
      if (this.userRoleItems[i].id == userId) {
        this.selectedUserRole = this.userRoleItems[i];
      }
    }
 }
 setradio(role)
 {
   //user role logic here
 }

}
