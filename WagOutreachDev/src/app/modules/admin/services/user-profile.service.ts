import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Headers, Http, Response    } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UserProfileService {
user_role : string;
search_string : string;
RoleName : string;
users: any[] = [];
_baseUrl: string = 'http://localhost:49940/';
constructor(private _http: Http) {
 }
 GetAllUsers(){
    return this._http.get(this._baseUrl + "/api/values/GetUserInformation").map((response: Response) => response.json());
 }
 GetUserById(id: number) : Observable<any>  {
    return this._http.get(this._baseUrl + "/api/values/GetUserInformation?id="+ id +"").map((response: Response) => response.json());      
 }
 GetUserByRole(RoleName: string) : Observable<any>  {
    return this._http.get(this._baseUrl + "/api/values/GetUserInformation?user_role="+ RoleName +"").map((response: Response) => response.json());
 }
GetUserRoles(){
    return this._http.get(this._baseUrl + "/api/values/GetUserRoles").map((response: Response) => response.json());
}
GetUserInformation(){
     return this._http.get('assets/showcase/data/userProfile.json').map((response: Response) => response.json());
}
public GetDirectB2B(searchString)
{
  return this._http.get(this._baseUrl + "api/values/GetB2BResults?user_role="+searchString+"").map((response: Response) => response.json());
}
public GetAutoCompleteData()
{
 return this._http.get(this._baseUrl + "api/values/AutoCompleteData").map((response: Response) => response.json());
}
public GetAvailableLocations(user_role)
{
 return this._http.get(this._baseUrl + "api/values/GetAvailableLocations?user_role="+user_role+"").map((response: Response) => response.json());
}
public GetUserInfoBySearch(user_role, search_string)
{
  let _url:string = this._baseUrl + "api/values/GetUserInformationBySearch?user_role="+ user_role + "&search_string=" + search_string+"";    
  return this._http.get(_url);
}

private formatErrors(error: any) {
  return Observable.throw(error.json());
}

}
