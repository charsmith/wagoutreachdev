import { Component, OnInit, DebugElement, ElementRef, HostListener, Input  } from '@angular/core';
import { OpportunitiesService } from '../../store/services/opportunities.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  displaySE: boolean = false;
  displayClinic:boolean=false;
  displayLocal:boolean=false;
  clinicHCSInfo:boolean=false;
  dispDlgReassignToHCS:boolean=false;
  LocalOpportunitiesInfo:boolean=false;
  DataList: any[];
  showVarHCS: boolean = false;
  HCSMetrics: any[] = [];
  HCSAction = [];
  HCSClinicDetails = [];
  HCSLocalLeads = [];
  selectedData: any;
  showVarHCSLocal:boolean=false;
  HCSInfoVisible:boolean=true;
  DMInfoVisible:boolean=false;
  StoreInfoVisible:boolean=false;
  CancelInfoVisible:boolean=false;
  UnassignLocalInfoVisible:boolean=true;
  LocalDistrictInfoVisible:boolean=false;
  AssignedLocalInfoVisible:boolean=false;
  dispDlgReassign:boolean=false;


  constructor(private _opportunityService: OpportunitiesService) { }

  ngOnInit() {
    this._opportunityService.getHCSData().subscribe((hcsdata) => {
     
      this.HCSMetrics = hcsdata.HCSMetrics;
      this.HCSAction=hcsdata.HCSAction;
    },
      error => {
        console.error("Error in fetching data", error);
      });

      this._opportunityService.getHCSClinicDetails().subscribe((hcsclinicDetail) => {
       
       
        this.HCSClinicDetails=hcsclinicDetail;
      
      },
        error => {
          console.error("Error in fetching data", error);
        });

        this._opportunityService.getHCSLocalLeads().subscribe((hcslocalLeads) => {
       
         
          this.HCSLocalLeads=hcslocalLeads;
        
        },
          error => {
            console.error("Error in fetching data", error);
          });
   // this.getUserList();
  }

  storeSearchHCS() {
    this.showVarHCS = !this.showVarHCS;
  }

  storeSearchHCSLocal(){
    this.showVarHCSLocal = !this.showVarHCSLocal;
  }
  onClickedOutside(e: Event) {
    this.showVarHCS = false;
  }
  onClickedOutsidee(e: Event) {
    this.showVarHCSLocal = false;
  }
  showDialog() {
    this.displaySE = true;
  }
  showDialogClinic() {
    this.displayClinic = true;
  }
  showLocalDialog() {
    this.displayLocal = true;
  }
  DlgReassignToHCS(){
    this.displayClinic = false;
   this.dispDlgReassignToHCS=true;
  }
  showDialogclinicHCSInfo(){
    this.clinicHCSInfo = !this.clinicHCSInfo;
  }
  showDialogLocalInfo(){
    this.LocalOpportunitiesInfo= !this.LocalOpportunitiesInfo;
  }

  DlgReassign(){
    this.displayClinic = false;
    this.dispDlgReassign=true;
  }
  SetTabHCS(e)
  {
    var index=e.index;
    if(index=="0")
    {
      this.HCSInfoVisible=true;
      this.DMInfoVisible=false;
      this.StoreInfoVisible=false;
      this.CancelInfoVisible=false;
    }
    else if(index=="1")
    {
      this.HCSInfoVisible=false;
      this.DMInfoVisible=true;
      this.StoreInfoVisible=false;
      this.CancelInfoVisible=false;
    }
    else if(index=="2")
    {
      this.HCSInfoVisible=false;
      this.DMInfoVisible=false;
      this.StoreInfoVisible=true;
      this.CancelInfoVisible=false;
    }
    else if(index=="3")
    {
      this.HCSInfoVisible=false;
      this.DMInfoVisible=false;
      this.StoreInfoVisible=false;
      this.CancelInfoVisible=true;
    }
   
  }
  SetTabLocal(e)
  {
    var index=e.index;
    if(index=="0")
    {
      this.UnassignLocalInfoVisible=true;
      this.LocalDistrictInfoVisible=false;
      this.AssignedLocalInfoVisible=false;
      
    }
    else if(index=="1")
    {
      this.UnassignLocalInfoVisible=false;
      this.LocalDistrictInfoVisible=true;
      this.AssignedLocalInfoVisible=false;
      
    }
    else if(index=="2")
    {
      this.UnassignLocalInfoVisible=false;
      this.LocalDistrictInfoVisible=false;
      this.AssignedLocalInfoVisible=true;
     
    }
   
  }
}
