import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfirmDialogModule, DialogModule, DataTableModule, TabViewModule, DataListModule } from 'primeng/primeng';
import { ClickOutsideModule } from 'ng4-click-outside';

@NgModule({
  imports: [
    CommonModule,
    ConfirmDialogModule,
    DialogModule,
    DataTableModule,
    TabViewModule,
   DataListModule,
   ClickOutsideModule
  ],
  declarations: [DashboardComponent]
})
export class HomeModule { }
