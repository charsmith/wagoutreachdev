import { Component, OnInit, Input } from '@angular/core';
import { FormsModule, FormGroup, FormArray, FormBuilder, FormControl, Validators, NG_VALIDATORS } from '@angular/forms';
import { CommonModule, DatePipe } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { OutreachProgramService } from '../../services/outreach-program.service';
import { ContractData, Clinic, Immunization, Immunization2, Immunizations } from '../../../../models/contract';
import { states } from '../../../../JSON/States';
import { OutReachProgramType } from '../../../../models/OutReachPrograms';
import { SessionDetails } from '../../../../utility/session';
import { Router } from '@angular/router';
import { ConfirmationService, KeyFilterModule, KeyFilter, KEYFILTER_VALIDATOR } from 'primeng/primeng';
import { ErrorMessages } from '../../../../config-files/error-messages';
import { environment } from '../../../../../environments/environment';
import { Util } from '../../../../utility/util';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css'],
  providers: [DatePipe]
})
export class LocationsComponent implements OnInit {
  @Input('group')
  public locationForm: FormGroup;
  locFormArray
  @Input('locFormArray')
  public locationFormArray: FormArray;
  @Input('locationNumber')
  locationNumber: number = 0;
  @Input('outReachProgramType')
  outReachProgramType: string = OutReachProgramType.contracts;
  locationData: any = {};
  immunizations: any[];
  states: any[];
  defaultStartTime: Date;
  defaultDate: Date = new Date();
  maxDateValue: Date = new Date();
  minDateValue: Date = new Date();
  dialogSummary: string;
  dialogMsg: string;
  display: boolean = false;
  locDlgDisplay: boolean = false;
  outReachTypeOtherVisible = false;
  isStoreIs_From_RestrictedState: boolean = false;
  constructor(private _localContractService: OutreachProgramService, private fb: FormBuilder,
    private confirmationService: ConfirmationService, private _utility: Util, private prouter: Router, private date_pipe: DatePipe) {
  }

  ngOnInit() {
    this.states = states;
    let today = new Date();
    if (this.outReachProgramType == OutReachProgramType.communityoutreach && this.isStoreIsfromRestrictedState()) {
      let today = new Date();
      this.defaultDate = new Date(today.getFullYear(), 8, 1, 12, 55, 55);
      this.minDateValue = new Date(today.getFullYear(), 8, 1, 12, 55, 55);
      this.maxDateValue = new Date(today.getFullYear() + 1, today.getMonth(), today.getDate() - 1, 12, 55, 55);
    }
    else {
      this.defaultDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 8, 0, 0);
      this.minDateValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 8, 0, 0);
      this.maxDateValue = new Date(today.getFullYear() + 5, today.getMonth(), today.getDate() - 1, 8, 0, 0);
    }
    this.defaultStartTime = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 8, 0, 0);
  }

  onlyNumberKey(event) {
    Util.onlyNumbers(event);
  }

  isFieldValid(field: string) {
    if (this.locationForm.get(field) != null)
      return !this.locationForm.get(field).valid && this.locationForm.get(field).touched;
  }

  isValidArrayField(fields: string, index: number) {
    let return_value = false;
    Object.keys(this.locationForm.controls).forEach(field => {
      const control = this.locationForm.get(field);
      if (control instanceof FormArray) {
        return_value = !control.controls[index].get(fields).valid && control.controls[index].get(fields).touched;
      }
    });
    return return_value;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  displayFieldCssexpiry(field: string) {
    return {
      'has-error': false,
      'has-feedback': false
    };
  }

  displayFieldCssForArray(field: string, index: number) {
    let return_value = this.isValidArrayField(field, index);
    return {
      'has-error': return_value,
      'has-feedback': return_value
    };
  }
  onSubmit() {

  }


  checkIfFluImmForCorpInvoiceSelected() {
    let added_immunizations = this._localContractService.getSelectedImmunizations();
    try {
      added_immunizations.forEach(rec => {
        if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1) &&
        (rec.isVoucherNeeded === '1' || rec.isVoucherNeeded === 1) && rec.immunizationName.toLowerCase().search('flu') !== -1) {
          throw true;
        }
      });
    } catch (e) {
      if (e === true)
        return true;
    }
    return false;
  }
  checkIfVoucherYesSelectedForFluCorpInvoiceSelected() {
    let added_immunizations = this._localContractService.getSelectedImmunizations();
    let isFluYes: boolean = false;
    let count: number = 0;
    try {
      added_immunizations.forEach(rec => {
        if ((rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1))) { //&& rec.immunizationName.toLowerCase().search('flu')!==-1
          //throw true;
          if (rec.isVoucherNeeded == 1 || rec.isVoucherNeeded == '1') {
            isFluYes = true;
          }
          count++;
        }
      });
    } catch (e) {
      if (e === true)
        return true;
    }
    if (count <= 0) {
      return true;
    }
    else if (count > 0 && !isFluYes) {
      return false;
    }

    return true;
  }

  checkIfVoucherYesSelectedForNonFluCorpInvoiceSelected() {
    let added_immunizations = this._localContractService.getSelectedImmunizations();
    let isNonFluYes: boolean = false;
    let count: number = 0;
    try {
      added_immunizations.forEach(rec => {
        if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1) && rec.immunizationName.toLowerCase().search('flu') == -1) {
          if (rec.isVoucher == true) {
            isNonFluYes = true;
          }
          count++;
          //throw true;
        }
      });
    } catch (e) {
      if (e === true)
        return true;
    }
    if (count <= 0) {
      return true;
    }
    else if (count > 0 && !isNonFluYes) {
      return false;
    }

    return true;
  }

  checkIfNonFluImmForCorpInvoiceSelected() {
    let added_immunizations = this._localContractService.getSelectedImmunizations();
    try {
      added_immunizations.forEach(rec => {
        if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1)
         && (rec.isVoucherNeeded === '1' || rec.isVoucherNeeded === 1) && rec.immunizationName.toLowerCase().search('flu') == -1) {
          throw true;
        }
        // else if(rec.paymentTypeId==6 && rec.sendInvoiceTo===true && rec.immunizationName.toLowerCase().search('flu') == -1){
        //     this.hasNonFluImmSelected = true;
        // }
      });
    } catch (e) {
      if (e === true)
        return true;
    }
    return false;
  }

  onClinicDateSelected(selectedDate: Date) {
    this.locationForm.controls["clinicDate"].setValue(new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate(), 12, 30, 55));
  }
  onOtherRadioClicked(OtherRadio: any) {

    if (this.outReachProgramType == OutReachProgramType.communityoutreach && OtherRadio === '4') {
      this.outReachTypeOtherVisible = true;
      this.locationForm.controls['coOutreachTypeDesc'].enable();
    }
    else {
      this.outReachTypeOtherVisible = false;
      this.locationForm.controls['coOutreachTypeDesc'].disable();
    }
  }

  copyPreviousLocation(formIndex: number, checked: boolean) {
    if (!checked) {
      this.locationFormArray.controls[formIndex].get('address1').setValue('');
      this.locationFormArray.controls[formIndex].get('address2').setValue('');
      this.locationFormArray.controls[formIndex].get('city').setValue('');
      this.locationFormArray.controls[formIndex].get('state').setValue('');
      this.locationFormArray.controls[formIndex].get('zipCode').setValue('');
    }
    else {
      this.locationFormArray.controls[formIndex].get('address1').setValue(this.locationFormArray.controls[formIndex - 1].get('address1').value);
      this.locationFormArray.controls[formIndex].get('address2').setValue(this.locationFormArray.controls[formIndex - 1].get('address2').value);
      this.locationFormArray.controls[formIndex].get('city').setValue(this.locationFormArray.controls[formIndex - 1].get('city').value);
      this.locationFormArray.controls[formIndex].get('state').setValue(this.locationFormArray.controls[formIndex - 1].get('state').value);
      this.locationFormArray.controls[formIndex].get('zipCode').setValue(this.locationFormArray.controls[formIndex - 1].get('zipCode').value);
    }
  }

  copyPreviousContactInfo(formIndex: number, checked: boolean) {
    if (!checked) {
      this.locationFormArray.controls[formIndex].get('contactFirstName').setValue('');
      this.locationFormArray.controls[formIndex].get('contactLastName').setValue('');
      this.locationFormArray.controls[formIndex].get('contactEmail').setValue('');
      this.locationFormArray.controls[formIndex].get('contactPhone').setValue('');
    }
    else {
      this.locationFormArray.controls[formIndex].get('contactFirstName').setValue(this.locationFormArray.controls[formIndex - 1].get('contactFirstName').value);
      this.locationFormArray.controls[formIndex].get('contactLastName').setValue(this.locationFormArray.controls[formIndex - 1].get('contactLastName').value);
      this.locationFormArray.controls[formIndex].get('contactEmail').setValue(this.locationFormArray.controls[formIndex - 1].get('contactEmail').value);
      this.locationFormArray.controls[formIndex].get('contactPhone').setValue(this.locationFormArray.controls[formIndex - 1].get('contactPhone').value);
    }
  }

  noClinicOptionChanged(formIndex: number, checked: boolean) {
    let clinicDateCtrl = this.locationFormArray.controls[formIndex].get('clinicDate');
    let startTimeCtrl = this.locationFormArray.controls[formIndex].get('startTime');
    let endTimeCtrl = this.locationFormArray.controls[formIndex].get('endTime');
    if (checked) {
      if (!this.checkIfVoucherYesSelectedForFluCorpInvoiceSelected()) {
        let sumMsg = ErrorMessages['errMsg'];
        let errMsg = ErrorMessages['voucherFluYes'];
        this.locationFormArray.controls[formIndex].get('isNoClinic').setValue(false);
        this.showDialogV2(sumMsg, errMsg);
        return;
      }
      // if( !this.checkIfVoucherYesSelectedForNonFluCorpInvoiceSelected() ){ 
      //   let sumMsg = ErrorMessages['errMsg'];
      //   let errMsg = ErrorMessages['voucherRoutineYes'];
      //   this.locationFormArray.controls[formIndex].get('isNoClinic').setValue(false);
      //   this.showDialogV2(sumMsg,errMsg);
      //   return;
      // }
      clinicDateCtrl.setValidators(null);
      clinicDateCtrl.setErrors(null);
      clinicDateCtrl.updateValueAndValidity();

      startTimeCtrl.setValidators(null);
      startTimeCtrl.setErrors(null);
      startTimeCtrl.updateValueAndValidity();

      endTimeCtrl.setValidators(null);
      endTimeCtrl.setErrors(null);
      endTimeCtrl.updateValueAndValidity();
    }
    else {
      clinicDateCtrl.setValidators([Validators.required]);
      clinicDateCtrl.updateValueAndValidity();

      startTimeCtrl.setValidators([Validators.required]);
      startTimeCtrl.updateValueAndValidity();

      endTimeCtrl.setValidators([Validators.required]);
      endTimeCtrl.updateValueAndValidity();
    }

  }
  showDialog(msgSummary: string, msgDetail: string) {
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
    this.display = true;
  }
  showDialogV2(msgSummary: string, msgDetail: string) {
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
    this.locDlgDisplay = true;
  }
  confirm() {
    let dtlMsg = ErrorMessages['clinicDateReminderBefore2WeeksEN'];
    let summary = ErrorMessages['impRmndr'];
    this.confirmationService.confirm({
      message: dtlMsg,
      header: summary,
      icon: 'fa fa-question-circle',
      accept: () => { },
      reject: () => {
        return;
      }
    });
  }

  isStoreIsfromRestrictedState() {
    var resrict_states = ["MO", "DC"];
    var current_date = new Date();
    //TODO get global store data to compare if store is from restircted state //MO DL
    if (resrict_states.indexOf(SessionDetails.GetState()) > -1) {
      if (current_date > new Date(environment.MO_State_From) && current_date < new Date(environment.MO_State_TO)) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }


}
