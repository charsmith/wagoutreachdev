import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewcontractAgreementComponent } from './viewcontract-agreement.component';

describe('ViewcontractAgreementComponent', () => {
  let component: ViewcontractAgreementComponent;
  let fixture: ComponentFixture<ViewcontractAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewcontractAgreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewcontractAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
