import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css']
})
export class ThankyouComponent implements OnInit {
  storeState:string ='';
  hasPrefilledForms:number = 0;
  pdfVarFormEn:string = '';
  pdfVarFormEsp:string = '';
  constructor(private route: ActivatedRoute) {     
    this.route.queryParams.subscribe(params => {
      this.storeState = params["storeState"];
      this.hasPrefilledForms = params["hasPrefilledForms"];
      this.pdfVarFormEn = params["pdfVarFormEn"];
      this.pdfVarFormEsp = params["pdfVarFormEsp"];
  });
  }

  ngOnInit() {

  }

}
