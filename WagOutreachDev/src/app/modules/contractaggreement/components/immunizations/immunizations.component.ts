import { Component, OnInit, ViewChild, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { OutreachProgramService } from '../../services/outreach-program.service';
import { DatePipe } from '@angular/common';
//import { Utility } from '../../../common/utility';
import {
  NgForm, FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray,
  ReactiveFormsModule
} from '@angular/forms';
import { Message } from 'primeng/api';
import { OutReachProgramType } from '../../../../models/OutReachPrograms';
import { ErrorMessages } from '../../../../config-files/error-messages';
import { states } from '../../../../JSON/States';
import { debug } from 'util';
import { Util } from '../../../../utility/util';
import { SessionDetails } from '../../../../utility/session';
import { ContractData, Immunization2 } from '../../../../models/contract';


@Component({
  selector: 'app-immunizations',
  templateUrl: './immunizations.component.html',
  styleUrls: ['./immunizations.component.css'],
  providers: [DatePipe]
})
export class ImmunizationsComponent implements OnInit {
  @Output() isSaved = new EventEmitter<string>();
  @ViewChild('imzForm') form;
  immunizationForm: FormGroup;
  immunizations: any[];
  isCopay: boolean = false;
  boundImmunizations: any[];
  selectedImmunization: any;
  selectedPaymentType: any;
  addedImmunizations: any[] = [];
  PaymentTypes: any[] = [];
  price: any;
  sendInvoiceTo: boolean = false;
  contract: any;
  message: string;
  formq: NgForm;
  msgs: Message[] = [];
  errorse: any;
  errorstatus: any;
  states: any[];
  isVoucher: any = 0;
  isFluType: boolean = false;
  copay: any = "";
  display: boolean = false;
  dialogSummary: string;
  dialogMsg: string;
  today = new Date();
  voucherExpirationDate: Date = new Date(this.today.getFullYear() + 1, this.today.getMonth(), this.today.getDate(), 12, 55, 55);
  maxDateValue: Date = this.voucherExpirationDate;
  minDateValue: Date = new Date();
  @Input('outReachProgramType')
  outReachProgramType: string = OutReachProgramType.contracts;
  isVoucherFieldNeeded = false;
  everGreenValue: any = '';
  displayDialog: boolean = false;
  paymentType: any;
  edit_button: boolean = false;
  btnContinue: boolean = true;
  btnUpdate: boolean = false;
  immunizationPk: any;
  rowIndex: any;
  clinic_agreement_pk: any = 0;
  validateLocForm: boolean = false;
  immunization_data: any[];
  immunization_data1: any[];
  cancel_check:boolean = false;
  ccRegex: RegExp = /^(?:[0-9]+(?:\.[0-9]{0,2})?)?$/;

  constructor(private formBuilder: FormBuilder, private _localContractService: OutreachProgramService,
    private utility: Util, private datePipe: DatePipe) {
    let date = new Date();
    this.voucherExpirationDate = new Date(date.getFullYear() + 1, date.getMonth(), date.getDate(), 12, 55, 55);
    this.maxDateValue = this.voucherExpirationDate;
    this.minDateValue = date;
  }

  ngOnInit() {
    this.formControls();
    this.clinic_agreement_pk = SessionDetails.GetAgreementPK();
    if (this.outReachProgramType === OutReachProgramType.communityoutreach) {
      this.isVoucherFieldNeeded = false;
    }

    this._localContractService.getImmunizationsList(this.clinic_agreement_pk, 1).subscribe((data) => {
      this.boundImmunizations = data.immunizationList.filter(item => item.isActive == true);
      this.immunizations = this.boundImmunizations;

      // we need to get contract details only Contract not for CO.
      if(this.outReachProgramType == OutReachProgramType.contracts){
      this.getContractAgreementData(this.clinic_agreement_pk);
      }

    });
    this.states = states;
  }
  getContractAgreementData(clinic_agreement_pk) {
    this._localContractService.getContractAgreementData(clinic_agreement_pk).subscribe((data) => {
      if (this.addedImmunizations.length <= 0) {
        this.addedImmunizations = data.clinicImmunizationList;
      } else {
        this.addedImmunizations = this._localContractService.getImmunizationsData();
      }
      this._localContractService.SetIscanceled(false);
      this.immunization_data = Object.assign([], this._localContractService.getSelectedImmunizations());
      this._localContractService.setImmunizationsData(data.clinicImmunizationList);
      if (this.addedImmunizations != null) {
        this.addedImmunizations.forEach(rec => {
          // handling of true verb getting from service so that data is populate correctly.
          rec.isTaxExempt>=1|| rec.isTaxExempt===true? rec.isTaxExempt=1:rec.isTaxExempt=0;
          rec.isCopay>=1|| rec.isCopay===true? rec.isCopay=1:rec.isCopay=0;
          rec.isVoucherNeeded>=1|| rec.isVoucherNeeded===true? rec.isVoucherNeeded=1:rec.isVoucherNeeded=0;
          const index = this.immunizations.findIndex(item => item.immunizationId == rec.immunizationPk && item.paymentTypeId == rec.paymentTypeId)
          this.boundImmunizations.splice(index, 1);
        });
      }
    });
  }

  formControls() {
    if (this.outReachProgramType === OutReachProgramType.communityoutreach) {
      this.immunizationForm = this.formBuilder.group({
        immunizationSelected: ['', Validators.required],
        paymentsSelected: ['', Validators.required],
        name: ['', null],
        phone: ['', null],
        address1: ['', null],
        address2: [null, null],
        email1: [null, null],
        email2: [null, null],
        city: [null, null],
        state: ['', null],
        zip: ['', null],
        isTaxExempt: ['', null],
        isCopay: ['', null],
        copayValue: ['', null],
        isVoucherNeeded: ['', null],
        expirationDate: [this.voucherExpirationDate, null]
      });
    }
    else {
      this.immunizationForm = this.formBuilder.group({
        immunizationSelected: ['', Validators.required],
        paymentsSelected: ['', Validators.required],
        // everGreen: [this._localContractService.getEverGreenData(), Validators.required],
        everGreen: [''],
        name: [null, null],
        phone: [null, null],
        address1: ['', null],
        address2: [null, null],
        email1: [null, null],
        email2: [null, null],
        city: [null, null],
        state: ['', null],
        zip: [null, null],
        isTaxExempt: ['', null],
        isCopay: ['', null],
        copayValue: ['', null],
        isVoucherNeeded: ['', null],
        expirationDate: [this.voucherExpirationDate, null]
      });
    }
  }
  ImmChanged(immunizationId: any) {
    this.immunizationForm.controls['paymentsSelected'].setValue('');
    this.price ='';
    this.PaymentTypes = [];
    this.paymentType = '0';
    this.PaymentTypes = this.boundImmunizations.filter(p =>
      p.immunizationId == immunizationId);

  }
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }
  isFieldValid(field: string) {
    if (this.immunizationForm.get(field) != null)
      return !this.immunizationForm.get(field).valid && this.immunizationForm.get(field).touched;
  }

  PmtChanged(paymentTypeId: any) {
    this.paymentType = paymentTypeId;
    if (paymentTypeId != 0) {
      this.price = (this.PaymentTypes.filter(p => p.paymentTypeId == paymentTypeId)[0].price);
      if (this.price == null)
        this.price = "N/A";
      this.sendInvoiceTo = (paymentTypeId == '6');
      this.selectedPaymentType = this.PaymentTypes.filter(p => p.paymentTypeId == paymentTypeId)
    }
    else {
      this.price = "";
      this.sendInvoiceTo = false;
    }
  }
  addimmunizationData() {
    this.edit_button = false;
    if (this.selectedPaymentType[0] !== undefined && this.selectedPaymentType[0].immunizationId > 0 && this.sendInvoiceTo) {
      if (this.immunizationForm.value.email1 != this.immunizationForm.value.email2) {
        let dtlMsg = ErrorMessages['noMatchingEmails'];
        let err = ErrorMessages['errMsg'];
        //this.msgs.push({ severity: 'error', summary: err, detail: dtlMsg });
        return;
      }
    }
    if (this.selectedPaymentType[0] !== undefined && this.selectedPaymentType[0].immunizationId > 0) {
      this.validateLocForm = true;
      this.addedImmunizations.push({
        "immunizationPk": this.selectedPaymentType[0].immunizationId,
        "immunizationName": this.selectedPaymentType[0].immunizationName,
        "immunizationSpanishName": this.selectedPaymentType[0].immunizationSpanishName,
        "paymentTypeId": this.selectedPaymentType[0].paymentTypeId,
        "paymentTypeName": this.selectedPaymentType[0].paymentTypeName,
        "paymentTypeSpanishName": this.selectedPaymentType[0].paymentTypeSpanishName,
        "sendInvoiceTo": this.sendInvoiceTo ? 1 : 0,
        "price": (this.selectedPaymentType[0].price == null || this.selectedPaymentType[0].price == "N/A" || this.selectedPaymentType[0].price == undefined) ? -1 : this.selectedPaymentType[0].price,
        "showprice": (this.selectedPaymentType[0].price == null || this.selectedPaymentType[0].price == "N/A" || this.selectedPaymentType[0].price < 0) ? 0 : 1,
        "name": this.sendInvoiceTo ? this.immunizationForm.value.name : null,
        "phone": this.sendInvoiceTo ? this.immunizationForm.value.phone : null,
        "email": this.sendInvoiceTo ? this.immunizationForm.value.email1 : null,
        "address1": this.sendInvoiceTo ? this.immunizationForm.value.address1 : null,
        "address2": this.sendInvoiceTo ? this.immunizationForm.value.address2 : null,
        "city": this.sendInvoiceTo ? this.immunizationForm.value.city : null,
        "isTaxExempt": this.sendInvoiceTo && this.selectedPaymentType[0].paymentTypeId==6? +this.immunizationForm.value.isTaxExempt : null,
        "state": this.sendInvoiceTo ? this.immunizationForm.value.state : null,
        "zip": this.sendInvoiceTo ? this.immunizationForm.value.zip : null,
        "isCopay": this.sendInvoiceTo &&  this.selectedPaymentType[0].paymentTypeId==6? +this.immunizationForm.value.isCopay : null,
        "copayValue": this.outReachProgramType != OutReachProgramType.communityoutreach ?
          (this.sendInvoiceTo && this.immunizationForm.value.isCopay == '1' ?
            +this.immunizationForm.value.copayValue : 0) : null,
        "isVoucherNeeded": this.sendInvoiceTo && this.selectedPaymentType[0].paymentTypeId==6? +this.immunizationForm.value.isVoucherNeeded:null,
        "voucherExpirationDate": this.sendInvoiceTo && this.isVoucher == 1 ? this.immunizationForm.value.expirationDate : null,
      });
      if (this.selectedPaymentType[0] !== undefined && this.selectedPaymentType[0].paymentTypeId == '6') {
        this.edit_button = true;
        this.sendInvoiceTo = false;
        this.clearImmFormFields();
      }
    }
    else {
      this.msgs = [];
      let dtlMsg = ErrorMessages['ContractCOImmunizationError'];
      let err = ErrorMessages['errMsg'];
      let crpInvEr = ErrorMessages['CorporateInvoiceError'];
      this.sendInvoiceTo ? this.msgs.push({ severity: 'error', summary: err, detail: crpInvEr }) :
        this.msgs.push({ severity: 'error', summary: err, detail: dtlMsg });

    }

    const ind = this.boundImmunizations.findIndex(item => item.immunizationId == this.selectedPaymentType[0].immunizationId
      && item.paymentTypeId == this.selectedPaymentType[0].paymentTypeId);

    this.boundImmunizations.splice(ind, 1);

    this.selectedPaymentType = 0;
    this.selectedImmunization = 0;
    this.clearImmFormFields();
    this.PaymentTypes = [];
    this.price = "";
    this._localContractService.SetIscanceled(true);
   this.paymentType = '0';
    this._localContractService.setImmunizationsData(this.addedImmunizations);
  }

  addFieldValue() {
    this.immunizationForm.controls['phone'].setValidators(null);
    this.immunizationForm.controls['phone'].setErrors(null);
    this.immunizationForm.controls['phone'].updateValueAndValidity();

    this.immunizationForm.controls['zip'].setValidators(null);
    this.immunizationForm.controls['zip'].setErrors(null);
    this.immunizationForm.controls['zip'].updateValueAndValidity();

    this.immunizationForm.controls['copayValue'].setValidators(null);
    this.immunizationForm.controls['copayValue'].setErrors(null);
    this.immunizationForm.controls['copayValue'].updateValueAndValidity();
    if (!this.immunizationForm.valid) {
      let dtlMsg = ErrorMessages['MandatoryFields'];
      if (this.addedImmunizations.length <= 0) { //&& this.everGreenValue !== ''
        dtlMsg = ErrorMessages['Atleast1Immunization'];
      }
      let err = ErrorMessages['errMsg'];
      this.showDialog(err, dtlMsg);
      this.utility.validateAllFormFields(this.immunizationForm);
      return;
    }
    if(this.selectedPaymentType[0]!=undefined && this.selectedPaymentType[0] !== null){
    this.immunizationPk = this.selectedPaymentType[0].immunizationId;
    }
    let immunization = this.addedImmunizations.filter(item => (item.immunizationPk == this.immunizationPk && item.paymentTypeId == 6));
    if (this.paymentType == '6') {
      this.isVoucher = 0;
      this.isCopay = false;
      var fluType = 'flu';
      let date = new Date();
      if (this.selectedPaymentType[0].immunizationName.toLowerCase().search(fluType) == -1) {
        if ((immunization != undefined || immunization != null)
          && (immunization[0] != undefined || immunization[0] != null)
          && (immunization[0].voucherExpirationDate != null || immunization[0].voucherExpirationDate != undefined)) {
          this.voucherExpirationDate = new Date(immunization[0].voucherExpirationDate);
        } else {
          this.voucherExpirationDate = new Date(date.getFullYear() + 1, date.getMonth(), date.getDate(), 12, 55, 55);
        }
        this.isFluType = false;
      }
      else {
        if (date.getMonth() == 0 || date.getMonth() == 1 || date.getMonth() == 2) {
          if ((immunization != undefined || immunization != null)
            && (immunization[0] != undefined || immunization[0] != null)
            && (immunization[0].voucherExpirationDate != null || immunization[0].voucherExpirationDate != undefined)) {
            this.voucherExpirationDate = new Date(immunization[0].voucherExpirationDate);
          } else {
            this.voucherExpirationDate = new Date(date.getFullYear(), 2, 31, 12, 55, 55);
          }
        }
        else {
          if ((immunization != undefined || immunization != null)
            && (immunization[0] != undefined || immunization[0] != null)
            && (immunization[0].voucherExpirationDate != null || immunization[0].voucherExpirationDate != undefined)) {
            this.voucherExpirationDate = new Date(immunization[0].voucherExpirationDate);
          } else {
            this.voucherExpirationDate = new Date(date.getFullYear() + 1, 2, 31, 12, 55, 55);
          }
        }
        this.isFluType = true;
      }
      this.maxDateValue = this.voucherExpirationDate;
      this.minDateValue = date;
      this.immunizationForm.controls['expirationDate'].setValue(this.voucherExpirationDate);
      this.displayDialog = true;
      this.btnContinue = true;
      this.btnUpdate = false;
    }
    else {
      if (this.selectedImmunization == 0 && this.selectedPaymentType == 0) {
        let dtlMsg = ErrorMessages['contractImmunizationOption'];
        let err = ErrorMessages['errMsg'];
        this.showDialog(err, dtlMsg);
        this.utility.validateAllFormFields(this.immunizationForm);
        return;
      }

      this.addimmunizationData(); // this adds the data to local DS.
    }
  }

  clearFormFields() {
    this.immunizationForm.value.immunizationSelected = '';
    this.immunizationForm.value.paymentsSelected = '';
    this.price = "";
    if (this.outReachProgramType === OutReachProgramType.communityoutreach) {
      this.immunizationForm = this.formBuilder.group({
        immunizationSelected: ['', this.addedImmunizations.length > 0 ? null : Validators.required],
        paymentsSelected: ['', this.addedImmunizations.length > 0 ? null : Validators.required],
        name: [null, null],
        phone: [null, null],
        address1: ['', null],
        address2: [null, null],
        email1: [null, null],
        email2: [null, null],
        city: [null, null],
        state: ['', null],
        zip: [null, null],
        isTaxExempt: ['', null],
        isCopay: ['', null],
        copayValue: ['', null],
        isVoucherNeeded: ['', null],
        expirationDate: [this.voucherExpirationDate, null]
      });
    }
    else {
      this.immunizationForm = this.formBuilder.group({
        immunizationSelected: ['', this.addedImmunizations.length > 0 ? null : Validators.required],
        paymentsSelected: ['', this.addedImmunizations.length > 0 ? null : Validators.required],
        everGreen: [this._localContractService.getEverGreenData()],
        name: [null, null],
        phone: [null, null],
        address1: ['', null],
        address2: [null, null],
        email1: [null, null],
        email2: [null, null],
        city: [null, null],
        state: ['', null],
        zip: [null, null],
        isTaxExempt: ['', null],
        isCopay: ['', null],
        copayValue: ['', null],
        isVoucherNeeded: ['', null],
        expirationDate: [this.voucherExpirationDate, null]
      });
    }
  }
  clearImmFormFields() {
    this.clearFormFields();
  }

  closeCTI() {
    this.sendInvoiceTo = false;
    this.clearFormFields();
  }

  deleteFieldValue(index: any) {
    let immunization = this.addedImmunizations;
    this.boundImmunizations.push({
      "immunizationId": immunization[index].immunizationPk,
      "immunizationName": immunization[index].immunizationName,
      "immunizationSpanishName": immunization[index].immunizationSpanishName,
      "paymentTypeId": immunization[index].paymentTypeId,
      "paymentTypeName": immunization[index].paymentTypeName,
      "paymentTypeSpanishName": immunization[index].paymentTypeSpanishName,
      "price":immunization[index].price,
      "sendInvoiceTo":immunization[index].sendInvoiceTo,
      "showprice":immunization[index].showprice,
      "address1":immunization[index].address1,
      "address2":immunization[index].address2,
      "city":immunization[index].city,
      "copayValue":immunization[index].copayValue,
      "email":immunization[index].email,
      "isCopay":immunization[index].isCopay,
      "isTaxExempt":immunization[index].isTaxExempt,
      "isVoucherNeeded":immunization[index].isVoucherNeeded,
      "name":immunization[index].name,
      "phone":immunization[index].phone,
      "state":immunization[index].state,
      "voucherExpirationDate":immunization[index].voucherExpirationDate,
      "zip":immunization[index].zip
    });
    this.addedImmunizations.splice(index, 1);
    if(this.addedImmunizations.length<=0)
    {
      this._localContractService.SetIscanceled(false);
    }

    this.selectedPaymentType = 0;
    this.selectedImmunization = 0;
    this.PaymentTypes = [];
    this.price = "";
    this.paymentType = '0'
    this.closeCTI();
    this.validateLocForm = true;
  }
  onClinicDateSelected(selectedDate: Date) {
    this.immunizationForm.controls["expirationDate"].setValue(new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate(), 12, 55, 55));
  }
  handleCopay(copayValue: any) {
    this.isCopay = copayValue == '1';
    if (copayValue == '1') {
      this.immunizationForm.controls['copayValue'].setValidators([Validators.required]);
      this.immunizationForm.controls['copayValue'].updateValueAndValidity();
    } else {
      this.immunizationForm.controls['copayValue'].setValidators(null);
      this.immunizationForm.controls['copayValue'].setErrors(null);
      this.immunizationForm.controls['copayValue'].updateValueAndValidity();
    }
  }

  handleVoucher(voucherneeded: any) {
    let immunization = this.addedImmunizations.filter(item => (item.immunizationPk == this.immunizationPk && item.paymentTypeId == 6));
    if (voucherneeded == '1') {
      this.isVoucher = 1;
      let date = new Date();
      if (!this.isFluType) {
        if ((immunization != undefined || immunization != null)
          && (immunization[0] != undefined || immunization[0] != null)
          && (immunization[0].voucherExpirationDate != null || immunization[0].voucherExpirationDate != undefined)) {
          this.voucherExpirationDate = new Date(immunization[0].voucherExpirationDate);
        } else {
          this.voucherExpirationDate = new Date(date.getFullYear() + 1, date.getMonth(), date.getDate(), 12, 55, 55);
        }
      }
      else {
        if (date.getMonth() == 0 || date.getMonth() == 1 || date.getMonth() == 2) {
          if ((immunization != undefined || immunization != null)
            && (immunization[0] != undefined || immunization[0] != null)
            && (immunization[0].voucherExpirationDate != null || immunization[0].voucherExpirationDate != undefined)) {
            this.voucherExpirationDate = new Date(immunization[0].voucherExpirationDate);
          } else {
            this.voucherExpirationDate = new Date(date.getFullYear(), 2, 31, 12, 55, 55);
          }
        }
        else {
          if ((immunization != undefined || immunization != null)
            && (immunization[0] != undefined || immunization[0] != null)
            && (immunization[0].voucherExpirationDate != null || immunization[0].voucherExpirationDate != undefined)) {
            this.voucherExpirationDate = new Date(immunization[0].voucherExpirationDate);
          } else {
            this.voucherExpirationDate = new Date(date.getFullYear() + 1, 2, 31, 12, 55, 55);
          }
        }
      }
      this.voucherExpirationDate = new Date(this.voucherExpirationDate);
      this.immunizationForm.controls['expirationDate'].setValue(this.voucherExpirationDate);
      this.maxDateValue = this.voucherExpirationDate;
      this.minDateValue = date;
    }
    else {
      this.isVoucher = 0;
    }
  }

  showDialog(msgSummary: string, msgDetail: string) {
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
    this.display = true;
  }

  radioCheck(radioChecked: any) {
    this.everGreenValue = radioChecked;
    this._localContractService.setEverGreenData(this.everGreenValue);
  }

  save(outReachPrgType?: string): boolean {
    if (this.addedImmunizations.length > 0 && this.outReachProgramType === OutReachProgramType.communityoutreach) {
      this._localContractService.setSelectedImmunizations(this.addedImmunizations);
      this._localContractService.setCancelSelectedImmunizations(this.addedImmunizations);
      return true;
    }
    else if (this.addedImmunizations.length > 0) { //&& this._localContractService.getEverGreenData() != ''
      this._localContractService.setSelectedImmunizations(this.addedImmunizations);
      this._localContractService.setCancelSelectedImmunizations(this.addedImmunizations);
      return true;
    }
    else {
      this.msgs = [];
      let dtlMsg = ErrorMessages['MandatoryFields'];
      if (this.addedImmunizations.length <= 0) {
        dtlMsg = ErrorMessages['Atleast1Immunization'];
      }
      let err = ErrorMessages['errMsg'];
      //this.msgs.push({ severity: 'error', summary: err, detail: dtlMsg });
      this.showDialog(err, dtlMsg);
      this.utility.validateAllFormFields(this.immunizationForm);
      return false;
    }
  }
  simpleSave(outReachPrgType?: string): boolean {
    if (this.addedImmunizations.length > 0 && this.outReachProgramType === OutReachProgramType.communityoutreach) {
      this._localContractService.setSelectedImmunizations(this.addedImmunizations);
      return true;
    }
    else if (this.addedImmunizations.length > 0) { //&& this._localContractService.getEverGreenData() != ''
      this._localContractService.setSelectedImmunizations(this.addedImmunizations);
      return true;
    }
  }

  displayFieldCssForArray(field: string, index: number) {
    let return_value = this.isValidArrayField(field, index);
    return {
      'has-error': return_value,
      'has-feedback': return_value
    };
  }

  isValidArrayField(fields: string, index: number) {
    let return_value = false;
    Object.keys(this.immunizationForm.controls).forEach(field => {
      const control = this.immunizationForm.get(field);
      if (control instanceof FormArray) {
        if (control.controls[index] !== undefined) {
          if (control.controls[index].get(fields) != null) {
            return_value = !control.controls[index].get(fields).valid && control.controls[index].get(fields).touched;
          }
        }
      }
    });
    return return_value;
  }

  displayDialogCC() {
    this.selectedPaymentType = 0;
    this.selectedImmunization = 0;
    this.clearImmFormFields();
    this.PaymentTypes = [];
    this.price = "";
    this.displayDialog = false;
  }

  editCorporateToInvoice(pk: any, index: any) {

    this.immunizationPk = pk;
    this.rowIndex = index;
    this.btnContinue = false;
    this.btnUpdate = true;
    let immunization = this.addedImmunizations.filter(item => (item.immunizationPk == pk && item.paymentTypeId == 6));
    this.isCopay = immunization[0].isCopay == '1';
    this.isVoucher = immunization[0].isVoucherNeeded == '1';
    // var fluType = 'flu';
    let date = new Date();
    if (immunization[0].immunizationName.toLowerCase().search('flu') == -1) {
      this.isFluType = false;
    }
    else {
      this.isFluType = true;
    }
    if(immunization[0].voucherExpirationDate !==null && immunization[0].voucherExpirationDate !== undefined) {
    immunization[0].voucherExpirationDate = new Date(immunization[0].voucherExpirationDate);
    immunization[0].maxDateValue = immunization[0].voucherExpirationDate;
    }
    immunization[0].minDateValue = date;
    this.immunizationForm = this.formBuilder.group({
      immunizationSelected: ['', null],
      paymentsSelected: ['', null],
      name: [immunization[0].name, Validators.required],
      phone: [immunization[0].phone, Validators.required],
      address1: [immunization[0].address1, Validators.required],
      address2: [immunization[0].address2, null],
      email1: [immunization[0].email, Validators.email],
      email2: [immunization[0].email, Validators.email],
      city: [immunization[0].city, Validators.required],
      state: [immunization[0].state, Validators.required],
      zip: [immunization[0].zip, Validators.required],
      isTaxExempt: [immunization[0].isTaxExempt, Validators.required],
      isCopay: [immunization[0].isCopay, Validators.required],// == 'Yes'
      copayValue: [immunization[0].isCopay == '1' ? immunization[0].copayValue : '', this.immunizationForm.value.isCopay == '1' ? Validators.required : null],
      isVoucherNeeded: [immunization[0].isVoucherNeeded, Validators.required],
      expirationDate: [immunization[0].voucherExpirationDate, (this.immunizationForm.value.isVoucherNeeded == '1' || this.immunizationForm.value.isVoucherNeeded == 1) ? Validators.required : null]
    });
    this.sendInvoiceTo = true;
    this.displayDialog = true;
  }

  displayDialogContinue() {
    if (this.paymentType == '6') {
      this.immunizationForm = this.formBuilder.group({
        immunizationSelected: ['', null],
        paymentsSelected: ['', null],
        name: [this.immunizationForm.value.name, Validators.required],
        phone: [this.immunizationForm.value.phone, Validators.required],
        address1: [this.immunizationForm.value.address1, Validators.required],
        address2: [this.immunizationForm.value.address2, null],
        email1: [this.immunizationForm.value.email1, Validators.email],
        email2: [this.immunizationForm.value.email2, Validators.email],
        city: [this.immunizationForm.value.city, Validators.required],
        state: [this.immunizationForm.value.state, Validators.required],
        zip: [this.immunizationForm.value.zip, Validators.required],
        isTaxExempt: [this.immunizationForm.value.isTaxExempt, Validators.required],
        isCopay: [this.immunizationForm.value.isCopay, Validators.required],
        copayValue: [this.immunizationForm.value.copayValue, this.immunizationForm.value.isCopay == '1' ? Validators.required : null],
        isVoucherNeeded: [this.immunizationForm.value.isVoucherNeeded, Validators.required],
        expirationDate: [this.immunizationForm.value.expirationDate, this.immunizationForm.value.isVoucherNeeded == '1' ? Validators.required : null]
      });
      if (this.immunizationForm.valid && this.immunizationForm.value.phone.length >= 10) {
        if (this.immunizationForm.controls['email1'].value !== this.immunizationForm.controls['email2'].value) {
          let dtlMsg = ErrorMessages['VerifyEmail'];
          let err = ErrorMessages['errMsg'];
          this.showDialog(err, dtlMsg);
          return;
        }
        if (!this.ccRegex.test(this.immunizationForm.value.copayValue)) {
          let dtlMsg = ErrorMessages['dotNotAllowedCharacter'];
          let err = ErrorMessages['errMsg'];
          this.showDialog(err, dtlMsg);
          return;
        }
        if (!this.checkVoucherExpiryDatesValidity(this.selectedPaymentType[0].immunizationName, this.immunizationPk)) {
          let dtlMsg = ErrorMessages['voucherExpiryInValid'];
          let err = ErrorMessages['resolve'];
          this.showDialog(err, dtlMsg);
          return;
        }
        this.addimmunizationData();
        this.clearFormFields();
        this.displayDialog = false;
      }
      else {
        this.utility.validateAllFormFields(this.immunizationForm);
      }
    }
  }

  displayDialogUpdate() {
    let immunization = this.addedImmunizations.filter(item => item.immunizationPk == this.immunizationPk && item.paymentTypeId == 6);
    this.immunizationForm = this.formBuilder.group({
      immunizationSelected: ['', null],
      paymentsSelected: ['', null],
      name: [this.immunizationForm.value.name, Validators.required],
      phone: [this.immunizationForm.value.phone, Validators.required],
      address1: [this.immunizationForm.value.address1, Validators.required],
      address2: [this.immunizationForm.value.address2, null],
      email1: [this.immunizationForm.value.email1, Validators.email],
      email2: [this.immunizationForm.value.email2, Validators.email],
      city: [this.immunizationForm.value.city, Validators.required],
      state: [this.immunizationForm.value.state, Validators.required],
      zip: [this.immunizationForm.value.zip, Validators.required],
      isTaxExempt: [this.immunizationForm.value.isTaxExempt, Validators.required],
      isCopay: [this.immunizationForm.value.isCopay, Validators.required],
      copayValue: [this.immunizationForm.value.copayValue, this.immunizationForm.value.isCopay == '1' ? Validators.required : null],
      isVoucherNeeded: [this.immunizationForm.value.isVoucherNeeded, Validators.required],
      expirationDate: [this.immunizationForm.value.expirationDate, this.immunizationForm.value.isVoucherNeeded == '1' ? Validators.required : null]
    });

    if (this.immunizationForm.valid && this.immunizationForm.value.phone.length >= 10) {
      if (this.immunizationForm.controls['email1'].value !== this.immunizationForm.controls['email2'].value) {
        let dtlMsg = ErrorMessages['VerifyEmail'];
        let err = ErrorMessages['errMsg'];
        this.showDialog(err, dtlMsg);
        return;
      }
      if (!this.ccRegex.test(this.immunizationForm.value.copayValue)) {
        let dtlMsg = ErrorMessages['dotNotAllowedCharacter'];
        let err = ErrorMessages['errMsg'];
        this.showDialog(err, dtlMsg);
        return;
      }
      if (!this.checkVoucherExpiryDatesValidity(immunization[0].immunizationName, immunization[0].immunizationPk)) {
        let dtlMsg = ErrorMessages['voucherExpiryInValid'];
        let err = ErrorMessages['resolve'];
        this.showDialog(err, dtlMsg);
        return;
      }
      this.addedImmunizations.splice(this.rowIndex, 1);
      this.addedImmunizations.push({
        "immunizationPk": immunization[0].immunizationPk,
        "immunizationName": immunization[0].immunizationName,
        "immunizationSpanishName": immunization[0].immunizationSpanishName,
        "paymentTypeId": immunization[0].paymentTypeId,
        "paymentTypeName": immunization[0].paymentTypeName,
        "paymentTypeSpanishName": immunization[0].paymentTypeSpanishName,
        "sendInvoiceTo": this.sendInvoiceTo ? "1" : "0",
        "price": (immunization[0].price == null || immunization[0].price == "N/A") ? "N/A" : immunization[0].price,
        "showprice": (immunization[0].price == null || immunization[0].price == "N/A") ? 0 : 1,/*todo*/
        "name": this.sendInvoiceTo ? this.immunizationForm.value.name : null,
        "phone": this.sendInvoiceTo ? this.immunizationForm.value.phone : null,
        "email": this.sendInvoiceTo ? this.immunizationForm.value.email1 : null,
        //"email2": this.sendInvoiceTo ? this.immunizationForm.value.email2 : null,
        "address1": this.sendInvoiceTo ? this.immunizationForm.value.address1 : null,
        "address2": this.sendInvoiceTo ? this.immunizationForm.value.address2 : null,
        "city": this.sendInvoiceTo ? this.immunizationForm.value.city : null,
        "isTaxExempt": this.sendInvoiceTo ? this.immunizationForm.value.isTaxExempt : null,
        "state": this.sendInvoiceTo ? this.immunizationForm.value.state : null,
        "zip": this.sendInvoiceTo ? this.immunizationForm.value.zip : null,
        "isCopay": this.sendInvoiceTo ? this.immunizationForm.value.isCopay : null,
        "copayValue": this.outReachProgramType != OutReachProgramType.communityoutreach ?
          (this.sendInvoiceTo && this.immunizationForm.value.isCopay == '1' ?
            this.immunizationForm.value.copayValue : '0') : '',
        "isVoucherNeeded": (this.isVoucher === true || this.isVoucher === 1) ? '1' : '0',
        "voucherExpirationDate": this.sendInvoiceTo && (this.isVoucher == 1 || this.isVoucher == 1) ? this.immunizationForm.value.expirationDate : null,
      });
      this.displayDialog = false;
      this.clearFormFields();
      this.selectedPaymentType = 0;
      this.selectedImmunization = 0;
      this.PaymentTypes = [];
      this.price = "";
      this.paymentType = '0';
    }
    else {
      this.utility.validateAllFormFields(this.immunizationForm);
    }

  }

  getPhoneNoInUSFormat(phone: string): string {
    return Util.telephoneNumberFormat(phone);
  }
  checkVoucherExpiryDatesValidity(immunizationName: string, immunizationPk: number) {
    let isVoucherValid: boolean = true;
    let isVaccineFluType: boolean = false;
    let selectedDate: string = this.datePipe.transform(this.immunizationForm.value.expirationDate, "MM/dd/yyyy");
    isVaccineFluType = immunizationName.toLowerCase().search('flu') !== -1 ? true : false;
    if (isVaccineFluType) {
      this.addedImmunizations.forEach(im => {
        if ((im.isVoucherNeeded === '1' || im.isVoucherNeeded === 1)
          && (im.sendInvoiceTo === '1' || im.sendInvoiceTo === 1)
          && im.immunizationName.toLowerCase().search('flu') !== -1
          && im.immunizationPk !== immunizationPk) {
          let imzDate: string = this.datePipe.transform(im.voucherExpirationDate, "MM/dd/yyyy");
          if (selectedDate.search(imzDate) === -1) {
            isVoucherValid = false;
            return isVoucherValid;
          }
        }
      });
    }
    else {
      this.addedImmunizations.forEach(im => {
        if ((im.isVoucherNeeded === '1' || im.isVoucherNeeded === 1) && (im.sendInvoiceTo === '1' || im.sendInvoiceTo === 1)
          && im.immunizationName.toLowerCase().search('flu') == -1
          && im.immunizationPk !== immunizationPk) {
          let imzDate: string = this.datePipe.transform(im.voucherExpirationDate, "MM/dd/yyyy");
          if (selectedDate.search(imzDate) === -1) {
            isVoucherValid = false;
            return isVoucherValid;
          }
        }
      });
    }
    return isVoucherValid;
  }
  cancelImmunization(): boolean {
    var compare_objects = false;
    this.cancel_check = this._localContractService.getIsCancel();
    compare_objects = this.cancel_check;// this.utility.compareTwoObjects(this.immunization_data, this.addedImmunizations);
    return compare_objects;
  }
}












