import { Component, OnInit } from '@angular/core';
import { OutreachProgramService } from '../../services/outreach-program.service';
import { WalgreenCo } from '../../../../models/contract';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Util } from '../../../../utility/util';

@Component({
  selector: 'app-witness',
  templateUrl: './witness.component.html',
  styleUrls: ['./witness.component.css'],
  providers: [DatePipe]
})
export class WitnessComponent implements OnInit {
preparedDate : Date;
witnessInfo1 : any;
witnessInfo : WalgreenCo;
witnessForm:FormGroup;
witness_form_cancel:FormGroup;
//voucherExpirationDate : Date = new Date();
 constructor(private _fb: FormBuilder,private _localContractService : OutreachProgramService,
    private _datePipe: DatePipe, private utility: Util) { }

  ngOnInit() {
      this.witnessInfo1 = this._localContractService.fetchContractAgreementData();
      this.witnessInfo = this.witnessInfo1.walgreenCoInfo;
      if(this.witnessInfo== null){
          this.witnessInfo = new WalgreenCo();
      }
      this.preparedDate =  this.witnessInfo!==null && this.witnessInfo.preparedDate!=='' ? new Date(this.witnessInfo.preparedDate):new Date();
      this.witnessForm = this._fb.group({
          name:this._fb.control(this.witnessInfo.name,Validators.required),
          title:this._fb.control(this.witnessInfo.title, Validators.required),
          preparedDate:this._fb.control(this.preparedDate,Validators.required),
          districtNumber:this._fb.control(this.witnessInfo.districtNumber,Validators.required)
      });
      this.witness_form_cancel = Object.assign([],this.witnessForm);
  }
  save(): boolean {  
      this.witnessInfo.preparedDate = this._datePipe.transform(this.preparedDate,"shortDate");      
      if(!this.isFormValid()){
        this.utility.validateAllFormFields(this.witnessForm);
        return false;
      }

        this._localContractService.setWalgreensWitnessData(this.witnessForm.value);
        return true;
    }
    simpleSave(): boolean {  
        this.witnessInfo.preparedDate = this._datePipe.transform(this.preparedDate,"shortDate");
          this._localContractService.setWalgreensWitnessData(this.witnessForm.value);
          return true;
      }
    displayFieldCss(field: string) {
        return {
          'has-error': this.isFieldValid(field),
          'has-feedback': this.isFieldValid(field)
        };
      }
      isFieldValid(field: string) {
        if (this.witnessForm.get(field) != null) {
          let ret:boolean = !this.witnessForm.get(field).valid && this.witnessForm.get(field).touched;
          if(ret){
            return ret;
          } else {
            if(field.search('name') !== -1 || field.search('title') !== -1 ){
              let validField:boolean = this.witnessForm.controls[field].value.trim().length > 0;
              return !validField;
            }
            else if(field.search('districtNumber') !== -1){
              let validNo:number = +this.witnessForm.controls[field].value ;
              if(validNo>0) {
              return false; }
              else {
              return true; }              
            }
          }
          return false;
        }
        // switch(field){
        //     case 'name':
        //     if(this.witnessInfo.name.length<=0) return true;
        //     case 'title':
        //     if(this.witnessInfo.title.length<=0) return true;
        //     case 'preparedDate':
        //     if(this.witnessInfo.preparedDate.length<=0) return true;
        //     case 'districtNumber':
        //     if(this.witnessInfo.districtNumber.length<=0) return true;
        // }
        // return false;
      }
      isFormValid(){
        let validForm:boolean = true;
        validForm = this.witnessForm.controls['name'].value.trim().length > 0 && this.witnessForm.controls['title'].value.trim().length > 0 ;
        let num:number= +this.witnessForm.controls['districtNumber'].value;
        validForm = num> 0 ? validForm && true:validForm && false;

        return validForm;
        
      }
        onlyNumberKey(event) {
          Util.onlyNumbers(event);
        }
        cancelWitness(): boolean {
          var compare_objects = false;
          compare_objects = this.utility.compareTwoObjects(this.witness_form_cancel.value, this.witnessForm.value);
          return compare_objects;
      }
}
