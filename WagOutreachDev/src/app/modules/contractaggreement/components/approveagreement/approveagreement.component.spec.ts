import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveagreementComponent } from './approveagreement.component';

describe('ApproveagreementComponent', () => {
  let component: ApproveagreementComponent;
  let fixture: ComponentFixture<ApproveagreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveagreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveagreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
