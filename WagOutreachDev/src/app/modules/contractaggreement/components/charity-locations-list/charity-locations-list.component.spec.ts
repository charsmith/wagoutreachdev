import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharityProgramListComponent } from './charity-program-list.component';

describe('CharityProgramListComponent', () => {
  let component: CharityProgramListComponent;
  let fixture: ComponentFixture<CharityProgramListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharityProgramListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharityProgramListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
