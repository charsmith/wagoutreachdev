import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FormsModule, FormGroup, FormArray, FormBuilder, FormControl, Validators, } from '@angular/forms';
import { OutreachProgramService } from '../../services/outreach-program.service';
import { LocationNumberPipe } from '../../pipes/location-number.pipe';
import { ContractData, Clinic, clinicLocation, Immunization, Immunizations } from '../../../../models/contract';
import { TimeToDatePipe } from '../../pipes/time-to-date.pipe';
import { DatePipe } from '@angular/common';
import { ConfirmDialogModule, ConfirmationService, Message } from 'primeng/primeng';
//import { Utility } from '../../../common/utility';
import { SessionDetails } from '../../../../utility/session';
import { Router } from '@angular/router';
import { ErrorMessages } from '../../../../config-files/error-messages';
import { Util } from '../../../../utility/util';
import { environment } from '../../../../../environments/environment.prod';
import { CharityProgram, ClinicList, ClinicImzQtyList, LogOutreachStatus } from '../../../../models/charityProgram';

@Component({
    selector: 'app-charity-locations-list',
    templateUrl: './charity-locations-list.component.html',
    styleUrls: ['./charity-locations-list.component.css'],
    providers: [LocationNumberPipe, TimeToDatePipe, DatePipe]
})

export class CharitylocationsListComponent implements OnInit {
    showHints: boolean = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff")) == 'yes') ? false : true;
    pageName: string = "charityprogram";

    public contractForm: FormGroup;
    public addedImmunizations: Immunization[];
    location_number: any = 0;
    //contractData: ContractData = new ContractData();
    clinicData: Clinic = new Clinic();
    contactLogPk: Number;
    dialogSummary: string;
    dialogMsg: string;
    display: boolean = false;
    errordisplay: boolean = false;
    msgs: Message[] = [];
    defaultDate: Date = new Date();
    maxDateValue: Date = new Date();
    minDateValue: Date = new Date();
    cancel_save: string;
    event_details: any;
    savePopUp: boolean = false;
    immunization_list:any[];
    address1: string;
    address2: string;
    city: string;
    state: string;
    zipCode: string;
    constructor(private _fb: FormBuilder,
        private _charityProgramService: OutreachProgramService,
        private _localContractService: OutreachProgramService,
        private _locationNumberPipe: LocationNumberPipe,
        private _timeToDatePipe: TimeToDatePipe,
        private _datePipe: DatePipe, private utility: Util,
        private confirmationService: ConfirmationService, private router: Router) {
    }

    ngOnInit() {
        this.immunization_list = [];
        this.contractForm = this._fb.group({
            clinicList: this._fb.array([])
        });
        let oppurtunitiesData: any = SessionDetails.GetopportunitiesData();
        this.event_details = SessionDetails.GetEventDetails();
        this.clinicData = this._charityProgramService.getClinicDetails('todo');
        if (this.isStoreIsfromRestrictedState()) {
            let today = new Date();
            this.defaultDate = new Date(today.getFullYear(), 8, 1, 8, 55, 55);
            this.minDateValue = new Date(today.getFullYear(), 8, 1, 8, 55, 55);
            this.maxDateValue = new Date(today.getFullYear() + 1, today.getMonth(), today.getDate(), 8, 55, 55);
        }
        else {
            let today = new Date();
            this.defaultDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 8, 55, 55);
            this.minDateValue = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 8, 55, 55);
            this.maxDateValue = new Date(today.getFullYear() + 5, today.getMonth(), today.getDate(), 8, 55, 55);
        }
        let loc = new ClinicList();
        this._localContractService.getStoreBusinessDetails(oppurtunitiesData.businessPk).subscribe((res) => {
            loc.address1 = this.address1 = res[0].address1;
            loc.address2 = this.address2 = res[0].address2;
            loc.city= this.city  = res[0].city;
            loc.state = this.state = res[0].state;
            loc.zipCode= this.zipCode  = res[0].zipCode;
            let imz: any = {};
            let clinic_immunizations: any[];
            clinic_immunizations = [];
            loc.clinicImzQtyList = new Array<ClinicImzQtyList>();
            this._localContractService.getImmunizationsList(0, 2).subscribe((rec) => {

                imz.immunizationPk = 1;
                imz.immunizationName = "test";
                imz.paymentTypeName = "test";
                imz.paymentTypeId = 2;
                imz.estimatedQuantity = "";
                imz.vouchersDistributed = "";
                //imz.estimatedQuantity = loc.clinicImzQtyList[0].estimatedQuantity;
                //imz.vouchersDistributed = loc.clinicImzQtyList[0].vouchersDistributed;
                clinic_immunizations.push(imz);

                loc.clinicImzQtyList = clinic_immunizations;
                this.immunization_list = loc.clinicImzQtyList;
                this.addLocation(loc);
                this.location_number = this.location_number + 1;
            });

        });
    }
    initLocation(location: ClinicList) {
        let clinic_immunizations: any[];
        let clinic_location: any = {};
        let start_time: Date;
        let end_time: Date;
        let imz: any = {};
        if (location === undefined) {
            clinic_immunizations = [];
            location = new ClinicList();
            location.location = "CLINIC LOCATION " + this._locationNumberPipe.transform(this.location_number + 1);
            if (location.clinicDate !== '') {
                start_time = this._timeToDatePipe.transform(location.clinicDate, location.startTime);
                end_time = this._timeToDatePipe.transform(location.clinicDate, location.endTime);
            }
          // this._localContractService.getImmunizationsList(0, 2).subscribe((rec) => {
                // imz.immunizationPk = 1;
                // imz.immunizationName = "test";
                // imz.paymentTypeName = "test";
                // imz.paymentTypeId = 2;
                // imz.estimatedQuantity = "";
                // imz.vouchersDistributed = "";
                // imz.estimatedQuantity = location.clinicImzQtyList[0].estimatedQuantity;
                // imz.vouchersDistributed = location.clinicImzQtyList[0].vouchersDistributed;
                // clinic_immunizations.push(imz);
                // location.clinicImzQtyList = clinic_immunizations;
                location.location = "CLINIC LOCATION " + this._locationNumberPipe.transform(this.location_number + 1);
                return this._fb.group({
                    clinicImzQtyList: this.initImmunizations(this.immunization_list),
                    location: this._fb.control(location.location),
                    contactFirstName: this._fb.control(location.contactFirstName, Validators.required),
                    contactLastName: this._fb.control(location.contactLastName, Validators.required),
                    contactEmail: this._fb.control(location.contactEmail, Validators.required),
                    contactPhone: this._fb.control(location.contactPhone, Validators.required),
                    clinicDate: this._fb.control(location.clinicDate, Validators.required),
                    startTime: this._fb.control(start_time, Validators.required),
                    endTime: this._fb.control(end_time, Validators.required),

                    //  disabling fields if the store state belongs to restricted state
                    address1: this._fb.control(this.isStoreIsfromRestrictedState() ? this.address1 : location.address1, Validators.required),
                    address2: this._fb.control(this.isStoreIsfromRestrictedState() ?  this.address2 : location.address2),
                    city: this._fb.control(this.isStoreIsfromRestrictedState() ? this.city : location.city, Validators.required),
                    state: this._fb.control(this.isStoreIsfromRestrictedState() ? this.state: location.state, Validators.required),
                    zipCode: this._fb.control(this.isStoreIsfromRestrictedState() ? this.zipCode : location.zipCode, Validators.required),
                    isNoClinic: this._fb.control(+location.isNoClinic, ),
                    isCurrent: this._fb.control(+location.isCurrent),
                    // previousContact: this._fb.control(+location.previousContact, null),
                    // fluExpiryDate: this._fb.control(location.fluExpiryDate),
                    // routineExpiryDate: this._fb.control(location.routineExpiryDate),
                    // latitude: this._fb.control(location.latitude),
                    // longitude: this._fb.control(location.longitude),
                    localEstmdShots: this._fb.control(this.immunization_list[0].estimatedQuantity, null),
                    localvouchersDist: this._fb.control(this.immunization_list[0].vouchersDistributed, Validators.required)
                });
    //     });

        }
        else {
            //clinic_immunizations = location.immunization;
            if (location.clinicDate !== '') {
                start_time = this._timeToDatePipe.transform(location.clinicDate, location.startTime);
                end_time = this._timeToDatePipe.transform(location.clinicDate, location.endTime);
            }
            return this._fb.group({
                clinicImzQtyList: this.initImmunizations(location.clinicImzQtyList),
                location: this._fb.control(location.location),
                contactFirstName: this._fb.control(location.contactFirstName, Validators.required),
                contactLastName: this._fb.control(location.contactLastName, Validators.required),
                contactEmail: this._fb.control(location.contactEmail, Validators.required),
                contactPhone: this._fb.control(location.contactPhone, Validators.required),
                clinicDate: this._fb.control(location.clinicDate, Validators.required),
                startTime: this._fb.control(start_time, Validators.required),
                endTime: this._fb.control(end_time, Validators.required),

                //  disabling fields if the store state belongs to restricted state
                address1: this._fb.control(this.isStoreIsfromRestrictedState() ? this.address1 : location.address1, Validators.required),
                address2: this._fb.control(this.isStoreIsfromRestrictedState() ?  this.address2 : location.address2),
                city: this._fb.control(this.isStoreIsfromRestrictedState() ? this.city : location.city, Validators.required),
                state: this._fb.control(this.isStoreIsfromRestrictedState() ? this.state: location.state, Validators.required),
                zipCode: this._fb.control(this.isStoreIsfromRestrictedState() ? this.zipCode : location.zipCode, Validators.required),

                isNoClinic: this._fb.control(+location.isNoClinic, ),
                isCurrent: this._fb.control(+location.isCurrent),
                //previousContact: this._fb.control(+location.previousContact, null),
                // fluExpiryDate: this._fb.control(location.fluExpiryDate),
                // routineExpiryDate: this._fb.control(location.routineExpiryDate),
                // latitude: this._fb.control(location.latitude),
                // longitude: this._fb.control(location.longitude),
                localEstmdShots: this._fb.control(location.clinicImzQtyList[0].estimatedQuantity, Validators.required),
                localvouchersDist: this._fb.control(location.clinicImzQtyList[0].estimatedQuantity, Validators.required)
            });
        }

    }
    initImmunizations(clinic_immunizations) {
        let imz_array: any[] = [];
        clinic_immunizations.forEach(imz => {
            imz.immunizationName = imz.immunizationName;
            imz.paymentTypeName = imz.paymentTypeName;
            imz_array.push(this._fb.group({
                clinicPk: this._fb.control(imz.clinicPk),
                immunizationPk: this._fb.control(imz.immunizationPk),
                estimatedQuantity: this._fb.control(imz.estimatedQuantity, null),
                immunizationName: this._fb.control(imz.immunizationName),
                paymentTypeName: this._fb.control(imz.paymentTypeName),
                paymentTypeId: this._fb.control(imz.paymentTypeId)
            }))
        })
        return this._fb.array(imz_array);
    }
    isStoreIsfromRestrictedState() {
        var resrict_states = ["MO", "DC"];
        var current_date = new Date();
        if (resrict_states.indexOf(SessionDetails.GetState()) > -1) {
            if (current_date > new Date(environment.MO_State_From) && current_date < new Date(environment.MO_State_TO)) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    addLocation(location) {
        const control = <FormArray>this.contractForm.controls['clinicList'];
        const addrCtrl = this.initLocation(location);
        control.push(addrCtrl);
    }
    removeLocation(i: number) {
        const control = <FormArray>this.contractForm.controls['clinicList'];
        control.removeAt(i);
    }

    save(): boolean {
        let clinic_immunizations: any[];
        var charity_program = new CharityProgram();
        let frmar: FormArray = this.contractForm.get('clinicList') as FormArray;
        clinic_immunizations = [];
        var location = new ClinicList();
        let imz: any = {};
        location.clinicImzQtyList = new Array<ClinicImzQtyList>();
        // for (let index = 0; index < frmar.length; index++) {
        //     const element = frmar.controls[index] as FormGroup;
        //     this.utility.validateAllFormFields(element);
        // }
        if (this.contractForm.valid) {
            let locString: String[] = [];
            try {
                this.contractForm.value.clinicList.forEach(loc => {
                    for (let i = 0; i < this.contractForm.value.clinicList.length; i++) {
                        loc.isNoClinic = this.contractForm.value.clinicList[i].isNoClinic == null ? false : true;
                        for (let j = 0; j < this.contractForm.value.clinicList[i].clinicImzQtyList.length; j++) {
                            this.contractForm.value.clinicList[i].clinicImzQtyList[j].clinicPk = this.contractForm.value.clinicList[i].clinicImzQtyList[j].clinicPk;
                            this.contractForm.value.clinicList[i].clinicImzQtyList[j].immunizationName = this.contractForm.value.clinicList[i].clinicImzQtyList[j].immunizationName;
                            this.contractForm.value.clinicList[i].clinicImzQtyList[j].paymentTypeName = this.contractForm.value.clinicList[i].clinicImzQtyList[j].paymentTypeName;
                            this.contractForm.value.clinicList[i].clinicImzQtyList[j].paymentTypeId = this.contractForm.value.clinicList[i].clinicImzQtyList[j].paymentTypeId;
                            this.contractForm.value.clinicList[i].clinicImzQtyList[j].estimatedQuantity = this.contractForm.value.clinicList[i].localEstmdShots;
                            this.contractForm.value.clinicList[i].clinicImzQtyList[j].vouchersDistributed = this.contractForm.value.clinicList[i].localvouchersDist;
                        }
                    }

                    if (loc.startTime != "")
                        loc.startTime = loc.startTime;
                    if (loc.endTime != "")
                        loc.endTime = loc.endTime;
                    let today = new Date();
                    let diffDays: number = loc.clinicDate.getMonth() == today.getMonth() ? Math.round(loc.clinicDate.getDate() - today.getDate()) : 15;
                    if (loc.isNoClinic == false && diffDays <= 14) {
                        var userProfile = SessionDetails.GetUserProfile();
                        locString.push(loc.location_number)
                        if (userProfile.userRole == "admin") {
                            // this.confirm(ErrorMessages['impRmndr'], ErrorMessages['clinicDateReminderBefore2WeeksEN']);
                            throw true;
                        }
                        else {
                            this.showDialog(ErrorMessages['impRmndr'], ErrorMessages['clinicDateReminderBefore2WeeksEN']);
                            throw false;
                        }
                    }
                });
                let imz: any = {};
                charity_program.logOutreachStatus = new LogOutreachStatus();
                //  charity_program.logOutreachStatus = this.event_details;
                charity_program.logOutreachStatus.businessPk = this.event_details.logOutreachStatus.businessPk;;
                charity_program.logOutreachStatus.firstName = this.event_details.logOutreachStatus.firstName;
                charity_program.logOutreachStatus.lastName = this.event_details.logOutreachStatus.lastName;
                charity_program.logOutreachStatus.jobTitle = this.event_details.logOutreachStatus.jobTitle;
                charity_program.logOutreachStatus.outreachProgram = this.event_details.logOutreachStatus.outreachProgram;
                charity_program.logOutreachStatus.outreachBusinessPk = this.event_details.logOutreachStatus.outreachBusinessPk;
                charity_program.logOutreachStatus.contactDate = this.event_details.logOutreachStatus.contactDate;
                charity_program.logOutreachStatus.outreachStatusId = this.event_details.logOutreachStatus.outreachStatusId;
                charity_program.logOutreachStatus.outreachStatusTitle = this.event_details.logOutreachStatus.outreachStatusTitle;
                charity_program.logOutreachStatus.feedback = this.event_details.logOutreachStatus.feedback;
                charity_program.logOutreachStatus.createdBy = this.event_details.logOutreachStatus.createdBy;
                charity_program.charityVoucherModel = this.contractForm.value;

                // if (this._charityProgramService.saveClinicLocations(charity_program)) {
                this._charityProgramService.insertCharityProgram(SessionDetails.GetStoreId(), 2, charity_program).subscribe((data: any) => {
                    this.showDialog(ErrorMessages['chSummary'], ErrorMessages['chSucccess']);
                    // if (data.errorS != null) {
                    //   if (data.errorS.errorCode == -4) {
                    //this.showDialog(ErrorMessages['contractAlert'], data.errorS.errorMessage, false);
                    //   }
                    // }
                });
                //this.showDialog(ErrorMessages['chSummary'], ErrorMessages['chSucccess']);
            }
            catch (e) {
                return e;
            }
            return true;
            // }
            // else {
            //     this.showErrorDialog(ErrorMessages['inValidForm'], ErrorMessages['MandatoryFields']);
            //     return false;
            // }
        }
        else {
            this.utility.validateAllFormFields(this.contractForm);
        }
    }
    handleValidationsOption() {
        try {
            this.contractForm.value.locations.forEach(loc => {
                if (loc.isNoClinic == true) {
                    if (loc.localvouchersDist.length <= 0 || loc.contactFirstName.length <= 0 || loc.contactLastName.length <= 0 || loc.contactPhone.length <= 0 ||
                        loc.contactPhone === null || loc.localEstmdShots.length <= 0) {
                        throw false;
                    }
                }
                else {
                    if (loc.address1.length <= 0 || loc.address2.length <= 0 || loc.city.length <= 0 || loc.clinicDate === null || loc.clinicDate === ""
                        || loc.endTime === null || loc.startTime === null || loc.endTime === "" || loc.startTime === "" ||
                        loc.state.length <= 0 || loc.localEstmdShots.length <= 0 || loc.contactEmail.length <= 0 ||
                        loc.contactFirstName.length <= 0 || loc.contactLastName.length <= 0 || loc.contactPhone.length <= 0 ||
                        loc.contactPhone === null || loc.localEstmdShots.length <= 0
                        || loc.localvouchersDist.length <= 0 || loc.zipCode === null || loc.zipCode.length <= 0) {
                        throw false;
                    }
                }
            }
            );
        }
        catch (e) {
            return e;
        }
        return true;
    }

    CancelCharity() {
        //if (this.contractForm.value.length > 0) {
        this.showDialogCancel(ErrorMessages['unSavedData'], ErrorMessages['charity_alert'])
        this.savePopUp = true;
        // }
        // else {
        //   if (this.router) this.router.navigate(['./communityoutreach/storehome']);
        // }
    }
    doNotSave() {
        this.savePopUp = false;
        if (this.router) this.router.navigate(['./communityoutreach/storehome']);
    }
    Continue() {
        this.savePopUp = false;
    }

    showDialogCancel(msgSummary: string, msgDetail: string) {
        this.dialogMsg = msgDetail;
        this.dialogSummary = msgSummary;
    }

    showDialog(msgSummary: string, msgDetail: string) {
        this.dialogMsg = msgDetail;
        this.dialogSummary = msgSummary;
        this.display = true;
    }
    showErrorDialog(msgSummary: string, msgDetail: string) {
        this.dialogMsg = msgDetail;
        this.dialogSummary = msgSummary;
        this.errordisplay = true;
    }
    confirm(hdr: string, msg: string) {
        this.confirmationService.confirm({
            message: msg,
            header: hdr,
            accept: () => {
                this._charityProgramService.saveClinicLocations(this.contractForm.value);
                //todo to display successful message after subscribe          
                this.showDialog(ErrorMessages['chSummary'], ErrorMessages['chSucccess']);
                return;
            },
            reject: () => {
                return;
            }
        });
    }

    displayNgMsg(msgSeverity: string, msgSummary: string, msgDetail: string) {
        this.msgs = [];
        this.msgs.push({ severity: msgSeverity, summary: msgSummary, detail: msgDetail });
    }

    okClicked() {
        if (this.router) this.router.navigate(['/communityoutreach/storehome']);
        this.display = false;
    }

    okErrorClicked() {
        this.errordisplay = false;
    }

}
