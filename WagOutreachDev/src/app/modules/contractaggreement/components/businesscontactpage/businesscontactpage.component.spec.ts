import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinesscontactpageComponent } from './businesscontactpage.component';

describe('BusinesscontactpageComponent', () => {
  let component: BusinesscontactpageComponent;
  let fixture: ComponentFixture<BusinesscontactpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinesscontactpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinesscontactpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
