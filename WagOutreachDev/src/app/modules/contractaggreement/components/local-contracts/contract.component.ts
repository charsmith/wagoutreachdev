import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, NgForm } from '@angular/forms';
import { OutreachProgramService } from '../../services/outreach-program.service';
import { ImmunizationsComponent } from '../../components/immunizations/immunizations.component';
import { LocationsListComponent } from '../../components/contract-locations-list/locations-list.component';
import { WitnessComponent } from '../../components/contract-pharmacy-info/witness.component';
import { WorkflowGuard } from '../../workflow/workflow-guard.service';
import { STEPS } from '../../workflow/workflow.model';
import { OutReachProgramType } from '../../../../models/OutReachPrograms';
import { AgreementComponent } from '../contract-agreement/agreement.component';
import { ErrorMessages } from '../../../../config-files/error-messages';
import { SessionDetails } from '../../../../utility/session';
import { ConfirmationService } from 'primeng/primeng';
import { Router } from '@angular/router';
import { MessageServiceService } from '../../../store/services/message-service.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.css'],
  providers: [WorkflowGuard]
})
export class ContractComponent implements OnInit {
  @ViewChild(ImmunizationsComponent) imzcmp: ImmunizationsComponent;
  @ViewChild(LocationsListComponent) loccmp: LocationsListComponent;
  @ViewChild(WitnessComponent) witcmp: WitnessComponent;
  public contractForm: FormGroup;
  cancel_check:boolean = false;
  
  showHints:boolean = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff"))=='yes')?false:true;
  pageName:string = "contract";

  step: number = 1;
  step_desc: string;
  contractData: any;
  form: NgForm
  errorse: any;
  errorstatus: any;
  OutReachProgramType: string;
  dialogSummary: string;
  dialogMsg: string;
  display: boolean = false;
  cancel_save: string;
  savePopUp: boolean = false;
  isLocationsFormValid:boolean = false;
  isPharmacyFormValid:boolean = false;
  constructor(private _fb: FormBuilder, private _localContractService: OutreachProgramService, private _workflowGuard: WorkflowGuard,
    private confirmationService: ConfirmationService, private _location: Location, private router: Router, private message_service: MessageServiceService) {

    this.OutReachProgramType = OutReachProgramType.contracts;

  }

  ngOnInit() {   
    this._localContractService.clearSelectedImmunizations();
    this._localContractService.clearClinicLocations();
    this._localContractService.getContractAgreement().subscribe(data => this.contractData = data);
    this.OutReachProgramType = OutReachProgramType.contracts;
  }

  GoToStep(step: number) {
    let step_desc = this.getStepDesc(step);
    this.step_desc = this.getStepDesc(this.step);
    if (step < this.step) {
      this.cacheInEnteredData(this.step_desc);
      this.step = step;
      this.step_desc = step_desc;
      return;
    }
    if (this.OnSave(step_desc, step, this.step)) {
      if (this.step_desc != step_desc) {
        this.step_desc = this._workflowGuard.verifyWorkFlow(step_desc);
        this.step = Object.keys(STEPS).findIndex(inx => inx == this.step_desc) + 1;
      }
    }
  }

  getStepDesc(step: number) {
    if (step === 1)
      return STEPS.immunization;
    if (step === 2)
      return STEPS.location;
    if (step === 3)
      return STEPS.witness;
    if (step === 4)
      return STEPS.review;
  }

  cacheInEnteredData(step_desc: any, currentStep?: number, prevStep?: number) {
    let return_value: boolean = false;
    if (this.step_desc === STEPS.immunization) {
      return_value = this.imzcmp.simpleSave();
    }
    else if (this.step_desc === STEPS.location) {
      if (this.loccmp != undefined) {
        this.loccmp.simpleSave();
        if(!this.loccmp.isFormValid()){
          this.isLocationsFormValid = false;
        } else {
          this.isLocationsFormValid = true;
        }
        
      }
    }
    else if (this.step_desc === STEPS.witness) {
      if (this.witcmp != undefined) {
        this.witcmp.simpleSave();
        if(!this.witcmp.isFormValid()){
          this.isPharmacyFormValid = false;
        } else {
          this.isPharmacyFormValid = true;
        }
      }
    }
  }

  OnSave(step_desc: any, currentStep: number, prevStep: number) {
    let return_value = false;
    if (this.step_desc === STEPS.immunization) {
      return_value = this.imzcmp.save();
    }
    if (this.step_desc === STEPS.location) {
      if (this.loccmp != undefined) {
        if (!this.loccmp.isFormValid()) {
          return false;
        }
      }

      let locString: String[] = [];
      if (this.loccmp.isClinicDateReminderBefore2WksReq(locString)) {
        let sumMsg = ErrorMessages['impRmndr'];
        let errMsg = ErrorMessages['clinicDateReminderBefore2WeeksEN'];
        var userProfile = SessionDetails.GetUserProfile();
        if ( userProfile.userRole.toLowerCase() == "director" ||
         userProfile.userRole.toLowerCase() == "supervisor" || userProfile.userRole.toLowerCase() == "regionalVp" ||
          userProfile.userRole.toLowerCase() == "regionalDirector" || userProfile.userRole.toLowerCase() == "admin" ) {
          this.step = 2;
          this.confirm(sumMsg, errMsg, step_desc);
          return false;
        }
        else {
          errMsg = ErrorMessages['clinicDateReminderAfter2WeeksENNonAdmin'];
          this.showDialog(sumMsg, errMsg);
          this.step = 2;
          return false;
        }
      }
      return_value = this.loccmp.save();
    }
    if (this.step_desc === STEPS.witness) {
      if (this.witcmp != undefined) {
        return_value = this.witcmp.save();
      }
    }
    if (currentStep > prevStep + 1) {
      //user has navigated one step further due to previous iteration.
      for (let count = prevStep + 1; count <= currentStep; count++) {
        switch (count) {
          case 2:
            if (this.imzcmp.validateLocForm || !this.isLocationsFormValid) {
              this.step = 2;
              this.imzcmp.validateLocForm = false;
              return false;
            }
            break;
            case 3:
            if(!this.isPharmacyFormValid){
              this.step = 3;
              return false;
            }
          default:
            break;
        }
      }
    }
    return return_value;
  }
  okClicked() {
    this.display = false;
  }
  saveBtnClicked() {
    let return_value = false;
    if (this.step_desc === STEPS.immunization || this.step <= 1) {
      return_value = this.imzcmp.simpleSave();
    }
    if (this.step_desc === STEPS.location || this.step == 2) {
      if (this.loccmp != undefined) {
        return_value = this.loccmp.simpleSave();
      }
    }
    if (this.step_desc === STEPS.witness || this.step >= 3)
      if (this.witcmp != undefined) {
        return_value = this.witcmp.simpleSave();
      }

    this._localContractService.saveInterimAgreementData(SessionDetails.GetStoreId(), 1).subscribe((data: any) => {
      switch (data) {
        case 'success':
          this.showDialog(ErrorMessages['contract'], ErrorMessages['contractSaved']);
          break;
        case 500:
          this.showDialog(ErrorMessages['unKnownError'], ErrorMessages['errMsg']);
          break;
        default:
          this.showDialog(ErrorMessages['unKnownError'], ErrorMessages['errMsg']);
          break;
      }
    },
      err => {
        switch (err) {
          case 500:
            this.showDialog(ErrorMessages['unKnownError'], err.json().Message);
            break;
          default:
            this.showDialog(ErrorMessages['unKnownError'], err.json().Message);
            break;
        }
      });
    return return_value;
  }


  showDialog(msgSummary: string, msgDetail: string) {
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
    this.display = true;
  }
  confirm(hdr: string, msg: string, step_desc: any) {
    this.confirmationService.confirm({
      message: msg,
      header: hdr,
      accept: () => {
        if (this.loccmp.save()) {
          if (this.step_desc != step_desc) {
            this.step_desc = this._workflowGuard.verifyWorkFlow(step_desc);
            this.step = Object.keys(STEPS).findIndex(inx => inx == this.step_desc) + 1;
          }
        }
        else {
          this.step = 2;
          return false;
        }
        return true;
      },
      reject: () => {
        this.step = 2;
        return false;
      }
    });
  }
  calculateNextStep(step: any) {
    let step_desc = this.getStepDesc(step);
    // this.step_desc = this.getStepDesc(this.step);
    //if (this.step_desc != step_desc) 
    {
      this.step_desc = this._workflowGuard.verifyWorkFlow(step_desc);
      this.step = 3;//Object.keys(STEPS).findIndex(inx => inx == this.step_desc) + 1;
    }
  }
  cancelContract() {
    let return_value = false;
    if (this.step_desc === STEPS.immunization || this.step <= 1) {
      return_value = this.imzcmp.cancelImmunization();
      if (return_value) {
        this.savePopUp = true;
        this.showDialogCancel(ErrorMessages['unSavedData'], ErrorMessages['contract_alert']);
      }
      else {
        this._location.back();
      }
    }
    if (this.step_desc === STEPS.location || this.step == 2) {
      //let return_value1 = false;
      this.cancel_check = this._localContractService.getIsCancel();
       return_value = this.loccmp.cancelLocationData();
      //return_value1 = this._localContractService.getCancelSelectedImmunizations.length>0? true:false;;
      //|| !return_value1
      if (return_value || this.cancel_check ) {
        this.savePopUp = true;
        this.showDialogCancel(ErrorMessages['unSavedData'], ErrorMessages['contract_alert']);
      }
      else {
        this._location.back();
      }
    }
    if (this.step_desc === STEPS.witness || this.step >= 3)
      if (this.witcmp != undefined) {
        //let return_value1 = false;
        this.cancel_check = this._localContractService.getIsCancel();
        return_value = this.witcmp.cancelWitness();
       // return_value1 = this._localContractService.getCancelClinicLocation.length>0? true:false;
       //|| !return_value1
        if (return_value || this.cancel_check ) {
          this.savePopUp = true;
          this.showDialogCancel(ErrorMessages['unSavedData'], ErrorMessages['contract_alert']);
        }
        else {
          this._location.back();
        }
      }
  }
  doNotSave() {
    this.savePopUp = false;
    if (this.router) this.router.navigate(['./communityoutreach/storehome']);
  }
  Continue() {
    this.savePopUp = false;
  }
  showDialogCancel(msgSummary: string, msgDetail: string) {
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
  }
}
