import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CharityLocationsComponent } from './charity-locations.component';


describe('CharityProgramComponent', () => {
  let component: CharityLocationsComponent;
  let fixture: ComponentFixture<CharityLocationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharityLocationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharityLocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
