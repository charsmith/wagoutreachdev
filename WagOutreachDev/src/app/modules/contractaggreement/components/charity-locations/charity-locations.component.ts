import { Component, OnInit, Input } from '@angular/core';

import {   FormsModule,FormGroup, FormArray, FormBuilder, FormControl,Validators } from '@angular/forms';
import { OutreachProgramService } from '../../services/outreach-program.service';
import { ContractData, Clinic, Immunization, Immunizations } from '../../../../models/contract';
import { states } from '../../../../JSON/States';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService,Message} from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { Router } from '@angular/router';
import {DialogModule} from 'primeng/dialog';
import { AppCommonSession } from '../../../common/app-common-session';
import { SessionDetails } from '../../../../utility/session';
import { environment } from '../../../../../environments/environment.prod';

@Component({
  selector: 'app-charity-locations',
  templateUrl: './charity-locations.component.html',
  styleUrls: ['./charity-locations.component.css']
})
export class CharityLocationsComponent implements OnInit {
  @Input('group')
  public locationForm: FormGroup;
  @Input('locFormArray')
  public locationFormArray: FormArray;
  @Input('locationNumber')
  clinicNumber: Number = 0;
  locationData: any = {};
  immunizations: any[]; 
  states: any[];
  defaultDate: Date;;
  maxDateValue: Date;
  minDateValue: Date ;
  display: boolean = false;
  msgs: Message[] = [];
  dialogSummary:string;
  dialogMsg:string;
  user_info:any;
  clinicdatavalue: any;
  cancel_save: string;
  savePopUp: boolean = false;

  constructor(private _localContractService: OutreachProgramService, private fb: FormBuilder, 
     private confirmationService: ConfirmationService,private prouter:Router,private messageService: MessageService, private commonsession: AppCommonSession) {    
    this.user_info =  this.commonsession.login_info;  
   }

  ngOnInit() {
    this.states = states; 
    if(this.isStoreIsfromRestrictedState()){
      let today = new Date();
      this.defaultDate = new Date(today.getFullYear(),8,1);
      this.minDateValue = new Date(today.getFullYear(),8,1);
      this.maxDateValue = new Date(today.getFullYear()+1, today.getMonth(), today.getDate()-1);
    }
    else{
      this.defaultDate = new Date();
      this.minDateValue= new Date();
      let today = new Date();
      this.maxDateValue  = new Date(today.getFullYear()+5, today.getMonth(), today.getDate()-1);
    }   

  }

  isStoreIsfromRestrictedState() {
    var resrict_states = ["MO", "DC"];
    var current_date = new Date();
    if (resrict_states.indexOf(SessionDetails.GetState()) > -1) {
        if (current_date > new Date(environment.MO_State_From) && current_date < new Date(environment.MO_State_TO)) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

  isFieldValid(field:  string) {
    
    return  !this.locationForm.get(field).valid  &&  this.locationForm.get(field).touched;

  }
  onCharityHHSVoucherChange()
  {
    
  }


  copyPreviousLocation(formIndex: number, checked: boolean) {
    if (!checked) {
      this.locationFormArray.controls[formIndex].get('address1').setValue('');
      this.locationFormArray.controls[formIndex].get('address2').setValue('');
      this.locationFormArray.controls[formIndex].get('city').setValue('');
      this.locationFormArray.controls[formIndex].get('state').setValue('');
      this.locationFormArray.controls[formIndex].get('zipCode').setValue('');
    }
    else {
      this.locationFormArray.controls[formIndex].get('address1').setValue(this.locationFormArray.controls[0].get('address1').value);
      this.locationFormArray.controls[formIndex].get('address2').setValue(this.locationFormArray.controls[0].get('address2').value);
      this.locationFormArray.controls[formIndex].get('city').setValue(this.locationFormArray.controls[0].get('city').value);
      this.locationFormArray.controls[formIndex].get('state').setValue(this.locationFormArray.controls[0].get('state').value);
      this.locationFormArray.controls[formIndex].get('zipCode').setValue(this.locationFormArray.controls[0].get('zipCode').value);
    }
  }

  copyPreviousContactInfo(formIndex: number, checked: boolean) {
    if (!checked) {
      this.locationFormArray.controls[formIndex].get('contactFirstName').setValue('');
      this.locationFormArray.controls[formIndex].get('contactLastName').setValue('');
      this.locationFormArray.controls[formIndex].get('contactEmail').setValue('');
      this.locationFormArray.controls[formIndex].get('contactPhone').setValue('');
    }
    else {
      this.locationFormArray.controls[formIndex].get('contactFirstName').setValue(this.locationFormArray.controls[0].get('contactFirstName').value);
      this.locationFormArray.controls[formIndex].get('contactLastName').setValue(this.locationFormArray.controls[0].get('contactLastName').value);
      this.locationFormArray.controls[formIndex].get('contactEmail').setValue(this.locationFormArray.controls[0].get('contactEmail').value);
      this.locationFormArray.controls[formIndex].get('contactPhone').setValue(this.locationFormArray.controls[0].get('contactPhone').value);
    }
  }

  onClinicDateSelected(selectedDate:Date)
  {
    let today = new Date();
    //if() to display if the user is non admin.

    if(SessionDetails.GetRoleID()=='1')
    {
      return;
    }
 
    if(selectedDate.getDate()-today.getDate()<=14)
    {
      this.showDialog('IMPORTANT REMINDER',
      'IMPORTANT REMINDER:If the clinic has not been scheduled at least two weeks prior to the clinic date, vaccine will not arrive in time for this clinic and you will need to work with local leadership to obtain vaccine from your area.' );
    }

  }
  displayFieldCss(field:  string) {    
   this.locationForm.updateValueAndValidity();
    if(this.locationForm.controls['isNoClinic'].value == true)
    { 
      if(field==='localvouchersDist' || field==='contactFirstName' || field==='contactLastName' ||
      field==='contactPhone' || field==='localEstmdShots' || field==='contactEmail' )
      {     
        return  {
          'has-error':  this.isFieldValid(field),
          'has-feedback':  this.isFieldValid(field)
        };
      }
      else
      return ;
    }

    return  {
      'has-error':  this.isFieldValid(field),
      'has-feedback':  this.isFieldValid(field)
    };
  }


displayNgMsg(msgSeverity:string, msgSummary:string, msgDetail:string) {
  this.msgs=[];
  //this.msgs.push({severity:msgSeverity,summary:msgSummary,detail:msgDetail});
  //this.messageService.add({severity:msgSeverity,summary:msgSummary,detail:msgDetail});  
}

confirm() {
  this.confirmationService.confirm({
      message: 'IMPORTANT REMINDER:If the clinic has not been scheduled at least two weeks prior to the clinic date, vaccine will not arrive in time for this clinic and you will need to work with local leadership to obtain vaccine from your area.',
      header:'IMPORTANT REMINDER',
      icon:'fa fa-question-circle',
      accept:()=>{},
      reject: () => {
        return;
      }
  });
}

showDialog(msgSummary:string, msgDetail:string) {
  this.dialogMsg=msgDetail;
  this.dialogSummary=msgSummary;
  this.display = true;
}

}
