import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractapproveComponent } from './contractapprove.component';

describe('ContractapproveComponent', () => {
  let component: ContractapproveComponent;
  let fixture: ComponentFixture<ContractapproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractapproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractapproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
