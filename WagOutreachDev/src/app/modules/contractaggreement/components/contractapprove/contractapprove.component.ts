import { Component, ViewChild, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { BusinesscontactpageComponent } from '../businesscontactpage/businesscontactpage.component';
import { ApproveagreementComponent } from '../approveagreement/approveagreement.component';
import { FieldErrorDisplayComponent } from '../../../common/components/field-error-display/field-error-display.component';
import { ContractapproveService } from '../../services/contractapprove.service';

import { ClientInfo } from '../../../../models/client-info';
import { LegalNoticeAddress } from '../../../../models/contract';
import { SessionDetails } from '../../../../utility/session';


@Component({
  selector: 'app-contractapprove',
  templateUrl: './contractapprove.component.html',
  styleUrls: ['./contractapprove.component.css']
})
export class ContractapproveComponent implements OnInit {
  step: any = 1;
  message: string;
  client: string;
  name: string;
  title: string;
  private reloadiv: boolean = false;
  englishLanguage: string;
  enheader: string;
  enattachment: string;
  language: string;
  showBtnEn: boolean = false;
  showBtnSp: boolean = true;
  enlanguageSelection: boolean;
  splanguageSelection: boolean;
  spanishLanguage: string;
  headerText: string;
  attachment: string;
  imz: string;
  payment: string;
  rates: string;
  test: string;
  errormessage: string;
  currentLanguage: string = 'english';

  @ViewChild(BusinesscontactpageComponent) business_contact_pgae: BusinesscontactpageComponent;
  @ViewChild(ApproveagreementComponent) approve_agreement: ApproveagreementComponent;
  @ViewChild(FieldErrorDisplayComponent) field_error_display;

  firstChild: string;

  @Output() myevent: EventEmitter<string> = new EventEmitter<string>();

  public heroForm: FormGroup;
  constructor(private _apiservice: ContractapproveService, private router: Router, private client_info: ClientInfo
    , private legalnotice_address: LegalNoticeAddress) {
  }

  ngOnInit() {
    let cpk = sessionStorage["clinic_agreement_pk"];
    this._apiservice.getContractAgreementApproveData(cpk).subscribe((data) => {
      SessionDetails.fromUserPage(false);
      if (data.isApproved || (data.notes!="" && data.notes!=null)) {
        SessionDetails.fromUserPage(true);
        this.router.navigateByUrl("/viewcontract");
      }
    });
  }

  GoToStep(step: number) {
    if (this.OnSave()) {
      this.step = step;
    }

  }
  OnSave() {
    if (this.step === 1) {
      return this.business_contact_pgae.save()
    }
    else {
      return true;
    }

  }
  languageChangeToSpanish() {
    this.showBtnEn = true;
    this.showBtnSp = false;

    if (this.step === 1) {
      this.business_contact_pgae.languageChangeToSpanish();
    }
    if (this.approve_agreement !== undefined)
      this.approve_agreement.languageChangeToSpanish();
    this.currentLanguage = 'spanish';
  }

  languageChangeToEnglish() {
    this.showBtnSp = true;
    this.showBtnEn = false;
    if (this.step === 1) {
      this.business_contact_pgae.languageChangeToEnglish();
    }
    if (this.approve_agreement !== undefined) {
      this.approve_agreement.languageChangeToEnglish();
    }
    this.currentLanguage = 'english';

  }
}
