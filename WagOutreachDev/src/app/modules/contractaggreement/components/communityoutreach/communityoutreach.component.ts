import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, NgForm, FormBuilder } from '@angular/forms';
import { STEPS, COSTEPS } from '../../workflow/workflow.model';
import { WorkflowGuard } from '../../workflow/workflow-guard.service';
import { ImmunizationsComponent } from '../immunizations/immunizations.component';
import { LocationsListComponent } from '../contract-locations-list/locations-list.component';
import { OutreachProgramService } from '../../services/outreach-program.service';
import { OutReachProgramType } from '../../../../models/OutReachPrograms';
import { ConfirmationService, KeyFilterModule, KeyFilter, KEYFILTER_VALIDATOR } from 'primeng/primeng';
import { Router } from '@angular/router';
import { SessionDetails } from '../../../../utility/session';
import { ErrorMessages } from '../../../../config-files/error-messages';
import { Location } from '@angular/common';


@Component({
  selector: 'app-communityoutreach',
  templateUrl: './communityoutreach.component.html',
  styleUrls: ['./communityoutreach.component.css']
  , providers: [WorkflowGuard]
})
export class CommunityoutreachComponent implements OnInit {
  @ViewChild(ImmunizationsComponent) imzcmp: ImmunizationsComponent;
  @ViewChild(LocationsListComponent) loccmp: LocationsListComponent;


  showHints:boolean = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff"))=='yes')?false:true;
  pageName:string = "communityoutreach";

  public contractForm: FormGroup;
  step: number = 1;
  step_desc: string;
  contractData: any;
  form: NgForm
  errorse: any;
  errorstatus: any;
  OutReachProgramType: string;
  nextBtnTitle: string = 'Next';

  display: boolean = false;
  interimDisplay: boolean = false;
  cnfdisplay: boolean = false;
  cnfcontinue: boolean = false;
  dialogSummary: string;
  dialogMsg: string;
  clinicdatavalue: any;
  cancel_save: string;
  savePopUp: boolean = false;

  isLocationsFormValid = true;

  constructor(private _fb: FormBuilder, private _location:Location, private _localContractService: OutreachProgramService, private _workflowGuard: WorkflowGuard,
    private router: Router, private confirmationService: ConfirmationService) {
  }

  ngOnInit() {
    this.OutReachProgramType = OutReachProgramType.communityoutreach;
    this._localContractService.clearSelectedImmunizations();
    this._localContractService.clearClinicLocations();
  }

  GoToStep(step: number) {
    let step_desc = this.getStepDesc(step);
    this.step_desc = this.getStepDesc(this.step);
    if (step < this.step) {
      this.step = step;
      step >= 2 ? this.nextBtnTitle = "Submit" : this.nextBtnTitle = "Next";
      return;
    }
    if (step >= 2) {
      this.step = 2;
    }
    if (this.OnSave(step)) {

      if (this.step_desc != step_desc) {
        this.step_desc = this._workflowGuard.verifyWorkFlow(step_desc);
        step >= 2 ? this.nextBtnTitle = "Submit" : this.nextBtnTitle = "Next";
        let locSteps = COSTEPS;
        this.step = Object.keys(locSteps).findIndex(inx => inx == this.step_desc) + 1;
      }
    }
  }
  getStepDesc(step: number) {
    if (step === 1)
      return STEPS.immunization;
    if (step >= 2)
      return STEPS.location;
  }
  cacheInEnteredData(step_desc: any, currentStep?: number, prevStep?: number) {
    let return_value: boolean = false;
    if (this.step_desc === STEPS.immunization) {
      return_value = this.imzcmp.simpleSave();
    }
    else if (this.step_desc === STEPS.location) {
      if (this.loccmp != undefined) {
        this.loccmp.simpleSave();
        if(!this.loccmp.isFormValid()){
          this.isLocationsFormValid = false;
        } else {
          this.isLocationsFormValid = true;
        }
      }
    }
  }
  OnSave(curentStep: Number) {
    let return_value = false;
    if (this.step_desc === STEPS.immunization) {
      return_value = this.imzcmp.save(this.OutReachProgramType);
      if (!return_value) {
        this.step = 1;
      }
    }

    if (this.step_desc === STEPS.location && !this.loccmp.isFormValid()) {
      return false;
    }
    let locString: String[] = [];
    if (this.step_desc === STEPS.location) {
      if (this.loccmp.isClinicDateReminderBefore2WksReq(locString)) {
        let sumMsg = ErrorMessages['impRmndr'];
        let errMsg = ErrorMessages['clinicDateReminderBefore2WeeksEN'];
        var userProfile = SessionDetails.GetUserProfile();
        if ( userProfile.userRole.toLowerCase() == "director" ||
         userProfile.userRole.toLowerCase() == "supervisor" || userProfile.userRole.toLowerCase() == "regionalVp" ||
          userProfile.userRole.toLowerCase() == "regionalDirector" || userProfile.userRole.toLowerCase() == "admin" ) {
          this.step = 2;
          this.confirm(sumMsg, errMsg);
          return true;
        }
        else {
          this.showDialog(sumMsg, errMsg);
          this.step = 2;
          return false;
        }
      }
      return_value = this.loccmp.save(this.OutReachProgramType);
      if (return_value) {
        if (this._localContractService.saveCommunityOutreachData()) {
          if(this.loccmp.submitCoData()){
          let sumMsg = ErrorMessages['coSummary'];
          let sMsg = ErrorMessages['coSucccess'];
          // this.showDialog(sumMsg, sMsg);
          }
          //this.step = 1;
        }
      }
    }
    return return_value;
  }

  showDialog(msgSummary: string, msgDetail: string) {
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
    this.display = true;
  }
  showDialogCancel(msgSummary: string, msgDetail: string) {
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
  }

  showInterimDialog(msgSummary: string, msgDetail: string) {
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
    this.interimDisplay = true;
  }

  cnfshowDialog(msgSummary: string, msgDetail: string) {
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
    this.cnfdisplay = true;
  }

  confirm(hdr: string, msg: string) {
    this.confirmationService.confirm({
      message: msg,
      header: hdr,
      accept: () => {
        if(this.loccmp.save(OutReachProgramType.communityoutreach)){
        if (this.loccmp.submitCoData()) {
          let sumMsg = ErrorMessages['coSummary'];
          let sMsg = ErrorMessages['coSucccess'];
          // this.step = 1;
          // this.showDialog(sumMsg, sMsg);
        }
      }
        else {
          this.step = 2;
          //let sumMsg = ErrorMessages['errMsg'];
          //let sMsg = ErrorMessages['unKnownError'];

          //this.showDialog(sumMsg, sMsg);
        }
        // if(this._localContractService.saveClinicLocations(this.contractForm.value))
        // {
        // //todo to display successful message after subscribe 
        // if (this._localContractService.saveCommunityOutreachData()) {
        //   this.step = 1;
        //   let sumMsg = ErrorMessages['coSummary'];
        //   let sMsg = ErrorMessages['coSucccess'];
        //   this.showDialog(sumMsg, sMsg);
        //   if (this.router) this.router.navigate(['./']);//todo          
        //   this.step = 1;
        // }         
        // }
        return true;
      },
      reject: () => {
        this.step = 2;
        return false;
      }
    });
  }
  okClicked() {
    if (this.router) this.router.navigate(['./communityoutreach/storehome']);//TODO
  }
  doNotSave() {
    this.savePopUp = false;
    if (this.router) this.router.navigate(['./communityoutreach/storehome']);
  }
  Continue() {
    this.savePopUp = false;
  }
  okInterimClicked() {
    this.interimDisplay = false;
  }
  cancelCommOutReach() {
    let return_value = false;
    if (this.step == 1) {
      return_value =  this.imzcmp.cancelImmunization();
      if (return_value) {
        this.savePopUp = true;
        this.showDialogCancel(ErrorMessages['unSavedData'], ErrorMessages['contract_alert']);
      }
      else {
        this._location.back();
      }
    }
   if (this.step == 2) {
    return_value = this.loccmp.cancelLocationData();   
    if (return_value) {
      this.savePopUp = true;
      this.showDialogCancel(ErrorMessages['unSavedData'], ErrorMessages['contract_alert']);
    }
    else {
      this._location.back();
    }
    }
  }

  coSubmitBtnClicked() {
    if (this._localContractService.saveInterimCommunityOutreachData(this.step)) {
      let sumMsg = ErrorMessages['coSummary'];
      let sMsg = ErrorMessages['coSucccess'];
      this.showDialog(sumMsg, sMsg);
      //this.step = 1;
    }
  }

  coSaveBtnClicked() {
    if (this.step == 1) {
      if (this._localContractService.getImmunizationsData() !== '') {
        if (this._localContractService.getImmunizationsData().length > 0) {
          this.imzcmp.save(this.OutReachProgramType);
        }
        else {
          //log co contact
          return;
        }
      }
      else {
        //log co contact.
        return;
      }
    }
    else if (this.step == 2) {
      this.loccmp.save(this.OutReachProgramType);
    }
    if (this._localContractService.saveInterimCommunityOutreachData(this.step)) {
      let sumMsg = ErrorMessages['coSummary'];
      let sMsg = ErrorMessages['coSucccess'];
      this.showInterimDialog(sumMsg, sMsg);
      //this.step = 1;
    }
  }

}
