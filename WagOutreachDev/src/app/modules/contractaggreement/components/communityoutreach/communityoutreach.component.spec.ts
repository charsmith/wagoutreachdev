import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityoutreachComponent } from './communityoutreach.component';

describe('CommunityoutreachComponent', () => {
  let component: CommunityoutreachComponent;
  let fixture: ComponentFixture<CommunityoutreachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityoutreachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityoutreachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
