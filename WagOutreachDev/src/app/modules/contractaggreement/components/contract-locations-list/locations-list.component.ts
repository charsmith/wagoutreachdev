import { Component, OnInit, AfterViewInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { OutreachProgramService } from '../../services/outreach-program.service';
import { LocationNumberPipe } from '../../pipes/location-number.pipe';
import { ContractData, Clinic, Immunization, Immunizations, Immunization2 } from '../../../../models/contract';
import { TimeToDatePipe } from '../../pipes/time-to-date.pipe';
import { DatePipe, Time } from '@angular/common';
import { OutReachProgramType } from '../../../../models/OutReachPrograms';
import { Router } from '@angular/router';
//import { Utility } from '../../../common/utility';
import { ConfirmationService, KeyFilterModule, KeyFilter, KEYFILTER_VALIDATOR } from 'primeng/primeng';
//import { Immunization2 } from '../../../../models/outReachPrograms/contract';
import { ErrorMessages } from '../../../../config-files/error-messages';
import { SessionDetails } from '../../../../utility/session';
import { String, StringBuilder } from 'typescript-string-operations';
import { outreachType } from '../../../../JSON/outReachType';
import { Util } from '../../../../utility/util';
import { environment } from '../../../../../environments/environment.prod';
import { CommunityOutreach } from '../../../../models/community-outreach-v1';

@Component({
    selector: 'app-locations-list',
    templateUrl: './locations-list.component.html',
    styleUrls: ['./locations-list.component.css'],
    providers: [LocationNumberPipe, TimeToDatePipe, DatePipe]
})
export class LocationsListComponent implements OnInit {
    public contractForm: FormGroup;
    public addedImmunizations: Immunization2[];
    //addedImmunizations1 : any; 
    location_number: any = 0;
    @Input('outReachProgramType')
    outReachProgramType: string = OutReachProgramType.contracts;
    contractData: ContractData;// = new ContractData();
    display: boolean = false;
    locListModalDlg: boolean = false;
    locConfirm250ModalDlg: boolean = false;
    coLocConfirm250ModalDlg: boolean = false;
    locConfirm25ModalDlg: boolean = false;

    dialogSummary: string;
    dialogMsg: string;
    fluExpiryDate: string = '';
    RoutineExpiryDate: string = '';
    userApprovedGt250: boolean = false;
    userApprovedLt25: boolean = false;
    address1: string;
    address2: string;
    city: string;
    state: string;
    display_message: boolean = false;
    zipCode: string;
    previous_location_data: any[];
    previous_locations: any[];
    is_previous_locations: boolean = false;
    locations_data: FormGroup;
    locations_data1: FormGroup;
    @Output() clinicSubmit: EventEmitter<number> = new EventEmitter();
    ccRegex: RegExp = /\d{1,5}/;
    constructor(private _fb: FormBuilder,
        private _localContractService: OutreachProgramService,
        private _locationNumberPipe: LocationNumberPipe,
        private _timeToDatePipe: TimeToDatePipe,
        private _datePipe: DatePipe, private confirmationService: ConfirmationService, private prouter: Router,
        private utility: Util) {

    }

    ngOnInit() {
        this.contractForm = this._fb.group({
            locations: this._fb.array([])
        });
        this.is_previous_locations = false;
        this.display_message = false;
debugger;
        //this._localContractService.clearSelectedImmunizations(); comment is needed as edit agreement click should work. but it 
        // is breaking co program.
        this.userApprovedGt250 = false;
        this.userApprovedLt25 = false;
        this.contractData = this._localContractService.fetchContractAgreementData();
        this.addedImmunizations = this.contractData.clinicImmunizationList;
        let oppurtunitiesData: any = SessionDetails.GetopportunitiesData();
        //For restrction states
        if (this.isStoreIsfromRestrictedState()) {
            this._localContractService.getpreviousseasoncliniclocations(this.contractData.clinicAgreementPk, oppurtunitiesData.businessPk).subscribe((res) => {
                this.previous_location_data = res.previousLocationsList;
                if (this.previous_location_data.length > 1) {
                    this.is_previous_locations = true;
                    this.display_message = true;
                }
                else {
                    this.display_message = false;
                }
            });
        }

        //this.addedImmunizations = this.contractData.contractAgreement.clinicImmunizations;
        if (this.contractData.clinicList.length > 0) { // for clinics which are already present
            this.contractData.clinicList.forEach((loc, index) => {
                let clinic_immunizations: any[] = [];
                this.addedImmunizations.forEach(rec => {
                    let imz: any = {};
                    let matching_imz = loc.clinicImzQtyList.filter(item => item.immunizationPk == rec.immunizationPk && item.paymentTypeId == rec.paymentTypeId);
                    imz.immunizationPk = rec.immunizationPk;
                    imz.immunizationName = rec.immunizationName;
                    imz.paymentTypeName = rec.paymentTypeName;
                    imz.paymentTypeId = rec.paymentTypeId;
                    imz.estimatedQuantity = matching_imz[0] ? matching_imz[0].estimatedQuantity : "";

                    if (rec.immunizationName.toLowerCase().search('flu') !== -1 && rec.voucherExpirationDate !== undefined && rec.voucherExpirationDate !== null) {
                        this.fluExpiryDate = rec.voucherExpirationDate;
                    }
                    if (rec.immunizationName.toLowerCase().search('flu') === -1 && rec.voucherExpirationDate !== undefined && rec.voucherExpirationDate !== null) {
                        this.RoutineExpiryDate = rec.voucherExpirationDate;
                    }
                    clinic_immunizations.push(imz);
                });

                loc.clinicImzQtyList = clinic_immunizations;
                this.addLocation(loc);
                //this.location_number = this.location_number + 1;
            });
        }
        else { //new contract

            let location: Clinic = new Clinic();
            location.location = "CLINIC LOCATION " + this._locationNumberPipe.transform(this.location_number + 1);

            this._localContractService.getStoreBusinessDetails(oppurtunitiesData.businessPk).subscribe((res) => {
                location.address1 = res[0].address1;
                location.address2 = res[0].address2;
                location.city = res[0].city;
                location.state = res[0].state;
                location.zipCode = res[0].zipCode;

                let clinic_immunizations: any[] = [];
                this.addedImmunizations = this.contractData.clinicImmunizationList;
                this.addedImmunizations.forEach(rec => {
                    let imz: any = {};
                    //let matching_imz = location.clinicImzQtyList.filter(item => item.immunizationPk == rec.immunizationPk && item.paymentTypeId == rec.paymentTypeId);
                    imz.immunizationPk = rec.immunizationPk;
                    imz.immunizationName = rec.immunizationName;
                    imz.paymentTypeName = rec.paymentTypeName;
                    imz.paymentTypeId = rec.paymentTypeId;
                    imz.estimatedQuantity = "";

                    if (rec.immunizationName.toLowerCase().search('flu') !== -1 && rec.voucherExpirationDate !== undefined && rec.voucherExpirationDate !== null) {
                        this.fluExpiryDate = rec.voucherExpirationDate;
                    }
                    if (rec.immunizationName.toLowerCase().search('flu') === -1 && rec.voucherExpirationDate !== undefined && rec.voucherExpirationDate !== null) {
                        this.RoutineExpiryDate = rec.voucherExpirationDate;
                    }
                    clinic_immunizations.push(imz);
                });

                location.clinicImzQtyList = clinic_immunizations;
                this.addLocation(location);
            });
        }

    }
    disableButton() {
        if (this.display_message) {
            return 'false';
        }
        else {
            return '';
        }
    }
    PreviousLocationChange(event) {
        if (event != "") {
            this.display_message = false;
            this.previous_locations = this.previous_location_data.filter(item => item.rowId == event);
        }
        else {
            this.display_message = true;

        }
    }
    initLocation(location: Clinic) {
        let clinic_immunizations: any[];
        let clinic_location: any = {};
        let start_time: string;
        let end_time: string;
        let previous_display: boolean = true;
        this.location_number = this.location_number + 1;
        if (this.previous_locations != undefined) {
            if (this.previous_locations.length > 0) {
                this.address1 = this.previous_locations[0].address;
                this.address2 = this.previous_locations[0].address2;
                this.city = this.previous_locations[0].city;
                this.state = this.previous_locations[0].state;
                this.zipCode = this.previous_locations[0].zip;
                previous_display = false;
            }
        }
        if (location === undefined) {
            clinic_immunizations = [];
            location = new Clinic();
            location.location = "CLINIC LOCATION " + this._locationNumberPipe.transform(this.location_number);

            let added_immunizations = this._localContractService.getSelectedImmunizations();//this.contractData.clinicImmunizationList;//
            added_immunizations.forEach(rec => {
                let imz: any = {};
                imz.immunizationPk = rec.immunizationPk;
                imz.immunizationName = rec.immunizationName;
                imz.paymentTypeName = rec.paymentTypeName;
                imz.paymentTypeId = rec.paymentTypeId;
                imz.estimatedQuantity = "";

                if (rec.voucherExpirationDate !== undefined && rec.voucherExpirationDate !== null) {
                    if (rec.immunizationName.toLowerCase().search('flu') !== -1 && rec.voucherExpirationDate.valueOf() > 0) {
                        this.fluExpiryDate = rec.voucherExpirationDate;
                    }
                    if (rec.immunizationName.toLowerCase().search('flu') === -1 && rec.voucherExpirationDate.valueOf() > 0) {
                        this.RoutineExpiryDate = rec.voucherExpirationDate;
                    }
                }
                clinic_immunizations.push(imz);
            });
        }
        else {
            clinic_immunizations = location.clinicImzQtyList;
            clinic_immunizations.forEach(rec => {
                if (rec.voucherExpirationDate !== undefined && rec.voucherExpirationDate !== null) {
                    if (rec.immunizationName.toLowerCase().search('flu') !== -1 && rec.voucherExpirationDate.valueOf() > 0) {
                        this.fluExpiryDate = rec.voucherExpirationDate;
                    }
                    if (rec.immunizationName.toLowerCase().search('flu') === -1 && rec.voucherExpirationDate.valueOf() > 0) {
                        this.RoutineExpiryDate = rec.voucherExpirationDate;
                    }
                }
            });
            if (this.isStoreIsfromRestrictedState() && location != undefined && previous_display) {
                this.address1 = location.address1;
                this.address2 = location.address2;
                this.city = location.city;
                this.state = location.state;
                this.zipCode = location.zipCode;
            }
        }
        // if (location.clinicDate !== '') {
        //     try {
        //     start_time = new Date().setTime(12);

        //     this._timeToDatePipe.transform(location.clinicDate, location.startTime);
        //     end_time = this._timeToDatePipe.transform(location.clinicDate, location.endTime);
        //     }catch(e){
        //         console.log(e);
        //     }// to fix the bug for crash
        // }
        let hasCorpInvoice = this.checkIfAnyImmunizationsHasCorpInvoice(location);
        let hasCorpInvAndVchrNd = this.checkIfAnyImmunizationsHasCorpInvoiceAndVoucherNeeded(location);
        let isNoClinic: number = (location.isNoClinic === undefined || location.isNoClinic === null || location.isNoClinic <= 0) ? 0 : +location.isNoClinic;
        if(!hasCorpInvAndVchrNd){
            isNoClinic = 0;
            location.isNoClinic = 0;
        }
        //isNoClinic = location.isNoClinic>=1 && location.isNoClinic != null && location.isNoClinic != undefined;//(hasCorpInvoice && hasCorpInvAndVchrNd) == true ? 1 : 0;
        // if(location.fluExpiryDate.length>0) {
        //     fluExpiryDate = location.fluExpiryDate;
        // }
        // if(location.routineExpiryDate.length>0) {
        //     RoutineExpiryDate = location.routineExpiryDate;
        // }
        if (this.outReachProgramType == OutReachProgramType.contracts) {
            return this._fb.group({
                clinicImzQtyList: this.initImmunizations(clinic_immunizations),
                location: this._fb.control(location.location),
                contactFirstName: this._fb.control(location.contactFirstName, Validators.required),
                contactLastName: this._fb.control(location.contactLastName, Validators.required),
                contactEmail: this._fb.control(location.contactEmail, Validators.required),
                contactPhone: this._fb.control(Util.telephoneNumber(location.contactPhone), Validators.required),
                clinicDate: this._fb.control(location.clinicDate !== '' ? new Date(location.clinicDate) :
                location.clinicDate, isNoClinic ? null : Validators.required),
                startTime: this._fb.control(location.startTime, isNoClinic ? null : Validators.required),
                endTime: this._fb.control(location.endTime, isNoClinic ? null : Validators.required),
                address1: this._fb.control(this.isStoreIsfromRestrictedState() ? (this.address1 != null || this.address1 != undefined) ? this.address1 : location.address1 : location.address1, Validators.required),
                address2: this._fb.control(this.isStoreIsfromRestrictedState() ? (this.address2 != null || this.address2 != undefined) ? this.address2 : location.address2 : location.address2),
                city: this._fb.control(this.isStoreIsfromRestrictedState() ? (this.city != null || this.city != undefined) ? this.city : location.city : location.city, Validators.required),
                state: this._fb.control(this.isStoreIsfromRestrictedState() ? (this.state != null || this.state != undefined) ? this.state : location.state : location.state, Validators.required),
                zipCode: this._fb.control(this.isStoreIsfromRestrictedState() ? (this.zipCode != null || this.zipCode != undefined) ? this.zipCode : location.zipCode : location.zipCode, Validators.required),

                estShots: this._fb.control(location.estShots),
                isReassign: this._fb.control(location.isReassign >= 1 ? 1 : 0, Validators.required),
                isNoClinic: this._fb.control(hasCorpInvoice ? isNoClinic : { value: hasCorpInvAndVchrNd ? 1 : 0, disabled: true }, null),
                previousContact: this._fb.control(+location.previousContact, null),
                previousLocation: this._fb.control(+location.previousLocation, null),
                isCurrent: this._fb.control(1),
                fluExpiryDate: this._fb.control({ value: new Date(this.fluExpiryDate), disabled: true }, null),
                routineExpiryDate: this._fb.control({ value: new Date(this.RoutineExpiryDate), disabled: true }, null),
                latitude: this._fb.control(location.latitude),
                longitude: this._fb.control(location.longitude)
            });

        }
        else if (this.outReachProgramType == OutReachProgramType.communityoutreach) {
                return this._fb.group({
                    clinicImzQtyList: this.initImmunizations(clinic_immunizations),
                    location: this._fb.control(location.location),
                    contactFirstName: this._fb.control(location.contactFirstName, Validators.required),
                    contactLastName: this._fb.control(location.contactLastName, Validators.required),
                    contactEmail: this._fb.control(location.contactEmail, Validators.required),
                    contactPhone: this._fb.control(Util.telephoneNumber(location.contactPhone), Validators.required),
                    clinicDate: this._fb.control(location.clinicDate, Validators.required),
                    startTime: this._fb.control(location.startTime, Validators.required),
                    endTime: this._fb.control(location.endTime, Validators.required),

                    //  disabling fields if the store state belongs to restricted state
                    address1: this._fb.control(this.isStoreIsfromRestrictedState() ? (this.address1 != null || this.address1 != undefined) ? this.address1 : location.address1 : location.address1, Validators.required),
                address2: this._fb.control(this.isStoreIsfromRestrictedState() ? (this.address2 != null || this.address2 != undefined) ? this.address2 : location.address2 : location.address2),
                city: this._fb.control(this.isStoreIsfromRestrictedState() ? (this.city != null || this.city != undefined) ? this.city : location.city : location.city, Validators.required),
                state: this._fb.control(this.isStoreIsfromRestrictedState() ? (this.state != null || this.state != undefined) ? this.state : location.state : location.state, Validators.required),
                zipCode: this._fb.control(this.isStoreIsfromRestrictedState() ? (this.zipCode != null || this.zipCode != undefined) ? this.zipCode : location.zipCode : location.zipCode, Validators.required),

                    isCurrent: this._fb.control(location.isCurrent),
                    previousContact: this._fb.control(+location.previousContact, null),
                    previousLocation: this._fb.control(+location.previousLocation, null),
   
                    coOutreachTypeId: this._fb.control(location.coOutreachTypeId, Validators.required),
                    coOutreachTypeDesc: this._fb.control(location.coOutreachTypeDesc, Validators.required),
                });
        }
    }

    checkIfAnyImmunizationsHasCorpInvoice(location: Clinic): boolean {
        let added_immunizations = this._localContractService.getSelectedImmunizations();
        try {
            let hasCorpInvoice: Boolean = false;
            var fluType = 'flu';
            let fluDone: Boolean = false;
            let routineDone: Boolean = false;
            added_immunizations.forEach(rec => {
                if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1)) {
                    hasCorpInvoice = true;
                    if (rec.immunizationName.toLowerCase().search(fluType) == -1 && !routineDone) {
                        location.routineExpiryDate = rec.voucherExpirationDate;
                        routineDone = true;
                    }
                    else {
                        if (!fluDone) {
                            location.fluExpiryDate = rec.voucherExpirationDate;
                            fluDone = true;
                        }
                    }
                }
            });
            if (hasCorpInvoice == true)
                return true;
        } catch (e) {
            if (e === true)
                return true;
        }
        return false;
    }

    checkIfAnyImmunizationsHasCorpInvoiceAndVoucherNeeded(location: Clinic): boolean {
        let added_immunizations = this._localContractService.getSelectedImmunizations();
        try {
            let hasCorpInvoiceNdVchrNd: Boolean = false;
            var fluType = 'flu';
            added_immunizations.forEach(rec => {
                if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1) && (rec.isVoucherNeeded === '1' || rec.isVoucherNeeded === 1)) {
                    hasCorpInvoiceNdVchrNd = true;
                }
            });
            if (hasCorpInvoiceNdVchrNd == true)
                return true;
        } catch (e) {
            if (e === true)
                return true;
        }
        return false;
    }

    isStoreIsfromRestrictedState() {
        var  resrict_states  =  ["MO",  "DC"];
        var  current_date  =  new  Date();
        if  (resrict_states.indexOf(SessionDetails.GetState())  >  -1) {
            if  (current_date  >  new  Date(environment.MO_State_From)  &&  current_date  <  new  Date(environment.MO_State_TO)) {
                return  true;
            }
            else  {
                return  false;
            }
        }
        else  {
            return  false;
        }
    }


    initImmunizations(clinic_immunizations) {
        let imz_array: any[] = [];
        clinic_immunizations.forEach(imz => {
            imz.immunizationName = imz.immunizationName;
            imz.paymentTypeName = imz.paymentTypeName;
            imz_array.push(this._fb.group({
                clinicPk: this._fb.control(imz.clinicPk),
                immunizationPk: this._fb.control(imz.immunizationPk),
                estimatedQuantity: this._fb.control(imz.estimatedQuantity, Validators.required),
                immunizationName: this._fb.control(imz.immunizationName),
                paymentTypeName: this._fb.control(imz.paymentTypeName),
                paymentTypeId: this._fb.control(imz.paymentTypeId)
            }))
        })
        return this._fb.array(imz_array);
    }

    addLocation(location) {
        const control = <FormArray>this.contractForm.controls['locations'];
        const addrCtrl = this.initLocation(location);
        this.locations_data1 = addrCtrl;
        control.push(addrCtrl);
        if (location != undefined) {
            this.locations_data = Object.assign([], addrCtrl);
        }
    }

    removeLocation(i: number) {
        let control = <FormArray>this.contractForm.controls['locations'];
        control.removeAt(i);
        this.location_number--;

        for (let index = 0; index < this.location_number; index++) {
            let fGrp: FormGroup = control.controls[index] as FormGroup;
            fGrp.controls['location'].setValue("CLINIC LOCATION " + this._locationNumberPipe.transform(index + 1));
        }
    }

    isFieldValid(field: string) {
        return !this.contractForm.get(field).valid && this.contractForm.get(field).touched;
    }

    displayFieldCss(field: string) {
        return {
            'has-error': this.isFieldValid(field),
            'has-feedback': this.isFieldValid(field)
        };
    }

    save(outReachPrgType?: string): boolean {
        if (this.isFormValid()) {
            // if(!this.checkSpecialCharacters()){
            //     let summary = ErrorMessages['specialCharacters'];
            //     let errMsg = ErrorMessages['errMsg'];
            //     this.showDialog(errMsg, summary);
            //     return false;
            // }
            this.contractForm.value.locations.forEach(loc => {
                try {
                    if (loc.startTime != "")
                        loc.startTime = this._datePipe.transform(loc.startTime, 'shortTime');
                    if (loc.endTime != "") {
                        loc.endTime = this._datePipe.transform(loc.endTime, 'shortTime');
                    }
                } catch (e) {
                    console.log(e); // here the exception is delibarately caught because when you click back, startTime & end
                    // Time is not correct timeformat. Hence we are ignoring this by catching here.
                }
            });
            try {
                let locDateTimeConflict = false;
                var locConflictList = [];
                var errMsgV1: string[] = [];
                var locationsList = Object.assign([], this.contractForm.value.locations);
                for (let count = 0; count < locationsList.length; count++) {
                    var loc = Object.assign({}, locationsList[count]);
                    if( loc.isNoClinic >= 1 || loc.isNoClinic === '1') { 
                        continue;
                    }
                    let locationsCpy = Object.assign([], locationsList);
                    var index = locationsCpy.findIndex(it => it.location === loc.location);
                    if (index > -1) {
                        locationsCpy.splice(index, 1);
                    }
                    
                    let conflictLocs = locationsCpy.filter(locItem => loc.clinicDate.getFullYear() === locItem.clinicDate.getFullYear() && loc.clinicDate.getMonth() === locItem.clinicDate.getMonth() && loc.clinicDate.getDate() === locItem.clinicDate.getDate() && ( locItem.isNoClinic <= 0 || locItem.isNoClinic === '0'));
                    let endDateAdd39Mts: Date;
                    if (loc.endTime.toString().toLowerCase().search('am') !== -1 || loc.endTime.toString().toLowerCase().search('pm') !== -1) {
                        endDateAdd39Mts = this._timeToDatePipe.transform(loc.clinicDate, loc.endTime);
                    }
                    else {
                        endDateAdd39Mts = loc.endTime;
                    }
                    let startDatetmToDt: Date
                    if (loc.startTime.toString().toLowerCase().search('am') !== -1 || loc.startTime.toString().toLowerCase().search('pm') !== -1) {
                        startDatetmToDt = this._timeToDatePipe.transform(loc.clinicDate, loc.startTime);
                    }
                    else {
                        startDatetmToDt = loc.startTime;
                    }
                    let add30MinTS: Date = new Date(loc.clinicDate.getFullYear(), loc.clinicDate.getMonth(), loc.clinicDate.getDate(), endDateAdd39Mts.getHours(), endDateAdd39Mts.getMinutes() + 30, endDateAdd39Mts.getSeconds());
                    let locStart: Date = new Date(startDatetmToDt);

                    if (conflictLocs.length > 0) {
                        var conflictLocsCpy = Object.assign([], conflictLocs);

                        for (let index = 0; index < conflictLocsCpy.length; index++) {
                            let element = Object.assign(conflictLocsCpy[index]);
                            if (element.startTime.toString().toLowerCase().search('am') !== -1 || element.startTime.toString().toLowerCase().search('pm') !== -1) {
                                element.startTime = this._timeToDatePipe.transform(element.clinicDate, element.startTime);
                            }
                            if (element.endTime.toString().toLowerCase().search('am') !== -1 || element.endTime.toString().toLowerCase().search('pm') !== -1) {
                                element.endTime = this._timeToDatePipe.transform(element.clinicDate, element.endTime);
                            }
                        }
                        conflictLocs = conflictLocsCpy.filter(locItem => loc.clinicDate.getFullYear() === locItem.clinicDate.getFullYear() && loc.clinicDate.getMonth() === locItem.clinicDate.getMonth() && loc.clinicDate.getDate() === locItem.clinicDate.getDate() && ((locItem.startTime.valueOf() >= locStart.valueOf() && locItem.startTime.valueOf() < add30MinTS.valueOf()) || (locItem.endTime.valueOf() > locStart.valueOf() && locItem.endTime.valueOf() < add30MinTS.valueOf())));
                        if (conflictLocs.length > 0) {
                            locConflictList.push({
                                srcLocName: loc,
                                dstLocName: conflictLocs
                            });

                        }

                        // we need this below logic to restore full datetime to short time as JS is affecting
                        // orignal memory location due to above for loop. Its doing the same for all JS,
                        // perhaps this is the behaviour of JS, even though I am copying the elements using
                        // Object.assign
                        try {
                            for (let index = 0; index < conflictLocsCpy.length; index++) {
                                let element = Object.assign(conflictLocsCpy[index]);
                                element.startTime = this._datePipe.transform(element.startTime, 'shortTime');
                                element.endTime = this._datePipe.transform(element.endTime, 'shortTime');
                            }
                        } catch (e) { console.log(e); }
                    }
                }
            } catch (e) {
                console.log(e);
            }
            if (locConflictList.length > 0) {
                if (locConflictList[0].dstLocName.length == 1) {
                    errMsgV1.push(String.Format(ErrorMessages['clincLocDateTimeSingleConflict'], locConflictList[0].dstLocName[0].location, this._datePipe.transform(locConflictList[0].dstLocName[0].clinicDate, "shortDate"), locConflictList[0].dstLocName[0].startTime, locConflictList[0].dstLocName[0].endTime, SessionDetails.GetopportunitiesData().businessName));
                    this.showDialogV2(ErrorMessages['resolve'], errMsgV1.join('<br/><br/>'));
                    return false;
                }
                if (locConflictList[0].dstLocName.length > 1) {
                    locConflictList[0].dstLocName.forEach(loc => {
                        errMsgV1.push(String.Format(ErrorMessages['clincLocDateTimeMultipleConflict'], loc.location, this._datePipe.transform(loc.clinicDate, "shortDate"), loc.startTime, loc.endTime, SessionDetails.GetopportunitiesData().businessName));
                    });
                    this.showDialogV2(ErrorMessages['resolve'], errMsgV1.join('<br/><br/>'));
                    return false;
                }
            }

            var estimatedQty = 0;
            var location25Names: string[] = [];
            var location250Names = [];
            var errMsg: string[] = [];
            var twentyFiveImmunizationError = true;
            var moreThan255ImmunizationError = false;
            for (let i = 0; i < this.contractForm.value.locations.length; i++) {

                for (let j = 0; j < this.contractForm.value.locations[i].clinicImzQtyList.length; j++) {
                    let qty: number = +this.contractForm.value.locations[i].clinicImzQtyList[j].estimatedQuantity;
                    if (qty >= 250) {
                        moreThan255ImmunizationError = true;
                        location250Names.push({
                            estimatedQuantity: qty, immunizationName: this.contractForm.value.locations[i].clinicImzQtyList[j].immunizationName,
                            location: this.contractForm.value.locations[i].location
                        });
                    }
                    else {
                        estimatedQty += qty;
                    }
                }
                if (estimatedQty < 25 && !moreThan255ImmunizationError) {
                    location25Names.push(this.contractForm.value.locations[i].location);
                    estimatedQty = 0;
                    moreThan255ImmunizationError = false;
                }
                else {
                    estimatedQty = 0;
                }
                moreThan255ImmunizationError = false;
            }
            if (!this.userApprovedGt250 && location250Names.length > 0) {
                this.userApprovedGt250 = false;
                this.userApprovedLt25 = false;
            }
            else {
                this.userApprovedGt250 = true;
            }
            if (!this.userApprovedLt25 && location25Names.length > 0) {
                this.userApprovedLt25 = false;
            }
            else {
                this.userApprovedLt25 = true;
            }
            if ((this.userApprovedGt250 && this.userApprovedLt25) || (location250Names.length <= 0 && location25Names.length <= 0)) {
                if( this.outReachProgramType == OutReachProgramType.contracts ) {
                this._localContractService.setClinicLocations(this.contractForm.value);
                this._localContractService.setCancelClinicLocations(this.contractForm.value);
                return true;
                }
                return false;
            }
            else if ((this.userApprovedGt250 ) || (location250Names.length <= 0 ) && this.outReachProgramType == OutReachProgramType.communityoutreach) {
                    this._localContractService.setCOClinicLocations(this.contractForm.value);
                    this._localContractService.setCancelClinicLocations(this.contractForm.value);
                    return true;                
            }
            else {
                if (!this.userApprovedGt250) {
                    location250Names.forEach(item => {
                        errMsg.push(String.Format(ErrorMessages['moreThan250Immunizations'], item.estimatedQuantity, item.immunizationName, item.location));
                    });
                    //this.confirm(ErrorMessages['errMsg'],errMsg.join('\n'));
                    if(this.outReachProgramType === OutReachProgramType.communityoutreach) {
                        this.coShowConfirm250ImmDialog(ErrorMessages['warning'], errMsg.join('<br/><br/>'));
                    } else {
                    this.showConfirm250ImmDialog(ErrorMessages['warning'], errMsg.join('<br/><br/>'));
                    }
                    
                    return false;
                }
                if (!this.userApprovedLt25 && this.outReachProgramType === OutReachProgramType.contracts) {
                    var userProfile = SessionDetails.GetUserProfile();
                    if (userProfile.userRole.toLowerCase() == "admin") {
                        this.showConfirm25ImmDialog(ErrorMessages['resolve'],
                            String.Format(ErrorMessages['MinNumberOfShots'], location25Names.join(',')));
                    }
                    else {
                        this.showDialogV2(ErrorMessages['resolve'], location25Names.join(','));
                    }
                }
                return false;
            }
        }
        else {
            let summary = ErrorMessages['MandatoryFields'];
            let errMsg = ErrorMessages['errMsg'];
            this.showDialog(errMsg, summary);
            return false;
        }
    }

    simpleSave(): boolean {
        this.contractForm.value.locations.forEach(loc => {
            try {
                if (loc.startTime != "")
                    loc.startTime = this._datePipe.transform(loc.startTime, 'shortTime');
                if (loc.endTime != "") {
                    loc.endTime = this._datePipe.transform(loc.endTime, 'shortTime');
                }
            } catch (e) {
                console.log(e); // here the exception is delibarately caught because when you say back, startTime & end
                // Time is not correct timeformat. Hence we are ignoring this by catching here.
            }
        });
        this._localContractService.setClinicLocations(this.contractForm.value);
        return true;
    }

    isFormValid(): boolean {
        let frmar: FormArray = this.contractForm.get('locations') as FormArray;
        for (let index = 0; index < frmar.length; index++) {
            const element = frmar.controls[index] as FormGroup;
            this.utility.validateAllFormFields(element);
        }
        return this.contractForm.valid;
    }

    checkSpecialCharacters(): boolean {
        let isValidField: boolean = true;
        try {
            for (let i = 0; i < this.contractForm.value.locations.length; i++) {
                if (!this.ccRegex.test(this.contractForm.value.locations[i].contactPhone.trim()) ||
                    !this.ccRegex.test(this.contractForm.value.locations[i].zipCode.trim())) {
                    throw false;
                }
                for (let j = 0; j < this.contractForm.value.locations[i].clinicImzQtyList.length; j++) {
                    if (!this.ccRegex.test(this.contractForm.value.locations[i].clinicImzQtyList[j].estimatedQuantity.trim())) {
                        throw false;
                    }
                }
            }
        } catch (e) {
            return e;
        }
    }

    showDialog(msgSummary: string, msgDetail: string) {
        this.dialogMsg = msgDetail;
        this.dialogSummary = msgSummary;
        this.display = true;
    }

    showDialogV2(msgSummary: string, msgDetail: string) {
        this.dialogMsg = msgDetail;
        this.dialogSummary = msgSummary;
        this.locListModalDlg = true;
    }
    showConfirm250ImmDialog(msgSummary: string, msgDetail: string) {
        this.dialogMsg = msgDetail;
        this.dialogSummary = msgSummary;
        this.locConfirm250ModalDlg = true;
    }
    coShowConfirm250ImmDialog(msgSummary: string, msgDetail: string) {
        this.dialogMsg = msgDetail;
        this.dialogSummary = msgSummary;
        this.coLocConfirm250ModalDlg = true;
    }
    showConfirm25ImmDialog(msgSummary: string, msgDetail: string) {
        this.dialogMsg = msgDetail;
        this.dialogSummary = msgSummary;
        this.locConfirm25ModalDlg = true;
    }

    okClickedV2() {
        this.locListModalDlg = false;
        switch (this.outReachProgramType) {
            case OutReachProgramType.contracts:
                break;
            case OutReachProgramType.communityoutreach:
                this.prouter.navigate(['./communityoutreach/storehome'])
            default:
                return;
        }
    }
    coAcceptGt250Imm() {
        this.coLocConfirm250ModalDlg = false;
        if (this._localContractService.saveClinicLocations(this.contractForm.value)) {
            if (this._localContractService.saveCommunityOutreachData()) {
                if(this.submitCoData()){
                let sumMsg = ErrorMessages['coSummary'];
                let sMsg = ErrorMessages['coSucccess'];
                this.showDialogV2(sumMsg, sMsg);
                }
            }
        }
        return;
    }
    coRejectGt250Imm() {
        this.coLocConfirm250ModalDlg = false;
        return;
    }

    acceptGt250Imm() {

        this.userApprovedGt250 = true;

        this.locConfirm250ModalDlg = false;
        if (this.userApprovedGt250 && this.userApprovedLt25) {
            if (this._localContractService.setClinicLocations(this.contractForm.value)) {
                this.clinicSubmit.emit(3);
            }
        }
        return;
    }
    rejectGt250Imm() {
        this.locConfirm250ModalDlg = false;
        return;
    }
    acceptLt25Imm() {
        this.userApprovedLt25 = true;

        this.locConfirm25ModalDlg = false;
        if (this.userApprovedGt250 && this.userApprovedLt25) {
            if (this._localContractService.setClinicLocations(this.contractForm.value)) {
                this.clinicSubmit.emit(3);
            }
        }
        return;
    }
    rejectLt25Imm() {
        this.locConfirm25ModalDlg = false;
        return;
    }
    confirm(hdr: string, msg: string) {
        this.confirmationService.confirm({
            message: msg,
            header: hdr,
            accept: () => {
                if (this._localContractService.setClinicLocations(this.contractForm.value)) {
                    this.clinicSubmit.emit(3);
                }
                return;
            },
            reject: () => {
                return;
            }
        });
    }

    isClinicDateReminderBefore2WksReq(locString: String[]): Boolean {
        let isClinicDateReminder: Boolean = false;
        if (!this.doesClinicsHaveNoClinicAndMiscOptions()) {
            return false;
        }
        try {
            let today = new Date();
            for (let index = 0; index < this.contractForm.value.locations.length; index++) {
                const loc = this.contractForm.value.locations[index];
                if (loc.isNoClinic !== null && loc.isNoClinic!== undefined && loc.isNoClinic >= 1) continue;
                let diffDays: number = loc.clinicDate.getMonth() == today.getMonth() ? Math.round(loc.clinicDate.getDate() - today.getDate()) : 15;
                if (diffDays <= 14) {
                    isClinicDateReminder = true;
                    locString.push(this.location_number);
                }
            }
        } catch (e) {
            return e;
        }
        return isClinicDateReminder;
    }

    doesClinicsHaveNoClinicAndMiscOptions(): boolean {
        let hasCorpInvNdVchNd: boolean = this._localContractService.checkIfAnyImmunizationsHasCorpInvoiceAndVoucherNeeded(undefined);
        let hasNoClinicAndMiscOptions: boolean = false;
        this.contractForm.value.locations.forEach(element => {
            let isNoClinic: boolean = false;
            element.isNoClinic >= 1 && hasCorpInvNdVchNd ? isNoClinic = false : isNoClinic = true;
            hasNoClinicAndMiscOptions = hasNoClinicAndMiscOptions || isNoClinic;
        });
        return hasNoClinicAndMiscOptions;
    }

    submitCoData(): boolean {
        debugger
        if (this.isFormValid()) {
            this.contractForm.value.locations.forEach(loc => {
                try {
                    if (loc.startTime != "")
                        loc.startTime = this._datePipe.transform(loc.startTime, 'shortTime');
                    if (loc.endTime != "") {
                        loc.endTime = this._datePipe.transform(loc.endTime, 'shortTime');
                    }
                } catch (e) { }
            });

            var location250Names = [];
            var errMsg: string[] = [];
            for (let i = 0; i < this.contractForm.value.locations.length; i++) {
                for (let j = 0; j < this.contractForm.value.locations[i].clinicImzQtyList.length; j++) {
                    let qty: number = +this.contractForm.value.locations[i].clinicImzQtyList[j].estimatedQuantity;
                    if (qty >= 250) {
                        location250Names.push({
                            estimatedQuantity: qty, immunizationName: this.contractForm.value.locations[i].clinicImzQtyList[j].immunizationName,
                            location: this.contractForm.value.locations[i].clinicLocation
                        });
                    }
                }
            }

            // if (location250Names.length > 0) {
            //     location250Names.forEach(item => {
            //         errMsg.push(String.Format(ErrorMessages['moreThan250Immunizations'], item.estimatedQuantity, item.immunizationName, item.location));
            //     });
            //     this.coShowConfirm250ImmDialog(ErrorMessages['resolve'], errMsg.join('<br/><br/>'));
            // }
            // else
             {
                if (this._localContractService.saveClinicLocations(this.contractForm.value)) {
                    if (this._localContractService.saveCommunityOutreachData()) {
                        let coData:CommunityOutreach = this._localContractService.getCommunityOutreachData();
                        coData.isApproved = 1;
                        coData.isEmailSent =1;
                        coData.approvedOrRejectedBy = "CommunityUpdate@WagOutreach.com";
                        coData.contactLogPk = -1;
                        coData.clinicAgreementPk = -1;
                        this._localContractService.saveCODataToDB(SessionDetails.GetStoreId(), 1, coData).subscribe((data: any) => {

                            switch (data.errorS.errorCode) {
                              // case 'success':
                              // case 200:
                              case null:
                              case "null":
                              case undefined:
                              case 0:
                              case '0':
                                this.showDialogV2(ErrorMessages['comOutreach'], ErrorMessages['coSaved']);
                                return true;
                            }
                          },
                            err => {
                              switch (err) {
                                case 500:
                                  this.showDialogV2(ErrorMessages['unKnownError'], err.json().Message);
                                  return false;
                                  
                                default:
                                  this.showDialogV2(ErrorMessages['unKnownError'], err.json().Message);
                                  return false;
                              }
                            });
                        }
                      
                        return true;
                    }
                }
            }
        
        else {
            let summary = ErrorMessages['MandatoryFields'];
            let errMsg = ErrorMessages['errMsg'];
            this.showDialog(errMsg, summary);
            return false;
        }
        return false;
    }
    cancelLocationData(): boolean {
        var compare_objects = false;
        compare_objects = this.utility.compareTwoObjects(this.locations_data.value, this.locations_data1.value);
        return compare_objects;
    }
}
