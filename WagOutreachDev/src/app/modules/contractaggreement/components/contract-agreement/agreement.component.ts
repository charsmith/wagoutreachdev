import { Component, OnInit, ViewChild, forwardRef, Output, EventEmitter, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { OutreachProgramService } from '../../services/outreach-program.service';
import { ContractData, WalgreenCo, Client, Clinic } from '../../../../models/contract';//Contract
import { ValidationHandler } from '../../Helpers/ValidationHandler';
import { AlertService } from '../../../../alert.service';
import { ContractComponent } from '../local-contracts/contract.component';
import { ErrorMessages } from '../../../../config-files/error-messages';
import { Util } from '../../../../utility/util';
import { String, StringBuilder } from 'typescript-string-operations';
import { SessionDetails } from '../../../../utility/session';
import { DatePipe } from '@angular/common';
//import { ClientInfo } from '../../../../models/client-info';

//import { Utility } from '../../../common/utility';
@Component({
  selector: 'app-agreement',
  templateUrl: './agreement.component.html',
  styleUrls: ['./agreement.component.css'],
  providers: [ValidationHandler,DatePipe]
})
export class AgreementComponent implements OnInit {
  clinicAgreement: ContractData;
  currentUser = [];
  userRecords = [];
  immunizationInformation = [];
  clinicInformation = [];
  clientInformation: Client;
  walGreensInfo: WalgreenCo;
  printAgreement: boolean = false;
  agreementEmails: string = '';
  dialogSummary: string;
  dialogMsg: string;
  display: boolean = false;
  enlanguageSelection: boolean;
  splanguageSelection: boolean;
  englishLanguage: string;
  enheader: string;
  enattachment: string;
  spanishLanguage: string;
  headerText: string;
  attachment: string;
  imz: string;
  payment: string;
  rates: string;
  language: string;
  success_message: boolean = false;
  today:Date = new Date();
  currentLanguage: string;
  @Output() clinicSubmit: EventEmitter<number> = new EventEmitter();
  @ViewChild(ContractComponent) cmp: ContractComponent;

  /////////////////////////////////////////// LANGUAGE Variables ///////////////////////
  
  // language variables
  inWitnessWhereOff:string ='';// "IN WITNESS WHEREOF,"

  inWitnessWhereOff2ndLine:string = '';// "Client and Walgreens have electronically executed this Agreement, as of the Effective Date.";
  
  wallGreensCommOffsiteTitle1:string = '';//"WALGREENS COMMUNITY OFF-SITE CLINIC AGREEMENT";

  wallGreensCommOffsiteTitle2:string = '';//"TERMS AND CONDITIONS";

  wallGreensResponsibilities:string = '';//"WALGREENS’ RESPONSIBILITIES";

  wallGreensResponsibilitiesPara1Heading:string = '';//"Covered Services."

  sendLegalNoticeToClient:string ='';//"Send Legal Notice To Client At:"

  sendLegalNoticeToWalgreens:string ='';//"Send Legal Notice To Walgreens At:"

 wallGreensResponsibilitiesCvrdServices:string ='';// "Subject to the limitations or restrictions imposed by federal and state contracts, laws, and regulations, and the availability of the appropriate Immunization, Walgreens will provide the Covered Services to Participants. With respect to such Covered Services, the Parties will comply with the procedures set forth herein. When required by state law, Walgreens will require Participants to provide a valid prescription from their physician or allow the health care professional to contact their physician to obtain a valid prescription; however, for influenza Immunizations, Walgreens will be responsible for obtaining standing orders from physicians. Participants will be required to complete a Walgreens’ vaccine administration record and consent form before receiving an Immunization.";
 
 profJudgementHeading:string='';//"Professional Judgement.";

 profJudgementText:string='';//"Walgreens may withhold Covered Services to a Participant for good cause, including but not necessarily limited to, Client’s or Participant’s (where applicable) failure to pay for Covered Services rendered; requests by Participant for services inconsistent with the legal and regulatory requirements; or where, in the professional judgment of the health care professional, the services should not be rendered.";

 provHealthCrProfHdng:string='';//"Provision of Healthcare Professional.";

 provHealthCrProfText:string='';//" If the Parties agree in writing that Walgreens will provide Covered Services at off-site locations, Walgreens will provide Client with the appropriate number of qualified health care professionals and technicians to provide Covered Services at such off-site locations. Any requests for additional personnel will be subject to mutual agreement by the Parties and may require additional agreed-upon fees to be paid by Client to Walgreens in accordance with this Agreement.";
 
 clientsResponsibilites:string='';//"CLIENT’S RESPONSIBILITIES";

 vouchersHeading:string='';//"Vouchers.";

 vouchersText:string='';//" If the Parties agree in writing that Walgreens will provide Covered Services upon receipt of a voucher, Client will provide Participants with a voucher (in a format agreeable to both Parties), which Participants may redeem at a participating Walgreens store location. Once the voucher is approved by both Parties it may not be modified. Client may not rescind, retract, reduce or deny payment owed to Walgreens for claims where Covered Services have been provided to its Participants, even if Client no longer considers the individual redeeming the voucher to be a Participant.";

 offSiteLocation:string='';//"Off-Site Locations.";

 important:string='';//"IMPORTANT";

 offSiteLocationText1:string='';//" If the Parties agree in writing, that Walgreens will provide Covered Services at off-site locations, Client will provide Participants with notice of the dates, times and locations for such off-site locations and provide a private, clean room location, tables and chairs for Walgreens’ personnel and Participants. Additionally, Client guarantees that an average minimum of 25 Immunizations will be administered to Participants at each of Client’s off-site locations per contract year (“";

 siteMin:string='';//"Site Minimum";

 siteAvg:string='';//"Site Average";

 siteMinText:string='';//"”). If Walgreens determines that the Site Minimum is not achieved for the contract year (determined by taking the total number of Immunizations administered at all off-site locations divided by the number of off-site locations in such contract year (“";

 siteAvgText:string='';//"”)), at Walgreens’ discretion, it will invoice Client for the difference between the Site Minimum and Site Average multiplied by the number of off-site events. The sum of which will be multiplied by the lowest reimbursement rate set forth in table in Attachment A and Client shall pay such amount within 30 days of being invoiced by Walgreens.";

 pmtAndBIlling:string='';//"PAYMENT AND BILLING";

 paymentHd:string='';//"Payment.";

 paymentText:string='';//" For Covered Services where: (i) Participant provides evidence of coverage under third-party insurance or a government funded program (e.g., Medicare) prior to the provision of Covered Services;(ii) and Walgreens is contracted such third-party insurance or government funded program, Walgreens will submit the claim for that Participant and any copayment, coinsurance, deductible owed by the Participant will be collected at the time of service or billed at a later date. If such evidence is not provided at the time of service, Walgreens will either, as agreed to by the Parties, collect from the Participant or invoice Client monthly at the lesser of the prices stated herein or the Usual and Customary Charge. If a claim for reimbursement is later denied, the Parties agree that Walgreens can seek reimbursement from the Participant. As used in this Agreement, “Usual and Customary Charge” shall refer to the amount charged to a cash customer for an Immunization by the administering pharmacy at the time of administration, exclusive of: (i) sales tax; (ii) discounts claimed, and (iii) discounts provided for prescription drug savings card or other similar discounts. Client will reimburse Walgreens within 30 days from receipt of the monthly invoice and must be sent to the remittance address stated on the invoice. The invoice will contain the following data elements, and no further information will be provided: Group ID, store number, prescription number, patient name, recipient number, physician name, cost, service fee, copayment amount, sales tax, total charge, date of service, and drug name/NDC. At the time of payment, Client will provide Walgreens with a written explanation of the specific claims for which payment is made.";

 latePayment:string='';//"Late Payment.";

 latePaymentText:string='';//" All sums owed by Client to Walgreens will bear interest of 1.5% per month from the date payment is due until paid; however, in no event will such interest rate be greater than the rate permitted by law. Client shall be solely responsible for any and all costs incurred by Walgreens in seeking collection of any delinquent amounts owed by Client. Walgreens may invoice Client for interest and costs due under this Section on a monthly basis and payment will be due within 30 days from receipt.";

 termAndTermCaps:string='';//"TERM AND TERMINATION";

 termAndTerm:string='';//"Term and Termination.";
 
 termAndTermText:string='';//" This Agreement will become effective on the Effective Date and shall continue in full force and effect for an initial term of one year. Upon expiration of the initial term, this Agreement will automatically renew for successive one-year terms. Either Party may terminate this Agreement at any time without cause by giving at least thirty (30) days’ prior written notice to the other Party.";

 effectAndTerminiation:string='';//"Effect of Termination."

 effectAndTerminiationText:string='';//" Termination will have no effect upon the rights or obligations of the Parties arising out of any transactions occurring prior to the effective date of such termination.";

 waiver:string='';//"Waiver.";

 waiverText:string='';//" No waiver by either Party with respect to any breach or default of any right or remedy and no course of dealing may be deemed to constitute a continuous waiver of any other breach or default or of any other right or remedy unless such waiver is expressed in writing by the Party to be bound.";

 insurAndIndemnify:string='';//"INSURANCE AND INDEMNIFICATION ";

 insurance:string='';//"Insurance.";

 insuranceText:string='';//" Each Party will self-insure or maintain at its sole expense, and in amounts consistent with industry standards, such insurance as may be necessary to insure each respective Party, its employees, and agents against any claim or claims for damages arising out of or in connection with its duties and obligations under this Agreement. Walgreens will automatically name Client as Additional Insured under its applicable insurance policy(ies). Evidence of such insurance can be downloaded from Walgreens’ website. Client will provide a memorandum or certificate of insurance coverage to Walgreens upon request.";

 indemnification:string='';//"Indemnification."

 indemnificationText:string='';//" To the extent permitted by law, each Party will indemnify, defend, and hold harmless the other Party, including its employees and agents, from and against any and all third-party claims or liabilities arising from the negligence or wrongful act of the indemnifying Party, its employees, or agents in carrying out its duties and obligations under the terms of this Agreement. This Section will survive the termination of this Agreement.";

 generalTerms:string ='';//" GENERAL TERMS ";

 confidentiality:string='';//"Confidentiality of PHI."

 confidentialityP1:string='';//" Both Parties warrant that they will maintain and protect the confidentiality of all individually identifiable health information specifically relating to Participants (“";

 phi:string = '';//"Protected Health Information”"

 phiAb:string='';//"“PHI”"

 confidentialityP2:string='';//") in accordance with the Health Insurance Portability and Accountability Act of 1996 and all applicable federal and state laws and regulations. However, nothing herein will limit either Party’s use of any aggregated Participant information that does not contain PHI. This Section will survive the termination of this Agreement.";

 Advertising:string ='';//"Advertising.";

 AdvertisingText:string ='';//" Neither Party may advertise or use any trademarks, service marks, or symbols of the other Party without first receiving the written consent of the Party owning the mark and/or symbol with the following exceptions: Client may use the name and the addresses of Walgreens' locations in materials to inform Participants that Walgreens provides Covered Services. Any other reference to Walgreens in any Client materials must be pre-approved, in writing, by Walgreens.";
 
 frcMaj:string='';//"Force Majeure."

 frcMajText:string='';//" The performance by either Party hereunder will be excused to the extent of circumstances beyond such Party’s reasonable control, such as flood, tornado, earthquake, or other natural disaster, epidemic, war, material destruction of facilities, fire, acts of terrorism, acts of God, etc. In such event, the Parties will use their best efforts to resume performance as soon as reasonably possible under the circumstances."

 compliance:string='';//"Compliance.";

 complianceText:string='';//" The Parties will comply with all applicable laws, rules, and regulations for each jurisdiction in which Covered Services are provided under this Agreement. Each Party will cooperate with reasonable requests by the other Party for information that is needed for its compliance with applicable laws, rules, and/or regulations.";

 assignment:string='';//"Assignment."

 assignmentText:string='';//" Neither Party may assign this Agreement to a third-party without the prior written consent of the other Party, except that either Party will have the right to assign this Agreement to any direct or indirect parent, subsidiary or affiliated company or to a successor company without such consent. Any permitted assignee will assume all obligations of its assignor under this Agreement. No assignment will relieve any Party of responsibility for the performance of any obligations which have already occurred. This Agreement will inure to the benefit of and be binding upon each Party, its respective successors and permitted assignees."

 notices:string='';//"Notices.";

 noticesText:string='';//" All notices provided for herein must be in writing, sent by U.S. certified mail, return receipt requested, postage prepaid, or by overnight delivery service providing proof of receipt to the address set forth following the signature blocks. Notices will be deemed delivered upon receipt or upon refusal to accept delivery.";
 
 entireAgr:string='';//"Entire Agreement.";

 entireAgrText:string='';//" This Agreement, which includes any and all attachments, exhibits, riders, and other documents referenced herein, constitutes the entire and full agreement between the Parties relating to the subject matter herein and supersedes any previous contract, for which the signatories are authorized to sign for, and no changes, amendments, or alterations will be effective unless reduced to a writing signed by a representative of each Party. Any prior agreements, documents, understandings, or representations relating to the subject matter of this Agreement not expressly set forth herein or referred to or incorporated herein by reference are of no force or effect.";

 rights:string = '';//"All rights reserved.";

 businessName:string ='';//"Business Name:";
 name:string = "";//"Name:";
 title:string = '';//"Title:";
 attentionTo: string;
 date:string = '';//"Date:";
 district:string = '';//"District#:"
 attentionToTxt:string = '';//"Attention To:"
 address1:string = '';//"Address1:";
 address2:string = '';//"Address2:";
 city:string = '';//"City:"
 state:string = '';//"State:";
 zip:string = '';//"Zip:"
 approveAgrmnt:string = '';//"Approve Agreement";
 approveText:string = '';//'Carefully review the Community Off-Site Agreement. If you agree to the conditions of the contract, please check "Approve" below and type your name into the Electronic Signature field. If there are any discrepancies in the Agreement, reject the Agreement and provide corrections in the notes field.';
 electrSign:string = '';//"Electronic Signature";
 notes:string = '';//"Notes:";
 pageTitle:string = '';//"COMMUNITY OFF-SITE CLINIC AGREEMENT";
 approve:string = '';//"Approve";
 reject:string = '';//"Reject";
 submit:string = '';//"Submit";
 printAgreementTxt:string = '';//"Print Agreement";
 clinicLocation:string= '';//"Clinic Location:";
 clinicDate:string = '';//"Clinic Date:";
 clinicTime:string = '';//"Clinic Time:";
 contact:string = '';//"Contact:";
 phone:string = '';//"Phone:";
 estShots:string =''// Est.Shots
 location:string = '';
 time:string = ''; 
 email:string = '';
 emailAgrmntTo= '';
 multipleEmailAddrss = '';
 sendAgreeMnt = '';
 editAgreeMnt = '';
 saveAgreeMnt = '';
 cancelTxt = '';
 noteTxt = '';
 table1 = '';
 fluExpiryDate = '';
 routineExpiryDate = '';
 sendInvoiceTo= '';
 isTaxExempt = ''
 isCopay= '';
 voucherNeeded = ''
 voucherExpiry = ''
 copayValue = '';
  //////////////////////////////////////////

  constructor(private _apiservice: OutreachProgramService, private router: Router, 
    private _validationHandler: ValidationHandler,
    private _alertService: AlertService, private utility: Util,
    private _datePipe:DatePipe) {
    this.success_message = false;
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.printAgreement = false;

    //  this._apiservice.getContractAgreement().subscribe((data) => {
    //    this.clinicAgreement = data;
    //    this.userRecords = data.clinicImmunizationList;
    // });
    this.clinicAgreement = this._apiservice.fetchContractAgreementData();
    this.userRecords = this.clinicAgreement.clinicImmunizationList;

    this.GetClinicInformation();
    this.GetImmunizationformation();
    this.GetClientInformation();
    this.GetWalgreensCOformation();
    this.enlanguageSelection = true;
    this.splanguageSelection = false;
    if(this.splanguageSelection) {
      this.languageChangeToSpanish();
    } else {
      this.languageChangeToEnglish();
    }
  }
  public GetClinicInformation() {
    this.clinicInformation = this.clinicAgreement.clinicList;
  }

  public GetImmunizationformation() {
    for (let i = 0; i < this.clinicAgreement.clinicList.length; i++) {
      this.immunizationInformation = this.clinicAgreement.clinicList[i].clinicImzQtyList;
    }
  }

  public GetClientInformation() {
    this.clientInformation = this.clinicAgreement.clientInfo != null ? this.clinicAgreement.clientInfo : new Client();
  }

  public GetWalgreensCOformation() {
    this.walGreensInfo = this.clinicAgreement.walgreenCoInfo != null ? this.clinicAgreement.walgreenCoInfo : new WalgreenCo();
  }

  generateArray(obj) {
    return Object.keys(obj).map((key) => { return obj[key] });
  }
  btnClick() {
    this.clinicSubmit.emit(1);
  };

  saveAgreement() {
    this.clinicAgreement.contractPostedByUser = 'Walgreens User';
    this.clinicAgreement.isEmailSent = 0;
    this.clinicAgreement.contactWagUser = '';
    this.clinicAgreement.isApproved = 0;
    this.clinicAgreement.businessUserEmails = this.agreementEmails;
    this.makeValidBooleans();
    this._apiservice.saveAgreementData(SessionDetails.GetStoreId(), 1, this.clinicAgreement).subscribe((data: any) => {

      switch (data.errorS) {
        // case 'success':
        // case 200:
        case null:
        case "null":
          this.showDialog(ErrorMessages['contract'], ErrorMessages['contractSaved'], true);
          break;
      }
    },
      err => {
        switch (err) {
          case 500:
            this.showDialog(ErrorMessages['unKnownError'], err.json().Message, false);
            break;
          default:
            this.showDialog(ErrorMessages['unKnownError'], err.json().Message, false);
            break;
        }
      });
  }

  sendEmail() {
    if (!this.utility.validateEmail(this.agreementEmails)) {
      let sumMsg = ErrorMessages['errMsg'];
      let sMsg = ErrorMessages['emailList'];
      this.showDialog(sumMsg, sMsg, false);
      return;
    }

    this.clinicAgreement.isEmailSent = 1;
    this.clinicAgreement.isApproved = 0;
    this.clinicAgreement.businessUserEmails = this.agreementEmails;
    this.clinicAgreement.contractPostedByUser = 'Walgreens User'
    this.clinicAgreement.contactWagUser = '';
    this.makeValidBooleans();
    this._apiservice.saveAgreementData(SessionDetails.GetStoreId(), 1, this.clinicAgreement).subscribe((data: any) => {
      if (data.errorS != null) {
        if (data.errorS.errorCode == -4) {
          this.showDialog(ErrorMessages['contractAlert'], data.errorS.errorMessage, false);
        }
      }
      switch (data.errorS) {
        case null:
        case "null":
          this.showDialog(ErrorMessages['contract'], ErrorMessages['contractEmailSent'], true);
          break; 
      }
    },
      err => {
        switch (err) {
          case 500:
            this.showDialog(ErrorMessages['unKnownError'], err.json().Message, false);
            break;
          default:
            this.showDialog(ErrorMessages['unKnownError'], err.json().Message, false);
            break;
        }
      });
  }
  makeValidBooleans(){
    this.clinicAgreement.clinicImmunizationList.forEach(im=>{
      if(im.sendInvoiceTo<=0){
        im.isTaxExempt= null;
        im.isCopay= null;
        im.isVoucherNeeded=null;
      } else {
        im.isTaxExempt<=0 ? im.isTaxExempt=1:im.isTaxExempt=0;
        im.isCopay>=1? im.isCopay=1:im.isCopay=0;
        im.isVoucherNeeded>=1? im.isVoucherNeeded=1:im.isVoucherNeeded=0;
        im.copayValue = +im.copayValue;
      } 
    });
    this.clinicAgreement.clinicList.forEach(cl=>{
      cl.isNoClinic>=1 ? cl.isNoClinic=1:cl.isNoClinic=0;
      cl.isReassign>=1? cl.isReassign=1:cl.isReassign=0;
      cl.isCurrent >=1? cl.isCurrent=1:cl.isCurrent=0;
    });
  }
  print() {
    this.printAgreement = true;

    let printContents, popupWin;

    var builder = new StringBuilder('<div class="container"><div class="row">');
    builder.Append(document.getElementById('approveContract').innerHTML);
    builder.Append('</div></div>');
    printContents = builder.ToString();
    //printContents = this.preparePrintHtml();
    popupWin = window.open('', '', 'top=0,left=0,height=95%,width=600');//location=1, status=1,scrollbars=1
    popupWin.document.open();
    popupWin.document.write(`
        <html>
          <head>
            <title>Wallgreens Contract</title>
  
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
  
            <!-- CORE CSS -->
            <link rel="stylesheet" href="https://unpkg.com/primeng@2.0.1/resources/themes/omega/theme.css" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
            <link rel="stylesheet" href="https://unpkg.com/primeng@2.0.1/resources/primeng.min.css" />
            <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
          
            <!-- THEME CSS -->
            <link href="assets/css/essentials.css" rel="stylesheet" type="text/css" />
            <link href="assets/css/layout.css" rel="stylesheet" type="text/css" />
            <link href="https://ng-forms-fields-validation.firebaseapp.com/validateSubmit" rel="stylesheet" type="text/css"/>
          
            <!-- PAGE LEVEL SCRIPTS -->
            <link href="assets/css/header-1.css" rel="stylesheet" type="text/css" />
            <link href="assets/css/color_scheme/darkblue.css" rel="stylesheet" type="text/css" id="color_scheme" />
            <link href="assets/css/media.css" rel="stylesheet" type="text/css" />
            <style>
            element.style {
              height: 135px;
            }
           
            .table td, .table th {
              border-top: 0px solid #e9ecef;
          }
            </style>
          </head>
      <body onload="window.print();window.close()">${printContents}</body>
   
        </html>`
    );
    popupWin.document.close();

  }
  cancelAgreementClicked() {
    this.router.navigate(['./communityoutreach/storehome']);
  }

  showDialog(msgSummary: string, msgDetail: string, success_message: boolean) {
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
    this.display = true;
    this.success_message = success_message;
  }
  okClicked() {
    this.display = false;
    if (this.success_message) {
      this.router.navigateByUrl('/communityoutreach/storehome');
    }    
  }

  languageChangeToSpanish() {
    this.enlanguageSelection = false;
    this.splanguageSelection = true;
    this._apiservice.languageChange().subscribe((data) => {
      this.spanishLanguage = data.spanish;
      this.headerText = data.header;
      this.attachment = data.attachmentheader;
      this.imz = data.imzsp;
      this.payment = data.paymentsp;
      this.rates = data.ratessp;

      this.businessName = data.clientsp;
      this.name = data.namesp;
      this.date = data.dateSP;
      this.district = data.districtSP;

      this.title = data.titlesp;
      this.attentionTo = data.attentionsp;
      this.address1 = data.address1sp;
      this.address2 = data.address2sp;
      this.city = data.citysp;
      this.state = data.statesp;
      this.zip = data.zipcodesp;
      //this.legal_notice = data.legalsp;
      //this.client_info = data.clientinfosp;
      this.approveAgrmnt = data.approveAgrmntSP;
      this.approveText = data.approveTextSP;
      this.electrSign = data.electrSignSP;
      this.notes = data.notesSP;
      this.pageTitle = data.pageTitleSP;

      this.inWitnessWhereOff= data.inWitnessWhereOffSP;
      this.inWitnessWhereOff2ndLine = data.inWitnessWhereOff2ndLineSP;
      this.sendLegalNoticeToClient = data.sendLegalNoticeToClientSP;
      this.sendLegalNoticeToWalgreens = data.sendLegalNoticeToWalgreensSP;
      this.wallGreensCommOffsiteTitle1 = data.wallGreensCommOffsiteTitle1SP;
      this.wallGreensCommOffsiteTitle2 = data.wallGreensCommOffsiteTitle2SP;
      this.wallGreensResponsibilities = data.wallGreensResponsibilitiesSP;
      this.wallGreensResponsibilitiesPara1Heading = data.wallGreensResponsibilitiesPara1HeadingSP;
      this.wallGreensResponsibilitiesCvrdServices = data.wallGreensResponsibilitiesCvrdServicesSP;
      this.wallGreensResponsibilitiesCvrdServices = data.wallGreensResponsibilitiesCvrdServicesSP;
      this.profJudgementHeading = data.profJudgementHeadingSP;
      this.profJudgementText = data.profJudgementTextSP;
      this.provHealthCrProfHdng = data.provHealthCrProfHdngSP;
      this.provHealthCrProfText = data.provHealthCrProfTextSP;
      this.clientsResponsibilites = data.clientsResponsibilitesSP;
      this.vouchersHeading = data.vouchersHeadingSP;
      this.vouchersText = data.vouchersTextSP;
      this.offSiteLocation = data.offSiteLocationSP;
      this.important = data.importantSP;
      this.offSiteLocationText1 = data.offSiteLocationText1SP;
      this.siteMin = data.siteMinSP;
      this.siteAvg = data.siteAvgSP;
      this.siteMinText = data.siteMinTextSP;
      this.siteAvgText = data.siteAvgTextSP;
      this.pmtAndBIlling = data.pmtAndBIllingSP;
      this.paymentHd =data.paymentSP;
      this.paymentText = data.paymentTextSP;
      this.latePayment = data.latePaymentSP;
      this.latePaymentText = data.latePaymentTextSP;
      this.termAndTermCaps = data.termAndTermCapsSP;
      this.termAndTerm = data.termAndTermSP;
      this.termAndTermText = data.termAndTermTextSP;
      this.effectAndTerminiation = data.effectAndTerminiationSP;
      this.effectAndTerminiationText = data.effectAndTerminiationTextSP;
      this.waiver = data.waiverSP;
      this.waiverText = data.waiverTextSP;
      this.insurAndIndemnify = data.insurAndIndemnifySP;
      this.insurance = data.insuranceSP;
      this.insuranceText = data.insuranceTextSP;
      this.indemnification = data.indemnificationSP;
      this.indemnificationText = data.indemnificationTextSP;
      this.generalTerms = data.generalTermsSP;
      this.confidentiality = data.confidentialitySP;
      this.confidentialityP1 = data.confidentialityP1SP;
      this.phi = data.phiSP;
      this.phiAb = data.phiAbSP;
      this.confidentialityP2 = data.confidentialityP2SP;
      this.Advertising = data.AdvertisingSP;
      this.AdvertisingText = data.AdvertisingTextSP;
      this. frcMaj = data.frcMajSP;
      this.frcMajText = data.frcMajTextSP;
      this.compliance = data.complianceSP;
      this.complianceText = data.complianceTextSP;
      this.assignment = data.assignmentSP;
      this.assignmentText = data.assignmentTextSP;
      this.notices = data.noticesSP;
      this.noticesText = data.noticesTextSP;
      this.entireAgr = data.entireAgrSP;
      this.entireAgrText = data.entireAgrTextSP;
      this.rights = data.rightsSP;
      this.approve = data.approveSP;
      this.reject = data.rejectSP;
      this.submit= data.submitSP;
      this.printAgreementTxt = data.printAgreementTxtCapsSP;

      this.clinicLocation= data.clinicLocationSP;
      this.clinicDate = data.clinicDateSP;
      this.clinicTime = data.clinicTimeSP;
      this.contact= data.contactSP;
      this.phone = data.phoneSP;
      this.estShots = data.estShotsSP;
      this.location = data.locationSP;
      this.time = data.timeSP;
      this.email = data.emailSP;
      this.emailAgrmntTo= data.emailAgreementToSP;
      this.multipleEmailAddrss = data.multipleEmailAddressSP;
      this.sendAgreeMnt = data.sendAgreementSP;
      this.editAgreeMnt = data.editAgreementSP;
      this.saveAgreeMnt = data.saveAgreementSP;
      this.cancelTxt = data.cancelSP;
      this.noteTxt = data.noteTextSP;
      this.table1 = data.table1SP;
      this.fluExpiryDate= data.fluExpiryDateSP;
      this.routineExpiryDate= data.routineExpiryDateSP;
      this.estShots = data.estShotsSP;
      this.sendInvoiceTo= data.sendInvoiceToSP;
      this.isTaxExempt = data.isTaxExemptSP;
      this.isCopay= data.isCopaySP;
      this.voucherNeeded = data.voucherNeededSP;
      this.voucherExpiry = data.voucherExpirySP;
      this.copayValue = data.copayValueSP;
    });
  }
  languageChangeToEnglish() {
    this.splanguageSelection = false;
    this.enlanguageSelection = true;

    this._apiservice.languageChange().subscribe((data) => {
      this.englishLanguage = data.english;
      this.enheader = data.englishheader;
      this.enattachment = data.englishattachment;
      this.imz = data.imzen;
      this.payment = data.payen;
      this.rates = data.ratesen;


      this.businessName = data.clienten;
      this.name = data.nameen;
      this.date = data.dateEN;
      this.district = data.districtEN;

      this.title = data.titleen;
      this.attentionTo = data.attentionen;
      this.address1 = data.address1en;
      this.address2 = data.address2en;
      this.city = data.cityen;
      this.state = data.stateen;
      this.zip = data.zipcodeen;
      //this.legal_notice = data.legalsp;
      //this.client_info = data.clientinfosp;
      this.approveAgrmnt = data.approveAgrmntEN;
      this.approveText = data.approveTextEN;
      this.electrSign = data.electrSignEN;
      this.notes = data.notesEN;
      this.pageTitle = data.pageTitleEN;

      this.inWitnessWhereOff= data.inWitnessWhereOffEN;
      this.inWitnessWhereOff2ndLine = data.inWitnessWhereOff2ndLineEN;
      this.sendLegalNoticeToClient = data.sendLegalNoticeToClientEN;
      this.sendLegalNoticeToWalgreens = data.sendLegalNoticeToWalgreensEN;
      this.wallGreensCommOffsiteTitle1 = data.wallGreensCommOffsiteTitle1EN
      this.wallGreensCommOffsiteTitle2 = data.wallGreensCommOffsiteTitle2EN;
      this.wallGreensResponsibilities = data.wallGreensResponsibilitiesEN;
      this.wallGreensResponsibilitiesPara1Heading = data.wallGreensResponsibilitiesPara1HeadingEN;
      this.wallGreensResponsibilitiesCvrdServices = data.wallGreensResponsibilitiesCvrdServicesEN;
      this.wallGreensResponsibilitiesCvrdServices = data.wallGreensResponsibilitiesCvrdServicesEN;
      this.profJudgementHeading = data.profJudgementHeadingEN;
      this.profJudgementText = data.profJudgementTextEN;
      this.provHealthCrProfHdng = data.provHealthCrProfHdngEN;
      this.provHealthCrProfText = data.provHealthCrProfTextEN;
      this.clientsResponsibilites = data.clientsResponsibilitesEN;
      this.vouchersHeading = data.vouchersHeadingEN;
      this.vouchersText = data.vouchersTextEN;
      this.offSiteLocation = data.offSiteLocationEN;
      this.important = data.importantEN;
      this.offSiteLocationText1 = data.offSiteLocationText1EN;
      this.siteMin = data.siteMinEN;
      this.siteAvg = data.siteAvgEN;
      this.siteMinText = data.siteMinTextEN;
      this.siteAvgText = data.siteAvgTextEN;
      this.pmtAndBIlling = data.pmtAndBIllingEN;
      this.paymentHd =data.paymentEN;
      this.paymentText = data.paymentTextEN;
      this.latePayment = data.latePaymentEN;
      this.latePaymentText = data.latePaymentTextEN;
      this.termAndTermCaps = data.termAndTermCapsEN;
      this.termAndTerm = data.termAndTermEN;
      this.termAndTermText = data.termAndTermTextEN;
      this.effectAndTerminiation = data.effectAndTerminiationEN;
      this.effectAndTerminiationText = data.effectAndTerminiationTextEN;
      this.waiver = data.waiverEN;
      this.waiverText = data.waiverTextEN;
      this.insurAndIndemnify = data.insurAndIndemnifyEN;
      this.insurance = data.insuranceEN;
      this.insuranceText = data.insuranceTextEN;
      this.indemnification = data.indemnificationEN;
      this.indemnificationText = data.indemnificationTextEN;
      this.generalTerms = data.generalTermsEN;
      this.confidentiality = data.confidentialityEN;
      this.confidentialityP1 = data.confidentialityP1EN;
      this.phi = data.phiEN;
      this.phiAb = data.phiAbEN;
      this.confidentialityP2 = data.confidentialityP2EN;
      this.Advertising = data.AdvertisingEN;
      this.AdvertisingText = data.AdvertisingTextEN;
      this. frcMaj = data.frcMajEN;
      this.frcMajText = data.frcMajTextEN;
      this.compliance = data.complianceEN;
      this.complianceText = data.complianceTextEN;
      this.assignment = data.assignmentEN;
      this.assignmentText = data.assignmentTextEN;
      this.notices = data.noticesEN;
      this.noticesText = data.noticesTextEN;
      this.entireAgr = data.entireAgrEN;
      this.entireAgrText = data.entireAgrTextEN;
      this.rights = data.rightsEN;

      this.approve = data.approveEN;
      this.reject = data.rejectEN;
      this.submit= data.submitEN;
      this.printAgreementTxt = data.printAgreementTxtCapsEN;

      this.clinicLocation= data.clinicLocationEN;
      this.clinicDate = data.clinicDateEN;
      this.clinicTime = data.clinicTimeEN;
      this.contact= data.contactEN;
      this.phone = data.phoneEN;
      this.estShots = data.estShotsEN;
      this.location = data.locationEN;
      this.time = data.timeEN;
      this.email = data.emailEN;
      this.emailAgrmntTo= data.emailAgreementToEN;
      this.multipleEmailAddrss = data.multipleEmailAddressEN;
      this.sendAgreeMnt = data.sendAgreementEN;
      this.editAgreeMnt = data.editAgreementEN;
      this.saveAgreeMnt = data.saveAgreementEN;
      this.cancelTxt = data.cancelEN;
      this.noteTxt = data.noteTextEN;
      this.table1 = data.table1EN;
      this.fluExpiryDate= data.fluExpiryDateEN;
      this.routineExpiryDate= data.routineExpiryDateEN;
      this.estShots = data.estShotsEN;
      this.sendInvoiceTo= data.sendInvoiceToEN;
      this.isTaxExempt = data.isTaxExemptEN;
      this.isCopay= data.isCopayEN;
      this.voucherNeeded = data.voucherNeededEN;
      this.voucherExpiry = data.voucherExpiryEN;
      this.copayValue = data.copayValueEN;
    });

    this.language = "TRANSLATE TO ENGLISH";
  }

changeLanguageToSpanish(){
    this.enlanguageSelection = false;
    this.splanguageSelection = true;
    this.languageChangeToSpanish();
   }

   changeLanguageToEnglish(){
    this.enlanguageSelection = true;
    this.splanguageSelection = false;
    this.languageChangeToEnglish();
   }

   getPhoneNoInUSFormat(phone:string):string{
    return Util.telephoneNumberFormat(phone);
  }
  checkIfAnyImmunizationsHasCorpInvoiceAndVoucherNeeded(location: Clinic): boolean {
    return this._apiservice.checkIfAnyImmunizationsHasCorpInvoiceAndVoucherNeeded(location) && location.isNoClinic >=1;
  }
  checkIfFluImmForCorpInvoiceSelected():boolean{
    return this._apiservice.checkIfFluImmForCorpInvoiceSelected();
  }
  checkIfNonFluImmForCorpInvoiceSelected():boolean{
    return this._apiservice.checkIfNonFluImmForCorpInvoiceSelected();
  }
  getVoucherExpiryDate(clinicLoc:Clinic):string{
    let vchrDate:Date;
    this.userRecords.forEach(imz=>{
      if(imz.isVoucherNeeded >=1 && imz.paymentTypeId == 6 && (imz.sendInvoiceTo === '1' || imz.sendInvoiceTo === 1) && imz.immunizationName.toLowerCase().search('flu') !== -1)
      vchrDate=new Date(imz.voucherExpirationDate);
      return vchrDate;
    });
    return this._datePipe.transform(vchrDate,'MM/dd/yyyy');
  }
  getRoutineExpiryDate(clinicLoc:Clinic):string{
    let vchrDate:Date;
    this.userRecords.forEach(imz=>{
      if(imz.isVoucherNeeded >=1 && imz.paymentTypeId == 6 && (imz.sendInvoiceTo === '1' || imz.sendInvoiceTo === 1) && imz.immunizationName.toLowerCase().search('flu') == -1)
      vchrDate=new Date(imz.voucherExpirationDate);
      return vchrDate;
    });
    return this._datePipe.transform(vchrDate,'MM/dd/yyyy');
  }
}
