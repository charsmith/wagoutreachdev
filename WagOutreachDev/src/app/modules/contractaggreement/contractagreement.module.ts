import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ApproveagreementComponent } from './components/approveagreement/approveagreement.component';
import { BusinesscontactpageComponent } from './components/businesscontactpage/businesscontactpage.component';
import { ContractapproveService } from './services/contractapprove.service';
import { FieldErrorDisplayComponent } from '../common/components/field-error-display/field-error-display.component';
import { ReusableModule } from '../common/components/reusable.module';
//import { Utility } from '../common/utility';
import { ThankyouComponent } from './components/thankyou/thankyou.component';
import { InputMaskModule, AccordionModule, KeyFilterModule, FieldsetModule, DialogModule, DataTableModule, TabViewModule, DataListModule,ConfirmDialogModule, CalendarModule, GrowlModule, MessagesModule, MessageModule, ConfirmationService } from 'primeng/primeng';
import { ClientInfo } from '../../models/client-info';
import { Globalvariables } from '../../models/globalvariables';
import { ContractapproveComponent } from './components/contractapprove/contractapprove.component';
import { NgPipesModule } from 'ngx-pipes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OutreachProgramService } from './services/outreach-program.service';
import { WorkflowService } from './workflow/workflow.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ContractComponent } from './components/local-contracts/contract.component';
import { ImmunizationsComponent } from './components/immunizations/immunizations.component';
import { LocationsComponent } from './components/contract-locations/locations.component';
import { WitnessComponent } from './components/contract-pharmacy-info/witness.component';
import { AgreementComponent } from './components/contract-agreement/agreement.component';
import { LocationsListComponent } from './components/contract-locations-list/locations-list.component';
import { CharityLocationsComponent } from './components/charity-locations/charity-locations.component';
import { CharitylocationsListComponent } from './components/charity-locations-list/charity-locations-list.component';
import { CommunityoutreachComponent } from './components/communityoutreach/communityoutreach.component';
import { Util } from '../../utility/util';
import { ViewcontractAgreementComponent } from './components/viewcontract-agreement/viewcontract-agreement.component';
import { LegalNoticeAddress } from '../../models/contract';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ReusableModule,
    KeyFilterModule,
    ConfirmDialogModule,
    DialogModule,  
     CalendarModule,
     BrowserAnimationsModule,
     GrowlModule,
     MessagesModule,
     MessageModule,
     ConfirmDialogModule,
     DialogModule,
     KeyFilterModule,
     ReusableModule,
     NgPipesModule
  ],
  declarations: [ContractComponent, ImmunizationsComponent, LocationsComponent, WitnessComponent,
    AgreementComponent, LocationsListComponent,
    CharityLocationsComponent, CharitylocationsListComponent,  CommunityoutreachComponent,ContractapproveComponent,BusinesscontactpageComponent, ApproveagreementComponent, ThankyouComponent, ViewcontractAgreementComponent
    ],
  providers: [OutreachProgramService, WorkflowService, MessageService, ConfirmationService,ContractapproveService,ClientInfo,LegalNoticeAddress,Globalvariables,Util],
})
export class ContractagreementModule { }
