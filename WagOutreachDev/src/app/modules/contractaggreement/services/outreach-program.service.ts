import { Injectable } from '@angular/core';
import { WorkflowService } from '../workflow/workflow.service';
import { STEPS } from '../workflow/workflow.model';
import { immunizationsArray } from './../../../JSON/Immunizations';
import { contractData } from './../../../JSON/Contract';
import { Observable } from 'rxjs';
import { Headers, Http, Response } from '@angular/http';
import { ContractData } from '../../../models/contract';
import { Clinic } from '../../../models/contract';
import { localBusinessArray } from './../../../JSON/localbusiness';
import { OutReachProgramType } from '../../../models/OutReachPrograms';
import { environment } from '../../../../environments/environment';
import { SpanishData } from '../../../JSON/Spanish';
import { Util } from '../../../utility/util';
import {coClinics, LogOutreachStatus,ErrorS,CommunityOutreach} from '../../../models/community-outreach-v1';

@Injectable()
export class OutreachProgramService {

    private formData: ContractData = new ContractData();
    private contractData: ContractData = new ContractData();
    private isImmunizationsFormValid: boolean = false;
    private isLocationsFormValid: boolean = false;
    private isWitnessFormValid: boolean = false;
    private outReachType: string;
    immunizations: any[] = [];
    immunization_data: any;
    everGreenData: any;
    cancel_check:boolean = false;
    private coData:CommunityOutreach = new CommunityOutreach();
    private coFormData:CommunityOutreach = new CommunityOutreach();
    constructor(private workflowService: WorkflowService, private _http: Http) {

    }

    getImmunizations() {
        this.immunizations = immunizationsArray.map(x => Object.assign({}, x));;
        return (this.immunizations);
    }
    getImmunizationsList(agreement_pk, clinic_type) {
        let url = environment.API_URL + environment.RESOURCES.GET_IMMUNIZATION_LIST + "/" + agreement_pk + "/" + clinic_type;
        return this._http.get(url,Util.getRequestHeaders())
            .map((response: Response) => response.json());
    }
    getOutReachType(): string {
        return this.outReachType;
    }
    setOutReachType(outReachType: string) {
        this.outReachType = outReachType;
    }
    fetchDataFromDB() {
        this.getContractAgreementData(123934).subscribe((data) => {
            this.formData = data;
            if (this.formData.clinicList.length > 0) {
                this.immunization_data = this.formData.clinicImmunizationList;
            }
        });

    }
    getSelectedImmunizations(): any {
        if (this.formData !== undefined)
            return this.formData.clinicImmunizationList;
        else
            return [];
    }

    setSelectedImmunizations(data: any) {
        this.isImmunizationsFormValid = true;
        this.formData.clinicImmunizationList = data;
        this.workflowService.validateStep(STEPS.immunization);
    }
    getCancelSelectedImmunizations(): any {
        if (this.formData !== undefined)
            return this.formData.clinicImmunizationList;
        else
            return [];
    }

    setCancelSelectedImmunizations(data: any) {
        this.isImmunizationsFormValid = true;
        this.formData.clinicImmunizationList = data;
        this.workflowService.validateStep(STEPS.immunization);
    }
    getClinicLocationsForNewContract(): any {
        // if (Object.keys(contractData).length > 0) {
        //     this.formData = contractData;
        // }
        return this.formData;
    }
    getWalgreensWitnessData() {
        return this.formData.walgreenCoInfo[0];
    }
    setWalgreensWitnessData(data: any) {
        this.isWitnessFormValid = true;
        this.formData.walgreenCoInfo = data;
        this.workflowService.validateStep(STEPS.witness);
    }
    getClinicLocation(i): any {
        return this.formData.clinicList[i];
    }
    getCOClinicLocation(i): any {
        return this.coFormData.clinicList[i];
    }

    clearClinicLocations() {
        this.formData.clinicList = [];
    }
    setClinicLocations(data: any): boolean {
        this.isLocationsFormValid = true;
        this.formData.clinicList = data.locations;
        this.workflowService.validateStep(STEPS.location);
        return true;
    }

setCOClinicLocations(data: any): boolean {
        this.isLocationsFormValid = true;
        this.coFormData.clinicList = data.locations;
        this.workflowService.validateStep(STEPS.location);
        return true;
    }

    setCancelClinicLocations(data: any): boolean {
        this.isLocationsFormValid = true;
        this.formData.clinicList = data.locations;
        
        this.workflowService.validateStep(STEPS.location);
        return true;
    }
    getCancelClinicLocation(i): any {
        return this.formData.clinicList[i];
    }
    getContractAgreement() {
        // Return the entire Form Data
        return Observable.of(this.formData);
    }

    resetFormData() {
        // Reset the workflow
        this.workflowService.resetSteps();
        // Return the form data after all this.* members had been reset
        this.formData.clear();
        this.isImmunizationsFormValid = this.isLocationsFormValid = this.isWitnessFormValid = false;
        return this.formData;
    }

    isFormValid() {
        // Return true if all forms had been validated successfully; otherwise, return false
        return this.isImmunizationsFormValid &&
            this.isLocationsFormValid &&
            this.isWitnessFormValid;
    }

    setImmunizationsData(data: any) {
        this.immunization_data = data;
    }

    setEverGreenData(data: any) {
        this.everGreenData = data;

    }
    SetIscanceled(is_cancel)
    {
      this.cancel_check = is_cancel;
    }
    getIsCancel():boolean
    {
      return this.cancel_check;
    }
    getEverGreenData() {
        if (this.everGreenData !== undefined) {
            return this.everGreenData;
        }
        else {
            return '';
        }
    }
    getImmunizationsData() {
        if (this.immunization_data !== undefined) {
            return this.immunization_data;
        }
        else {
            return [];
        }
    }
    clearSelectedImmunizations() {
        if (this.immunization_data !== undefined) {
            this.immunization_data = [];
        }
        this.immunization_data = [];

    }
    getAllClinicData() {
        return localBusinessArray;
    }
    getClinicDetails(id: any/*todo*/): any {
        let contract_data: any[];
        contract_data = this.getAllClinicData();
        var clinic = new Clinic();
        clinic.contactFirstName = contract_data[0].firstName;
        clinic.contactLastName = contract_data[0].lastName;
        clinic.contactPhone = contract_data[0].localContactPhone;
        clinic.contactEmail = contract_data[0].localContactEmail;
        clinic.address1 = contract_data[0].address;
        clinic.address2 = contract_data[0].address2;
        clinic.state = contract_data[0].state;
        clinic.zipCode = contract_data[0].zipCode;

        return clinic;
    }
    saveClinicLocations(values: any): boolean {
        this.coData.clinicList = this.coFormData.clinicList;

        return true;
    }
    saveCommunityOutreachData(): boolean {
        //todo construct the json from immunization and contract form;
        this.coData.clinicImmunizationList = this.immunization_data
        this.coData.clinicList = this.coFormData.clinicList;
        return true;
    }

    getCommunityOutreachData(){
        return this.coData;
    }

    saveInterimCommunityOutreachData(step: number): boolean {
        if (step == 1) {
            this.contractData.clinicImmunizationList = this.immunization_data;
            return true;
        }
        else if (step == 2) {
            this.contractData.clinicImmunizationList = this.immunization_data;
            this.contractData.clinicList = this.formData.clinicList;
            return true;
        }

        return false;
    }
    //Spanish Data
    // languageChange() {
    //     return this._http.get('/assets/showcase/data/Spanish.json').map((response: Response) => response.json());
    // }
    languageChange(): Observable<any> {
        return Observable.of(SpanishData);
    }
    getContractAgreementData(clinic_agreement_pk) {
        if (this.immunization_data.length <= 0) {
            let url = environment.API_URL + environment.RESOURCES.GET_CONTRACT_AGREEMENT + "/" + clinic_agreement_pk;
            return this._http.get(url,Util.getRequestHeaders()).map((response: Response) => this.formData = response.json());
        } else {
            return Observable.of(this.formData);
        }

    }

    fetchContractAgreementData() {
        return this.formData;
    }

    saveAgreementData(store_id, clinic_type, clinic_agreement) {

        let url = environment.API_URL + environment.RESOURCES.UPDATE_CONTRACT_AGREEMENT + "/" + store_id + "/" + clinic_type;
        return this._http.post(url, clinic_agreement,Util.getRequestHeaders()).map((response: Response) => response.json());
    }

    saveCODataToDB(store_id, clinic_type, coData) {

        let url = environment.API_URL + environment.RESOURCES.UPDATE_CO_AGREEMENT + "/" + store_id + "/" + clinic_type;
        return this._http.post(url, coData,Util.getRequestHeaders()).map((response: Response) => response.json());
    }
    
    saveInterimAgreementData(store_id, clinic_type) {
        this.makeValidBooleans();
        let url = environment.API_URL + environment.RESOURCES.UPDATE_CONTRACT_AGREEMENT + "/" + store_id + "/" + clinic_type;
        return this._http.post(url, this.formData,Util.getRequestHeaders()).map((response: Response) => response.json())
            .catch((err) => {
                return Observable.throw(err)
            });
    }
    makeValidBooleans(){
        this.formData.clinicImmunizationList.forEach(im=>{
            if(im.sendInvoiceTo<=0){
              im.isTaxExempt= null;
              im.isCopay= null;
              im.isVoucherNeeded=null;
            } else {
              im.isTaxExempt<=0 ? im.isTaxExempt=1:im.isTaxExempt=0;
              im.isCopay>=1? im.isCopay=1:im.isCopay=0;
              im.isVoucherNeeded>=1? im.isVoucherNeeded=1:im.isVoucherNeeded=0;
              im.copayValue = +im.copayValue;
            } 
          });
        this.formData.clinicList.forEach(cl=>{
          cl.isNoClinic>=1 ? cl.isNoClinic=1:cl.isNoClinic=0;
          cl.isReassign>=1? cl.isReassign=1:cl.isReassign=0;
          cl.isCurrent >=1? cl.isCurrent=1:cl.isCurrent=0;
        });
      }
    getStoreBusinessDetails(businessId: any) {
        let url = environment.API_URL + environment.RESOURCES.GET_OPPURTUNITY_DETAILS + "/" + businessId;
        return this._http.get(url,Util.getRequestHeaders())
            .map((response: Response) => response.json());
    }
    getpreviousseasoncliniclocations(clinic_agreement_pk: any,business_pk:any) {
        let url = environment.API_URL + environment.RESOURCES.GET_PREVIOUSSEASON_CLINIC_LOCATIONS + "/" + clinic_agreement_pk + "/" + business_pk;
        return this._http.get(url,Util.getRequestHeaders())
            .map((response: Response) => response.json());
    }

    checkIfAnyImmunizationsHasCorpInvoiceAndVoucherNeeded(location: Clinic): boolean {
        let added_immunizations = this.getSelectedImmunizations();
        try {
            let hasCorpInvoiceNdVchrNd: Boolean = false;
            var fluType = 'flu';
            added_immunizations.forEach(rec => {
                if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1 ) && ( rec.isVoucherNeeded === '1' || rec.isVoucherNeeded === 1)) {
                    hasCorpInvoiceNdVchrNd = true;
                }
            });
            if (hasCorpInvoiceNdVchrNd == true)
                return true;
        } catch (e) {
            if (e === true)
                return true;
        }
        return false;
    }

    checkIfFluImmForCorpInvoiceSelected() {
        let added_immunizations = this.getSelectedImmunizations();
        try {
          added_immunizations.forEach(rec => {
            if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1) &&
             ( rec.isVoucherNeeded === '1' || rec.isVoucherNeeded === 1)
             && rec.immunizationName.toLowerCase().search('flu') !== -1) {
              throw true;
            }
          });
        } catch (e) {
          if (e === true)
            return true;
        }
        return false;
      }

      checkIfNonFluImmForCorpInvoiceSelected() {
        let added_immunizations = this.getSelectedImmunizations();
        try {
          added_immunizations.forEach(rec => {
            if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1) &&
             ( rec.isVoucherNeeded === '1' || rec.isVoucherNeeded === 1)
             && rec.immunizationName.toLowerCase().search('flu') == -1) {
              throw true;
            }
            // else if(rec.paymentTypeId==6 && rec.sendInvoiceTo===true && rec.immunizationName.toLowerCase().search('flu') == -1){
            //     this.hasNonFluImmSelected = true;
            // }
          });
        } catch (e) {
          if (e === true)
            return true;
        }
        return false;
      }
      insertCharityProgram(store_id,clinic_type,contract_agreement_model) {
        let url = environment.API_URL + environment.RESOURCES.INSERT_CHARITY_PROGRAM + "/" + store_id + "/" + clinic_type;
        return this._http.post(url, contract_agreement_model, Util.getRequestHeaders())
            .map((response: Response) => response.json());
    }
}
