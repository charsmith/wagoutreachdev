import { TestBed, inject } from '@angular/core/testing';

import { ContractapproveService } from './contractapprove.service';

describe('ContractapproveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContractapproveService]
    });
  });

  it('should be created', inject([ContractapproveService], (service: ContractapproveService) => {
    expect(service).toBeTruthy();
  }));
});
