import { TestBed, inject } from '@angular/core/testing';

import { ContractAgreementService } from './contract-agreement.service';

describe('ContractAgreementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContractAgreementService]
    });
  });

  it('should be created', inject([ContractAgreementService], (service: ContractAgreementService) => {
    expect(service).toBeTruthy();
  }));
});
