import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { immunizationsArray } from './../../../JSON/Immunizations';
import { environment } from '../../../../environments/environment';
import { SpanishData } from '../../../JSON/Spanish';
import { Util } from '../../../utility/util';
import { Clinic } from '../../../models/contract';

@Injectable()
export class ContractapproveService {
  private messageSource = new BehaviorSubject<string>("default message");
  currentMessage = this.messageSource.asObservable();

  _baseUrl: string = 'http://localhost:49940/';
  contract: any;
  private formData: any;
  private contractData: any;
  private client_info: any;
  private legal_info: any;
  cancel_check:boolean = false;
  constructor(private _http: Http) {
    Observable.of(immunizationsArray).subscribe(res => {
    this.contract = res;
      this.updateContractDetails(this.contract);
    });
  }
  getContractAgreement() {
    return this._http.get('assets/showcase/data/ContractAgreement_Sample_JSON.json').map((response: Response) => response.json());
  }

  getContractAgreementApproveData(clinic_agreement_pk: number) {
    let url = environment.API_URL + environment.RESOURCES.GET_CONTRACT_AGREEMENT + "/" + clinic_agreement_pk;
    return this._http.get(url,Util.getRequestHeaders()).map((response: Response) => this.formData = response.json());
  }

  //Spanish Data
  // languageChange() {
  //   return this._http.get('/assets/showcase/data/Spanish.json').map((response: Response) => response.json());
  // }
  languageChange(): Observable<any> {
    return Observable.of(SpanishData);
  }
  changeMessage(message: string) {
    this.messageSource.next(message)
  }
  getSelectedClientLegalInfo(): any {
    return this.client_info;
  }
  getSelectedLegalInfo(): any {
    return this.legal_info;
  }

  setSelectedClientLegalInfo(data: any) {
    this.client_info = data;
  }
  setSelectedLegalInfo(data: any) {
    this.legal_info = data;
  }

  private contractDetails: BehaviorSubject<any> = new BehaviorSubject("");
  currentContract = this.contractDetails.asObservable();

  updateContractDetails(contract: any) {
    this.contractDetails.next(contract);
  }

  saveAgreementData(store_id, clinic_type, clinic_agreement) {
    let url = environment.API_URL + environment.RESOURCES.UPDATE_CONTRACT_AGREEMENT + "/" + store_id + "/" + clinic_type;
    return this._http.post(url, clinic_agreement,Util.getRequestHeaders()).map((response: Response) => response.json());
}


checkIfAnyImmunizationsHasCorpInvoiceAndVoucherNeeded(location: Clinic): boolean {
  let added_immunizations = this.formData.clinicImmunizationList;
  try {
      let hasCorpInvoiceNdVchrNd: Boolean = false;
      var fluType = 'flu';
      added_immunizations.forEach(rec => {
          if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1 ) && ( rec.isVoucherNeeded === '1' || rec.isVoucherNeeded === 1)) {
              hasCorpInvoiceNdVchrNd = true;
          }
      });
      if (hasCorpInvoiceNdVchrNd == true)
          return true;
  } catch (e) {
      if (e === true)
          return true;
  }
  return false;
}

checkIfFluImmForCorpInvoiceSelected() {
  let added_immunizations = this.formData.clinicImmunizationList;
  try {
    added_immunizations.forEach(rec => {
      if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1) &&
      ( rec.isVoucherNeeded === '1' || rec.isVoucherNeeded === 1) &&
       rec.immunizationName.toLowerCase().search('flu') !== -1) {
        throw true;
      }
    });
  } catch (e) {
    if (e === true)
      return true;
  }
  return false;
}

checkIfNonFluImmForCorpInvoiceSelected() {
  let added_immunizations = this.formData.clinicImmunizationList;
  try {
    added_immunizations.forEach(rec => {
      if (rec.paymentTypeId == 6 && (rec.sendInvoiceTo === '1' || rec.sendInvoiceTo === 1) &&
      ( rec.isVoucherNeeded === '1' || rec.isVoucherNeeded === 1) &&
       rec.immunizationName.toLowerCase().search('flu') == -1) {
        throw true;
      }
      // else if(rec.paymentTypeId==6 && rec.sendInvoiceTo===true && rec.immunizationName.toLowerCase().search('flu') == -1){
      //     this.hasNonFluImmSelected = true;
      // }
    });
  } catch (e) {
    if (e === true)
      return true;
  }
  return false;
}

}
