import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserauthenticationComponent } from './components/userauthentication/userauthentication.component';
import { ReusableModule } from '../common/components/reusable.module';
import { LoginComponent } from './components/login/login.component';
import { SelectModule } from 'ng-select';
import { FormsModule }   from '@angular/forms';
import { SignoutComponent } from './components/signout/signout.component';

@NgModule({
  imports: [
    CommonModule,
    ReusableModule,
    SelectModule,
    FormsModule
  ],
  declarations: [UserauthenticationComponent, LoginComponent, SignoutComponent]
})
export class AuthModule { }
