import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppCommonSession } from '../../../common/app-common-session';
import { SessionDetails } from '../../../../utility/session';
import { userLoginData } from '../../../../JSON/userLoginData';
import { UserLoginInfo } from '../../../../models/userLoginInfo';
import { AuthenticationService } from '../../../common/services/authentication.service';
import { UserInfo } from '../../../../models/userInfo';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  users = [];
  usersInfo = [];
  loginError:boolean=false;
  RoleID: string;
  contractpk: number;
  storeID:number;
  emailAddress:string;
  password:string;
  constructor(private router: Router, private commonsession: AppCommonSession,private authservice: AuthenticationService) { }

  ngOnInit() {
    this.removeSession();  
    if(sessionStorage.unauth)
    {
      this.loginError=true;
    }
  }
  removeSession() {
    sessionStorage["roleID"] = null;
    sessionStorage["storeId"] = null;
    sessionStorage.removeItem("hintsOff");
  }  
  
  getDashboard() {  
    var userInfo = new UserLoginInfo();
    userInfo.userName = this.emailAddress;
    userInfo.password = this.password;
    SessionDetails.SetUserInfo(userInfo);
    this.authservice.getUserProfile().subscribe((data: any) => {     
      if(data.dataList.length>0)
      {
        var userProfile = new UserInfo();
        userProfile.email=data.dataList[0].email;
        userProfile.userName=data.dataList[0].userName;
        userProfile.userPk=data.dataList[0].userPk;
        userProfile.userRole=data.dataList[0].userRole;
        userProfile.isAdmin=data.dataList[0].isAdmin;
        userProfile.isPowerUser=data.dataList[0].isPowerUser;
        userProfile.firstName=data.dataList[0].firstName;
        userProfile.lastName=data.dataList[0].lastName;
        userProfile.locationType=data.dataList[0].locationType;
        userProfile.assignedLocations=data.dataList[0].assignedLocations;
        SessionDetails.SetUserProfile(userProfile);      
        sessionStorage.removeItem("unauth");

        this.router.navigate(['/landing']);
      }
    }, err => {  
      if(err.status==401)
      {     
        SessionDetails.SetUserInfo(null);
        console.log(err.statusText);
        
      }
      this.loginError=true;
      sessionStorage["unauth"]="unauth";
    });  
    
   
  }
  getUserInfo(pk) {  
    let info = userLoginData;
    let loginInfo = info.filter(x => x.pk == pk)[0];
    return loginInfo;
  }
}
