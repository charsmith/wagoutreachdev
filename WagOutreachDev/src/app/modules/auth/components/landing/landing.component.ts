import { Component, OnInit } from '@angular/core';
//import { SessionDetails } from '../utility/session';
import { Router } from '@angular/router';
import { SessionDetails } from '../../../../utility/session';
import { LandingPageDetails } from '../../../../JSON/LandingPageDetails';
import { HeaderserviceService } from '../../../common/services/headerservice.service';
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  opportunitiesContacted: any;
  clinicsScheduled: any;
  vaccinesAdministered: any;
  srEventsScheduled: any;
  isDashboard: boolean = false;
  constructor(private router: Router, private service: HeaderserviceService) { }

  ngOnInit() {

    this.service.getWagsOutreachCounts().subscribe((data) => {
      this.opportunitiesContacted = data.opportunitiesContacted;
      this.clinicsScheduled = data.clinicsScheduled;
      this.vaccinesAdministered = data.vaccinesAdministered;
      this.srEventsScheduled = data.srEventsScheduled;
    });
    var userProfile = SessionDetails.GetUserProfile();
    if (userProfile.userRole.toLowerCase() == "store manager" || userProfile.userRole.toLowerCase() == "pharmacy manager") {
      this.isDashboard = true;
    }
    else {
      this.isDashboard = false;
    }
  }
  GoToDashboard() {
    var userProfile = SessionDetails.GetUserProfile();
    if (userProfile.userRole.toLowerCase() == "district manager" || userProfile.userRole.toLowerCase() == "director" || userProfile.userRole.toLowerCase() == "healthcare supervisor" || userProfile.userRole.toLowerCase() == "regional vice president" || userProfile.userRole.toLowerCase() == "regional healthcare director" || userProfile.userRole.toLowerCase() == "admin") {
      this.router.navigate(['/communityoutreach/home']);
    }

    if (userProfile.userRole.toLowerCase() == "store manager" || userProfile.userRole.toLowerCase() == "pharmacy manager") {
      this.router.navigate(['/communityoutreach/storehome']);
    }
  }

  GoToOpportunities() {
    var userProfile = SessionDetails.GetUserProfile();
    if (userProfile.userRole.toLowerCase() == "district manager" || userProfile.userRole.toLowerCase() == "director – rx & retail ops" || userProfile.userRole.toLowerCase() == "healthcare supervisor" || userProfile.userRole.toLowerCase() == "regional vice president" || userProfile.userRole.toLowerCase() == "regional healthcare director" || userProfile.userRole.toLowerCase().toLowerCase() == "admin" || userProfile.userRole.toLowerCase() == "store manager" || userProfile.userRole.toLowerCase() == "pharmacy manager") {
      this.router.navigateByUrl('/communityoutreach/storehome');
    }
  }
  GoTo() {
    var userProfile = SessionDetails.GetUserProfile();
    if (userProfile.userRole.toLowerCase() == "district manager" || userProfile.userRole.toLowerCase() == "director – rx & retail ops" || userProfile.userRole.toLowerCase() == "healthcare supervisor" || userProfile.userRole.toLowerCase() == "regional vice president" || userProfile.userRole.toLowerCase() == "regional healthcare director" || userProfile.userRole.toLowerCase().toLowerCase() == "admin" || userProfile.userRole.toLowerCase() == "store manager" || userProfile.userRole.toLowerCase() == "pharmacy manager") {
      this.router.navigate(['/communityoutreach/storehome']);
    }
  }
}
