import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionDetails } from '../../../../utility/session';
import { AuthenticationService } from '../../../common/services/authentication.service';
import { EncryptDecrypt } from '../../../../models/encryptDecrypt';
import { userLoginData } from '../../../../JSON/userLoginData';
import { UserLoginInfo } from '../../../../models/userLoginInfo';
import { UserInfo } from '../../../../models/userInfo';
import { ContractapproveService } from '../../../contractaggreement/services/contractapprove.service';

@Component({
  selector: 'app-userauthentication',
  templateUrl: './userauthentication.component.html',
  styleUrls: ['./userauthentication.component.css']
})
export class UserauthenticationComponent implements OnInit {
 
  constructor(private router: Router,private authenticationService: AuthenticationService,private contractApproveService:ContractapproveService) {    
    let currentUrl = this.router.url;
    this.isAuthorise(currentUrl);
  }

  ngOnInit() {    
  }
  isAuthorise(encodedValue: string)
  {
    if(encodedValue.includes("/contractAgreementForUser/"))
    {
      let encodedData= encodedValue.replace('/contractAgreementForUser/','');
      var encryptData=new EncryptDecrypt();
      encryptData.encryptedS=encodedData;
      encryptData.decryptedS="";
      let data = this.getUserInfo(41)
      var userInfo=new UserLoginInfo();
      userInfo.pk=data.pk
      userInfo.userName=data.userName;
      userInfo.password=data.password;
      userInfo.role=data.role;
      SessionDetails.SetUserInfo(userInfo);
      this.authenticationService.postEncryptionDecryption(encryptData).subscribe((data: any) =>{
       let decrypotedData= JSON.parse(data._body).decryptedS
       if(decrypotedData!=null || decrypotedData!=undefined)
       {
         let splitedData=decrypotedData.split("|");
         sessionStorage["clinic_agreement_pk"] = splitedData[1];
         sessionStorage["storeid"] = splitedData[0];
         sessionStorage["approveRejectEmail"]=splitedData[2];
         this.router.navigate(['/contractAgreementForUser'])
       }      
      });
    }
    else if(encodedValue.includes("/contractAgreement/"))
    {    
      let encodedData= encodedValue.replace('/contractAgreement/','');
      var encryptData=new EncryptDecrypt();
      encryptData.encryptedS=encodedData;
      encryptData.decryptedS="";
      let data = this.getUserInfo(41)
      var userInfo=new UserLoginInfo();
      userInfo.pk=data.pk
      userInfo.userName=data.userName;
      userInfo.password=data.password;
      userInfo.role=data.role;
      SessionDetails.SetUserInfo(userInfo);
     // var userProfile = new UserInfo();
      this.authenticationService.postEncryptionDecryption(encryptData).subscribe((data: any) =>{
       let decrypotedData= JSON.parse(data._body).decryptedS
       if(decrypotedData!=null || decrypotedData!=undefined)
       {  
         let splitedData=decrypotedData.split("|");         
         //let userPk=splitedData[0];
         userInfo.userName=splitedData[1];
         userInfo.password=splitedData[2];
         let storeid=splitedData[3];
         let clinic_agreement_pk=splitedData[4];
         SessionDetails.SetUserInfo(userInfo);

        this.authenticationService.getUserProfile().subscribe((data: any) => {     
          
          if(data.dataList.length>0)
          {
            var userProfile = new UserInfo();
            userProfile.email=data.dataList[0].email;
            userProfile.userName=data.dataList[0].userName;
            userProfile.userPk=data.dataList[0].userPk;
            userProfile.userRole=data.dataList[0].userRole;
            userProfile.isAdmin=data.dataList[0].isAdmin;
            userProfile.isPowerUser=data.dataList[0].isPowerUser;
            userProfile.firstName=data.dataList[0].firstName;
            userProfile.lastName=data.dataList[0].lastName;
            userProfile.locationType=data.dataList[0].locationType;
            userProfile.assignedLocations=data.dataList[0].assignedLocations;
            SessionDetails.SetUserProfile(userProfile);
            this.contractApproveService.getContractAgreementApproveData(clinic_agreement_pk).subscribe((data) => {
              if(data.isApproved)
              {
                SessionDetails.fromUserPage(true);
                sessionStorage["clinic_agreement_pk"] = clinic_agreement_pk;
                this.router.navigateByUrl("/viewcontract");
              } 
              else
              {
                  SessionDetails.SetAgreementPK(clinic_agreement_pk);           
                  this.router.navigateByUrl("/communityoutreach/contract");           
              } 
             
            }); 
          }
    
        }, err => {   
          if(err.status==401)
          {
            SessionDetails.SetUserInfo(null);
            console.log(err.statusText);
          }
        });  
          
       }     
      });
    }
    else{
      this.router.navigate(['/login'])
    }  
  }
  getUserInfo(pk) {  
    let info = userLoginData;
    let loginInfo = info.filter(x => x.pk == pk)[0];
    return loginInfo;
  }
}
