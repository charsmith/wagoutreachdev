import { Component, OnInit, ViewChild } from '@angular/core';
import { SessionDetails } from '../../../../utility/session';
import { ProgramType } from '../../../../utility/enum';
import { debug } from 'util';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { FollowUp } from '../../../../models/FollowUp';
import { ContactlogService } from '../../services/contactlog.service';
import { ScheduleeventService } from '../../services/scheduleevent.service';
import { Router } from '@angular/router';
import { FollowUpDetailsList, FollowUpDetails } from '../../../../models/FollowUpDetailsList';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { MessageServiceService } from '../../services/message-service.service';
import { Location, DatePipe } from '@angular/common';
import { ErrorMessages } from '../../../../config-files/error-messages';
import { Util } from '../../../../utility/util';

@Component({
  selector: 'app-followup',
  templateUrl: './followup.component.html',
  styleUrls: ['./followup.component.css'],
  providers: [DatePipe]
})
//
export class FollowupComponent  implements OnInit {
  @ViewChild('form')
  //form: NgForm;
  //extends FormCanDeactivate
  // #form="ngForm"
  // super();
  showHints:boolean = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff"))=='yes')?false:true;
  pageName:string = "followUp";

  defaultDate: Date = new Date();
  maxDateValue: Date = new Date();
  minDateValue: Date = new Date();
  date3: Date;
  sr_followup: boolean = false;
  ip_followup: boolean = false;
  followup_date: Date;
  followupForm: FormGroup;
  followup_save: any[] = [];
  get_contact_log: any;
  follow_up: FollowUp;
  displayOnSucc: boolean = false;
  get_follow_up: any;
  get_followup_data: any = 0;
  status: boolean = false;
  contact_log_IP: any = 0;
  contact_log_SR: any = 0;
  page_name: any = "";
  savePopUp: boolean = false;
  dialogSummary: string;
  dialogMsg: string;
  follow_up_details: FollowUpDetailsList = new FollowUpDetailsList();
  doNotSaveText: string = "";
  setUp: string = "";

  constructor(private formBuilder: FormBuilder, private _location: Location,
    private schedule_event_service: ScheduleeventService, private router: Router, private utility: Util, private message_service: MessageServiceService,
    private date_pipe: DatePipe) {      
    
     }

  ngOnInit() {  
    let email = "reshma.shah@walgreens.com";
    let today = new Date();
    this.date3 = new Date();
    this.minDateValue = new Date();
    this.date3.setDate(this.date3.getDate() + 7);
    // this.minDateValue = new Date(today.getFullYear(), 8, 1);
    if (SessionDetails.GetProgramType() == ProgramType.SR) {
      this.sr_followup = true;
      this.ip_followup = false;
    }
    if (SessionDetails.GetProgramType() == ProgramType.IP) {
      this.sr_followup = false;
      this.ip_followup = true;
    }
    if (SessionDetails.GetProgramType() == ProgramType.SRIP) {
      this.sr_followup = true;
      this.ip_followup = true;
    }

    this.followupForm = this.formBuilder.group({
      imzemail: [email, Validators.required],
      imzdate: [this.date3, Validators.required],
      sremail: [email, Validators.required],
      srdate: [this.date3, Validators.required],
      doNotSendImz: [false, null],
      doNotSendSR: [false, null]

    });
    this.status = SessionDetails.GetFollowUp();
    this.page_name = SessionDetails.GetPageName();
    if (this.status) {
      this.get_follow_up = SessionDetails.GetopportunitiesData();
      this.getFollowpData();
    }
    this.follow_up_details = this.followupForm.value;
  }
  isFieldValid(field: string) {
    return !this.followupForm.get(field).valid && this.followupForm.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }
  onSave() {
    if (this.followupForm.valid) {
      var followup_details_list = new FollowUpDetails();
      var followup_details_list1 = new FollowUpDetailsList();

      if (this.get_followup_data.length > 0) {
        for (let i = 0; i < this.get_followup_data.length; i++) {
          if (this.get_followup_data[i].outreachEffortCode == "IP") {
            followup_details_list.contactLogPk = this.get_followup_data[i].contactLogPk;
            followup_details_list.emailReminder = this.followupForm.controls["doNotSendImz"].value;
            followup_details_list.outreachEffortCode = this.get_followup_data[i].outreachEffortCode;
            followup_details_list.outreachPk = this.get_followup_data[i].outreachPk;
            followup_details_list.updatedBy = this.get_followup_data[i].updatedBy;
            followup_details_list.updatedDate = new Date();
            followup_details_list.emailTo = this.followupForm.controls["imzemail"].value;
            followup_details_list.followupDate = this.followupForm.controls["imzdate"].value;
          }
          if (this.get_followup_data[i].outreachEffortCode == "SR") {
            followup_details_list.contactLogPk = this.get_followup_data[i].contactLogPk;
            followup_details_list.emailReminder = this.followupForm.controls["doNotSendSR"].value;
            followup_details_list.outreachEffortCode = this.get_followup_data[i].outreachEffortCode;
            followup_details_list.outreachPk = this.get_followup_data[i].outreachPk;
            followup_details_list.updatedBy = this.get_followup_data[i].updatedBy;
            followup_details_list.updatedDate = new Date();
            followup_details_list.emailTo = this.followupForm.controls["sremail"].value;
            followup_details_list.followupDate = this.followupForm.controls["srdate"].value;
          }
          followup_details_list1.followUpDetailsList.push(followup_details_list);
          this.follow_up_details = followup_details_list1;
          followup_details_list = new FollowUpDetails();

        }
        this.schedule_event_service.updateFollowUpEvent(followup_details_list1).subscribe((data: any) => {
          if (data.status == "200") {
            this.displayOnSucc = true;
          }
        });
      }
      else {
        this.follow_up = new FollowUp();
        this.get_contact_log = SessionDetails.GetLogContact();
        this.follow_up.logOutreachStatus.businessPk = this.get_contact_log.logOutreachStatus.businessPk;
        this.follow_up.logOutreachStatus.firstName = this.get_contact_log.logOutreachStatus.firstName;
        this.follow_up.logOutreachStatus.lastName = this.get_contact_log.logOutreachStatus.lastName;
        this.follow_up.logOutreachStatus.jobTitle = this.get_contact_log.logOutreachStatus.jobTitle;
        this.follow_up.logOutreachStatus.outreachProgram = this.get_contact_log.logOutreachStatus.outreachProgram;
        this.follow_up.logOutreachStatus.outreachBusinessPk = this.get_contact_log.logOutreachStatus.outreachBusinessPk;
        this.follow_up.logOutreachStatus.contactDate = this.get_contact_log.logOutreachStatus.contactDate;
        this.follow_up.logOutreachStatus.outreachStatusId = this.get_contact_log.logOutreachStatus.outreachStatusId;
        this.follow_up.logOutreachStatus.outreachStatusTitle = this.get_contact_log.logOutreachStatus.outreachStatusTitle;
        this.follow_up.logOutreachStatus.feedback = this.get_contact_log.logOutreachStatus.feedback;
        this.follow_up.logOutreachStatus.createdBy = this.get_contact_log.logOutreachStatus.createdBy;
        this.follow_up.logOutreachStatus.clinicAgreementPk = 0;
        this.follow_up.followUpDetails.createdBy = this.follow_up.logOutreachStatus.createdBy;

        if (this.follow_up.logOutreachStatus.outreachProgram == 'IP') {
          this.follow_up.followUpDetails.emailTo = this.followupForm.value.imzemail;
          this.follow_up.followUpDetails.followupDate = this.followupForm.value.imzdate;
          this.follow_up.followUpDetails.isSendEmailReminder = this.followupForm.value.doNotSendImz;
        }
        if (this.follow_up.logOutreachStatus.outreachProgram == 'SR') {
          this.follow_up.followUpDetails.emailTo = this.followupForm.value.sremail;
          this.follow_up.followUpDetails.followupDate = this.followupForm.value.srdate;
          this.follow_up.followUpDetails.isSendEmailReminder = this.followupForm.value.doNotSendSR;
        }
        this.schedule_event_service.postFollowUpEvent(this.follow_up).subscribe((data: any) => {
          if (data.status == "200") {
            this.displayOnSucc = true;
          }
        });
      }
    }
  }
  onFolloUpIPDateSelected(select_date: Date) {
    this.followupForm.controls['imzdate'].setValue(new Date(select_date.getFullYear(), select_date.getMonth(), select_date.getDate(), 12, 30, 55));
  }
  onFolloUpSRDateSelected(select_date: Date) {
    this.followupForm.controls['srdate'].setValue(new Date(select_date.getFullYear(), select_date.getMonth(), select_date.getDate(), 12, 30, 55));
  }
  onClick() {
    this.message_service.sendProfileChange("profilechange");
    this.router.navigateByUrl('/communityoutreach/storehome');
  }
  getFollowpData() {

    if (this.page_name != "") {
      if (this.get_follow_up.outreachEffortCode == "IP") {
        this.contact_log_IP = this.get_follow_up.contactLogPk;
      }
      if (this.get_follow_up.outreachEffortCode == "SR") {
        this.contact_log_SR = this.get_follow_up.contactLogPk;
      }
    }
    else {
      if (this.get_follow_up.outreachEffort.IP.outreachStatus == 2) {
        this.contact_log_IP = this.get_follow_up.outreachEffort.IP.contactLogPk;
      }
      if (this.get_follow_up.outreachEffort.SR.outreachStatus == 2) {
        this.contact_log_SR = this.get_follow_up.outreachEffort.SR.contactLogPk;
      }
    }
    this.schedule_event_service.getFollowUpEvent(this.contact_log_IP, this.contact_log_SR).subscribe((data: any) => {
      this.get_followup_data = data.followUpDetailsList;
      if (this.get_followup_data.length > 0) {
        for (let i = 0; i < this.get_followup_data.length; i++) {
          if (this.get_followup_data[i].outreachEffortCode == "IP") {
            this.followupForm.controls["imzemail"].setValue(this.get_followup_data[i].emailTo);
            this.followupForm.controls["imzdate"].setValue(new Date(this.get_followup_data[i].followupDate));
            this.followupForm.controls["doNotSendImz"].setValue(this.get_followup_data[i].emailReminder);
          }
          if (this.get_followup_data[i].outreachEffortCode == "SR") {
            this.followupForm.controls["sremail"].setValue(this.get_followup_data[i].emailTo);
            this.followupForm.controls["srdate"].setValue(new Date(this.get_followup_data[i].followupDate));
            this.followupForm.controls["doNotSendSR"].setValue(this.get_followup_data[i].emailReminder);
          }
        }
      }
      this.follow_up_details = this.followupForm.value;
    });

  }
  onCancel() {
    var compare_objects = false;
    if (this.status) {
      compare_objects = this.utility.compareTwoObjects(this.follow_up_details, this.followupForm.value);
      if (compare_objects) {
        this.doNotSaveText = "Cancel";
        this.setUp = "Ok";
        this.showDialog(ErrorMessages['unSavedData'], ErrorMessages['contract_alert']);
      }
      else {
        this._location.back();
      }
    }
    else {
      // compare_objects = this.deepCompare(this.follow_up_details,this.followupForm.value);
      // if(compare_objects)
      // {
      this.doNotSaveText = "Do Not Save";
      this.setUp = "Continue Setup";
      this.showDialog(ErrorMessages['unSavedData'], ErrorMessages['followp_alert']);
      // }
      // else
      // {
      //   this._location.back();
      // }
    }

  }
  doNotSave() {
    this._location.back();
  }
  Continue() {
    this.savePopUp = false;
  }
  showDialog(msgSummary: string, msgDetail: string) {
    this.savePopUp = true;
    this.dialogMsg = msgDetail;
    this.dialogSummary = msgSummary;
  }
}
