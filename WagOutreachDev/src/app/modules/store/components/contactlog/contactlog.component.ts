import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Contactlogpipe } from '../../pipes/contactlogpipe';
import { StoreOpportunityComponent } from '../../../common/components/store-opportunity/store-opportunity.component';
import { ContactlogService } from '../../services/contactlog.service';
import { AppCommonSession } from '../../../common/app-common-session';
import { SessionDetails } from '../../../../utility/session';
import { FilterPipe } from '../../pipes/filter-pipe';
import { FollowupComponent } from '../follow-up/followup.component';
import { MessageServiceService } from '../../../store/services/message-service.service';
import { ProgramType } from '../../../../utility/enum';

@Component({
  selector: 'app-contactlog',
  templateUrl: './contactlog.component.html',
  styleUrls: ['./contactlog.component.css'],
  providers: [Contactlogpipe, FilterPipe]
})
export class ContactlogComponent implements OnInit {

  showHints: boolean = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff")) == 'yes') ? false : true;
  pageName: string = "viewcontactlogHistory";

  contactlogList: any[] = [];
  date: string;
  businessPk1: string;
  data: any[] = [];
  employees: any[] = [];
  contactLogData: any;
  contactLogHistory: any;
  selectedBusinessName: string;
  value: any;
  GetAssignedBusinessesData: string[] = [];
  businessData: any;
  businessName: string;
  selected: any = 0;
  loghistory: string = "";
  logTitle: string = "";
  businesspk: any;
  id: any;
  private sub: any;
  viewlog: boolean = false;
  view_log_history: boolean = false;
  message: string;
  assinedData: any;
  contact_data: any;
  business_name: any;
  user_info: any;
  contact_log_history: any;
  business_type: any;
  displayLog: boolean = false;
  contact_log_pk: number = 0;
  outreach_effort_code: boolean = false;

  constructor(private _contactlog: ContactlogService, private router: Router, private _sortByPipe: FilterPipe, private _locationNumberPipe: Contactlogpipe, private route: ActivatedRoute, private commonsession: AppCommonSession, private message_service: MessageServiceService) {
    this.contact_log_history = SessionDetails.GetopportunitiesData();
    if (this.contact_log_history.businessPk > 0) {
      this.view_log_history = true;
      this.business_type = SessionDetails.GetBusinessType();
      this.GetContactLogs(this.contact_log_history.businessPk);
    }
  }
  public GetContactLogs(businessPk) {
    this.outreach_effort_code = false;
    this.user_info = this.commonsession.login_info;
    this._contactlog.GetContactLogs(businessPk).subscribe((res) => {
      this.contact_data = res.dataList;
      this.selectedBusinessName = this.contact_log_history.businessName;
      var contact_log_data;
      var contact_log_history;
      contact_log_data = this._locationNumberPipe.transform(this.contact_data.filter(item => item.isCurrentSeason == true), 'contactDate');
      contact_log_history = this._locationNumberPipe.transform(this.contact_data.filter(item => item.isCurrentSeason == false), 'contactDate');
      this.contactLogData = this._sortByPipe.sortBy(contact_log_data, "contactDate");
      this.contactLogHistory = this._sortByPipe.sortBy(contact_log_history, "contactDate");
      if (this.contactLogHistory.length > 0) {
        // for (let i = 0; i < this.contactLogHistory.length; i++) {
        //   for (let j = 0; j < this.contactLogHistory[i].value.length; j++) {
        //     //if (this.contactLogHistory[i].value[j].isScheduledClinics) {
        //       this.outreach_effort_code = this.contactLogHistory[i].value[j].isScheduledClinics?true:false;
        //     //}
        //   }
        //}
        // 
        this.loghistory = "Past Outreach Logs";

      }
      if (this.contactLogData.length > 0) {
        this.logTitle = "Current Outreach Logs";
      }
    });
  }
  public GetAssignedBusinesses(business_pk: any) {
    this._contactlog.GetAssignedBusinesses().subscribe((data) => {
      this.GetAssignedBusinessesData = data;
    });
    this.assinedData = this.GetAssignedBusinessesData;
    this.selectedBusinessName = this.assinedData[0].businessName;
  }
  ngOnInit() {
    SessionDetails.EventDetails("");
    SessionDetails.SetPageName("");
    SessionDetails.SetAgreementPK(0);
    this.outreach_effort_code = false;
  }
  GoTo(contact_log, outreach_status) {
    if (outreach_status == 'Contract Initiated' && contact_log.isCurrentSeason) {
      SessionDetails.SetAgreementPK(contact_log.clinicAgreementPk);
      this.router.navigateByUrl("/communityoutreach/contract");
    }
    if (outreach_status == 'Contract Initiated' && !contact_log.isCurrentSeason) {
      SessionDetails.fromUserPage(false);
      sessionStorage["clinic_agreement_pk"] = contact_log.clinicAgreementPk;
      this.router.navigateByUrl("/viewcontract");
    }
    else if (outreach_status == 'Event Scheduled') {
      SessionDetails.EventDetails(contact_log);
      this.router.navigateByUrl("/communityoutreach/scheduleevent");
    }
    else if (outreach_status == 'Follow Up') {
      SessionDetails.SetFollowUp(true);
      SessionDetails.SetPageName("followUp");
      SessionDetails.opportunitiesData(contact_log);
      SessionDetails.SetProgramType(contact_log.outreachEffortCode == "IP" ? ProgramType.IP : ProgramType.SR);
      this.router.navigateByUrl("/communityoutreach/followUp");
    }
    else if (outreach_status == 'Community Outreach') {
      this.router.navigateByUrl("/communityoutreach/communityoutreach");
    }
  }
  removeLogEntry(contact_log_pk) {
    this.displayLog = true;
    this.contact_log_pk = contact_log_pk;
  }
  cancelConfirmation() {
    this.displayLog = false;
  }
  deleteLogEntry() {
    if (this.contact_log_pk > 0) {
      this._contactlog.DeleteContactLog(this.contact_log_pk).subscribe((data: any) => {
        if (data.status == "200") {
          this.GetContactLogs(this.contact_log_history.businessPk);
          this.displayLog = false;
          this.contact_log_pk = 0;
        }
      });
    }
  }
  // public onChange(val: any) {
  //   if (val.businessPk != undefined) {
  //     this.selectedBusinessName = val.businessName;
  //     this.GetContactLogs(val.businessPk);
  //   }
  //   else {
  //     //this.contactLogData = '[{}]';
  //     //this.contactLogHistory= '[{}]';
  //   }
  // }
}

