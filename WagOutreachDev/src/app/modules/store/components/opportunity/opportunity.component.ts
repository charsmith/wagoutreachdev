import {
  NgModule,
  Component,
  OnInit,
  ViewChild,
  Injectable,
  ViewContainerRef
} from '@angular/core';

import {
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';

import { Router } from '@angular/router';
import { AddBusiness } from './../../../../models/AddBusiness';
import { AddBusinessService } from '../../services/AddBusiness.Service';
import { APP_ROUTES } from './../../../../routes/app.routes';
import { Routes } from '@angular/router/src/config';
//import { Utility } from '../../../common/utility';
import { Observable } from 'rxjs/Observable';
import { StoresListService } from '../../services/stores-list.service';
import { StoreModel } from '../../../../models/Store';
import { String, StringBuilder } from 'typescript-string-operations';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService, Message } from 'primeng/api';
import { Util } from '../../../../utility/util';
import { AddOutreachOpportunity, OutreachEffort } from '../../../../models/OutreachOpportunity';
import { SessionDetails } from '../../../../utility/session';
import { ActionType, PageType } from '../../../../utility/enum';
import { MessageServiceService } from '../../services/message-service.service';

@Component({
  selector: 'app-opportunity',
  templateUrl: './opportunity.component.html',
  styleUrls: ['./opportunity.component.css']
})
export class OpportunityComponent implements OnInit {
  states: string[] = [
    "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
    "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "PR", "RI", "SC",
    "SD", "TN", "TX", "UT", "VA", "WA", "WV", "WI", "WY"]

  protected model: AddBusiness = new AddBusiness();
  public AddBusinessView: Boolean;
  public BusinessInformation: any[] = [];
  business_info: boolean = false;
  isUserAdmin: Boolean = false;
  addBusinessForm: FormGroup;
  form_edit: FormBuilder;
  phone_search: string;
  address_search: string;
  dupilicate_search: boolean;
  LocalBusinesses: any[] = [];
  duplicate_message: string;
  duplicate_message1: string;
  duplicate_message2: string;
  search_string: string;
  duplicate_business: string;
  duplicate_business1: string = "";
  duplicate_business2: boolean;
  msgs: Message[] = [];
  phoneValue: string;
  isEdit: boolean = false;
  business_name: string;
  buttonName: string;
  outreachOpportunity: AddOutreachOpportunity
  constructor(protected _AddBusinessService: AddBusinessService, private message_service: MessageServiceService, private utility: Util,
    private formBuilder: FormBuilder, private confirmationService: ConfirmationService, private prouter: Router) {
    this.formControls();
    this.AddBusinessView = true;
    this.isEdit = false;
    this.buttonName = "Save"
    //this.toastr.setRootViewContainerRef(vcr);
  }

  onPhoneFocus() {
    //if( !this.addBusinessForm.valid)
    //return;

    this.duplicate_business1 = "";
    if (this.AddBusinessView) {
      this.validatePhoneNo();
    }

  }

  validatePhoneNo() {
    const control = this.addBusinessForm.get('Phone');
    if (control.dirty && control.touched) {
      this.phone_search = this.addBusinessForm.value.Phone;
      let dupilicateItems = this._AddBusinessService.duPlicatePhoneSearch(this.phone_search);
      this.dupilicate_search = dupilicateItems.duplicate as boolean;
      let len = Object.keys(dupilicateItems.addBusiness).length as Number;
      if (this.dupilicate_search && len > 1) {
        var msg = String.Format('There is more than one business assigned to a Walgreens store with the same phone number ( store {0} ).  To eliminate duplicate follow up activity this business will not be added to your contacts',
          dupilicateItems.addBusiness[0].SeniorOutReachStore);
        this.displayNgMsg('error', 'Duplicate Business', msg);
        this._AddBusinessService.getFindDuplicateBusiness(this.phone_search).subscribe((data) => { this.duplicate_business1 = data; });
      }
      else if (this.dupilicate_search && len == 1) {
        var msg = String.Format('There is already a business assigned to a Walgreens store with the same phone number ( store {0} ).  To eliminate duplicate follow up activity this business will not be added to your contacts',
          dupilicateItems.addBusiness[0].SeniorOutReachStore);

        this.displayNgMsg('error', 'Duplicate Business', msg);
        this._AddBusinessService.getFindDuplicateBusiness(this.phone_search).subscribe((data) => { this.duplicate_business1 = data; });
      }

    }
  }
  displayNgMsg(msgSeverity: string, msgSummary: string, msgDetail: string) {
    this.msgs = [];
    this.msgs.push({ severity: msgSeverity, summary: msgSummary, detail: msgDetail });
  }
  onBlurMethod1() {
    if (this.AddBusinessView) {
      this.validateAddress();
    }
  }

  validateAddress() {
    const control = this.addBusinessForm.get('Address1');
    if ((this.addBusinessForm.get('Address1').dirty && this.addBusinessForm.get('Address1').touched) ||
      (this.addBusinessForm.get('Address2').dirty && this.addBusinessForm.get('Address2').touched) ||
      (this.addBusinessForm.get('City').dirty && this.addBusinessForm.get('City').touched) ||
      (this.addBusinessForm.get('state').dirty && this.addBusinessForm.get('state').touched) ||
      (this.addBusinessForm.get('ZipCode').dirty && this.addBusinessForm.get('ZipCode').touched)) {
      this.address_search = this.getFormValue();
      if (this.address_search != null) {
        this.dupilicate_search = this._AddBusinessService.duPlicateAddressSearch(this.address_search);
        if (this.dupilicate_search) {
          //this.toastr.error('TODO for storeID:There is already a business assigned to a Walgreens store with the same address (Store  13. How to get this number?). To eliminate duplicate follow up activity this business will not be added to your contacts',null, {toastLife:7000});
          this.displayNgMsg('error', 'Duplicate Business',
            'TODO for storeID:There is already a business assigned to a Walgreens store with the same address (Store  13. How to get this number?). To eliminate duplicate follow up activity this business will not be added to your contacts');
        }
        else {
          this.duplicate_message1 = "";
        }
      }
    }
  }

  getFormValue() {
    return this.removeSpace(this.addBusinessForm.value.Address1 + this.addBusinessForm.value.Address2 + this.addBusinessForm.value.City + this.addBusinessForm.value.state + this.addBusinessForm.value.ZipCode);
  }
  removeSpace(text: string) {
    if (text.length > 0) {
      return text.replace(/[\s]/g, '');
    }

  }
  ngOnInit() {    
    this.outreachOpportunity = new AddOutreachOpportunity();

    this.addBusinessForm = this.formBuilder.group({
      BusinessSelected: ['', ''],
      BusinessInfoSelected: ['1', ''],
      BusinessName: [null, Validators.required],
      Phone: [null, Validators.required],
      FirstName: [null, ''],
      TollFree: [null, ''],
      LastName: [null, ''],
      EMail: [null, Validators.email],
      JobTitle: [null, ''],
      WebsiteURL: [null, ''],
      EmploymentSize: [null, ''],
      Address2: ['', ''],
      CharityHHSVoucher: [null, ''],
      CommunityOutreach: [null, ''],
      City: ['', Validators.required],
      SeniorOutreach: [null, ''],
      Industry: [null, Validators.required],
      Address1: ['', Validators.required],
      assignedStore: [null, Validators.required],
      //County: [null, ''],
      ZipCode: ['', Validators.required],
      state: ['', Validators.required],
      ImmunizationProgram: [null, ''],
      SeniorOutReachStore: [null, ''],
      ImmunizationProgramStore: [null, '']
      //,ContractInitiated:[null,'']
    });
    this._AddBusinessService.getStoreLocalBusinessList().subscribe((data) => { this.LocalBusinesses = data; });
    this._AddBusinessService.getDuplicateBusiness().subscribe((data) => { this.duplicate_business = data; });
    this.isUserAdmin = this.CheckIfUserIsAdmin();

    //edit
    if (SessionDetails.GetActionType() == ActionType.editOportunity) {
      this.isEdit = true;
      this.buttonName = "Update"
      this.AddBusinessView = false;
      this._AddBusinessService.getStoreLocalBusinessList();
      let  data = SessionDetails.GetopportunitiesData()
      this.business_name  =  data.businessName;
      this.onBusinessSelected(data.businessPk);

      this.BusinessInformation = this._AddBusinessService.getStoreBusinessInfo();
      this.model.BusinessInfoSelected = 1;
      sessionStorage.removeItem("actionType");
    }

  }
  formControls() {
    this.addBusinessForm = this.formBuilder.group({
      BusinessSelected: ['', ''],
      BusinessName: [null, Validators.required],
      BusinessInfoSelected: ['1', ''],
      Phone: [null, Validators.required],
      FirstName: [null, ''],
      EMail: [null, Validators.email],
      TollFree: [null, ''],
      LastName: [null, ''],
      JobTitle: [null, ''],
      WebsiteURL: [null, ''],
      EmploymentSize: [null, ''],
      Address2: [null, ''],
      CharityHHSVoucher: [null, ''],
      CommunityOutreach: [null, ''],
      City: [null, Validators.required],
      SeniorOutreach: [null, ''],
      Industry: [null, Validators.required],
      Address1: [null, Validators.required],
      //County: [null, ''],
      ZipCode: [null, Validators.required],
      assignedStore: [null, Validators.required],
      //ZipCode4: ['', ''],
      state: [null, Validators.required],
      ImmunizationProgram: [null, ''],
      SeniorOutReachStore: [null, ''],
      ImmunizationProgramStore: [null, '']
    });


  }
  onAddEditBusinessSubmit() {

    if (this.isEdit)
      this.EditBusiness();
    else
      this.AddBusiness();
  }

  isFieldValid(field: string) {
    return !this.addBusinessForm.get(field).valid && this.addBusinessForm.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  private selectedLink: any;
  setradio(e: any): void {
    this.selectedLink = e;
  }

  AddToStore() {
    this._AddBusinessService.AddToStore(this.selectedLink).subscribe(res => {
      this.displayNgMsg('success', 'Business Added',
        "Business added successfully to selected store...")
    },
      error => this.displayNgMsg('error', 'Server Error', (<any>error).toString()));
  }

  AddBusiness() {
    this.utility.validateAllFormFields(this.addBusinessForm);
    if (this.addBusinessForm.controls['BusinessName'].value != "" && this.addBusinessForm.controls['Phone'].value != "" &&
      this.addBusinessForm.controls['Industry'].value != "" && this.addBusinessForm.controls['Address1'].value != "" &&
      this.addBusinessForm.controls['state'].value != "" && this.addBusinessForm.controls['City'].value != "" &&
      this.addBusinessForm.controls['ZipCode'].value != "") {
      let SeniorOutReach = this.addBusinessForm.controls['SeniorOutreach'].value;
      let ImmunizationProgram = this.addBusinessForm.controls['ImmunizationProgram'].value;
      if ((SeniorOutReach == false || SeniorOutReach == null) && (ImmunizationProgram == false || ImmunizationProgram == null)) {
        this.displayNgMsg('error', 'Program Selection', String.Format("Senior OutReach Program and/or Immunization Program should be selected to Add your Business"));
        return;
      }
      // let CharityHHSVoucher = this.addBusinessForm.controls['CharityHHSVoucher'].value;
      // let CommunityOutreach = this.addBusinessForm.controls['CommunityOutreach'].value;
      // if (((CharityHHSVoucher == true) || (CommunityOutreach == true)) &&
      //   (ImmunizationProgram != true)) {
      //   let msg = "";
      //   this.displayNgMsg('error', 'Immunization Program', String.Format("Immunization program is not selected.  But you have selected either Charity Program or Community Outreach or both. Please select Immunization program."));
      //   return;
      // }
      // if (((CharityHHSVoucher == true) || (CommunityOutreach == true)) &&
      //   (ImmunizationProgram != true)) {
      //   let msg = "";
      //   if (CharityHHSVoucher == true) msg = "Charity Program";
      //   if (CommunityOutreach == true) msg = "Community OutReach Program";
      //   this.displayNgMsg('error', 'Program Selection', String.Format("{0} is selected. But you have not selected  Immunization Program. Charity Program & Community OutReach Program are supported in Immunization Program only. Please select Immunization Program to proceed. ", msg));
      //   return;
      // }


      this.outreachOpportunity.address1 = this.addBusinessForm.controls['Address1'].value;
      this.outreachOpportunity.address2 = this.addBusinessForm.controls['Address2'].value;
      this.outreachOpportunity.businessName = this.addBusinessForm.controls['BusinessName'].value;
      this.outreachOpportunity.city = this.addBusinessForm.controls['City'].value;
      this.outreachOpportunity.email = this.addBusinessForm.controls['EMail'].value;
      this.outreachOpportunity.employmentSize = this.addBusinessForm.controls['EmploymentSize'].value;
      this.outreachOpportunity.firstName = this.addBusinessForm.controls['FirstName'].value;
      this.outreachOpportunity.industry = this.addBusinessForm.controls['Industry'].value;
      this.outreachOpportunity.jobTitle = this.addBusinessForm.controls['JobTitle'].value;
      this.outreachOpportunity.lastName = this.addBusinessForm.controls['LastName'].value;
      this.outreachOpportunity.phone = this.addBusinessForm.controls['Phone'].value;
      this.outreachOpportunity.state = this.addBusinessForm.controls['state'].value;
      this.outreachOpportunity.tollFree = this.addBusinessForm.controls['TollFree'].value;
      this.outreachOpportunity.zipCode = this.addBusinessForm.controls['ZipCode'].value;
      if (this.addBusinessForm.controls['SeniorOutreach'].value != null) {
        var outreachEffort = new OutreachEffort();
        outreachEffort.outreachProgram = "SR";
        outreachEffort.storeId = 1;
        outreachEffort.storePk = 1;
        outreachEffort.isCommunityInitiated = false;
        this.outreachOpportunity.outreachEffort.push(outreachEffort);
      }
      if (this.addBusinessForm.controls['ImmunizationProgram'].value != null) {
        var outreachEffort = new OutreachEffort();
        outreachEffort.outreachProgram = "IP";
        outreachEffort.storeId = 1;
        outreachEffort.storePk = 1;
        outreachEffort.isCommunityInitiated = false;
        this.outreachOpportunity.outreachEffort.push(outreachEffort);
      }

      //this._AddBusinessService.saveStoreBusiness(this.addBusinessForm.value).subscribe(res => {
      this._AddBusinessService.addOpportunity(this.outreachOpportunity).subscribe(res => {
        let Result = res;
        if (Result.return_value == -1 || Result.return_value == 0) {
          if (Result.contactlog_pk > 0) {
            if (this.addBusinessForm.value.CharityHHSVoucher == true) {
              if (this.prouter) this.prouter.navigate(['./communityoutreach/AddBusiness']);//TODO
            }
            else if (this.addBusinessForm.value.CommunityOutreach == true) {
              if (this.prouter) this.prouter.navigate(['./communityoutreach/AddBusiness']);//TODO
            }
            this.displayNgMsg('success', 'Business Added', "New Business Added successfully");
            if (this.prouter) this.prouter.navigate(['./communityoutreach/AddBusiness']);
          }
          else {
            this.displayNgMsg('success', 'Business Added', "New Business Added successfully");
            if (this.prouter) this.prouter.navigate(['./communityoutreach/AddBusiness']);
          }

        }
        else if (Result.return_value == -2) {
          let error_message: string = Result.error_message.replace(/\s+/g, ',');
          if (error_message.lastIndexOf(',') != -1) {
            let last_string = error_message.substring(error_message.lastIndexOf(','));
            error_message = error_message.replace(last_string, last_string.replace(',', ' and'));
          }

          if (Result.dt_dup_business.length > 0) {
            this.duplicate_business1 = Result.dt_dup_business;//TODO to verify the error msg.              
            this.displayNgMsg('error', 'More than 1 Duplicate Business',
              String.Format('There is already a business assigned to a Walgreens store with the same {0}.  To eliminate duplicate follow up activity this business will not be added to your contacts.',
                Result.error_message));
          }
          else {
            this.displayNgMsg('error', 'Duplicate Business',
              String.Format('There is already a business assigned to a Walgreens store with the same {0}.  To eliminate duplicate follow up activity this business will not be added to your contacts',
                Result.error_message));
          }

        }
        else if (Result.return_value == -3) {
          //if(this.model.IsUserAdmin)
          {
            //this.toastr.warning("");
          }
          //else
          {
            //if (this.prouter) this.prouter.navigate(['./'])
            //TODO
          }
        }
      },
        error => this.displayNgMsg('error', 'Server Error', (<any>error).toString())
      );
    }
  }

  Cancel() {
    this.prouter.navigate(['/communityoutreach/storehome']);
  }
  onCharityHHSVoucherChange() {
    if (this.AddBusinessView && this.addBusinessForm.value.CommunityOutreach == true && this.addBusinessForm.value.CharityHHSVoucher == true) {
      this.displayNgMsg('info', 'Program Selection', "Community OutReach Program is already selected. Community Outreach Program is automatically unselected as both programs are not supported");
      this.addBusinessForm.controls['CommunityOutreach'].setValue(false);
    }
  }
  confirm() {
    this.confirmationService.confirm({
      message: 'The store you are assigning this business to is in a different state than the business’s location.?',
      header: 'Confirmation',
      icon: 'fa fa-question-circle',
      accept: () => {
        this._AddBusinessService.updateStoreBusiness(this.addBusinessForm.value);
        this.displayNgMsg('success', 'Business Update', "Business updated successfully");
      },
      reject: () => {
        return;
      }
    });
  }
  isBusinessStateStoreStateDiff(BusinessId: Number, selectedState: string): Boolean {

    return false;
  }

  format() {
    if (this.addBusinessForm.value.Phone != undefined) {
      this.phoneValue = Util.telephoneNumberFormat(this.addBusinessForm.value.Phone);
    }

  }

  //Edit business related changes
  onBusinessSelected(businessId) {
    this.model.businessId = businessId;
    var model = new AddBusiness();
    this._AddBusinessService.getStoreBusinessDetails(businessId).subscribe((res) => {
      this.model = res;
      let edit_business_data = this.model;
      //add_business.BusinessInfoSelected = 1;
      // this.addBusinessForm = this.form_edit.group(add_business);
      this.addBusinessForm.controls['Address1'].setValue(edit_business_data[0].address1);
      this.addBusinessForm.controls['Address2'].setValue(edit_business_data[0].address2);
      this.addBusinessForm.controls['BusinessName'].setValue(edit_business_data[0].businessName);
      this.addBusinessForm.controls['City'].setValue(edit_business_data[0].city);
      this.addBusinessForm.controls['EMail'].setValue(edit_business_data[0].email);
      this.addBusinessForm.controls['EmploymentSize'].setValue(edit_business_data[0].employmentSize == "N/A" ? "" : edit_business_data[0].employmentSize);
      this.addBusinessForm.controls['FirstName'].setValue(edit_business_data[0].firstName);
      this.addBusinessForm.controls['Industry'].setValue(edit_business_data[0].industry);
      this.addBusinessForm.controls['JobTitle'].setValue(edit_business_data[0].jobTitle);
      this.addBusinessForm.controls['LastName'].setValue(edit_business_data[0].lastName);
      this.addBusinessForm.controls['Phone'].setValue(Util.telephoneNumberFormat(edit_business_data[0].phone));
      this.addBusinessForm.controls['state'].setValue(edit_business_data[0].state);
      this.addBusinessForm.controls['TollFree'].setValue(edit_business_data[0].tollFree);
      this.addBusinessForm.controls['ZipCode'].setValue(edit_business_data[0].zipCode);
      if (edit_business_data[0].outreachEffortList.length >= 1) {
        if (edit_business_data[0].outreachEffortList[0].outreachProgram == "SR") {
          this.addBusinessForm.controls['SeniorOutreach'].setValue(true);
          this.addBusinessForm.controls['assignedStore'].setValue(edit_business_data[0].outreachEffortList[0].storeId);
        }
        if (edit_business_data[0].outreachEffortList[0].outreachProgram == "IP") {

          this.addBusinessForm.controls['ImmunizationProgram'].setValue(true);
          this.addBusinessForm.controls['assignedStore'].setValue(edit_business_data[0].outreachEffortList[0].storeId);
          this.addBusinessForm.controls['CommunityOutreach'].setValue(edit_business_data[0].outreachEffortList[0].isCommunityInitiated);
          this.model.CharityHHSVoucher = edit_business_data[0].outreachEffortList[0].isCharityInitiated;
          this.model.CommunityOutreach = edit_business_data[0].outreachEffortList[0].isCommunityInitiated;
          this.model.ContractInitiated = edit_business_data[0].outreachEffortList[0].ContractInitiated;
        }
      }
      if (edit_business_data[0].outreachEffortList.length > 1) {
        if (edit_business_data[0].outreachEffortList[1].outreachProgram == "IP") {

          this.addBusinessForm.controls['ImmunizationProgram'].setValue(true);
          this.addBusinessForm.controls['assignedStore'].setValue(edit_business_data[0].outreachEffortList[1].storeId);
          this.addBusinessForm.controls['CommunityOutreach'].setValue(edit_business_data[0].outreachEffortList[1].isCommunityInitiated);
          this.model.CharityHHSVoucher = edit_business_data[0].outreachEffortList[1].isCharityInitiated;
          this.model.CommunityOutreach = edit_business_data[0].outreachEffortList[1].isCommunityInitiated;
          this.model.ContractInitiated = edit_business_data[0].outreachEffortList[1].ContractInitiated;

        }
        if (edit_business_data[0].outreachEffortList[1].outreachProgram == "SR") {

          this.addBusinessForm.controls['SeniorOutreach'].setValue(true);
          this.addBusinessForm.controls['assignedStore'].setValue(edit_business_data[0].outreachEffortList[1].storeId);
          this.addBusinessForm.controls['CommunityOutreach'].setValue(edit_business_data[0].outreachEffortList[1].isCommunityInitiated);
          this.model.CharityHHSVoucher = edit_business_data[0].outreachEffortList[1].isCharityInitiated;
          this.model.CommunityOutreach = edit_business_data[0].outreachEffortList[1].isCommunityInitiated;
        }
      }
      if (this.model.CharityHHSVoucher == true || this.model.CommunityOutreach == true) {
        // this.addBusinessForm.controls['CharityHHSVoucher'].setValue(this.model.CharityHHSVoucher);
        // this.addBusinessForm.controls['CommunityOutreach'].setValue(this.model.CommunityOutreach);
        //this.addBusinessForm.controls['CharityHHSVoucher'].disable();
        this.addBusinessForm.controls['CommunityOutreach'].disable();

      }
    });

  }
  onBusinessInfoSelected(id) {

    this.business_info = false;
    if (id == 2) {
      this.business_info = true;
    }
    if (id == 2) {
      if (this.isUserAdmin) {
        this.addBusinessForm.controls['ImmunizationProgram'].enable();
        this.addBusinessForm.controls['SeniorOutreach'].enable();
      }
      if (this.model.ContractInitiated || this.model.ContractRejected || this.model.ContractSent ||
        this.model.CharityHHSVoucher || this.model.CommunityOutreach || this.model.ScheduledEvent) {
        this.addBusinessForm.controls['ImmunizationProgramStore'].disable();
        this.addBusinessForm.controls['SeniorOutReachStore'].disable();
      }
      else {
        this.addBusinessForm.controls['ImmunizationProgramStore'].enable();
        this.addBusinessForm.controls['SeniorOutReachStore'].enable();
      }
      this.EnableDisableControls(false);
    }
    else {
      this.addBusinessForm.controls['ImmunizationProgram'].disable();
      this.addBusinessForm.controls['SeniorOutreach'].disable();
      this.addBusinessForm.controls['ImmunizationProgramStore'].disable();
      this.addBusinessForm.controls['SeniorOutReachStore'].disable();

      this.EnableDisableControls(true);
    }

  }

  private EnableDisableControls(enable: Boolean) {
    enable ? this.addBusinessForm.controls['BusinessName'].enable() : this.addBusinessForm.controls['BusinessName'].disable();
    enable ? this.addBusinessForm.controls['Phone'].enable() : this.addBusinessForm.controls['Phone'].disable();
    enable ? this.addBusinessForm.controls['FirstName'].enable() : this.addBusinessForm.controls['FirstName'].disable();
    enable ? this.addBusinessForm.controls['LastName'].enable() : this.addBusinessForm.controls['LastName'].disable();
    enable ? this.addBusinessForm.controls['EMail'].enable() : this.addBusinessForm.controls['EMail'].disable();
    enable ? this.addBusinessForm.controls['TollFree'].enable() : this.addBusinessForm.controls['TollFree'].disable();
    enable ? this.addBusinessForm.controls['Industry'].enable() : this.addBusinessForm.controls['Industry'].disable();
    enable ? this.addBusinessForm.controls['JobTitle'].enable() : this.addBusinessForm.controls['JobTitle'].disable();
    enable ? this.addBusinessForm.controls['WebsiteURL'].enable() : this.addBusinessForm.controls['WebsiteURL'].disable();
    enable ? this.addBusinessForm.controls['Address1'].enable() : this.addBusinessForm.controls['Address1'].disable();
    enable ? this.addBusinessForm.controls['Address2'].enable() : this.addBusinessForm.controls['Address2'].disable();
    enable ? this.addBusinessForm.controls['County'].enable() : this.addBusinessForm.controls['County'].disable();
    enable ? this.addBusinessForm.controls['state'].enable() : this.addBusinessForm.controls['state'].disable();
    enable ? this.addBusinessForm.controls['City'].enable() : this.addBusinessForm.controls['City'].disable();
    enable ? this.addBusinessForm.controls['ZipCode'].enable() : this.addBusinessForm.controls['ZipCode'].disable();
    enable ? this.addBusinessForm.controls['EmploymentSize'].enable() : this.addBusinessForm.controls['EmploymentSize'].disable();
    enable ? this.addBusinessForm.controls['CharityHHSVoucher'].enable() : this.addBusinessForm.controls['CharityHHSVoucher'].disable();
    enable ? this.addBusinessForm.controls['CommunityOutreach'].enable() : this.addBusinessForm.controls['CommunityOutreach'].disable();
    enable ? this.addBusinessForm.controls['ZipCode4'].enable() : this.addBusinessForm.controls['ZipCode4'].disable();
  }
  // onCharityHHSVoucherChange()
  // {

  //   if( !this.AddBusinessView && ( this.model.ContractInitiated== true || 
  //     this.model.CommunityOutreach || this.model.CharityHHSVoucher ))
  //   {

  //       this.addBusinessForm.controls['CommunityOutreach'].disable();
  //       this.addBusinessForm.controls['CharityHHSVoucher'].disable();

  //   }
  //   if(this.addBusinessForm.controls['CommunityOutreach'].enabled && this.addBusinessForm.controls['CharityHHSVoucher'].enabled )
  //   {
  //     if ( this.addBusinessForm.controls['CommunityOutreach'].value == true ) 
  //     {
  //       this.displayNgMsg('error','Program',"Community Outreach already selected, you cannot select both programs."); 
  //       this.addBusinessForm.controls['CharityHHSVoucher'].setValue(false);
  //     }
  //   }
  //   else
  //   {
  //     this.addBusinessForm.controls['CharityHHSVoucher'].setValue(this.addBusinessForm.controls['CharityHHSVoucher'].enabled);
  //   }
  // }
  onCommunityOutreachChange() {
    if (!this.AddBusinessView && (this.model.ContractInitiated == true || this.model.CommunityOutreach || this.model.CharityHHSVoucher)) {

      this.addBusinessForm.controls['CommunityOutreach'].disable();
      this.addBusinessForm.controls['CharityHHSVoucher'].disable();

    }
    if (this.addBusinessForm.controls['CommunityOutreach'].enabled && this.addBusinessForm.controls['CharityHHSVoucher'].enabled) {
      if (this.addBusinessForm.controls['CharityHHSVoucher'].value == true) {
        this.displayNgMsg('error', 'Duplicate Business', "Charity Program already selected, you cannot select both programs.");
        this.addBusinessForm.controls['CommunityOutreach'].setValue(false);
      }
    }
    else {
      this.addBusinessForm.controls['CommunityOutreach'].setValue(this.addBusinessForm.controls['CommunityOutreach'].enabled);
    }
  }
  onSeniorOutReachChange() {
    if (this.business_info == false) {
      this.addBusinessForm.controls['SeniorOutreach'].setValue(this.model.SeniorOutreach);
      this.addBusinessForm.controls['ImmunizationProgram'].setValue(this.model.ImmunizationProgram);
      this.addBusinessForm.controls['SeniorOutreach'].disable();
      this.addBusinessForm.controls['ImmunizationProgram'].disable();
    }
    if (this.business_info == true) {
      if ((this.addBusinessForm.controls['CommunityOutreach'].value == true || this.addBusinessForm.controls['CharityHHSVoucher'].value == true) &&
        this.addBusinessForm.controls['SeniorOutreach'].value == true && this.addBusinessForm.controls['ImmunizationProgram'].value == false) {
        let msg = "";
        if (this.addBusinessForm.controls['CommunityOutreach'].value == true) { msg = "Community OutReach Program"; }
        else if (this.addBusinessForm.controls['CharityHHSVoucher'].value == true) { msg = "Charity HHS Program"; }

        this.displayNgMsg('error', 'Program', String.Format("As {0} is selected, Senior Outreach Program is not supported unless you select Immunization program or both. Automatically selecting Immunization Program.", msg));
        this.addBusinessForm.controls['ImmunizationProgram'].setValue(true); // get confirmation

      }
      if ((this.model.SeniorOutreach == true && this.addBusinessForm.controls['SeniorOutreach'].value == false) && (this.model.ContractInitiated || this.model.ContractRejected || this.model.ContractSent || this.model.CharityHHSVoucher || this.model.CommunityOutreach
        || this.model.ScheduledEvent)) {
        let msg = "";
        if (this.model.ContractInitiated) { msg = "Contract Initiated" }
        if (this.model.ContractRejected) { msg = "Contract Rejected" }
        if (this.model.ContractSent) { msg = "Contract Sent" }
        if (this.model.CharityHHSVoucher) { msg = "Charity Program" }
        if (this.model.CommunityOutreach) { msg = "Community Outreach" }
        this.displayNgMsg('info', 'Program', String.Format(" The status of this business is '{0}'. Hence you cannot unselect this option. Automatically selecting this program... ", msg));
        this.addBusinessForm.controls['SeniorOutreach'].setValue(true);

      }

    }

  }
  onImmunizationProgramChange() {

    if (this.business_info == false) {
      this.addBusinessForm.controls['SeniorOutreach'].setValue(this.model.SeniorOutreach);
      this.addBusinessForm.controls['ImmunizationProgram'].setValue(this.model.ImmunizationProgram);
      this.addBusinessForm.controls['SeniorOutreach'].disable();
      this.addBusinessForm.controls['ImmunizationProgram'].disable();
    }
    if (this.business_info == true) {
      if ((this.addBusinessForm.controls['CommunityOutreach'].value == true || this.addBusinessForm.controls['CharityHHSVoucher'].value == true) &&
        this.addBusinessForm.controls['ImmunizationProgram'].value == false) {
        let msg = "";
        if (this.addBusinessForm.controls['CommunityOutreach'].value == true) { msg = "Community OutReach Program"; }
        else if (this.addBusinessForm.controls['CharityHHSVoucher'].value == true) { msg = "Charity HHS Program"; }

        this.displayNgMsg('error', 'Program', String.Format("As {0} is selected, You cannot unselect Immunization Program. Automatically selecting Immunization Program.", msg));
        this.addBusinessForm.controls['ImmunizationProgram'].setValue(true); // get confirmation

      }
      if ((this.model.ImmunizationProgram == true && this.addBusinessForm.controls['ImmunizationProgram'].value == false) && (this.model.ContractInitiated || this.model.ContractRejected || this.model.ContractSent || this.model.CharityHHSVoucher || this.model.CommunityOutreach
        || this.model.ScheduledEvent)) {
        let msg = "";
        if (this.model.ContractInitiated) { msg = "Contract Initiated" }
        if (this.model.ContractRejected) { msg = "Contract Rejected" }
        if (this.model.ContractSent) { msg = "Contract Sent" }
        if (this.model.CharityHHSVoucher) { msg = "Charity Program" }
        if (this.model.CommunityOutreach) { msg = "Community Outreach" }
        this.displayNgMsg('info', 'Program', String.Format(" The status of this business is '{0}'. Hence you cannot unselect this option. Automatically selecting this program... ", msg));
        this.addBusinessForm.controls['ImmunizationProgram'].setValue(true);
      }
    }
  }
  CheckIfUserIsAdmin(): Boolean {
    return true;
  }

  onSeniorOutReachStoreChange() {

    const control = this.addBusinessForm.get('SeniorOutReachStore');
    if (control.dirty && control.touched) {
      if ( /*!this._AddBusinessService.IsSeniorOutReachStoreValid(this.model.businessId,this.addBusinessForm.value.SeniorOutReachStore) ||*/   this.addBusinessForm.controls['SeniorOutReachStore'].value == "") {
        this.displayNgMsg('error', 'Invalid Store', String.Format("Given Store number '{0}' is invalid. Please enter valid store number to proceed", this.addBusinessForm.controls['SeniorOutReachStore'].value))
      }
    }
  }
  onImmunizationProgramStoreChange() {

    const control = this.addBusinessForm.get('ImmunizationProgramStore');
    if (control.dirty && control.touched) {
      if ( /* !this._AddBusinessService.IsImmunizationProgStoreValid(this.model.businessId,this.addBusinessForm.value.ImmunizationProgramStore) || */  this.addBusinessForm.controls['ImmunizationProgramStore'].value == "") {
        this.displayNgMsg('error', 'Invalid Store', String.Format("Given Store number '{0}' is invalid. Please enter valid store number to proceed", this.addBusinessForm.controls['ImmunizationProgramStore'].value))
      }
    }
  }
  EditBusiness() {
    // if (this.addBusinessForm.valid)
    if (this.addBusinessForm.controls['BusinessName'].value == null && this.addBusinessForm.controls['Phone'].value == null &&
      this.addBusinessForm.controls['Industry'].value == null && this.addBusinessForm.controls['Address1'].value == null &&
      this.addBusinessForm.controls['state'].value == null && this.addBusinessForm.controls['City'].value == null &&
      this.addBusinessForm.controls['ZipCode'].value == null) {
      this.displayNgMsg('error', 'Select Business', "Please select any business from dropdown or fill required fields to continue ...");
      return;
    }
    if (this.addBusinessForm.controls['BusinessName'].value != "" && this.addBusinessForm.controls['Phone'].value != "" &&
      this.addBusinessForm.controls['Industry'].value != "" && this.addBusinessForm.controls['Address1'].value != "" &&
      this.addBusinessForm.controls['state'].value != "" && this.addBusinessForm.controls['City'].value != "" &&
      this.addBusinessForm.controls['ZipCode'].value != "") {
      if (!this.business_info) {
        this.utility.validateAllFormFields(this.addBusinessForm);
        this.validatePhoneNo();
        this.validateAddress();
      }

      let SeniorOutReach = this.addBusinessForm.controls['SeniorOutreach'].value;
      let ImmunizationProgram = this.addBusinessForm.controls['ImmunizationProgram'].value;
      let store = this.addBusinessForm.controls['assignedStore'].value;
      if ((SeniorOutReach == false || SeniorOutReach == null) && (ImmunizationProgram == false || ImmunizationProgram == null)) {
        this.displayNgMsg('error', 'Program', String.Format("Senior OutReach Program and/or Immunization Program should be selected to update your Business"));
        return;
      }
      if (SeniorOutReach == true && (store == null || store == "")) {
        this.displayNgMsg('error', 'Program', String.Format("Please enter the store id for Senior Outreach"));
        return;
      }
      else if (ImmunizationProgram == true && (store == null || store == "")) {
        this.displayNgMsg('error', 'Program', String.Format("Please enter the store id for Immunization Program"));
        return;
      }

      //let CharityHHSVoucher = this.addBusinessForm.controls['CharityHHSVoucher'].value;
      let CommunityOutreach = this.addBusinessForm.controls['CommunityOutreach'].value;
      //if (((CharityHHSVoucher == true) || (CommunityOutreach == true)) && (ImmunizationProgram != true)) {

      if (((CommunityOutreach == true)) && (ImmunizationProgram != true)) {
        let msg = "";
        this.displayNgMsg('error', 'Program', String.Format("Either Charity Program or Community Outreach program is selected.  But you have not selected Immunization program. Please select any of the program to proceed."));
        return;
      }
      if (this.model.ContractInitiated /*|| CharityHHSVoucher == true || CommunityOutreach == true*/) {
        let msg = "";
        if (this.model.ContractInitiated == true) msg = "Contract Initiated";
        // if (CharityHHSVoucher) msg = "Charity Program";
        if (CommunityOutreach) msg = "Community Outreach";

        this.displayNgMsg('error', 'Store', String.Format("This store has already has {0}. You cannot update this business.", msg));
        return;
      }

      if (this.isBusinessStateStoreStateDiff(Number(this.model.BusinessSelected), this.addBusinessForm.controls['state'].value)) {
        this.confirm();
        return;
      }
      //  if((  ( CharityHHSVoucher  == true) ||  (CommunityOutreach == true ) ) &&
      //  ( ImmunizationProgram != true  ))
      //  {
      //    let msg = "";
      //    if(CharityHHSVoucher == true ) msg = "Chrity Program";
      //    if(CommunityOutreach == true  ) msg = "Community OutReach Program";
      //    this.toastr.error(String.Format("{0} is selected. But you have not selected Immunization Program. Chrity Program & Community OutReach Program are supported in Immunization Program only. Please select Immunization Program to proceed. " ,msg ));
      //    return;
      //  }
      this._AddBusinessService.updateStoreBusiness(this.addBusinessForm.value).subscribe(res => {
        let Result = res;
        /*if( this.addBusinessForm.value.ImmunizationProgram == true && this.addBusinessForm.value.CharityHHSVoucher == true)
        {
          if ( this.router) this.router.navigate(['/CharityProgramURL']); //TODO
        }
        else if( this.addBusinessForm.value.ImmunizationProgram == true && this.addBusinessForm.value.CommunityOutreach == true)
        {
          if ( this.router) this.router.navigate(['/communityOutreachProgram']); //TODO
        }
        else if( this.addBusinessForm.value.ImmunizationProgram == true )
        {
          this.displayNgMsg('success','Business Updated',"Business updated successfully.");
        }
        else
        {*/
        this.displayNgMsg('success', 'Business updated', "Business updated successfully");
        //}
      },
        error => this.displayNgMsg('error', 'Server Error', (<any>error).toString())
      );
    }
    else {
      this.displayNgMsg('error', 'Select Business', "Please select any business from dropdown and/or fill required fields to continue ...");
    }
  }



}

