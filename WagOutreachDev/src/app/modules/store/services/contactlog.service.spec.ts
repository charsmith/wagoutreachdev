import { TestBed, inject } from '@angular/core/testing';

import { ContactlogService } from './contactlog.service';

describe('ContactlogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContactlogService]
    });
  });

  it('should be created', inject([ContactlogService], (service: ContactlogService) => {
    expect(service).toBeTruthy();
  }));
});
