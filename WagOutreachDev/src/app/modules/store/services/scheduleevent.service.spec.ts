import { TestBed, inject } from '@angular/core/testing';

import { ScheduleeventService } from './scheduleevent.service';

describe('ScheduleeventService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScheduleeventService]
    });
  });

  it('should be created', inject([ScheduleeventService], (service: ScheduleeventService) => {
    expect(service).toBeTruthy();
  }));
});
