import { TestBed, inject } from '@angular/core/testing';

import { StoresListService } from './stores-list.service';

describe('StoresListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StoresListService]
    });
  });

  it('should be created', inject([StoresListService], (service: StoresListService) => {
    expect(service).toBeTruthy();
  }));
});
