import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { storesArray} from './../../../JSON/Stores';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { StoreModel} from './../../../models/Store';
import { AddBusiness } from './../../../models/AddBusiness';
import { Headers, Http, Response    } from '@angular/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class StoresListService {
 baseUrl = 'http://localhost:49940/api/Values/';
 storeslist: any[] = [];
//  private defaultStore = new BehaviorSubject<stor
  constructor(private http: Http) { }
  getStoresList(): Observable<any>{
    let url = this.baseUrl + '/stores';    
     return Observable.of(storesArray);
    // return this.http.get(url)
    // .map((response) => response.json())
    // .catch((error) => {
    //   return Observable.throw(error || 'Server error: Failed to get opportunities');
    // });
  }
 
 
}
