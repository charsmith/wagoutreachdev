import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { opportunitiesarray } from './../../../JSON/Opportunities';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { storeStatus } from '../../../JSON/store-status';
import { dncClients } from '../../../JSON/DNCClients';
import { scheduleClinics } from '../../../JSON/scheduleClinics';
import { outreachStatus } from '../../../JSON/outreach-status';
import { outreachType } from '../../../JSON/outReachType';
import { HttpHeaders } from '@angular/common/http';
import { StoreHome } from '../../../JSON/StoreHome';
import { environment } from '../../../../environments/environment';
import { storesArray } from '../../../JSON/Stores';
import { Subject } from 'rxjs/Rx';
import { HCSData } from '../../../JSON/HCSData';
import { HCSClinicDetails } from '../../../JSON/HCSClinicDetails';
import { HCSLocalLeads } from '../../../JSON/HCSLocaLeads';
import { Util } from '../../../utility/util';
import { Router } from '@angular/router';

@Injectable()
export class OpportunitiesService {

  opportunities: any[] = [];
  baseUrl = 'http://localhost:49975/api/Values/';
  baseUrl1 = 'http://192.168.2.118:55/';
  private business_pk: any;
  data_role: any;
  constructor(private http: Http, private router: Router) {

  }
  // getOpportunities(): Observable<any> {
  //   let url = this.baseUrl + '/opportunities';
  //   //return Observable.of(opportunitiesarray);

  //   return Observable.of(StoreHome);
  //   // return this.http.get(url)
  //   // .map((response) => response.json())
  //   // .catch((error) => {
  //   //   return Observable.throw(error || 'Server error: Failed to get opportunities');
  //   // });
  // } 

  getMessage1() {
    this.data_role;
  }
  getOpportunities(store_id: number, user_pk: number, user_role: string) {  
    let url = environment.API_URL + environment.RESOURCES.GET_ASSIGNED_BUSINESSES + "/" + store_id
      + "/" + user_pk + "/" + user_role + "/" + "2" + " ";
    return this.http.get(url,Util.getRequestHeaders())
      .map((response: Response) => response.json());
  }


  getOpportunities1() {
    return this.http.get("http://sg-lt-153:7588/api/values/GetAssignedBusinesses").map((response: Response) => {
      Observable.of(response.json());
    });
    //return StoreHome;

  }
  // getStoreStatus(): Observable<any> {
  //   return this.http.get(this.baseUrl + "/getWorkflowMonitorStatus").map((response: Response) =>{
  //      return Observable.of(response.json());
  //   })
  // }
  getStoreStatus(): Observable<any> {
    // return this.http.get(this.baseUrl + "/getWorkflowMonitorStatus").map((response: Response) =>{
    //    return Observable.of(storeStatus);
    // })
    return Observable.of(storeStatus);
  }
  getScheduleClinics() {
    return scheduleClinics;

  }
  getMetricsCounts() {
    return this.http.get(this.baseUrl + "/getMetricsCounts").map((response: Response) => response.json());
  }
  getActionItemsCounts() {
    return this.http.get(this.baseUrl + "/getActionItemsCounts").map((response: Response) => response.json());
  }
  setBusinessPk(data: any) {
    this.business_pk = data;
    localStorage.setItem("business_pk", data);
  }
  getBusinessPk(): any {
    return this.business_pk;
  }
  getNationalContractClients() {
    let url=environment.API_URL + environment.RESOURCES.GET_NATIONAL_CONTRACT_CLIENTS;
    return this.http.get(url,Util.getRequestHeaders())
      .map((response: Response) => response.json());
    //return Observable.of(dncClients);
  }
  deleteNationalContractClients(DNCData: any) {
    //TODO userPK 
    let url = environment.API_URL + environment.RESOURCES.DELETE_NATIONAL_CONTRACT_CLIENTS + "/41";
    return this.http.post(url, DNCData,Util.getRequestHeaders()).catch((err) => Util.handleError(err, this.router));
    // return this.http.post(url, DNCData).map((res) => res.json()).catch(this.handleError);      

  }
  setLoginUserInfo(user_role: any) {
    return this.http.get(this.baseUrl + "GetUserInformation?user_role=" + user_role + "").map((response: Response) => response.json());
  }
  getOutreachStatus(): Observable<any> {
    let url = environment.API_URL + environment.RESOURCES.GET_OUTREACH_STATUS_LABELS;
    return this.http.get(url,Util.getRequestHeaders()).map((response: Response) => response.json());
  }

  getOutreachType() {
    return outreachType;
    //return  this.http.get(this.baseUrl + "getOutreachStatus").map((response: Response) => response.json());
  }
  
  getHCSData(): Observable<any> {
    return Observable.of(HCSData);
  }
  getHCSClinicDetails(): Observable<any> {
    return Observable.of(HCSClinicDetails);
  }

  getHCSLocalLeads(): Observable<any> {
    return Observable.of(HCSLocalLeads);
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    //console.warn(errMsg);
    return Observable.throw(errMsg);
  }
}




