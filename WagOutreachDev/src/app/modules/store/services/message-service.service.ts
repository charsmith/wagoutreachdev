import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable()
export class MessageServiceService {
    private subject = new Subject<any>();
    private subjectHint = new Subject<any>();
    private subjectProfile = new Subject<any>();
    private turnOffOnHintsFromFooter = new Subject<any>();
    private turnOffOnHintsFromComponent = new Subject<any>();

    sendStoreId(message: string) {
        this.subject.next({ text: message});
    }

    clearStoreId() {
        this.subject.next({text: ""});
    }

    getStoreID(): Observable<any> {
        return this.subject.asObservable();
    }
    sendStoreProfile(store_id: string) {
        this.subject.next({ text: store_id});
    }

    clearStoreProfile() {
        this.subject.next({text: ""});
    }

    getStoreProfile(): Observable<any> {
        return this.subject.asObservable();
    }
      
    sendProfileChange(message: string) {
        this.subjectProfile.next({ text: message });
    }

    getProfileChange(): Observable<any> {
        return this.subjectProfile.asObservable();
    }

    setTurnOnOffLocalHintsFromFooter(turn_on_off: boolean) {
        this.turnOffOnHintsFromFooter.next({ text: turn_on_off });
    }

    getTurnOnOffLocalHintsFromFooter(): Observable<any> {
        return this.turnOffOnHintsFromFooter;
    }

    setTurnOnOffLocalHintsFromComponent(turn_on_off: boolean) {
        this.turnOffOnHintsFromComponent.next({ text: turn_on_off });
    }

    getTurnOnOffLocalHintsFromComponent(): Observable<any> {
        return this.turnOffOnHintsFromComponent;
    }
}
