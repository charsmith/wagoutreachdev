import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { contractData } from '../../../JSON/contactlog';
import { Observable } from 'rxjs/Observable';
import { contactLogData } from '../../../JSON/contactlogdata';
import { environment } from '../../../../environments/environment';
import { Util } from '../../../utility/util';
import { Router } from '@angular/router';

@Injectable()
export class ContactlogService {

  constructor(private http: Http, private router: Router) { }
  // public GetContactLogs(businessPk,businessType):Observable<any> {
  //   return this.http.get(this.baseUrl + "GetContactLogs?business_pk=" + businessPk + " &business_type="+businessType+"").map((response: Response) => response.json());
  // }

  public GetContactLogs1(business_pk, business_type): Observable<any> {
    return Observable.of(contactLogData);
  }

  public GetContactLogs(business_pk): Observable<any> {
    let url = environment.API_URL + environment.RESOURCES.GET_OUTREACH_CONTACT_LOGS + "/" + business_pk;
    return this.http.get(url,Util.getRequestHeaders()).map((response: Response) => response.json());
  }
  public DeleteContactLog(contact_log_pk): Observable<any> {
    let url = environment.API_URL + environment.RESOURCES.DELETE_CONTACT_LOG + "/" + contact_log_pk;
    return this.http.delete(url, Util.getRequestHeaders()).catch((err) => Util.handleError(err, this.router));
  }

  public GetAssignedBusinesses(): Observable<any> {
    return Observable.of(contractData);
    //return this.http.get(this.baseUrl + "GetAssignedBusinesses12").map((response: Response) => response.json());
  }


}
