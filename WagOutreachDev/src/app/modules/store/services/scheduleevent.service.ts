import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Headers, Http, Response } from '@angular/http';
import { FollowUp } from '../../../models/FollowUp';
import { Util } from '../../../utility/util';
import { Router } from '@angular/router';
import { Schedulevent } from '../../../models/schedulevent';
@Injectable()
export class ScheduleeventService {

  constructor(private http: Http, private router: Router) { }
  getScheduleEvents(contact_log_pk) {
    let url = environment.API_URL + environment.RESOURCES.GET_SCHEDULE_EVENT + "/" + contact_log_pk;
    return this.http.get(url,Util.getRequestHeaders())
      .map((response: Response) => response.json());
  }
  postFollowUpEvent(follow_up) {
    let url = environment.API_URL + environment.RESOURCES.POST_FOLLOWUP_EVENT;
    return this.http.post(url, follow_up,Util.getRequestHeaders()).catch((err) => Util.handleError(err, this.router));
  }
  postScheduleEvent(scheduleEvent: FollowUp) {
    let url = environment.API_URL + environment.RESOURCES.POST_SCHEDULE_EVENT;
    return this.http.post(url, scheduleEvent,Util.getRequestHeaders()).catch((err) => Util.handleError(err, this.router));
  }

  upadteScheduleEvent(scheduleEvent: Schedulevent) {
    let url = environment.API_URL + environment.RESOURCES.UPDATE_SCHEDULE_EVENT;
    return this.http.post(url, scheduleEvent,Util.getRequestHeaders()).catch((err) => Util.handleError(err, this.router));
  }

  postSimpleUpEvent(contact_log) {
    let url = environment.API_URL + environment.RESOURCES.POST_SIMPLE_EVENT;
    return this.http.post(url, contact_log,Util.getRequestHeaders()).catch((err) => Util.handleError(err, this.router));
  }


  updateFollowUpEvent(follow_up) {
    let url = environment.API_URL + environment.RESOURCES.UPDATE_FOLLOWUP_EVENT;
    return this.http.post(url, follow_up,Util.getRequestHeaders()).catch((err) => Util.handleError(err, this.router));
  }

  getFollowUpEvent(contact_log_pk_IP, contact_log_pk_SR) {
    let url = environment.API_URL + environment.RESOURCES.GET_FOLLOWUP_EVENT + "/" + contact_log_pk_IP + "/" + contact_log_pk_SR;
    return this.http.get(url,Util.getRequestHeaders())
      .map((response: Response) => response.json());
  }

}
