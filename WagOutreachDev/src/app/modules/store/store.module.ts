import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoresListService } from '../store/services/stores-list.service';
import { AddBusinessService } from '../store/services/AddBusiness.Service';
import { OpportunitiesService } from '../store/services/opportunities.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReusableModule } from '../common/components/reusable.module';
import { InputMaskModule, AccordionModule, KeyFilterModule, FieldsetModule, DialogModule, DataTableModule, TabViewModule, DataListModule, CalendarModule } from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { GrowlModule, MessagesModule, MessageModule } from 'primeng/primeng';
import { AppCommonSession } from '../common/app-common-session';
import { TableModule } from 'primeng/table';
import { ClickOutsideModule } from 'ng4-click-outside';

import { ScheduleEventComponent } from './components/schedule-event/schedule-event.component';

import { FollowupComponent } from './components/follow-up/followup.component';
import { OpportunityComponent } from './components/opportunity/opportunity.component';
import { ContactlogComponent } from './components/contactlog/contactlog.component';
import { Contactlogpipe } from './pipes/contactlogpipe';
import { ContactlogService } from './services/contactlog.service';
import { Opportunities } from '../../models/opportunities';
import { ScheduleeventService } from './services/scheduleevent.service';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ClickOutsideModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    ReusableModule,
    InputMaskModule,
    AccordionModule,
    KeyFilterModule,
    FieldsetModule,
    BrowserAnimationsModule,
    ConfirmDialogModule,
    DialogModule,
    DataTableModule,
    TabViewModule,
    DataListModule,
    GrowlModule,
    MessagesModule,
    MessageModule,
    CalendarModule,
    TableModule

  ],
  declarations: [ScheduleEventComponent, FollowupComponent, OpportunityComponent, ContactlogComponent, Contactlogpipe],
  providers: [StoresListService,OpportunitiesService, AddBusinessService, ScheduleeventService, ConfirmationService, AppCommonSession, Opportunities, ContactlogService]
})
export class StoreModule { }
