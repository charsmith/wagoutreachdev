import { Component, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { MessageServiceService } from '../../../store/services/message-service.service';
import { Subscription } from 'rxjs';
import { HintMessage } from '../../../../JSON/hintMessages';

@Component({
    selector: 'hintscomponent',
    templateUrl: './popuphints.component.html',
    styleUrls: ['../footer/footer.component.css']
})
export class HintsPopupComponent implements OnChanges {
    //title = 'I\'m a nested component';  
    dispChild: boolean = false;
    hintMessages:string[];
    @Input() dispHintDialog: boolean = true;
    @Input() stepNumber:number;
    @Output() turnOffHintsChange = new EventEmitter<boolean>();
    @Input() title: string = "";

    currentHintNumber:number =0;
    backButton: boolean = true;
    nextButton: boolean = true;
    hintHdng: string = "";
    hintMsg: string="";
turnOnOffHints:boolean=false;

    constructor(private message_service: MessageServiceService) {
        //this.hintMessages = HintMessage[this.title]["admin"];
        this.message_service.getTurnOnOffLocalHintsFromFooter().subscribe(message => {
            if(message.text){
                localStorage.removeItem("hintsOff");
                sessionStorage.removeItem("hintsOff");                
                this.turnOnOffHints = false;
            }else{                
                sessionStorage.setItem("hintsOff","yes");
            }
            this.dispHintDialog = message.text;
        });
    }

    turnOffHints(event: any) {
        this.dispHintDialog = false;
        this.turnOffHintsChange.emit(!event.path[0].checked);
        this.message_service.setTurnOnOffLocalHintsFromComponent(!event.path[0].checked);
        localStorage.setItem("hintsOff","yes"); 
    }

    public showMsg() {
        this.dispHintDialog = true;
        //alert('This is child component');
    }
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes["title"]) {
            this.title = changes["title"].currentValue;
            this.hintMessages = HintMessage[this.title]["admin"];
            if(this.hintMessages.length>0){
                if(this.currentHintNumber==0)
                {
                    this.backButton=false;
                }
            }
            if(this.hintMessages.length==1){
                this.backButton = false;
                this.nextButton = false;
            }
            this.loadCurrentHints();
            this.dispHintDialog = changes["dispHintDialog"].currentValue;
        }
        if (changes["stepNumber"]) {
            this.backButton = false;
            this.nextButton = false;
            this.stepNumber = changes["stepNumber"].currentValue;
            if (!changes["title"]) {
                this.loadCurrentHints();
                this.dispHintDialog = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff"))=='yes')?false:true;
            }
        }
    }
    loadCurrentHints() {
        if (this.stepNumber) {
            this.hintHdng = this.hintMessages[this.stepNumber-1]["hintHdng"];
            this.hintMsg = this.hintMessages[this.stepNumber-1]["hintMsg"];
        }
        else {
            this.hintHdng = this.hintMessages[this.currentHintNumber]["hintHdng"];
            this.hintMsg = this.hintMessages[this.currentHintNumber]["hintMsg"];
        }
    }
    loadStepWiseHints(){
         this.hintHdng = this.hintMessages[this.stepNumber]["hintHdng"];
        this.hintMsg = this.hintMessages[this.stepNumber]["hintMsg"];
    }
    next(step: number) {
        this.backButton = true;
        this.currentHintNumber = this.currentHintNumber + step;
        if (this.hintMessages.length - 1 == this.currentHintNumber) {
          this.nextButton = false;
        }
        this.loadCurrentHints()
        //this.hintHdng = this.hintMessages[this.currentHintNumber]["hintHdng"];
        //this.hintMsg = this.hintMessages[this.currentHintNumber]["hintMsg"];
      }
    
      back(step: number) {
        this.currentHintNumber = this.currentHintNumber - step;
        this.nextButton = true;
        if (this.currentHintNumber == 0) {
          this.backButton = false;
        }
        this.loadCurrentHints();
        //this.hintHdng = this.hintMessages[this.currentHintNumber]["hintHdng"];
        //this.hintMsg = this.hintMessages[this.currentHintNumber]["hintMsg"];
      }
}