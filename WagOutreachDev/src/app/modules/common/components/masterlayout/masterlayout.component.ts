import { Component, OnInit, ElementRef, HostListener, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { Observable, Subject, Observer, Subscription } from 'rxjs';
import { NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';

import { StoreModel } from '../../../../models/Store';
import { MessageServiceService } from '../../../store/services/message-service.service';
import { ActionType } from '../../../../utility/enum';
import { SessionDetails } from '../../../../utility/session';

import { StoresListService } from '../../../store/services/stores-list.service';
import { StoreProfile } from '../../../../models/storeProfile';
import { OpportunitiesService } from '../../../store/services/opportunities.service';
import { HeaderserviceService } from '../../services/headerservice.service';
import { StoreOpportunityComponent } from '../store-opportunity/store-opportunity.component';


@Component({
  selector: 'app-masterlayout',
  templateUrl: './masterlayout.component.html',
  styleUrls: ['./masterlayout.component.css']
})
export class MasterlayoutComponent implements OnInit {
  showVar: boolean = false;
  isAdmin: boolean = false;
  isPowerUser: boolean = false;
  header_title: string;
  menu: string[]
  openElements: boolean[] = [];
  storeInfo: any;
  district_number: any;
  displayName: any;
  displayNumber: any;
  last_access: any = new Date();
  storeAddress: string = '';
  stores: StoreModel[] = [];
  stores1: any[] = [];
  searching = false;
  searchFailed = false;
  visible: boolean = true;
  model: any;
  results: Object;
  searchTerm$ = new Subject<string>();
  search$: Observable<string[]>;
  selectedNumber: string = '';
  title_name: any;
  data_role: any;
  store_id: any = "";
  on_select: boolean = false;
  isStoreTitle: boolean = true;
  isShowHeaderFooter: boolean = true;
  subscription: Subscription;
  message: any;
  userProfile :any;
 
  constructor(public router: Router, private message_service: MessageServiceService, private _opportunityService: OpportunitiesService, titleService: Title, private _headerservice: HeaderserviceService, private _storeslistservice: StoresListService, private store_profile: StoreProfile) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        let title = this.getTitle(router.routerState, router.routerState.root).join('-');
        titleService.setTitle("Walgreens Community Outreach");
        if (title == "contract") {
          this.header_title = "COMMUNITY OFF-SITE AGREEMENT";
          this.isStoreTitle = false;
        }
        else if (title == "addBusiness")
          if (SessionDetails.GetActionType() == ActionType.addOportunity) {
            this.header_title = "ADD AN OPPORTUNITY";
            this.isShowHeaderFooter = true;
          }
          else {
            this.header_title = "OPPORTUNITY PROFILE";
            this.isShowHeaderFooter = true;
          }
        else if (title == "contactlog") {
          this.header_title = "CONTACT LOG HISTORY";
          this.isShowHeaderFooter = true;
        }
        else if (title == "charityProgram") {
          this.header_title = "CHARITY (HHS VOUCHER)";
          this.isStoreTitle = true;
          this.isShowHeaderFooter = true;
        }
        else if (title == "scheduleevent") {
          this.header_title = "SR SCHEDULED EVENT INFORMATION";
          this.isStoreTitle = false;
          this.isShowHeaderFooter = true;
        }
        else if (title == "userprofile") {
          this.header_title = "USER PROFILES";
          this.isStoreTitle = true;
          this.isShowHeaderFooter = true;
        }
        else if (title == "Local Clinic Details") {
          this.header_title = "CLINIC DETAILS";
          this.isStoreTitle = true;
          this.isShowHeaderFooter = true;
        }
        else if (title == "Community Outreach Clinic Details") {
          this.header_title = "CLINIC DETAILS";
          this.isStoreTitle = true;
          this.isShowHeaderFooter = true;
        }
        else if (title == "followUp") {
          this.header_title = "SCHEDULED FOLLOW-UP REMINDERS";
          this.isStoreTitle = false;
          this.isShowHeaderFooter = true;
        }
        else if (title == "Charity Program Clinic Details") {
          this.header_title = "CLINIC DETAILS";
          this.isStoreTitle = true;
          this.isShowHeaderFooter = true;
        }
        else if (title == "contractAgreementForUser") {
          this.header_title = "CONTRACTING CLIENT INFORAMTION";
          this.isStoreTitle = true;
          this.isShowHeaderFooter = false;
        }
        else if (title == "resources") {
          this.header_title = "OUTREACH MATERIALS";
          this.isStoreTitle = true;
          this.isShowHeaderFooter = true;
        }
        else if (title == "soresources") {
          this.header_title = "SENIOR OUTREACH MATERIALS";
          this.isStoreTitle = true;
          this.isShowHeaderFooter = true;
        }
        else if (title == "imzresources") {
          this.header_title = "IMMUNIZATION OUTREACH MATERIALS";
          this.isStoreTitle = true;
          this.isShowHeaderFooter = true;
        }
        else if (title == "Community Outreach Program") {
          this.header_title = "COMMUNITY OUTREACH";
          this.isStoreTitle = false;
          this.isShowHeaderFooter = true;
        }
        else if (title == "viewcontract") {
          this.header_title = "COMMUNITY OFF-SITE AGREEMENT";
          this.isStoreTitle = false;
          this.isShowHeaderFooter = false;
        }else if (title == "activityreport") {
          this.header_title = "Activity Report";
          this.isStoreTitle = true;
        }
        else if (title == "scheduledclinicsreport") {
          this.header_title = "Scheduled Clinics Report";
          this.isStoreTitle = true;
        }
        else if (title == "scheduledeventsreport") {
          this.header_title = "Scheduled Events Report";
          this.isStoreTitle = true;
        }
        else if (title == "vaccinepurchasingreport") {
          this.header_title = "Vaccine Purchase Report";
          this.isStoreTitle = true;
        }
        else if (title == "immunizationfinancereport") {
          this.header_title = "Immunization Finance Report";
          this.isStoreTitle = true;
        }
        else if (title == "groupidassignmentreport") {
          this.header_title = "Group ID Assignment Report";
          this.isStoreTitle = true;
        }
        else if (title == "scheduledappointmentsreport") {
          this.header_title = "Scheduled Appointments Report";
          this.isStoreTitle = true;
        }
        else if (title == "revisedclinicdatesreport") {
          this.header_title = "Revised Cilnic Dates & Volumes Report";
          this.isStoreTitle = true;
        }
        else if (title == "weeklygroupidassignmentsreport") {
          this.header_title = "Weekly Group ID Assignments Report";
          this.isStoreTitle = true;
        }
        else if (title == "weeklyvaccinepurchasereport") {
          this.header_title = "Weekly Vaccine Purchasing Report";
          this.isStoreTitle = true;
        }
        else {
          this.header_title = "COMMUNITY OUTREACH";
          this.isStoreTitle = true;
          this.isShowHeaderFooter = true;
        }
      }
    });
    this.userProfile = SessionDetails.GetUserProfile();
    this.on_select = false;
    this.subscription = this.message_service.getProfileChange().subscribe(message => {
      this.message = message;
      if (this.message.text!= "") {
        this.getStoreAddress(this.userProfile.userPk, null, SessionDetails.GetStoreId());
      }
    });

  }

  ngOnInit() {
    this.on_select = false;
    this.GetMenu();
    this.store_id = SessionDetails.GetStoreId();
    this.getStoreAddress(this.userProfile.userPk, null, this.store_id == null ? null : this.store_id);
  }
   ngOnDestroy() {
     this.subscription.unsubscribe();
 }
  // sleep(milliseconds) {
  //   var start = new Date().getTime();
  //   for (var i = 0; i < 1e7; i++) {
  //     if ((new Date().getTime() - start) > milliseconds) {
  //       break;
  //     }
  //   }
  // }

  formatter = (result: any) => result.storeId + " - " +
    result.address + ", " + result.city + ","
    + result.state + " " + result.zip || '';




  search = (text$: Observable<string>) =>
    text$
      .debounceTime(400)
      .distinctUntilChanged()
      .do(() => this.searching = true)
      .switchMap(term =>
        this._headerservice.storeSearch(this.userProfile.userPk, term.trim(), 0)
          .do(() => {
            // this.searchFailed = false
          })
          .catch(() => {
            // this.searchFailed = true;
            return Observable.of([]);
          }))
      .do(() => this.searching = false);

  
  getStoreAddress(user_pk, store_search, store_id) {

    this._headerservice.getStoresProfile(user_pk, store_search, store_id).subscribe((res) => {
      this.store_profile = res.data;    
      SessionDetails.StoreId(this.store_profile[0].storeId);
      SessionDetails.setState(this.store_profile[0].state);
      this.storeAddress =
        this.store_profile[0].storeId + " - " +
        this.store_profile[0].address + ", " + this.store_profile[0].city + ", "
        + this.store_profile[0].state + " " + this.store_profile[0].zip;
      this.last_access = this.store_profile[0].lastStatusProvided;
      if (this.on_select) {
        this.message_service.sendStoreId(this.store_profile[0].storeId);
      }
    });
  }
  onSelect(item) {
    this.on_select = true;
    this.model = item.item.name;
    //SessionDetails.StoreId(item.item.storeId);
    this.getStoreAddress(this.userProfile.userPk, null, item.item.storeId);
    this.visible = false;
    this.showVar = false;
  }
  getTitle(state, parent) {
    var data = [];
    if (parent && parent.snapshot.data && parent.snapshot.data.title) {
      data.push(parent.snapshot.data.title);
    }

    if (state && parent) {
      data.push(... this.getTitle(state, state.firstChild(parent)));
    }
    return data;
  }
  storeSearch() {
    this.model = '';
    this.showVar = !this.showVar;
  }
  onClickedOutside(e: Event) {   
    this.isStoreTitle = true;
    if (this.title_name == 'contract') {
      this.isAdmin = false;
    }
    this.showVar = false;
  }
  GetMenu() {    
    
    if (this.userProfile.userRole.toLowerCase().toLowerCase() == "admin") {
      this.isAdmin = true;
      this.menu = this._headerservice.getAdminMenuItems();
    }
    else if (this.userProfile.userRole.toLowerCase() == "store manager" || this.userProfile.userRole.toLowerCase() == "pharmacy manager") {
      this.isAdmin = false;
      this.menu = this._headerservice.getStoreManagerMenuItems();
    }
    else if (this.userProfile.userRole.toLowerCase() == "district manager") {
      this.isPowerUser = true;
      this.isAdmin = false;
      this.menu = this._headerservice.getDistrictManagerMenuItems();
     
      this._headerservice.getStoresProfile(this.userProfile.userPk, null, null).subscribe((res) => {
      
        this.storeInfo = res.data;
      });       
      this.displayName = "DISTRICT";
      this.displayNumber = 12;
    }
    else if (this.userProfile.userRole.toLowerCase() == "director – rx & retail ops") {
      this.isPowerUser = true;
      this.isAdmin = false;
      this.menu = this._headerservice.getDirectorRxRetailMenuItems();    
      this._headerservice.getStoresProfile(this.userProfile.userPk, null, null).subscribe((res) => {
     
      this.storeInfo = res.data;
    });
      this.displayName = "AREA";
      this.displayNumber = 5;
    }
    else if (this.userProfile.userRole.toLowerCase() == "healthcare supervisor") {
      this.isPowerUser = true;
      this.isAdmin = false;
      this.menu = this._headerservice.getHcsManagerMenuItems();
      this._headerservice.getStoresProfile(this.userProfile.userPk, null, null).subscribe((res) => {    
      this.storeInfo = res.data;
    });
      this.displayName = "AREA";
      this.displayNumber = 5;
    }
    else if (this.userProfile.userRole.toLowerCase() == "regional vice president" || this.userProfile.userRole.toLowerCase() == "regional healthcare director") {
      this.isPowerUser = true;
      this.isAdmin = false;
      this.menu = this._headerservice.getRvpMenuItems();     
      this._headerservice.getStoresProfile(this.userProfile.userPk, null, null).subscribe((res) => {        
        this.storeInfo = res.data;
      });
      this.displayName = "REGION";
      this.displayNumber = 5;
    }
  }
  selectDropdownStore(selectedData:any)
  {  
    this.on_select=true;
    this.getStoreAddress(this.userProfile.userPk, null, selectedData);
  }
}

