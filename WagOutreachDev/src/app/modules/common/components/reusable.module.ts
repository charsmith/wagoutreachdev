import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FieldErrorDisplayComponent } from './field-error-display/field-error-display.component';

import { FooterComponent } from './footer/footer.component';

import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { FilterPipe } from '../../store/pipes/filter-pipe';
import { DataTableModule, AccordionModule, CalendarModule, DialogModule, ConfirmDialogModule, InputSwitchModule } from 'primeng/primeng';
import { ClickOutsideModule } from 'ng4-click-outside';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AlertComponent } from '../../../alert/alert.component';
import { MessageServiceService } from '../../store/services/message-service.service';
import { AlertService } from '../../../alert.service';
import { StoreProfile } from '../../../models/storeProfile';
import { HeaderserviceService } from '../services/headerservice.service';
import { MasterlayoutComponent } from './masterlayout/masterlayout.component';
import { StoreOpportunityComponent } from './store-opportunity/store-opportunity.component';
import { LocationNumberPipe } from '../../contractaggreement/pipes/location-number.pipe';
import { TimeToDatePipe } from '../../contractaggreement/pipes/time-to-date.pipe';
import { HintsPopupComponent } from './popupHints/popuphints.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng4LoadingSpinnerModule,
    DataTableModule,
    AccordionModule,
    CalendarModule,
    DialogModule,
    RouterModule,
    ClickOutsideModule,
    InputSwitchModule,
    NgbModule.forRoot(),
    ConfirmDialogModule,
  ],
  declarations: [FieldErrorDisplayComponent,FooterComponent,MasterlayoutComponent,AlertComponent,FilterPipe,LocationNumberPipe,TimeToDatePipe, MasterlayoutComponent, StoreOpportunityComponent,HintsPopupComponent],
  providers:[HeaderserviceService,MessageServiceService,AlertService,StoreProfile],
  exports:[FieldErrorDisplayComponent,FooterComponent,LocationNumberPipe,TimeToDatePipe,HintsPopupComponent]
})
export class ReusableModule { }
