import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { FormGroup, FormBuilder, Validators, Form, NgForm, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MessageService } from 'primeng/components/common/messageservice';
import { Ng4LoadingSpinnerModule, Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { OpportunitiesService } from '../../../store/services/opportunities.service';
import { FilterPipe } from '../../../store/pipes/filter-pipe';
import { AppCommonSession } from '../../app-common-session';
import { MessageServiceService } from '../../../store/services/message-service.service';
import { SessionDetails } from '../../../../utility/session';
import { environment } from '../../../../../environments/environment';
import { HeaderDetails } from '../../../../JSON/HeaderDetails';
import { ActionType, ProgramType, PageType } from '../../../../utility/enum';
import { Opportunities } from '../../../../models/opportunities';
import { Util } from '../../../../utility/util';
import { logOutreachStatus } from '../../../../models/logOutreachStatus';
import { ScheduleeventService } from '../../../store/services/scheduleevent.service';
import { FollowUpDetails, FollowUp } from '../../../../models/FollowUp';
import { stat } from 'fs';
import { ErrorMessages } from '../../../../config-files/error-messages';
import { String, StringBuilder } from 'typescript-string-operations';
import { HeaderserviceService } from '../../services/headerservice.service';



//import { EILSEQ } from 'constants';

@Component({
  selector: 'app-store-opportunity',
  templateUrl: './store-opportunity.component.html',
  styleUrls: ['./store-opportunity.component.css'],
  providers: [OpportunitiesService, FilterPipe]
})
export class StoreOpportunityComponent implements OnInit {

  showHints: boolean = ((localStorage.getItem("hintsOff") || sessionStorage.getItem("hintsOff")) == 'yes') ? false : true;
  pageName: string = "storehome";
  opportunitiesData_List: any[] = [];
  opportunities: any[] = [];
  schedule_clinics: any[] = [];
  upcoming_events: any[] = [];
  past_events: any[] = [];
  store_status: any[] = [];
  metrics_count: any[] = [];
  actionitems_count: string;
  css_color: string;
  display: boolean = false;
  displayInfoDlg: boolean = false;
  displaySE: boolean = false;
  dnc_clients: any[] = [];
  moment: any;
  dialogMsg: string;
  get_data: any;
  user_info: any;
  sort_by: string;
  opportunities_data: any;
  searchText: any;
  dialogOptConLog: boolean = false;
  outreach_name: any;
  outreach_status: any[] = [];
  outreach_status_data: any;
  business_contacted: any;
  defaultDate: Date = new Date();
  maxDateValue: Date = new Date();
  minDateValue: Date = new Date('Mon May 01 2017 00:00:00');
  outreachstatus: any;
  error_message: any;
  first_name: string;
  last_name: string;
  feedback: any = '';
  outreach_status_title: any;
  logcontact: FormGroup;
  message: any;
  selectedCountry: string;
  outreach_type: any;
  outreach_type_data: any;
  outreach_status_info: any;
  outreach_status_value: any = "";
  opportunities1: any[] = [];
  firstDate: Date
  secondDate: Date;
  outreachStartDateIP: string = "05/01/2016";
  data_role: any;
  message1: any;
  subscription: Subscription;
  business_name: string;
  job_title: boolean = false;
  loader: boolean = true;
  store_profile: any;
  display_message: boolean = false;
  dispContractInitiated: boolean = false;
  event_details: any;
  contract_initiated: boolean = false;
  community_outreach: boolean = false;
  edit_buisness_data: any;
  contract_followup: boolean = false;
  selectedDNCData: any[] = [];
  selectedDataList: any[] = [];
  displayDNC: boolean = false;
  defaultValue: string;
  opportunityForm: FormGroup;
  store_id: any;
  status: boolean = true;
  state_specific: string;
  state_specific_display: boolean = false;
  userProfile: any;
  constructor(private _sortByPipe: FilterPipe, private formBuilder: FormBuilder, private _opportunityService: OpportunitiesService, private router: Router,
    private commonsession: AppCommonSession, private message_service: MessageServiceService, private schedule_event_service: ScheduleeventService, private ng4LoadingSpinnerService: Ng4LoadingSpinnerService, private opportunitie: Opportunities,
    private _fb: FormBuilder, private utility: Util, private _headerservice: HeaderserviceService) {

    this.userProfile = SessionDetails.GetUserProfile();
    SessionDetails.EventDetails("");
    SessionDetails.opportunitiesData("");
    SessionDetails.SetLogContact("");
    SessionDetails.SetBusinessType("");
    SessionDetails.SetPageName("");
    SessionDetails.SetAgreementPK(0);
    SessionDetails.SetFollowUp(false);
    this.user_info = this.commonsession.login_info;
    this.status = true;
    this.subscription = this.message_service.getStoreID().subscribe(message => {
      this.message = message;
      this.store_id = this.message.text;
      if (this.store_id != "") {
        this.searchText = ""
        this.getAssignedBusiness(this.store_id);
        this.message.status = false;
      }
    });
  }

  ngOnInit() {

    this.opportunityForm = this.formBuilder.group({
      defaultSortBY: ["1", Validators.required]
    });
    var date = new Date();
    var test = date.getTime();
    this.searchText = ""
    this.logcontact = this._fb.group({
      select_outreach: this._fb.control('', Validators.required),
      businessName: this._fb.control('', Validators.required),
      firstName: this._fb.control('', Validators.required),
      lastName: this._fb.control('', Validators.required),
      jobTitle: this._fb.control('', Validators.required),
      defaultDate: this._fb.control('', Validators.required),
      outreachstatus: this._fb.control('', Validators.required),
      feedback: this._fb.control('', Validators.required)
    });
    this.outreachstatus = 0;
    this.user_info = this.commonsession.login_info;
    this.get_data = environment;
    this.store_profile = SessionDetails.GetStoreId();
    this.state_specific = SessionDetails.GetState();
    if (this.store_profile == null) {
      this._headerservice.getStoresProfile(this.userProfile.userPk, null, null).subscribe((res) => {
        this.store_profile = res.data;
        SessionDetails.StoreId(this.store_profile[0].storeId);
        this.getAssignedBusiness(this.store_profile[0].storeId);
      });
    }
    else {
      this.getAssignedBusiness(this.store_profile);
    }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  getNationalContractClients() {
    let startTime: Date = new Date();
    console.log(String.Format("Started calling this._opportunityService.getNationalContractClients at {0}:{1}:{2}:{3}", startTime.getHours(), startTime.getMinutes(), startTime.getSeconds(), startTime.getMilliseconds()));

    //Gets Do Not Call Clients
    this._opportunityService.getNationalContractClients().subscribe((res) => {
      let endTime: Date = new Date();
      console.log(String.Format("Ending this._opportunityService.getNationalContractClients() at {0}:{1}:{2}:{3}", endTime.getHours(), endTime.getMinutes(), endTime.getSeconds(), endTime.getMilliseconds()));

      this.dnc_clients = res
    });
  }
  getAssignedBusiness(store_id) {

    this.store_profile = SessionDetails.GetStoreId();
    this.opportunityForm = this.formBuilder.group({
      defaultSortBY: ["1", Validators.required]
    });
    this.loader = true;
    this.ng4LoadingSpinnerService.show();
    this.opportunitiesData_List = [];
    this.schedule_clinics = [];
    this.upcoming_events = [];
    this.past_events = [];

    let startTime: Date = new Date();
    console.log(String.Format("Started calling this._opportunityService.getOpportunities at {0}:{1}:{2}:{3}", startTime.getHours(), startTime.getMinutes(), startTime.getSeconds(), startTime.getMilliseconds()));
    this._opportunityService.getOpportunities(store_id, this.userProfile.userPk, this.userProfile.userRole).subscribe((opportunities) => {
      let endTime: Date = new Date();
      console.log(String.Format("Ending this._opportunityService.getOpportunities at {0}:{1}:{2}:{3}", endTime.getHours(), endTime.getMinutes(), endTime.getSeconds(), endTime.getMilliseconds()));
      this.store_status = opportunities.workflowMonitorStatusList;

      if (this.store_status.length < 4) {
        let goal_count = HeaderDetails.to_goal;
        let completed_count = HeaderDetails.to_complete;
        this.store_status.push({ "outreachStatusId": 5, "outreachStatus": "Schedule SR Events", "statusCountByStore": (goal_count - completed_count), "statusCountByDist": null })
      }
      let upcomingEvents;
      let pastEvents;
      //Opportunities
      this.schedule_clinics = opportunities.eventsList;
      upcomingEvents = this.schedule_clinics.filter((item => new Date(item.eventDate) > new Date()));
      pastEvents = this.schedule_clinics.filter(item => new Date(item.eventDate) < new Date());
      this.upcoming_events = this._sortByPipe.sortBy(upcomingEvents, "eventDate", "eventDate");
      this.past_events = this._sortByPipe.sortBy(pastEvents, "eventDate", "eventDate");

      this.defaultValue = "1";
      this.opportunities = this._sortByPipe.sortBy(opportunities.opportunityList, "employmentSize");
      let loopstartTime: Date = new Date();
      console.log(String.Format("Started looping for UI binding at {0}:{1}:{2}:{3}", loopstartTime.getHours(), loopstartTime.getMinutes(), loopstartTime.getSeconds(), loopstartTime.getMilliseconds()));

      for (var i = 0; i < this.opportunities.length; i++) {
        var opportunitiesData = new Opportunities();
        opportunitiesData.businessPk = this.opportunities[i].businessPk;
        opportunitiesData.businessName = this.opportunities[i].businessName;
        opportunitiesData.phone = this.opportunities[i].phone;
        opportunitiesData.industry = this.opportunities[i].industry;
        opportunitiesData.employmentSize = this.opportunities[i].employmentSize;
        opportunitiesData.isMilitaryOpportunity = this.opportunities[i].isMilitaryOpportunity;
        opportunitiesData.jobTitle = this.opportunities[i].jobTitle;
        opportunitiesData.firstName = this.opportunities[i].firstName;
        opportunitiesData.lastName = this.opportunities[i].lastName;
        opportunitiesData.isPreviousClient = this.opportunities[i].isPreviousClient;
        opportunitiesData.isStandardized = this.opportunities[i].isStandardized;

        if (this.opportunities[i].outreachEffortList.length >= 1) {
          if (this.opportunities[i].outreachEffortList[0].outreachProgram == "SR") {
            opportunitiesData.outreachEffort.SR.outreachStatusTitle = this.opportunities[i].outreachEffortList[0].outreachStatusTitle;
            opportunitiesData.outreachEffort.SR.lastContact = this.opportunities[i].outreachEffortList[0].lastContact;
            opportunitiesData.outreachEffort.SR.outreachProgram = this.opportunities[i].outreachEffortList[0].outreachProgram;
            opportunitiesData.lastContact = this.opportunities[i].outreachEffortList[0].lastContact;
            opportunitiesData.outreachEffort.SR.outreachStatus = this.opportunities[i].outreachEffortList[0].outreachStatus;
            opportunitiesData.outreachEffort.SR.isScheduledClinics = this.opportunities[i].outreachEffortList[0].isScheduledClinics;
            opportunitiesData.outreachEffort.SR.outreachBusinessPk = this.opportunities[i].outreachEffortList[0].outreachBusinessPk;
            opportunitiesData.outreachEffort.SR.contactLogPk = this.opportunities[i].outreachEffortList[0].contactLogPk;
            opportunitiesData.outreachEffort.SR.clinicAgreementPk = this.opportunities[i].outreachEffortList[0].clinicAgreementPk;
          }

          if (this.opportunities[i].outreachEffortList[0].outreachProgram == "IP") {
            opportunitiesData.outreachEffort.IP.outreachStatusTitle = this.opportunities[i].outreachEffortList[0].outreachStatusTitle;
            opportunitiesData.outreachEffort.IP.lastContact = this.opportunities[i].outreachEffortList[0].lastContact;
            opportunitiesData.outreachEffort.IP.outreachProgram = this.opportunities[i].outreachEffortList[0].outreachProgram;
            opportunitiesData.lastContact = this.opportunities[i].outreachEffortList[0].lastContact;
            opportunitiesData.outreachEffort.IP.outreachStatus = this.opportunities[i].outreachEffortList[0].outreachStatus;
            opportunitiesData.outreachEffort.IP.isScheduledClinics = this.opportunities[i].outreachEffortList[0].isScheduledClinics;
            opportunitiesData.outreachEffort.IP.outreachBusinessPk = this.opportunities[i].outreachEffortList[0].outreachBusinessPk;
            opportunitiesData.outreachEffort.IP.contactLogPk = this.opportunities[i].outreachEffortList[0].contactLogPk;
            opportunitiesData.outreachEffort.IP.clinicAgreementPk = this.opportunities[i].outreachEffortList[0].clinicAgreementPk;
          }
        }

        if (this.opportunities[i].outreachEffortList.length > 1) {
          if (this.opportunities[i].outreachEffortList[1].outreachProgram == "IP") {
            opportunitiesData.outreachEffort.IP.outreachStatusTitle = this.opportunities[i].outreachEffortList[1].outreachStatusTitle;
            opportunitiesData.outreachEffort.IP.lastContact = this.opportunities[i].outreachEffortList[1].lastContact;
            opportunitiesData.outreachEffort.IP.outreachProgram = this.opportunities[i].outreachEffortList[1].outreachProgram;
            opportunitiesData.outreachEffort.IP.outreachStatus = this.opportunities[i].outreachEffortList[1].outreachStatus;
            opportunitiesData.outreachEffort.IP.isScheduledClinics = this.opportunities[i].outreachEffortList[1].isScheduledClinics;
            opportunitiesData.outreachEffort.IP.outreachBusinessPk = this.opportunities[i].outreachEffortList[1].outreachBusinessPk;
            opportunitiesData.outreachEffort.IP.contactLogPk = this.opportunities[i].outreachEffortList[1].contactLogPk;
            opportunitiesData.outreachEffort.IP.clinicAgreementPk = this.opportunities[i].outreachEffortList[1].clinicAgreementPk;
            if (this.opportunities[i].outreachEffortList[0].lastContact == this.opportunities[i].outreachEffortList[1].lastContact) {
              opportunitiesData.lastContact = this.opportunities[i].outreachEffortList[0].lastContact;
            }
            if (new Date(this.opportunities[i].outreachEffortList[0].lastContact) > new Date(this.opportunities[i].outreachEffortList[1].lastContact)) {
              opportunitiesData.lastContact = this.opportunities[i].outreachEffortList[0].lastContact;
            }
            if (new Date(this.opportunities[i].outreachEffortList[1].lastContact) > new Date(this.opportunities[i].outreachEffortList[0].lastContact)) {
              opportunitiesData.lastContact = this.opportunities[i].outreachEffortList[1].lastContact;
            }
          }
          if (this.opportunities[i].outreachEffortList[1].outreachProgram == "SR") {
            opportunitiesData.outreachEffort.SR.outreachStatusTitle = this.opportunities[i].outreachEffortList[1].outreachStatusTitle;
            opportunitiesData.outreachEffort.SR.lastContact = this.opportunities[i].outreachEffortList[1].lastContact;
            opportunitiesData.outreachEffort.SR.outreachProgram = this.opportunities[i].outreachEffortList[1].outreachProgram;
            opportunitiesData.outreachEffort.SR.outreachStatus = this.opportunities[i].outreachEffortList[1].outreachStatus;
            opportunitiesData.outreachEffort.SR.isScheduledClinics = this.opportunities[i].outreachEffortList[1].isScheduledClinics;
            opportunitiesData.outreachEffort.SR.outreachBusinessPk = this.opportunities[i].outreachEffortList[1].outreachBusinessPk;
            opportunitiesData.outreachEffort.SR.contactLogPk = this.opportunities[i].outreachEffortList[1].contactLogPk;
            opportunitiesData.outreachEffort.SR.clinicAgreementPk = this.opportunities[i].outreachEffortList[1].clinicAgreementPk;
            if (this.opportunities[i].outreachEffortList[0].lastContact == this.opportunities[i].outreachEffortList[1].lastContact) {
              opportunitiesData.lastContact = this.opportunities[i].outreachEffortList[0].lastContact;
            }
            if (new Date(this.opportunities[i].outreachEffortList[0].lastContact) > new Date(this.opportunities[i].outreachEffortList[1].lastContact)) {
              opportunitiesData.lastContact = this.opportunities[i].outreachEffortList[0].lastContact;
            }
            if (new Date(this.opportunities[i].outreachEffortList[1].lastContact) > new Date(this.opportunities[i].outreachEffortList[0].lastContact)) {
              opportunitiesData.lastContact = this.opportunities[i].outreachEffortList[1].lastContact;
            }
          }
        }
        this.opportunitiesData_List.push(opportunitiesData);
      }
      let loopendTime: Date = new Date();

      this.opportunities_data = opportunities;
      this.ng4LoadingSpinnerService.hide();
      console.log(String.Format("Loop ending for UI binding at {0}:{1}:{2}:{3}", loopendTime.getHours(), loopendTime.getMinutes(), loopendTime.getSeconds(), loopendTime.getMilliseconds()));
    },
      error => {
        console.error("Error in fetching opportunities", error);
        this.ng4LoadingSpinnerService.hide();
      })
    // this.loader=false;
    // this.ng4LoadingSpinnerService.hide();
    //setTimeout(function () {

    //}.bind(this), 5000);
  }
  createRange(number) {
    var items: number[] = [];
    for (var i = 1; i <= number; i++) {
      items.push(i);
    }
    return items;
  }

  displayFieldCss1(index: any) {
    switch (index) {
      case 'Contact Client':
        this.css_color = "widget-info clearfix mb30 bg-cyan";
        break;
      case 'Complete/Report Clinic':
        this.css_color = "widget-info clearfix mb30 bg-green";
        break;
      case 'Follow-up':
        this.css_color = "widget-info clearfix mb30 bg-magenta";
        break;
      case 'Perform Clinic':
        this.css_color = "widget-info clearfix mb30 bg-yellow";
        break;
      case 'Schedule SR Events':
        this.css_color = "widget-info clearfix mb30 bg-lipurple";
        break;

    }
  }

  isOppDisabled(Opportunity: any) {
    return Opportunity.imz.indexOf('No Client Interest') > -1 || Opportunity.imz.indexOf('Business Closed') > -1;
  }
  getColor(status: string, outreach_type: string) {
    if (outreach_type == "imz") {
      return (status.indexOf("Contract Initiated") > -1 || status.indexOf("Contract Sent") > -1) ? 'green' : '';
    }
    else if (outreach_type == "so")
      return (status.indexOf("Event Scheduled") > -1) ? 'green' : ((status.indexOf("No Outreach") > -1) ? 'red' : '');
  }

  getColor1(status: string, contact_date: string) {
    this.firstDate = new Date();
    this.secondDate = new Date(contact_date);
    if (contact_date != null && contact_date != "") {
      if (status == null) {
        status = "No Outreach";
      }
      if (status.indexOf("Contract Initiated") > -1 || status.indexOf("Contract Sent") > -1)
        return '#3ba401';
      else if (status.indexOf("Contract Rejected") > -1)
        return '#cf003c'
      else if (this.calculateDays(this.firstDate, this.secondDate) >= 14)
        return '#cf003c';
      else if (this.calculateDays(this.firstDate, this.secondDate) >= 7 && this.calculateDays(this.firstDate, this.secondDate) <= 13)
        return '#fea102';
    }
    else {
      this.secondDate = new Date(this.outreachStartDateIP);
      if (this.calculateDays(this.firstDate, this.secondDate) >= 14)
        return '#cf003c';
      else if (this.calculateDays(this.firstDate, this.secondDate) >= 7 && this.calculateDays(this.firstDate, this.secondDate) <= 13)
        return '#fea102';
    }
  }

  getColor2(status: string, contact_date: string) {
    this.firstDate = new Date();
    this.secondDate = new Date(contact_date);

    if (contact_date != null && contact_date != "") {
      if (status == null) {
        status = "No Outreach";
      }
      if (status.indexOf("Contact Client") > -1 && this.calculateDays1(this.firstDate, this.secondDate) >= 17)
        return 'bg-success';
      else if (status.indexOf("Contact Client") > -1 && this.calculateDays1(this.firstDate, this.secondDate) <= 17)
        return 'bg-danger';
      else if (status.indexOf("Confirmed") > -1 && this.calculateDays(this.firstDate, this.secondDate) <= 14 && this.calculateDays1(this.firstDate, this.secondDate) <= 17)
        return 'bg-success';
      else if (status.indexOf("Completed") != -1 && this.calculateDays(this.firstDate, this.secondDate) >= 14)
        return 'bg-success';
      else if (status.indexOf("Cancelled") != -1)
        return 'bg-success';

    }
  }

  calculateDays(date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date1_ms - date2_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
  }
  calculateDays1(date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
  }
  opportunityClick() {
  }
  showDialog(opp: any) {
    SessionDetails.SetAgreementPK(0);
    SessionDetails.SetFollowUp(false);
    this.contract_initiated = false;
    this.community_outreach = false;
    this.contract_followup = false;
    this.opportunitie = opp;
    SessionDetails.opportunitiesData(opp);
    if (this.opportunitie.outreachEffort.IP.outreachStatus == '3' && !this.opportunitie.outreachEffort.IP.isScheduledClinics) {
      this.contract_initiated = true;
    }
    else if (this.opportunitie.outreachEffort.IP.outreachStatus == '12' && !this.opportunitie.outreachEffort.IP.isScheduledClinics) {
      this.community_outreach = true;
    }
    if (this.opportunitie.outreachEffort.IP.outreachStatus == '2' || this.opportunitie.outreachEffort.SR.outreachStatus == '2') {
      this.contract_followup = true;
    }
    this.display = true;
  }

  showEvents(opp: any) {
    this.event_details = opp;
    SessionDetails.EventDetails(this.event_details);
    if (this.event_details.clinicType == "Event Scheduled") {
      this.router.navigateByUrl('/communityoutreach/scheduleevent');
    }
    if (this.event_details.clinicType == "Local") {
      this.router.navigateByUrl('/communityoutreach/LocalClinicProgramDetails');
    }
    if (this.event_details.clinicType == "Community Outreach") {
      this.router.navigateByUrl('/communityoutreach/CommunityOutreachProgramDetails');
    }
  }
  ContactLogInfo(id) {
    SessionDetails.SetBusinessType(id);
    this.router.navigateByUrl("/communityoutreach/viewcontactlog");
  }
  showDialogOptConLog() {
    this._opportunityService.getOutreachStatus().subscribe((outreach_status) => {
      this.outreach_status_data = outreach_status.filter(item => (item.outreachStatusId != 0 && item.outreachStatus != 'Active' && item.isActive == true));

      var immunization_status = ['3', '8', '12'];
      this.job_title = false;
      this.display_message = false;
      this.outreach_status_value = "";
      this.dispContractInitiated = false;
      this.business_name = this.opportunitie.businessName;
      if ((this.opportunitie.outreachEffort.IP.isScheduledClinics && this.opportunitie.outreachEffort.SR.outreachProgram == null) ||
        (immunization_status.indexOf(this.opportunitie.outreachEffort.IP.outreachStatus) > -1 && this.opportunitie.outreachEffort.SR.outreachProgram == null)) {
        this.display = false;
        this.dispContractInitiated = true;
      }
      else {
        this.outreach_type = this._opportunityService.getOutreachType();
        // if (this.opportunitie.outreachEffort.split(',').length - 1 >= 2) {

        if (this.opportunitie.outreachEffort.IP.outreachProgram != null
          && this.opportunitie.outreachEffort.SR.outreachProgram != null) {
          this.outreach_type_data = this._opportunityService.getOutreachType();
        }
        else {
          //this.outreach_type_data = this.outreach_type.filter(item => item.category == this.opportunitie.outreachEffort.split(',')[1]);
          let outreachValue;
          if (this.opportunitie.outreachEffort.SR.outreachProgram != null) {
            outreachValue = this.opportunitie.outreachEffort.SR.outreachProgram;

          }
          if (this.opportunitie.outreachEffort.IP.outreachProgram != null) {
            outreachValue = this.opportunitie.outreachEffort.IP.outreachProgram;
          }
          this.outreach_type_data = this.outreach_type.filter(item => item.category == outreachValue);
          this.outreach_status_value = this.outreach_type_data[0].outreachStatus;

        }

        this.logcontact = this._fb.group({
          select_outreach: this._fb.control('', Validators.required),
          businessName: this._fb.control(this.opportunitie.businessName, null),
          firstName: this._fb.control(this.opportunitie.firstName, null),
          lastName: this._fb.control(this.opportunitie.lastName, null),
          jobTitle: this._fb.control(this.opportunitie.jobTitle, null),
          defaultDate: this._fb.control(this.defaultDate, null),
          outreachstatus: this._fb.control('', Validators.required),
          feedback: this._fb.control('', null)
        });
        this.outreachstatus = 0;
        this.business_contacted = this.opportunitie.businessName;
        this.dialogOptConLog = true;
        this.display = false;
        if (this.outreach_status_value != '') {
          this.onChange(this.outreach_type_data[0].category);
        }
      }
    });
  }
  onChange(select_outreach: any) {
    this.display_message = false;
    var immunization_status = [3, 8, 12];
    this.job_title = false;
    if (this.outreach_status_value != '') {
      this.logcontact = this._fb.group({
        select_outreach: this._fb.control(select_outreach, Validators.required),
        businessName: this._fb.control(this.opportunitie.businessName, null),
        firstName: this._fb.control(this.opportunitie.firstName, null),
        lastName: this._fb.control(this.opportunitie.lastName, null),
        jobTitle: this._fb.control(this.opportunitie.jobTitle, null),
        defaultDate: this._fb.control(this.defaultDate, null),
        outreachstatus: this._fb.control('', Validators.required),
        feedback: this._fb.control('', null)
      });
    }
    this.outreach_status = this._sortByPipe.sortBy(this.outreach_status_data.filter(
      item => {
        return (item.outreachProgram == select_outreach
          && item.outreachStatusId != 9 && item.outreachStatusId != 10 && item.isActive == true
        )
      }), "sortOrder");

    // if (this.opportunitie.outreachStatus.split(',').length - 1 >= 2) {
    if (this.opportunitie.outreachEffort.IP.outreachProgram != null && this.opportunitie.outreachEffort.SR.outreachProgram != null) {
      if (select_outreach == 'SR')
        this.outreach_status_info = this.opportunitie.outreachEffort.SR.outreachStatus;
      else
        this.outreach_status_info = this.opportunitie.outreachEffort.IP.outreachStatus;
    }
    else {
      if (this.opportunitie.outreachEffort.IP.outreachProgram != null) {
        this.outreach_status_info = this.opportunitie.outreachEffort.IP.outreachStatus;
      }
      if (this.opportunitie.outreachEffort.SR.outreachProgram != null) {
        this.outreach_status_info = this.opportunitie.outreachEffort.SR.outreachStatus;
      }
    }
    // if (this.outreach_status_info == "0" || this.outreach_status_info == null) {
    //   this.logcontact.controls['outreachstatus'].setValue("");
    // }
    // else {
    //   this.logcontact.controls['outreachstatus'].setValue(this.outreach_status_info);
    // }
    this.logcontact.controls['outreachstatus'].setValue("");
    // if (this.outreach_status_info == '4') {
    //   this.job_title = true;
    // }
    //this.logcontact.value.outreachstatus = this.outreach_status.filter(item => item.outreachStatusId == this.outreach_status_info);
    if (immunization_status.indexOf(this.outreach_status_info) > -1 && select_outreach != 'SR') {
      this.display_message = true;
    }
  }
  disableButton() {
    if (this.display_message) {
      return 'false';
    }
    else {
      return '';
    }

  }
  onOutreachStatusChange(outreach_pk: any) {
    this.job_title = false;
    this.logcontact.value.outreachstatus = this.outreach_status.filter(item => item.outreachStatusId == outreach_pk);
    this.outreach_status_info = outreach_pk;
    if (this.outreach_status_info == '4') {
      this.job_title = true;
    }
  }
  redirectToPage(outreach_status) {
    SessionDetails.SetAgreementPK(this.opportunitie.outreachEffort.IP.clinicAgreementPk);
    if (outreach_status == '3')
      this.router.navigateByUrl("/communityoutreach/contract");
    else if (outreach_status == '12')
      this.router.navigateByUrl("/communityoutreach/communityoutreach");
  }
  onSubmit() {
    this.store_profile = SessionDetails.GetStoreId();
    this.logcontact = this._fb.group({
      select_outreach: this._fb.control(this.logcontact.value.select_outreach, Validators.required),
      businessName: this._fb.control(this.opportunitie.businessName, null),
      firstName: this._fb.control(this.logcontact.value.firstName, null),
      lastName: this._fb.control(this.logcontact.value.lastName, null),
      jobTitle: this._fb.control(this.logcontact.value.jobTitle, null),
      defaultDate: this._fb.control(this.logcontact.value.defaultDate, null),
      outreachstatus: this._fb.control(this.logcontact.value.outreachstatus, Validators.required),
      feedback: this._fb.control(this.logcontact.value.feedback, null)
    });
    if (this.logcontact.valid) {
      this.logcontact.controls["feedback"].setValidators(null);
      this.logcontact.value.outreachstatus = this.outreach_status.filter(item => item.outreachStatusId == this.outreach_status_info);

      if (this.logcontact.value.outreachstatus[0].outreachStatusId > 0) {
        if (this.logcontact.value.outreachstatus[0].outreachStatusId == 4) {
          this.logcontact = this._fb.group({
            select_outreach: this._fb.control(this.logcontact.value.select_outreach, Validators.required),
            businessName: this._fb.control(this.opportunitie.businessName, null),
            firstName: this._fb.control(this.logcontact.value.firstName, Validators.required),
            lastName: this._fb.control(this.logcontact.value.lastName, Validators.required),
            jobTitle: this._fb.control(this.logcontact.value.jobTitle, Validators.required),
            defaultDate: this._fb.control(this.logcontact.value.defaultDate, null),
            outreachstatus: this._fb.control(this.logcontact.value.outreachstatus, Validators.required),
            feedback: this._fb.control(this.logcontact.value.feedback, Validators.required)
          });
          if (this.logcontact.valid) {
          }
          else {
            this.utility.validateAllFormFields(this.logcontact);
            if (this.outreach_status_info == "0") {
              this.logcontact.controls['outreachstatus'].setValue("");
            }
            else {
              this.logcontact.controls['outreachstatus'].setValue(this.outreach_status_info);
            }
          }

        }
        var log_outreach_status = new FollowUp();
        log_outreach_status.logOutreachStatus.businessPk = this.opportunitie.businessPk;
        log_outreach_status.logOutreachStatus.firstName = this.logcontact.value.firstName;
        log_outreach_status.logOutreachStatus.lastName = this.logcontact.value.lastName;
        log_outreach_status.logOutreachStatus.jobTitle = this.logcontact.value.jobTitle;
        log_outreach_status.logOutreachStatus.feedback = this.logcontact.value.feedback;
        log_outreach_status.logOutreachStatus.contactDate = this.logcontact.value.defaultDate;
        if (this.logcontact.value.select_outreach == "IP") {
          log_outreach_status.logOutreachStatus.outreachBusinessPk = this.opportunitie.outreachEffort.IP.outreachBusinessPk;
          log_outreach_status.logOutreachStatus.outreachProgram = this.logcontact.value.outreachstatus[0].outreachProgram;
          log_outreach_status.logOutreachStatus.outreachStatusTitle = this.logcontact.value.outreachstatus[0].outreachStatus;
          log_outreach_status.logOutreachStatus.outreachStatusId = this.logcontact.value.outreachstatus[0].outreachStatusId;
        }
        else if (this.logcontact.value.select_outreach == "SR") {
          log_outreach_status.logOutreachStatus.outreachBusinessPk = this.opportunitie.outreachEffort.SR.outreachBusinessPk;
          log_outreach_status.logOutreachStatus.outreachProgram = this.logcontact.value.outreachstatus[0].outreachProgram;
          log_outreach_status.logOutreachStatus.outreachStatusTitle = this.logcontact.value.outreachstatus[0].outreachStatus;
          log_outreach_status.logOutreachStatus.outreachStatusId = this.logcontact.value.outreachstatus[0].outreachStatusId;
        }

        log_outreach_status.logOutreachStatus.createdBy = 41;

        if (this.logcontact.value.outreachstatus[0].outreachStatusId == 3 && this.logcontact.value.outreachstatus[0].outreachProgram == 'IP') {
          if (this.opportunitie.outreachEffort.IP.isScheduledClinics) {
            alert('An Immunization Event has already been scheduled for this outreach opportunity. Please log contacts regarding the event under the Event Details.');
          }
          else {
            this.logContactPrograms(log_outreach_status, 3);
          }
        }
        else if (this.logcontact.value.outreachstatus[0].outreachProgram == 'IP' && this.logcontact.value.outreachstatus[0].outreachStatusId == 8) {
          SessionDetails.EventDetails(log_outreach_status);
          this.router.navigateByUrl("/communityoutreach/charityprogram");
          //this.logContactPrograms(log_outreach_status, 8);
        }
        else if (this.logcontact.value.outreachstatus[0].outreachProgram == 'IP' && this.logcontact.value.outreachstatus[0].outreachStatusId == 12) {
          SessionDetails.EventDetails(log_outreach_status);
          this.router.navigateByUrl("/communityoutreach/communityoutreach");
          //this.logContactPrograms(log_outreach_status, 12);
        }
        else if (this.logcontact.value.outreachstatus[0].outreachProgram == 'SR' && this.logcontact.value.outreachstatus[0].outreachStatusId == 3) {
          SessionDetails.EventDetails(log_outreach_status);
          this.router.navigateByUrl("/communityoutreach/scheduleevent");
        }
        else if ((this.logcontact.value.outreachstatus[0].outreachProgram == 'SR' || this.logcontact.value.outreachstatus[0].outreachProgram == 'IP') && this.logcontact.value.outreachstatus[0].outreachStatusId == 5) {
          this.logContact(log_outreach_status);
        }
        else if ((this.logcontact.value.outreachstatus[0].outreachProgram == 'SR' || this.logcontact.value.outreachstatus[0].outreachProgram == 'IP') && this.logcontact.value.outreachstatus[0].outreachStatusId == 4) {
          this.dialogOptConLog = false;
          this.logContact(log_outreach_status);
        }
        else if (this.logcontact.value.outreachstatus[0].outreachProgram == 'SR' && this.logcontact.value.outreachstatus[0].outreachStatusId == 1) {
          this.dialogOptConLog = false;
          this.logContact(log_outreach_status);

        }
        else if ((this.logcontact.value.outreachstatus[0].outreachProgram == 'SR' || this.logcontact.value.outreachstatus[0].outreachProgram == 'IP') && this.logcontact.value.outreachstatus[0].outreachStatusId == 2) {

          if (this.logcontact.value.outreachstatus[0].outreachProgram == 'SR') {
            SessionDetails.SetProgramType(ProgramType.SR);
          }
          if (this.logcontact.value.outreachstatus[0].outreachProgram == 'IP') {
            SessionDetails.SetProgramType(ProgramType.IP);
          }
          SessionDetails.SetLogContact(log_outreach_status);
          this.router.navigateByUrl("/communityoutreach/followUp");
        }
      }
      else {
        this.error_message = "Outreach status is required";
      }
    }
    else {
      this.utility.validateAllFormFields(this.logcontact);
    }

  }
  logContact(log_outreach_status) {
    this.schedule_event_service.postSimpleUpEvent(log_outreach_status).subscribe((data: any) => {
      if (data.status == "200") {
        this.dialogOptConLog = false;
        this.message = "The Business Contact was logged successfully!";
        this.getAssignedBusiness(this.store_profile);
        this.message_service.sendProfileChange("profilechange");
      }
    });
  }
  logContactPrograms(log_outreach_status, outreach_status) {
    this.schedule_event_service.postSimpleUpEvent(log_outreach_status).subscribe((data: any) => {
      if (data.status == "200") {
        if (outreach_status == 3) {
          this.dialogOptConLog = false;
          this.message_service.sendProfileChange("profilechange");
          SessionDetails.SetAgreementPK(data.json() != null ? data.json().logOutreachStatus.clinicAgreementPk : 0);
          this.router.navigateByUrl("/communityoutreach/contract");
        }
        else if (outreach_status == 8) {
          this.dialogOptConLog = false;
          this.message_service.sendProfileChange("profilechange");
          SessionDetails.SetAgreementPK(data.json() != null ? data.json().logOutreachStatus.clinicAgreementPk : 0);
          this.router.navigateByUrl("/communityoutreach/charityprogram");
        }
        else if (outreach_status == 12) {
          this.dialogOptConLog = false;
          this.message_service.sendProfileChange("profilechange");
          SessionDetails.SetAgreementPK(data.json() != null ? data.json().logOutreachStatus.clinicAgreementPk : 0);
          this.router.navigateByUrl("/communityoutreach/communityoutreach");
        }
      }
    });
  }
  isFieldValid(field: string) {
    return !this.logcontact.get(field).valid && this.logcontact.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }
  selectName(sort_by) {
    if (sort_by == 1)
      this.opportunitiesData_List = this._sortByPipe.sortBy(this.opportunitiesData_List, "employmentSize");
    else if (sort_by == 2)
      this.opportunitiesData_List = this._sortByPipe.sortBy(this.opportunitiesData_List, "isPreviousClient");
    else if (sort_by == 3)
      this.opportunitiesData_List = this._sortByPipe.sortBy(this.opportunitiesData_List, "lastContact");
    else if (sort_by == 4)
      this.opportunitiesData_List = this._sortByPipe.sortBy(this.opportunitiesData_List, "lastContact", "latest");
    else if (sort_by == 5) {
      for (let i = 0; i < this.opportunities.length; i++) {
        this.opportunities1 = this._sortByPipe.sortBy(this.opportunitiesData_List, "outreachEffort");
        this.opportunitiesData_List = this.opportunities1;
      }
    }
    else
      this.opportunities = this.opportunities_data;
  }

  showDialogInfo() {
    this.displayInfoDlg = true;
  }
  showErrorDialog(msgDetail: string) {
    this.dialogMsg = msgDetail;
    this.state_specific_display = true;
  }
  addBusiness() {
    var errMsg: string[] = [];
    var resrict_states = ["MO", "DC"];
    var current_date = new Date();
    this.state_specific = SessionDetails.GetState();
    this.state_specific_display = false;
    if (resrict_states.indexOf(this.state_specific) > -1) {
      if (current_date > new Date(environment.MO_State_From) && current_date < new Date(environment.MO_State_TO)) {
        let sMsg = null;
        if (this.state_specific == "MO") {
          errMsg.push(String.Format(ErrorMessages['restrictedStates'], "Missouri", "20"));
          sMsg = errMsg;
          this.showErrorDialog(sMsg);
        }
        else if (this.state_specific == "DC") {
          errMsg.push(String.Format(ErrorMessages['restrictedStates'], "Columbia ", "15"));
          sMsg = errMsg;
          this.showErrorDialog(sMsg);
        }
      }
      else {
        SessionDetails.SetActionType(ActionType.addOportunity);
        this.router.navigateByUrl('/communityoutreach/opportunity');
      }
    }
    else {
      SessionDetails.SetActionType(ActionType.addOportunity);
      this.router.navigateByUrl('/communityoutreach/opportunity');
    }
  }
  onContactDateSelected(selectedDate: Date) {
    this.logcontact.controls["defaultDate"].setValue(new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate(), 12, 30, 55));
  }
  okClicked() {
    this.state_specific_display = false;
  }
  EditBusiness() {
    SessionDetails.SetActionType(ActionType.editOportunity);
    this.edit_buisness_data = this.opportunitie;
    SessionDetails.opportunitiesData(this.edit_buisness_data);
    this.router.navigateByUrl('/communityoutreach/opportunity');
  }
  EventDetails() {

    if (this.event_details.clinicType == "Event Scheduled") {
      this.router.navigateByUrl('/communityoutreach/scheduleevent');
    }
  }

  followUpReminders() {
    SessionDetails.SetFollowUp(true);
    if (this.opportunitie.outreachEffort.SR.outreachStatus == '2') {
      SessionDetails.SetProgramType(ProgramType.SR);
    }
    if (this.opportunitie.outreachEffort.IP.outreachStatus == '2') {
      SessionDetails.SetProgramType(ProgramType.IP);
    }
    if (this.opportunitie.outreachEffort.SR.outreachStatus == '2' && this.opportunitie.outreachEffort.IP.outreachStatus == '2') {
      SessionDetails.SetProgramType(ProgramType.SRIP);
    }
    this.router.navigateByUrl('/communityoutreach/followUp');
  }
  deleteDNC() {
    if (this.selectedDataList.length > 0) {
      this.displayDNC = true;
    }
  }

  cancelConfirmation() {
    this.displayDNC = false;
  }
  deleteConfirmationDNC() {

    if (this.selectedDataList.length > 0) {
      for (var i = 0; i < this.selectedDataList.length; i++) {
        this.selectedDNCData.push(this.selectedDataList[i].pk);
      }
      this._opportunityService.deleteNationalContractClients(this.selectedDNCData).subscribe((data: any) => {
        if (data.status == "200") {
          this._opportunityService.getNationalContractClients().subscribe((res) => {
            this.dnc_clients = res
          });
          this.displayDNC = false;
          this.selectedDataList = [];
        }
        else {
          // alert("Issue with deleting");

        }
      });
    }
    this.displayDNC = false;

  }
}