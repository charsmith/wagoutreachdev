import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreOpportunityComponent } from './store-opportunity.component';

describe('StoreOpportunityComponent', () => {
  let component: StoreOpportunityComponent;
  let fixture: ComponentFixture<StoreOpportunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreOpportunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreOpportunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
