import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from "@angular/router";
import { environment } from '../../../../environments/environment';
import { Util } from '../../../utility/util';
import { EncryptDecrypt } from '../../../models/encryptDecrypt';


@Injectable()
export class AuthenticationService {

  constructor(private http: Http, private router: Router) { }

  getLoginUser(encodedData: string): Observable<any> {
    var url = environment.API_URL + environment.RESOURCES.VERIFY_USER_GET + "/" + encodedData;
    
    return this.http.get(url).map((res) => res.json()).catch((err) => Util.handleError(err, this.router));
}

postEncryptionDecryption(encryptDecrypt: EncryptDecrypt): Observable<any> {
  var url = environment.API_URL + environment.RESOURCES.POST_ENCRYPTION_DECRYPTION;  
  return this.http.post(url, encryptDecrypt,Util.getRequestHeaders()).catch((err) => Util.handleError(err, this.router));
}

getUserProfile(): Observable<any> {
  var url = environment.API_URL + environment.RESOURCES.GET_USER_PROFILE;  
  return this.http.get(url,Util.getRequestHeaders()).map((res) => res.json()).catch((err) => Util.handleError(err, this.router));
}

}
