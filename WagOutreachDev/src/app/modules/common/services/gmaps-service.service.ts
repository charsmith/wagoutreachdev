import { Injectable } from '@angular/core';
// import { GoogleMapsAPIWrapper } from '@agm/core';
// import { MapsAPILoader } from '@agm/core';
import { Observable, Observer } from 'rxjs';

declare var google: any;

@Injectable()
export class GMapsService  { //extends GoogleMapsAPIWrapper

  constructor() { //private __loader: MapsAPILoader, private __zone: NgZone
//    super(__loader, __zone);
}
getLatLan(address: string) {
  console.log('Getting Address - ', address);

  let geocoder = new google.maps.Geocoder();
  return Observable.create(observer => {
      geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
              observer.next(results[0].geometry.location);
              observer.complete();
          } else {
              console.log('Error - ', results, ' & Status - ', status);
              observer.next({});
              observer.complete();
          }
      });
  })
}

getGeoCoding(address:string){
 let geocoder:any = new google.maps.Geocoder();
 return Observable.create(observer => {
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            observer.next(results[0].geometry.location);
            observer.complete();
        } else {
            console.log('Error - ', results, ' & Status - ', status);
            observer.next({});
            observer.complete();
        }
    });
})
}
/*getGeocoding(address: string) {
    return Observable.create(observer => {
      try {
          //at this point the variable google may be still undefined (google maps scripts still loading)
          //so load all the scripts, then...
        this.__loader.load().then(() => {
            
              let geocoder = new google.maps.Geocoder();
              geocoder.geocode({ address }, (results, status) => {
                console.log('Getting Address - ', address);
                  if (status === google.maps.GeocoderStatus.OK) {
                      const place = results[0].geometry.location;
                      observer.next(place);
                      observer.complete();
                      debugger
                  } else {
                      console.error('Error - ', results, ' & Status - ', status);
                      if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                          observer.error('Address not found!');
                      }else {
                          observer.error(status);
                      }
                      debugger
                      observer.complete();
                  }
              });
          });
      } catch (error) {
        debugger
          observer.error('error getGeocoding' + error);
          observer.complete();
      }

  });
}*/

}
