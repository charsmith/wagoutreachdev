import { FormGroup, FormControl } from "@angular/forms";
import { ErrorHandler, Injectable, Injector } from "@angular/core";
import { Headers, Http, Response    } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { LogData } from "../../../models/logData";


@Injectable()
export class LoggerService {
  constructor(private http: Http) { 
  
  }
  log(error)
  {
   console.log(error);
  }
  logMessage(logData:LogData): Observable<any>
  {
    let url = "http://localhost:49940/api/values/logData";
    return this.http.post(url, logData).map((res) => res.json()).catch(this.handleError);
    //return this.http.post(url, opportunity).map((res) => Util.extractData(res)).catch((err) => Util.handleError(err, this.router));
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    //console.warn(errMsg);
    return Observable.throw(errMsg);
  }
  }
  


