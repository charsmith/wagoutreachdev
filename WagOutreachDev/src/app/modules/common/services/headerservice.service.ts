import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { adminmenuItems, storeManagermenuItems, districtManagermenuItems, healthcareSupervisormenuItems, regionalVicePresidentmenuItems, directorRxRetailmenuItems } from '../../../JSON/Menu';
import { distirctManger } from '../../../JSON/districtmanager';
import { Http, Response, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { storesArray } from '../../../JSON/Stores';
import { StoreProfile } from '../../../models/storeProfile';
import { HintMessage } from '../../../JSON/hintMessages';
import { Util } from '../../../utility/util';
import { String, StringBuilder } from 'typescript-string-operations';
import { adminFooterMenuItems, storeManagerFooterMenuItems, districtManagerFooterMenuItems, healthcareSupervisorFooterMenuItems, directorRxRetailFooterMenuItems, regionalVicePresidentFooterMenuItems } from '../../../JSON/footerMenu';

//import { menuItems } from  './../../../JSON/Menu';

@Injectable()
export class HeaderserviceService {
  baseUrl1 = 'http://192.168.2.118:55/';
  constructor(private http: Http, private store_profile: StoreProfile) { }
  getAdminMenuItems() {
    return adminmenuItems;
  }
  getStoreManagerMenuItems() {
    return storeManagermenuItems;
  }
  getDistrictManagerMenuItems() {
    return districtManagermenuItems;
  }
  getHcsManagerMenuItems() {
    return healthcareSupervisormenuItems;
  }
  getRvpMenuItems() {
    return regionalVicePresidentmenuItems;
  }
  getDirectorRxRetailMenuItems() {
    return directorRxRetailmenuItems;
  }
  getStoreData() {
    return distirctManger;
  }
  getAdminFooterMenu() {
    return adminFooterMenuItems;
  }
  getStoreManagerFooterMenu() {
    return storeManagerFooterMenuItems;
  }

  getDistrictManagerFooterMenu() {
    return districtManagerFooterMenuItems;
  }

  getHealthcareSupervisorFooterMenu() {
    return healthcareSupervisorFooterMenuItems;
  }

  getDirectorRxRetailFooterMenu() {
    return directorRxRetailFooterMenuItems;
  }
  getRegionalVicePresidentFooterMenu() {
    return regionalVicePresidentFooterMenuItems;
  }


  getStoresList(user_id: number, store_id: string): Observable<any> {
    //  let url = this.baseUrl + '/stores';  
    return this.http.get("http://localhost:49940/api/values/GetStoreData?store_id=" + store_id + "")
      .map((response: Response) => response.json());
  }

  storeSearch(user_pk: number, store_search: string, store_id: number): Observable<any> {
    if (store_search.length >= 2) {
      let startTime: Date = new Date();
      console.log(String.Format("Started calling storeSearch at {0}:{1}:{2}:{3}", startTime.getHours(), startTime.getMinutes(), startTime.getSeconds(), startTime.getMilliseconds()));

      let url = environment.API_URL + environment.RESOURCES.GET_STORE_PROFILE + "/" + user_pk + "/" + store_search + "/" + 0;
      return this.http.get(url, Util.getRequestHeaders())
        .map(this.extractData)
        .catch(this.handleError);
    }
    return Observable.of([]);
  }

  private extractData(res: Response) {
    let body = res.json();
    let startTime: Date = new Date();
    console.log(String.Format("Ending storeSearch at {0}:{1}:{2}:{3}", startTime.getHours(), startTime.getMinutes(), startTime.getSeconds(), startTime.getMilliseconds()));

    return body.data;
  }
  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
  getStoresProfile(user_pk: number, store_search: string, store_id: number) {
    let url = environment.API_URL + environment.RESOURCES.GET_STORE_PROFILE + "/" + user_pk + "/" + store_search + "/" + store_id;
    return this.http.get(url, Util.getRequestHeaders())
      .map((response: Response) => response.json());
  } 

  getWagsOutreachCounts(): Observable<any> {
    let url = environment.API_URL + environment.RESOURCES.GET_WAGSOUTREACH_COUNTS;
    return this.http.get(url, Util.getRequestHeaders()).map((response: Response) => response.json());
  }
}
