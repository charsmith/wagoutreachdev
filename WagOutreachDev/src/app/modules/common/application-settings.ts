import { environment } from "../../../environments/environment";

export class ApplicationSettings {
    report_start_year: string;
    constructor() {

    }
   public getOutreachReportStartYear(): string {
     this.report_start_year = environment.reportStartYear;
     return this.report_start_year;
}
}


