export class PreviousClinicLocation
{
    naClinicAddress1: string;
    naClinicAddress2: string;
    naClinicCity: string;
    naClinicState: string;
    naClinicZip: string;
    naClinicAddress: string;
    isRemoved: number;
    rowId: number;
}