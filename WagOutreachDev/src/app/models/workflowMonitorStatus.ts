export class WorkflowMonitorStatus
{
    outreachStatusId: number;
    outreachStatus: string;
    statusCountByStore: number;
    statusCountByDist: number;
}