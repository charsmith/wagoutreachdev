export class ContactLog
{
    outreachBusinessPk: number;
    outreachPk: number;
    outreachEffort: string;
    outreachEffortCode: string;
    storeId: number;
    businessPk: number;
    contactLogPk: number;
    contactName: string;
    feedback: string;
    outreachStatusId: number;
    outreachStatus: string;
    contactDate: Date;
    created: Date;
    dateDeleted: Date;
    isPreviousSeasonLog: boolean;
    createdBy: string;
    businessType: number;
    clinicLocation: string;
    isDeleted: boolean;
    deletedBy: string;
    immunizationName:string;
    totalImmAdministered: number;
    totalHours:  number;
}