export class AddBusiness {
                 public businessId:Number = -1;
                 public BusinessName:string = '';
                 public Phone:string = '';
                 public FirstName:string = '';
                 public LastName:string = '';
                 public EMail:string = '';
                 public TollFree:string = '';
                 public Industry:string = '';
                 public JobTitle: string = '';
                 public WebsiteURL:string ='';
                 public Address1:string ='';
                 public EmploymentSize:Number=0;
                 public Address2:string ='';
                 public CharityHHSVoucher:Boolean=false;
                 public CommunityOutreach:Boolean=false;
                 public City:string='';
                 public ZipCode:string = '';
                 public ZipCode4:string = '';
                 public state:string = '';
                 public County:string="";
                 public SeniorOutreach:Boolean=false;
                 public ImmunizationProgram:Boolean=false;
                 public BusinessSelected:string;
                 public BusinessInfoSelected:Number=1;
                 public SeniorOutReachStore:string;
                 public ImmunizationProgramStore:string;
                 public ContractInitiated:Boolean = false;
                 public ContractRejected:Boolean = false;
                 public ContractSent:Boolean = false;
                 public ScheduledEvent:Boolean = false;
                 public addressmatchkey:string ="";
    constructor( ){}
    }