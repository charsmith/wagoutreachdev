export class ActivityReport {
    canExport: number;
    outputType: string;
    filterBy: number;
    regionNumber: number;
    areaNumber: number;
    districtNumber: number;
    outreachStartDate: string;
    timePeriod: string;
    outreachEffort: number;
    constructor(can_export: number, output_type: string, filter_by: number, regional_number: number, area_number: number, district_number: number, outreach_start_date: string, time_period: string, outreach_effort: number) {
        this.canExport = can_export;
        this.outputType = output_type;
        this.filterBy = filter_by;
        this.regionNumber = regional_number;
        this.areaNumber = area_number;
        this.districtNumber = district_number;
        this.outreachStartDate = outreach_start_date;
        this.timePeriod = time_period;
        this.outreachEffort = outreach_effort;
    }
}