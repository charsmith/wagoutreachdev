export class StoreUserEmail {
    storeID: string;
    address: string;
    storeManagerEmail: string;
    pharmacyManagerEmail: string;
    districtManagerEmail: string;
    healthcareSupervisorEmail: string;
    directorRetailOpsEmail: string;
}