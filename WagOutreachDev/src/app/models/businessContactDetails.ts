export class BusinessContactDetail
{
    outreachBusinessPk: string;
    storeId: string;
    businessType: number;
    businessPk: string;
    businessName: string;
    firstName: string;
    lastName: string;
    address: string;
    address2: string;
    city: string;
    state: string;
    zip: string;
    phone: string;
    website: string;
    tollfree: string;
    industry: string;
    latitude: string;
    longitude: string;
    actualLocationEmploymentSize: string;
    isStandardized: string;
    jobTitle: string;
    businessContactEmail: string;
}