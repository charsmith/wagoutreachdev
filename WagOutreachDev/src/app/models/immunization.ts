export class Immunization
{
    immunizationId: number;
    immunizationName: string;
    immunizationSpanishName: string;
    paymentOption: number;
    price: number;
    directBillPrice: number;
    isActive: boolean;
    paymentTypeId: number;
    paymentTypeName: string;
    paymentTypeSpanishName: string;
    paymentGroup: number;
    isSelected: string;
    isPaymentSelected: string;
    name: string;
    address1: string;
    address2: string;
    city: string;
    state: string;
    zip: string;
    phone: string;
    email: string;
    tax: string;
    copay: string;
    copayValue: string;
    isVoucherNeeded: string;
    voucherExpirationDate: string;
    showprice: number;
}