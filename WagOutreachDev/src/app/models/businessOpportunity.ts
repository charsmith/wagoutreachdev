export class BusinessOpportunityModel
{
    businessId: number;
    businessName: string;
    Industry: string;
    firstName: string;
    lastName: string;
    jobTitle: string;
    phone: string;
    address: string;
    address2: string;
    state: string;
    zip: string;
    zip4: string;
    isWalgreensBusiness: boolean;
    employmentSize: number;
    contacts: string;
    isPreviousClient: boolean;
    isB2BSmallBusiness: boolean;
    isDirectB2BMail: boolean;  
    lastContact: Date;
    outreachStatus: OutreachStatus;
    outreachStatusId: OutreachStatusID; 
    sicCode: number; 
    sicPriority: number;
}

export class OutreachStatus
{
    IP:string;
    SO:string;
}

export class OutreachStatusID
{
    IP:number;
    SO:number;
}