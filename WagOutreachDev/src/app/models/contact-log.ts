export class ContactLog {
    outreachBusinessPk: number;
    outreachPk: number;
    outreachEffort: string;
    outreachEffortCode: string;
    storeId: number;
    businessPk: number;
    contactLogPk: number;
    contactName: string;
    feedback?: any;
    outreachStatusId: number;
    outreachStatus: string;
    contactDate: Date;
    created: Date;
    dateDeleted?: any;
    isPreviousSeasonLog: boolean;
    createdBy: string;
    businessType: number;
    clinicLocation?: any;
    isDeleted: boolean;
    deletedBy?: any;
    immunizationName?: any;
    totalImmAdministered?: any;
    totalHours?: any;
}