export class logOutreachStatus
{
    businessPk: number;
    firstName: string;
    lastName: string;
    jobTitle: string;
    outreachProgram: string;
    outreachBusinessPk: number;
    contactDate: string;
    outreachStatusId: number;
    outreachStatusTitle: string;
    feedback: string;
    createdBy: number;
    errorCode: number;
    errorMessage: string;
    clinicAgreementPk: number;
}