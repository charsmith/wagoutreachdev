export class LegalNoticeAddress {
    attentionTo: string = '';
    address1: string= '';
    address2: string= '';
    city: string= ''; 
    state: string= '';
    zipCode: string= '';
}
