    export class LocationDetail {
        ImmunizationDetails: ImmunizationDetail[];
        outreachBusinessPk: number;
        contractedStoreId: number;
        storeState: string;
        clinicType: string;
        clinicPk: number;
        clinicStoreId: number;
        clinicAgreementPk: number;
        clinicNumber: number;
        naClinicLocation: string;
        naContactFirstName: string;
        naContactLastName: string;
        naContactEmail: string;
        naClinicAddress1: string;
        naClinicAddress2: string;
        confirmedClientName?: any;
        naClinicCity: string;
        naClinicState: string;
        naClinicZip: string;
        naClinicContactPhone: string;
        clinicDate: Date;
        clinicScheduledOn: Date;
        naClinicStartTime: string;
        naClinicEndTime: string;
        naClinicEstimatedVolume?: any;
        isNoClinic: boolean;
        naClinicTotalImmAdministered?: any;
        naClinicVouchersDist?: any;
        contactLogPk: number;
        isPreviousSeasonLog: boolean;
        clinicAgreementXml: string;
        maxClinicLocationId: number;
        clinicLatitude?: any;
        clinicLongitude?: any;
        isReassign: number;
        isCompleted: boolean;
        isConfirmed: boolean;
        isCancelled: boolean;
        coPayFLUGroupId?: any;
        coPayROUTINEGroupId?: any;
        fluExpiryDate?: any;
        routineExpiryDate?: any;
        marketNumber: number;
        coOutreachTypeId?: any;
        coOutreachTypeDesc?: any;
        constructor(){           
            this.ImmunizationDetails = [];
           this.naClinicCity = "";
            
        }
    }

    export class ImmunizationDetail {
        pk: number;
        clinicPk: number;
        immunizationPk: number;
        immunizationName: string;
        price: number;
        estimatedQuantity: number;
        totalImmAdministered?: any;
        vouchersDistributed?: any;
        paymentTypeID: number;
        paymentTypeName: string;
        name: string;
        address1?: any;
        address2?: any;
        city?: any;
        state: string;
        zip?: any;
        phone?: any;
        email?: any;
        tax: string;
        isCoPay: string;
        copayValue?: any;
        isVoucherNeeded: string;
        voucherExpirationDate?: any;
    }

    export class ClinicBusinessInfo {
        displayName: string;
    }

    export class ClinicBillingInfo {
        displayName: string;
    }

    export class Field {
        displayName: string;
        value: string;
    }

    export class ClinicDetails2 {
        field: Field[];
        displayName: string;
    }

    export class PostClinicInfo {
        displayName: string;
    }

    export class ClinicUpdates {
        clinicBusinessInfo: ClinicBusinessInfo;
        clinicBillingInfo: ClinicBillingInfo;
        clinicDetails: ClinicDetails2;
        postClinicInfo: PostClinicInfo;
    }
    export class HistoryLogItem {
        date:Date;
        action:string;
        updatedField:string;
        update:string;
        user:string;
        constructor() {
            this.user="";
        }
    }
    export class HistoryLog {
        HistoryLogItems:HistoryLogItem[];
        constructor() {
            this.HistoryLogItems = [];
        }
    }

    export class StoreUserDetails {
        storeID: string;
        address: string;
        storeManagerEmail: string;
        pharmacyManagerEmail: string;
        districtManagerEmail: string;
        healthcareSupervisorEmail: string;
        directorRetailOpsEmail: string;
        constructor (){
            this.storeID="";
        }
    }

    export class BusinessInformation {
        businessName: string;
        businessAddress: string;
        businessAddress2: string;
        businessCity: string;
        businessState: string;
        businessZip: string;
        firstName: string;
        lastName: string;
        jobTitle: string;
        businessContactEmail: string;
        phone: string;
        constructor(){
            this.businessContactEmail="";
        }
    }

    export class BillingVaccineInformation {
        naClinicAddlComments?: any;
        naClinicSplBillingInstr?: any;
        recipientId: string;
        naClinicPlanId: string;
        naClinicGroupId: string;
        //ImmunizationDetails: ImmunizationDetail[];
        constructor(){
            this.recipientId= "";
           // this.ImmunizationDetails = [];
        }
    }

    export class PharmacistPostClinicInformation {
        pharmacistName?: any;
        pharmacistPhone: string;
        totalHours?: any;
        feedbackNotes:string;
        constructor() {
            this.pharmacistPhone='';
        }
    }   
  
    export class ClinicDetails {
        LocationDetails: LocationDetail[];
        HistoryLog: HistoryLog;
        StoreUserDetails: StoreUserDetails;
        BusinessInformation: BusinessInformation;
        BillingVaccineInformation: BillingVaccineInformation;
        PharmacistPostClinicInformation: PharmacistPostClinicInformation;
        constructor(){
            this.LocationDetails= new Array<LocationDetail>();
            this.HistoryLog = new HistoryLog();
            this.StoreUserDetails = new StoreUserDetails();
            this.BusinessInformation = new BusinessInformation();
            this.BillingVaccineInformation = new BillingVaccineInformation();
            this. PharmacistPostClinicInformation = new PharmacistPostClinicInformation();

        }

    }


