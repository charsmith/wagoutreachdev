    export class LogOutreachStatus {
        businessPk: number;
        firstName: string;
        lastName: string;
        jobTitle: string;
        outreachProgram: string;
        outreachBusinessPk: number;
        contactDate: Date;
        outreachStatusId: number;
        outreachStatusTitle: string;
        feedback: string;
        createdBy: number;
        clinicAgreementPk: number;
        contactLogPk: number;
    }

    export class ClinicImzQtyList {
        clinicPk: number;
        immunizationPk: number;
        immunizationName: string;
        paymentTypeId: number;
        estimatedQuantity: number;
        paymentTypeName: string;
        vouchersDistributed: number;
    }

    export class ClinicList {
        pk: number;
        location: string;
        contactFirstName: string;
        contactLastName: string;
        contactEmail: string;
        contactPhone: string;
        clinicDate: string='';
        startTime: string;
        endTime: string;
        address1: string;
        address2: string;
        city: string;
        state: string;
        zipCode: string;
        isNoClinic: boolean;
        isCurrent: boolean;
        clinicImzQtyList: ClinicImzQtyList[];
    }

    export class CharityVoucherModel {
        clinicAgreementPk: number;
        contactLogPk: number;
        isApproved: boolean;
        approvedOrRejectedBy: string;
        isEmailSent: boolean;
        dateCreated: Date;
        lastUpdatedDate: Date;
        contactWagUser: string;
        clinicList: ClinicList[];
    }

    export class ErrorS {
        errorCode: number;
        errorMessage: string;
    }

    export class CharityProgram {
        //public clinicList: Array<ClinicList> = new Array<ClinicList>();
        logOutreachStatus: LogOutreachStatus;
        charityVoucherModel: CharityVoucherModel;
        errorS: ErrorS;
    }

