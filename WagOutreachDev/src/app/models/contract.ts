export class Immunization {
    public clinicPk:number = 0;
    public immunizationPk: string = '';
    public immunizationName: string = '';
    public paymentTypeName: string = '';
    public paymentTypeId: string = '';
    public estimatedQuantity: number;
    public clear() {
        this.clinicPk = 0;
        this.immunizationPk = '';
        this.immunizationName = '';
        this.paymentTypeName = '';
        this.paymentTypeId = '';
        this.estimatedQuantity = 0;
    }
}
export class clinicLocation {
    public clinicLocation: string = ''
    public localContactName: string = ''
    public localContactEmail: string = ''
    public localContactPhone: string = ''
    public clinicDate: string = ''
    public startTime: string = ''
    public endTime: string = ''
    public address1: string = ''
    public address2: string = ''
    public city: string = ''
    public state: string = ''
    public zipCode: string = ''
    public isReassign: string = ''
    public isNoClinic: string = ''
    public isRemoved: string = ''
    public fluExpiryDate: string = ''
    public routineExpiryDate: string = ''
    public naClinicLatitude: string = ''
    public naClinicLongitude: string = ''
    public localEstmdShots:string=''
    public localvouchersDist:string=''
    // constructor(){
    //     this.isNoClinic
    // }
}
export class Clinic {
    public clinicImzQtyList: Immunization[] = [];
    public location: string = ''
    public contactFirstName: string = ''
    public contactName:string = '';
    public contactLastName: string = ''
    public contactEmail: string = ''
    public contactPhone: string = ''
    public clinicDate: string = ''
    public startTime: string = ''
    public endTime: string = ''
    public address1: string = ''
    public address2: string = ''
    public city: string = ''
    public state: string = ''
    public zipCode: string = ''
    public isReassign: number = 0;
    public isNoClinic: number ;
    public previousContact: string = ''
    public previousLocation: string = ''
    public isCurrent: number = 1;
    public fluExpiryDate: string = ''
    public routineExpiryDate: string = ''
    public latitude: string = ''
    public longitude: string = ''
    public estShots:string='';
    public localvouchersDist:string=''
    public coOutreachTypeId:string;
    public coOutreachTypeDesc:string;
    constructor(){
        this.isReassign = 0;
        this.isNoClinic = 0;
    }
}

export class Immunization2 {    
    public immunizationPk: string;
    public immunizationName: string;
    public immunizationSpanishName: string; 
    public price: number;
    public paymentTypeId: string;
    public paymentTypeName: string;
    public estimatedQuantity: number;
    public paymentTypeSpanishName: string
    public sendInvoiceTo: number;
    public name: string;
    public address1: string;
    public city: string;
    public state: string;
    public zip: string;
    public phone: string;
    public email: string;
    public email2: string;
    public isTaxExempt: number;
    public isCopay: number;
    public copayValue: number;
    public isVoucherNeeded: number;
    public voucherExpirationDate: string
    public showprice: number;
    public clear() {
        this.immunizationPk = '';
        this.immunizationName = '';
        this.paymentTypeName = '';
        this.paymentTypeId = '';
        this.estimatedQuantity = 0;
        this.isCopay = 0;
        this.isVoucherNeeded = 0;
        this.sendInvoiceTo = 0;
        this.showprice = 1;
    }
}

export class Immunizations {
    public Immunization: Immunization2[] = [];
}

export class Client {
    public client: string = '';
    public name: string = '';
    public title: string = '';
    public reviewedDate: string = '';
}

export class WalgreenCo {
    public name: string = '';
    public title: string = '';
    public preparedDate: string = '';
    public districtNumber: string = '';
}

export class LegalNoticeAddress {
    public attentionto: string
    public address1: string
    public address2: string
    public city: string
    public state: string
    public zipCode: string
    constructor(){
    this.attentionto= '';
    this.address1= '';
    this.address2= '';
    this.city= '';
    this.state= '';
    this.zipCode= '';
    }
}

export class contractThankYou{
    public storeState:string = '';
    public hasPrefilledForms:number = 0;
    public pdfVarFormEn:string = '';
    public pdfVarFormEsp:string = '';
}

export class ContractData {
    clear() {
        this.clinicImmunizationList.forEach(imz => imz.clear());
    }
    clinicAgreementPk:number;
    contactLogPk:number;
    isApproved: number;
    approvedOrRejectedBy: string;
    signature: string;
    isEmailSent: number = 0;
    businessUserEmails:string;
    notes: string;
    dateCreated: Date;
    contractPostedByUser:string;
    contactWagUser:string;
    public clinicList: Array<Clinic> = new Array<Clinic>();
    public clinicImmunizationList:Immunization2[]=[];
    public clientInfo: Client;
    public walgreenCoInfo: WalgreenCo;
    public legalNoticeAddressInfo: LegalNoticeAddress;
    public  contracTthankYou:contractThankYou;
    constructor() {
        this.contractPostedByUser = 'Walgreens User';
        this.contactWagUser = '';
        this.isEmailSent = 0;        
        this.isApproved = 0;
        this.clientInfo = new Client();
        this.walgreenCoInfo = new WalgreenCo();
        this.legalNoticeAddressInfo = new LegalNoticeAddress();
        this.contracTthankYou = new contractThankYou();
    }
}
