import {Immunization, Immunization2} from './contract'

export class coClinics {
  public location: string;
  public  contactFirstName: string;
   public contactLastName: string;
  public  contactEmail: string;
  public  contactPhone: string;
   public clinicDate: string;
  public  startTime: string;
   public endTime: string;
  public  address1: string;
  public address2: string;
  public  city: string;
  public  state: string;
  public  zipCode: string;
  public  isCurrent: number;
   public coOutreachTypeId: number;
   public coOutreachTypeDesc: string;
  public  clinicImzQtyList: Immunization[];
}
export class ErrorS {
   public errorCode: string;
   public errorMessage: string;
   constructor(){
     this.errorCode='0';
     this.errorMessage = '';
   }
}
export class LogOutreachStatus {
   public businessPk: number;
  public  firstName: string;
   public lastName: string;
   public jobTitle: string;
  public  outreachProgram: string;
  public  outreachBusinessPk: number;
   public contactDate: string;
   public outreachStatusId: number;
   public outreachStatusTitle: string;
   public feedback: string;
   public createdBy: number;
   public clinicAgreementPk: number;
   constructor(){
    this.businessPk = 0;
    this.clinicAgreementPk = -1;
    this.outreachBusinessPk = -1;

   }
}
export class CommunityOutreach {
  public logOutreachStatus: LogOutreachStatus = new LogOutreachStatus();
  public clinicAgreementPk: number;
  public contactLogPk: number;
  public isApproved: number;
  public approvedOrRejectedBy: string;
  public isEmailSent: number;
  public dateCreated: string;
  public lastUpdatedDate: string;
  public contactWagUser: string;
  public clinicImmunizationList: Immunization2[];
  public clinicList: Array<coClinics> 
  public errorS: ErrorS 
  constructor(){
    this.clinicAgreementPk = -1;
    this.contactLogPk = -1;
    this.isApproved = 0;
    this.approvedOrRejectedBy = "CommunityUpdate@WagOutreach.com",
    this.isEmailSent = 0;
    this.clinicImmunizationList = [];
    this.clinicList  = new Array<coClinics>();
    this.errorS = new ErrorS();
    this.logOutreachStatus = new LogOutreachStatus();
  }
}
