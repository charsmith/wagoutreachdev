
export class AddOutreachOpportunity {
    businessPk: number;
    businessName: string;
    firstName: string;
    lastName: string;
    jobTitle: string;
    email: string;
    phone: string;
    address1: string;
    address2: string;
    city: string;
    state: string;
    zipCode: string;
    tollFree: string;
    industry: string;
    employmentSize: number;
    outreachEffort: OutreachEffort[];
    errorCode: number;
    errorMessage: string;
    constructor()
    {
        this.outreachEffort= new Array<OutreachEffort>();
    }
}

export class OutreachEffort {
    outreachProgram: string;
    outreachBusinessPk: number;
    storeId: number;
    storePk: number;
    isCharityInitiated: boolean;
    isCommunityInitiated: boolean;
}
