import { Routes } from '@angular/router';
import { HomeRouteGuard } from '../guards/home.route.gaurd';
import { AdminRouteGuard } from '../guards/admin.route.guard';

import { UserProfileComponent } from '../modules/admin/components/user-profile/user-profile.component';
import { EditUserComponent } from '../modules/admin/components/user-profile/edit-user/edit-user.component';
import { AddUserComponent } from '../modules/admin/components/user-profile/add-user/add-user.component';
import { UserauthenticationComponent } from '../modules/auth/components/userauthentication/userauthentication.component';

import { LandingComponent } from '../modules/auth/components/landing/landing.component';
import { CoClinicDetailsComponent } from '../modules/clinicdetails/components/co-clinic-details/co-clinic-details.component';
import { CharityProgramClinicDetailsComponent } from '../modules/clinicdetails/components/charity-program-clinic-details/charity-program-clinic-details.component';
import { LocalClinicDetailsComponent } from '../modules/clinicdetails/components/local-clinic-details/local-clinic-details.component';
import { CorporateClinicDetailsComponent } from '../modules/clinicdetails/components/corporate-clinic-details/corporate-clinic-details.component';
import { CommonclinicdetailsComponent } from '../modules/clinicdetails/components/commonclinicdetails.component';


import { ImmunizationComponent } from '../modules/resources/components/immunization/immunization.component';
import { LoginComponent } from '../modules/auth/components/login/login.component';
import { DashboardComponent } from '../modules/home/dashboard/dashboard.component';
import { ScheduleEventComponent } from '../modules/store/components/schedule-event/schedule-event.component';
import { MasterlayoutComponent } from '../modules/common/components/masterlayout/masterlayout.component';
import { StoreOpportunityComponent } from '../modules/common/components/store-opportunity/store-opportunity.component';
import { SenioroutreachComponent } from '../modules/resources/components/senioroutreach/senioroutreach.component';
import { FollowupComponent } from '../modules/store/components/follow-up/followup.component';
import { OpportunityComponent } from '../modules/store/components/opportunity/opportunity.component';
import { ContactlogComponent } from '../modules/store/components/contactlog/contactlog.component';
import { ContractapproveComponent } from '../modules/contractaggreement/components/contractapprove/contractapprove.component';
import { ApproveagreementComponent } from '../modules/contractaggreement/components/approveagreement/approveagreement.component';
import { ThankyouComponent } from '../modules/contractaggreement/components/thankyou/thankyou.component';
import { ContractComponent } from '../modules/contractaggreement/components/local-contracts/contract.component';
import { CharitylocationsListComponent } from '../modules/contractaggreement/components/charity-locations-list/charity-locations-list.component';
import { CommunityoutreachComponent } from '../modules/contractaggreement/components/communityoutreach/communityoutreach.component';
import { ViewcontractAgreementComponent } from '../modules/contractaggreement/components/viewcontract-agreement/viewcontract-agreement.component';
import { TrainingvideosComponent } from '../modules/resources/components/trainingvideos/trainingvideos.component';
import { SignoutComponent } from '../modules/auth/components/signout/signout.component';
import { AgreementComponent } from '../modules/contractaggreement/components/contract-agreement/agreement.component';
//import { ScheduledclinicsreportComponent } from '../modules/reports/components/scheduleclinicreport/scheduledclinicsreport.component';
// import { GroupidassignmentreportComponent } from '../modules/reports/components/groupidassignmentreport/groupidassignmentreport.component';
// import { WeeklygroupidassignmentsComponent } from '../modules/reports/components/weeklygroupidassignmentsreport/weeklygroupidassignments.component';

// import { WeeklyvaccinepurhaseComponent } from '../modules/reports/components/weeklyvaccinepuchasingreport/weeklyvaccinepurchase.component';
// import { ImmunizationfinancereportComponent } from '../modules/reports/components/immunizationfinancereport/immunizationfinancereport.component';
// import { RevisedclinicdatesComponent } from '../modules/reports/components/revisedclinicdates/revisedclinicdates.component';
// import { ScheduledAppointmentsreportComponent } from '../modules/reports/components/scheduledappointmentsreport/scheduledappointmentsreport.component';
// import { ActivityreportComponent } from '../modules/reports/components/activityreport/activityreport.component';
// import { ScheduledeventsreportComponent } from '../modules/reports/components/scheduledeventsreport/scheduledeventsreport.component';
// import { VaccinePurchasingReportComponent } from '../modules/reports/components/vaccinepurchasingreport/vaccinepurchasingreport.component';


export const APP_ROUTES: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent},
  { path: 'landing', component: LandingComponent, canActivate: [HomeRouteGuard] },
  { path: 'thankyou', component: ThankyouComponent },
  { path: 'signout', component: SignoutComponent },
  { path: 'contractAgreementForUser', component: ContractapproveComponent, data: { title: 'contractAgreementForUser' }},
  { path: 'viewcontract', component: ViewcontractAgreementComponent, data: { title: 'viewcontract' }},
  {
    path: 'communityoutreach', component: MasterlayoutComponent,  children: [
      { path: 'storehome', component: StoreOpportunityComponent, canActivate: [AdminRouteGuard] },
      { path: 'home', component: DashboardComponent },
      { path: 'soresources', component: SenioroutreachComponent, data: { title: 'soresources' } } ,
      { path: 'trainingvideos', component: TrainingvideosComponent, data: { title: 'trainingvideos' } },
      { path: 'imzresources', component: ImmunizationComponent, data: { title: 'imzresources' } },
      // { path: 'scheduledclinicsreport', component: ScheduledclinicsreportComponent, data: { title: 'scheduledclinicsreport' } },
      // { path: 'groupidassignmentreport', component: GroupidassignmentreportComponent, data: { title: 'groupidassignmentreport' } },
      // { path: 'weeklygroupidassignmentsreport', component: WeeklygroupidassignmentsComponent, data: { title: 'weeklygroupidassignmentsreport' } },
      // { path: 'vaccinepurchasingreport', component: VaccinePurchasingReportComponent, data: { title: 'vaccinepurchasingreport' } },
      // { path: 'weeklyvaccinepurchasereport', component: WeeklyvaccinepurhaseComponent, data: { title: 'weeklyvaccinepurchasereport' } },
      // { path: 'immunizationfinancereport', component: ImmunizationfinancereportComponent, data: { title: 'immunizationfinancereport' } },
      // { path: 'revisedclinicdatesreport', component: RevisedclinicdatesComponent, data: { title: 'revisedclinicdatesreport' } },
      // { path: 'scheduledappointmentsreport', component: ScheduledAppointmentsreportComponent, data: { title: 'scheduledappointmentsreport' } },
      // { path: 'activityreport', component: ActivityreportComponent, data: { title: 'activityreport' } },
      // { path: 'scheduledeventsreport', component: ScheduledeventsreportComponent, data: { title: 'scheduledeventsreport' } },
      { path: 'userprofile', component: UserProfileComponent, data: { title: 'userprofile' }, canActivate: [AdminRouteGuard] },
      { path: 'contract', component: ContractComponent, data: { title: 'contract' }, canActivate: [AdminRouteGuard] },
      { path: 'edit_user/:id', component: EditUserComponent, canActivate: [AdminRouteGuard] },
      { path: 'add-user', component: AddUserComponent, canActivate: [AdminRouteGuard] },
      { path: 'followUp', component: FollowupComponent,data: { title: 'followUp' }, canActivate: [AdminRouteGuard]},
      //{ path: 'contractAgreementForUser', component: ContractapproveComponent, data: { title: 'contractAgreementForUser' }},
      { path: 'approveagreement', component: ApproveagreementComponent, canActivate: [AdminRouteGuard] },
      { path: 'scheduleevent', component: ScheduleEventComponent, data: { title: 'scheduleevent' }, canActivate: [AdminRouteGuard] },
      { path: 'viewcontactlog', component: ContactlogComponent, data: { title: 'contactlog' }, canActivate: [AdminRouteGuard] },
      { path: 'viewcontactlogHistory', component: ContactlogComponent, canActivate: [AdminRouteGuard] },
      { path: 'opportunity', component: OpportunityComponent, data: { title: 'addBusiness' }, canActivate: [AdminRouteGuard] },

      { path: 'charityprogram', component: CharitylocationsListComponent, data: { title: 'charityProgram' }, canActivate: [AdminRouteGuard] },
      { path: 'communityoutreach', component: CommunityoutreachComponent, data: { title: 'Community Outreach Program' }, canActivate: [AdminRouteGuard] },
      { path: 'CommunityOutreachProgramDetails', component: CommonclinicdetailsComponent, data: { title: 'Community Outreach Clinic Details' }, canActivate: [AdminRouteGuard] },
      { path: 'CharityProgramDetails', component: CharityProgramClinicDetailsComponent, data: { title: 'Charity Program Clinic Details' }, canActivate: [AdminRouteGuard] },
      { path: 'LocalClinicProgramDetails', component: CommonclinicdetailsComponent, data: { title: 'Local Clinic Details' }, canActivate: [AdminRouteGuard] },
      { path: 'CorporateClinicProgramDetails', component: CorporateClinicDetailsComponent, data: { title: 'Corporate Clinic Details' }, canActivate: [AdminRouteGuard] },
      { path: 'viewcontractForPrev', component: AgreementComponent, data: { title: 'viewcontract' }}
    ]
  },
  { path: 'unauthorize', component: UserauthenticationComponent, canActivate: [HomeRouteGuard] },
  { path: '**', component: UserauthenticationComponent, canActivate: [HomeRouteGuard] }
];
