export const outreachStatus : any[] =[
  {
    "outreachStatusId": 0,
    "outreachStatus": "No Outreach",
    "outreachProgram": "SR",
    "isActive": true,
    "sortOrder": 1
  },
  {
    "outreachStatusId": 1,
    "outreachStatus": "Active",
    "outreachProgram": "SR",
    "isActive": true,
    "sortOrder": 2
  },
  {
    "outreachStatusId": 2,
    "outreachStatus": "Follow-up",
    "outreachProgram": "SR",
    "isActive": true,
    "sortOrder": 3
  },
  {
    "outreachStatusId": 3,
    "outreachStatus": "Event Scheduled",
    "outreachProgram": "SR",
    "isActive": true,
    "sortOrder": 4
  },
  {
    "outreachStatusId": 4,
    "outreachStatus": "No Client Interest",
    "outreachProgram": "SR",
    "isActive": true,
    "sortOrder": 5
  },
  {
    "outreachStatusId": 5,
    "outreachStatus": "Business Closed",
    "outreachProgram": "SR",
    "isActive": true,
    "sortOrder": 6
  },
  {
    "outreachStatusId": 0,
    "outreachStatus": "Contact Client",
    "outreachProgram": "AC",
    "isActive": true,
    "sortOrder": 1
  },
  {
    "outreachStatusId": 1,
    "outreachStatus": "Confirmed",
    "outreachProgram": "AC",
    "isActive": true,
    "sortOrder": 2
  },
  {
    "outreachStatusId": 2,
    "outreachStatus": "Cancelled",
    "outreachProgram": "AC",
    "isActive": true,
    "sortOrder": 3
  },
  {
    "outreachStatusId": 3,
    "outreachStatus": "Completed",
    "outreachProgram": "AC",
    "isActive": true,
    "sortOrder": 4
  },
  {
    "outreachStatusId": 0,
    "outreachStatus": "No Outreach",
    "outreachProgram": "IP",
    "isActive": true,
    "sortOrder": 1
  },
  {
    "outreachStatusId": 1,
    "outreachStatus": "Active",
    "outreachProgram": "IP",
    "isActive": false,
    "sortOrder": 2
  },
  {
    "outreachStatusId": 2,
    "outreachStatus": "Follow-up",
    "outreachProgram": "IP",
    "isActive": true,
    "sortOrder": 3
  },
  {
    "outreachStatusId": 3,
    "outreachStatus": "Contract Initiated",
    "outreachProgram": "IP",
    "isActive": true,
    "sortOrder": 4
  },
  {
    "outreachStatusId": 4,
    "outreachStatus": "No Client Interest",
    "outreachProgram": "IP",
    "isActive": true,
    "sortOrder": 5
  },
  {
    "outreachStatusId": 5,
    "outreachStatus": "Business Closed",
    "outreachProgram": "IP",
    "isActive": true,
    "sortOrder": 6
  },
  {
    "outreachStatusId": 6,
    "outreachStatus": "Undecided",
    "outreachProgram": "IP",
    "isActive": false,
    "sortOrder": 7
  },
  {
    "outreachStatusId": 7,
    "outreachStatus": "Contacted Last Season",
    "outreachProgram": "IP",
    "isActive": false,
    "sortOrder": 8
  },
  {
    "outreachStatusId": 8,
    "outreachStatus": "Charity (HHS Voucher)",
    "outreachProgram": "IP",
    "isActive": false,
    "sortOrder": 9
  },
  {
    "outreachStatusId": 9,
    "outreachStatus": "DM Contacted",
    "outreachProgram": "IP",
    "isActive": true,
    "sortOrder": 10
  },
  {
    "outreachStatusId": 10,
    "outreachStatus": "HCS Contacted",
    "outreachProgram": "IP",
    "isActive": true,
    "sortOrder": 11
  },
  {
    "outreachStatusId": 11,
    "outreachStatus": "Vote & Vax",
    "outreachProgram": "IP",
    "isActive": false,
    "sortOrder": 12
  },
  {
    "outreachStatusId": 12,
    "outreachStatus": "Community Outreach",
    "outreachProgram": "IP",
    "isActive": true,
    "sortOrder": 13
  }
]