export const COProgram: any = {
   "ImmunizationsList":[
      {
         "immunizationId":29,
         "immunizationName":"Influenza - Standard/PF Injectable (trivalent)",
         "immunizationSpanishName":"Influenza - Vacuna inyectable trivalente en dosis estándar",
         "paymentOption":0,
         "price":25.99,
         "directBillPrice":25.00,
         "isActive":true,
         "paymentTypeId":6,
         "paymentTypeName":"Corporate to Invoice Employer Directly",
         "paymentTypeSpanishName":"Corporativo al Empleador directamente Factura",
         "paymentGroup":0,
         "isSelected":"False",
         "isPaymentSelected":"False",
         "name":"",
         "address1":"",
         "address2":"",
         "city":"",
         "state":"",
         "zip":"",
         "phone":"",
         "email":"",
         "tax":"",
         "copay":"",
         "copayValue":"",
         "isVoucherNeeded":"",
         "voucherExpirationDate":""
      }
   ],
   "ClinicAgreement":{
      "Clinics":{
         "clinic":{
            "immunization":{
               "pk":"32",
               "paymentTypeId":"7",
               "estimatedQuantity":"20"
            },
            "clinicLocation":"CLINIC LOCATION A",
            "localContactName":"Mark Samples",
            "LocalContactPhone":"4680546805",
            "LocalContactEmail":"mark.samples@testemail.com",
            "Address1":"2208 N COLISEUM BLVD",
            "Address2":"NULL",
            "city":"FORT WAYNE",
            "state":"IN",
            "zipCode":"46805",
            "clinicDate":"8/5/2017 12:00:00 AM",
            "startTime":"2:00pm",
            "endTime":"3:00pm",
            "coOutreachTypeId":"4",
            "coOutreachTypeDesc":"Gifts"
         }
      },
      "Immunizations":{
         "Immunization":{
            "pk":"32",
            "immunizationName":"Influenza - Standard Injectable Quadrivalent",
            "price":"37.00",
            "paymentTypeId":"7",
            "paymentTypeName":"Cash Paid by Employee",
            "name":"",
            "address1":"",
            "address2":"",
            "city":"",
            "state":"",
            "zip":"",
            "phone":"",
            "email":"",
            "tax":"",
            "copay":"",
            "copayValue":""
         }
      }
   },
   "BusinessDetails":{
      "outreachBusinessPk":"702952",
      "storeId":"5624",
      "businessType":1,
      "businessPk":"317020",
      "businessName":"WEST COUNTY SCHOOL DISTRICT",
      "firstName":"Stephanie",
      "lastName":"Warden",
      "address":"768 OLD HWY 8",
      "address2":"",
      "city":"LEADWOOD",
      "state":"MO",
      "zip":"63601",
      "phone":"",
      "website":"",
      "tollfree":"",
      "industry":"",
      "latitude":"",
      "longitude":"",
      "actualLocationEmploymentSize":"",
      "isStandardized":"True",
      "jobTitle":"elementary nurse",
      "businessContactEmail":""
   },
   "OutreachTypes":{
      "1":"Brownbag",
      "2":"Health Fair",
      "3":"Presentation/Education",
      "4":"Other"
   }
}