export const CorporateClinicDetailsJSON: any = {
    "LocationDetails": [
        {
            "clinicPk": 54280,
            "outreachBusinessPk": 1351699,
            "clinicStoreId": 1234,
            "businessPk": 851871,
            "businessName": "Inter Valley Health Plan - Clinic 1",
            "naClinicLocation": "Claremont MIC Office",
            "naContactFirstName": "Monica",
            "naContactLastName": "Crow",
            "naClinicAddress1": "1601 Monte Vista Avenue",
            "naClinicAddress2": "",
            "naClinicCity": "Claremont",
            "naClinicState": "CA",
            "naClinicZip": "91711",
            "naClinicContactPhone": "909-623-6333",
            "naClinicContactPhoneExt": "307",
            "naContactEmail": "mcrow@IVHP.COM",
            "naClinicDate": "10/28/2017",
            "naClinicStartTime": "11:00AM",
            "naClinicEndTime": "1:00PM",
            "naClinicCoverageType": "employee",
            "naClinicCopay": "0",
            "naClinicAddlComments": "",
            "naClinicSplBillingInstr": "",
            "naClinicPlanId": "Please review participant medical card",
            "naClinicGroupId": "review patient medical card (5933WBMH for cash)",
            "dateAdded": "2017-04-28T21:18:48.79",
            "isScheduled": true,
            "designPk": 252,
            "recipientId": "8-digit patient DOB,5-digit patient zip code,5-digit store#",
            "confirmedClientName": "",
            "pharmacistName": "",
            "pharmacistPhone": "",
            "totalHours": "",
            "clinicRoom": "Room 200",
            "leadStoreId": null,
            "isCompleted": false,
            "isConfirmed": false,
            "isCancelled": true,
            "clinicType": 3
        }
    ],
    "BillingVaccineInformation": {

        "ImmunizationDetails": [
            {
                "pk": 183606,
                "clinicPk": 54280,
                "immunizationPk": 55,
                "immunizationName": "Standard Trivalent",
                "estimatedQuantity": 25,
                "totalImmAdministered": null
            },
            {
                "pk": 183607,
                "clinicPk": 54280,
                "immunizationPk": 62,
                "immunizationName": "TDAP",
                "estimatedQuantity": 50,
                "totalImmAdministered": null
            },
            {
                "pk": 183608,
                "clinicPk": 54280,
                "immunizationPk": 66,
                "immunizationName": "Hepatitis A - Adolescent",
                "estimatedQuantity": 75,
                "totalImmAdministered": null
            },
            {
                "pk": 183609,
                "clinicPk": 54280,
                "immunizationPk": 73,
                "immunizationName": "Meningococcal (MPSV4)",
                "estimatedQuantity": 100,
                "totalImmAdministered": null
            }
        ]
    },
    "HistoryLog": {
        "HistoryLogItems": [
            {
                "date": "2018/03/26",
                "action": "Completed",
                "updatedField": "Billing & Vaccine Information:..",
                "update": "Completed Clinic",
                "user": "CommunityUpdate@WagOutreach.com"
            },
            {
                "date": "2018/03/24",
                "action": "Updated",
                "updatedField": "Pharmacist & Post Clinic Information::..",
                "update": "Confirmed Clinic",
                "user": "CommunityUpdate@WagOutreach.com"
            }

        ]
    }
}