export const distirctManger: any[] = [
    {
      "storeId": 3632,
      "address": "3632 - 2400 S JACKSON ST, SEATTLE, WA 98144",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 3840,
      "address": "3840 - 14656 AMBAUM BLVD SW, BURIEN, WA 98166",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 4898,
      "address": "4898 - 6330 35TH AVE SW, SEATTLE, WA 98126",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 5950,
      "address": "5950 - 4412 RAINIER AVE S, SEATTLE, WA 98118",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 6194,
      "address": "6194 - 222 PIKE ST, SEATTLE, WA 98101",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 6259,
      "address": "6259 - 9456 16TH AVE SW, SEATTLE, WA 98106",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 6590,
      "address": "6590 - 1531 BROADWAY, SEATTLE, WA 98122",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 6890,
      "address": "6890 - 500 15TH AVE E, SEATTLE, WA 98112",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 6901,
      "address": "6901 - 28817 MILITARY RD S, FEDERAL WAY, WA 98003",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 7700,
      "address": "7700 - 34008 HOYT RD SW, FEDERAL WAY, WA 98023",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 9423,
      "address": "9423 - 23003 PACIFIC HWY S, DES MOINES, WA 98198",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 10995,
      "address": "10995 - 3716 S 144TH ST, TUKWILA, WA 98168",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 11856,
      "address": "11856 - 566 DENNY WAY, SEATTLE, WA 98109",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    },
    {
      "storeId": 15321,
      "address": "15321 - 1001 BROADWAY, SUITE 102-103, SEATTLE, WA 98122",
      "districtNumber": 207,
      "districtName": "207 - Seattle South"
    }
  ];