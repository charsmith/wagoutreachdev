

export const adminmenuItems: any[] = [{
  title: "Home",
  active: true,
  url: "/communityoutreach/home",
  navigationItems: [   
    {
      title: "Charity Program",
      url: "/communityoutreach/charityprogram"
    }
    ,
    {
      title: "Community Outreach Clinic Details",
      url: "/communityoutreach/CommunityOutreachProgramDetails"
    }
    ,
    {
      title: "Charity Program Clinic Details",
      url: "/communityoutreach/CharityProgramDetails"
    }
    ,
    {
      title: "Corporate Clinic Details",
      url: "/communityoutreach/CorporateClinicProgramDetails"
    }
    ,
    {
      title: "Local Clinic Details",
      url: "/communityoutreach/LocalClinicProgramDetails"
    }
  ]

},{
  title: "Store",
  url: "/communityoutreach/storehome",

},
// {
//   title: "Map",
//   url: "/communityoutreach/storehome",

// },
{
  title: "Resources",
  url: "/communityoutreach/storehome",
  navigationItems: [
    {
      title: "Training Videos",
      url: "/communityoutreach/trainingvideos"
    },
    {
      title: "Outreach Materials",
      url: "/",
      navigationItems: [
        {
          title: "Immunization Program",
          url: "/communityoutreach/imzresources"
        }  ,
        {
          title: "Senior Outreach",
          url: "/communityoutreach/soresources"
        }
      
      ]
    }
  ]

},
{
  title: "Reports",
  active: true,
  url: "/communityoutreach/scheduleclinicreport",
  navigationItems: [
    
     {
      title: "Immunization Programs",
      url: "/",
      navigationItems: [
        {
          title: "Scheduled Clinics",
          url: "/communityoutreach/scheduledclinicsreport"
        },
        {
          title: "Group ID Assignments",
          url: "/communityoutreach/groupidassignmentreport"
        },
        {
          title: "Weekly Group ID Assignments",
          url: "/communityoutreach/weeklygroupidassignmentsreport"
        },
        {
          title: "Vaccine Purchasing",
          url: "/communityoutreach/vaccinepurchasingreport"
        },
        {
          title: "Weekly Vaccine Purchasing",
          url: "/communityoutreach/weeklyvaccinepurchasereport"
        },
        {
          title: "Immunization Finance",
          url: "/communityoutreach/immunizationfinancereport"
        },
        {
          title: "Revised Clinic Dates & Volumes",
          url: "/communityoutreach/revisedclinicdatesreport"
        },
        {
          title: "Scheduled Appointments",
          url: "/communityoutreach/scheduledappointmentsreport"
        }
      ]
    },
    {
      title: "Senior Outreach",
      url: "/",
      navigationItems: [
        {
          title: "Activity",
          url: "/communityoutreach/activityreport"
        },
        {
          title: "Scheduled Events",
          url: "/communityoutreach/scheduledeventsreport"
        }
      ]
    }
   /* {
      title: "Potential Local Business List and Contact Status",
      url: "/"
    },
    {
      title: "Scheduled Clinic Status",
      url: "/"
    }
    ,
    {
      title: "Store Compliance Report",
      url: "/"
    }
    ,
    {
      title: "User Access Log",
      url: "/"
    }
    ,
    {
      title: "High Level Status Report",
      url: "/"
    }
    ,
    {
      title: "Scheduled Clinic Report",
      url: "/"
    }
    ,
    {
      title: "Initiated Contracts Report",
      url: "/"
    }
    ,
    {
      title: "Workflow Monitor Report",
      url: "/"
    }
    ,
    {
      title: "Scheduled Appointments Report",
      url: "/"
    }
    ,
    {
      title: "Vaccine Purchasing Report",
      url: "/"
    }
    ,
    {
      title: "Weekly Vaccine Purchasing Report",
      url: "/"
    }
    ,
    {
      title: "Direct B2B Mail Campaign Result",
      url: "/"
    }
    ,
    {
      title: "Group ID Assignments Report",
      url: "/"
    }
    ,
    {
      title: "Weekly Group ID Assignments Report",
      url: "/"
    }
    ,
    {
      title: "Minimum Shot Exceptions Report",
      url: "/"
    }
    ,
    {
      title: "Revised Clinic Dates and Volumes Report",
      url: "/"
    }
    ,
    {
      title: "Immunization Finance Report",
      url: "/"
    } */
  ]
},
// {
//   title: "Admin",
//   active: true,
//   url: "/communityoutreach/storehome",
//   navigationItems: [
//     {
//       title: "User Profile",
//       url: "/communityoutreach/userprofile"
//     },
//     {
//       title: "Scheduler Site Setup",
//       url: "/"
//     },
//     {
//       title: "Upload Scheduled Clinics",
//       url: "/"
//     },
//     {
//       title: "Store Profile Uploader",
//       url: "/"
//     },
//     {
//       title: "Post Custom Message",
//       url: "/"
//     }
//   ]
// },
{
  title: "Sign Out",
  url: "/login",

}
]

export const storeManagermenuItems: any[] = [{
  title: "Home",
  url: "/communityoutreach/storehome",

},
{
  title: "Store",
  url: "/communityoutreach/storehome",

},
// {
//   title: "Map",
//   url: "/communityoutreach/storehome",

// },
{
  title: "Resources",
  url: "/communityoutreach/resources",

},
{
  title: "Reports",
  active: true,
  navigationItems: [
    {
      title: "Potential Local Business List and Contact Status",
      url: "/"
    },
    {
      title: "Scheduled Clinic Status",
      url: "/"
    }
    ,
    {
      title: "Store Compliance Report",
      url: "/"
    }
    ,
    {
      title: "Scheduled Clinic Report",
      url: "/"
    }
  ]
},


{
  title: "Sign Out",
  url: "/login",

}
]

export const districtManagermenuItems: any[] = [{
  title: "Home",
  url: "/communityoutreach/storehome",

},
{
  title: "Store",
  url: "/communityoutreach/storehome",

},
// {
//   title: "Map",
//   url: "/communityoutreach/storehome",

// },
{
  title: "Resources",
  url: "/communityoutreach/resources",

},
{
  title: "Reports",
  active: true,
  navigationItems: [
    {
      title: "Potential Local Business List and Contact Status",
      url: "/"
    },
    {
      title: "Scheduled Clinic Status",
      url: "/"
    }
    ,
    {
      title: "User Access Log",
      url: "/"
    }
    ,
    {
      title: "District High Level Status Report",
      url: "/"
    }
    ,
    {
      title: "Scheduled Clinic Report",
      url: "/"
    }
    ,
    {
      title: "Workflow Monitor Report",
      url: "/"
    }
  ]
},


{
  title: "Sign Out",
  url: "/login",

}
]

export const healthcareSupervisormenuItems: any[] = [{
  title: "Home",
  url: "/communityoutreach/home",

},
{
  title: "Store",
  url: "/communityoutreach/storehome",

},
// {
//   title: "Map",
//   url: "/communityoutreach/storehome",

// },
{
  title: "Resources",
  url: "/communityoutreach/resources",

},
{
  title: "Reports",
  active: true,
  navigationItems: [
    {
      title: "Potential Local Business List and Contact Status",
      url: "/"
    },
    {
      title: "Scheduled Clinic Status",
      url: "/"
    }
    ,
    {
      title: "Store Compliance Report",
      url: "/"
    }
    ,
    {
      title: "User Access Log",
      url: "/"
    }
    ,
    {
      title: "Area High Level Status Report",
      url: "/"
    }
    ,
    {
      title: "Scheduled Clinic Report",
      url: "/"
    }
    ,
    {
      title: "Workflow Monitor Report",
      url: "/"
    }
  ]
},


{
  title: "Sign Out",
  url: "/login",

}
]

export const regionalVicePresidentmenuItems: any[] = [{
  title: "Home",
  url: "/communityoutreach/storehome",

},
{
  title: "Store",
  url: "/communityoutreach/storehome",

},
// {
//   title: "Map",
//   url: "/communityoutreach/storehome",

// },
{
  title: "Resources",
  url: "/communityoutreach/resources",

},
{
  title: "Reports",
  active: true,
  navigationItems: [
    {
      title: "Potential Local Business List and Contact Status",
      url: "/"
    },
    {
      title: "Scheduled Clinic Status",
      url: "/"
    }
    ,
    {
      title: "Store Compliance Report",
      url: "/"
    }
    ,
    {
      title: "User Access Log",
      url: "/"
    }
    ,
    {
      title: "Region High Level Status Report",
      url: "/"
    }
    ,
    {
      title: "Scheduled Clinic Report",
      url: "/"
    }
    ,
    {
      title: "Workflow Monitor Report",
      url: "/"
    }
  ]
},


{
  title: "Sign Out",
  url: "/login",

}
]

export const directorRxRetailmenuItems: any[] = [{
  title: "Home",
  url: "/communityoutreach/storehome",

},
{
  title: "Store",
  url: "/communityoutreach/storehome",

},
// {
//   title: "Map",
//   url: "/communityoutreach/storehome",

// },
{
  title: "Resources",
  url: "/communityoutreach/resources",

},
{
  title: "Reports",
  active: true,
  navigationItems: [
    {
      title: "Potential Local Business List and Contact Status",
      url: "/"
    },
    {
      title: "Scheduled Clinic Status",
      url: "/"
    }
    ,
    {
      title: "Store Compliance Report",
      url: "/"
    }
    ,
    {
      title: "User Access Log",
      url: "/"
    }
    ,
    {
      title: "Area High Level Status Report",
      url: "/"
    }
    ,
    {
      title: "Scheduled Clinic Report",
      url: "/"
    }
    ,
    {
      title: "Workflow Monitor Report",
      url: "/"
    }
  ]
},


{
  title: "Sign Out",
  url: "/login",

}
]