export const contractData : any = {
  "contractAgreement": {
  "isApproved": "1",
	"approvedOrRejectedBy": "vishnu.mugdhasani@senecaglobal.com",
	"signature": "MVVR",
	"isEmailSent": "1",
    "clinicList":  [
        {
          "location": "CLINIC LOCATION A",
          "contactFirstName": "",
          "contactLastName": "",
          "contactEmail": "",
          "contactPhone": "",
          "clinicDate": "",
          "startTime": "",
          "endTime": "",
          "address1": "",
          "address2": "",
          "city": "",
          "state": "",
          "zipCode": "",
          "isReassign": "",
		  "isNoClinic": "",
		  "isRemoved": "",
		  "fluExpiryDate": "",
		  "routineExpiryDate": "",
          "latitude": "",
          "longitude": "",
          "clinicImzQtyList": [ 
            {
              "clinicPk": 122222,
              "immunizationPk": "29",
			  "immunizationName": "Influenza - Standard/PF Injectable (trivalent)",
			  "paymentTypeName": "Corporate to Invoice Employer Directly",
              "paymentTypeId": "6",
              "estimatedQuantity": "100"
            },
            {
              "clinicPk": 122222,
              "immunizationPk": "29",
			  "immunizationName": "Influenza - Standard/PF Injectable (trivalent)",
			  "paymentTypeName": "Submit Claims to Medical Insurance",
              "paymentTypeId": "9",
              "estimatedQuantity": "75"
            },
            {
              "clinicPk": 122222,
              "immunizationPk": "30",
			   "immunizationName": "Influenza - High Dose",
			   "paymentTypeName": "Submit Claims to Pharmacy Insurance",
              "paymentTypeId": "8",
              "estimatedQuantity": "50"
            },
            {
              "clinicPk": 122222,
              "immunizationPk": "33",
			  "immunizationName": "Pneumovax 23",
			  "paymentTypeName": "Cash Paid by Employee",
              "paymentTypeId": "7",
              "estimatedQuantity": "25"
            }
          ]
        }
        
      ],
      "clinicImmunizationList": [
        {
          "immunizationPk": "29",
          "immunizationName": "Influenza - Standard/PF Injectable (trivalent)",
          "immunizationSpanishName": "Influenza - Vacuna inyectable trivalente en dosis estándar",
          "price": "25.00",
          "paymentTypeId": "6",
          "paymentTypeName": "Corporate to Invoice Employer Directly",
          "paymentTypeSpanishName": "Corporativo al Empleador directamente Factura",
		  "sendInvoiceTo":"yes",
          "name": "Jeanie Barrett",
          "address1": "5901 Downey Ave",
          "address2":"",
          "city": "Long Beach",
          "state": "CA",
          "zip": "90805",
          "phone": "562-634-4693",
          "email": "jeanie.samples@testemail.com",
          "isTaxExempt": "Yes",
          "isCopay": "Yes",
          "copayValue": "120.00",
          "isVoucherNeeded": "Yes",
          "voucherExpirationDate": "04/15/2018",
		  "showprice":"yes"
        },
        {
          "immunizationPk": "29",
          "immunizationName": "Influenza - Standard/PF Injectable (trivalent)",
          "immunizationSpanishName": "Influenza - Vacuna inyectable trivalente en dosis estándar",
          "paymentTypeId": "9",
          "paymentTypeName": "Submit Claims to Medical Insurance",
          "paymentTypeSpanishName": "Reclamación a ser sometida al Seguro Médico",
		  "sendInvoiceTo":"no",
		  "showprice":"no"
        },
        {
          "immunizationPk": "30",
          "immunizationName": "Influenza - High Dose",
          "immunizationSpanishName": "Influenza - Dosis alta",
          "paymentTypeId": "8",
          "paymentTypeName": "Submit Claims to Pharmacy Insurance",
          "paymentTypeSpanishName": "Reclamación a ser sometida al Seguro de la Farmacia",
		   "sendInvoiceTo":"no",
		   "showprice":"no"
        },
        {
          "immunizationPk": "33",
          "immunizationName": "Pneumovax 23",
          "immunizationSpanishName": "Pneumovax 23",
          "price": "95.00",
          "paymentTypeId": "7",
          "paymentTypeName": "Cash Paid by Employee",
          "paymentTypeSpanishName": "Efectivo pagado por el empleado",
		   "sendInvoiceTo":"no",
		   "showprice":"yes"
        }
      ],
    "Client": [{
      "header": "Walgreens",
      "name": "Mathews Samples",
      "title": "Accountant",
      "date": "02/05/2018"
    }],
    "WalgreenCo": [{
      "name": "David Samples",
      "title": "Admin",
      "date": "02/05/2018 11:20:07 PM",
      "districtNumber": "254"
    }],
    "LegalNoticeAddress": {
      "-attentionTo": "SenecaGlobalTest",
      "-address1": "1901, North Block, 4th Street",
      "-city": "Whitefield",
      "-state": "IL",
      "-zipCode": "60948"
    }
  }
}