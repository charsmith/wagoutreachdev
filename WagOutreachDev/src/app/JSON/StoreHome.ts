export const StoreHome: any = {
  "opportunityList": [
    
    {
      "businessPk": 629042,
      "businessName": "BEDFORD PUBLIC LIBRARY",
      "phone": "(530) 487-0751",
      "industry": "alzheimers facility",
      "firstName": "tom ",
      "lastName": "skittle",
      "jobTitle": "manager",
      "isStandardized": false,
      "employmentSize": "N/A",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1025553,
          "lastContact": "2017-08-28T16:53:57.72",
          "outreachStatus": 1,
          "outreachStatusTitle": "Confirmed",
          "isScheduledClinics": true
        }
      ]
    },
    {
      "businessPk": 303627,
      "businessName": "ADULT DAY HEALTH CARE",
      "phone": "(530) 342-2345",
      "industry": "DAY CARE CENTERS-ADULT",
      "firstName": "DIANE",
      "lastName": "COOPER-PUCKETT",
      "jobTitle": "directore",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 689031,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 710360,
          "lastContact": "2017-08-28T16:48:16.59",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303628,
      "businessName": "BRIGHTSTAR CARE",
      "phone": "(530) 332-9699",
      "industry": "HOME HEALTH SERVICE",
      "firstName": "DONNA",
      "lastName": "ZIMMERMAN",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 689032,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 710361,
          "lastContact": "2017-08-28T16:57:38.09",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303629,
      "businessName": "CALIFORNIA DEPT-SOCIAL SVC",
      "phone": "(530) 895-6143",
      "industry": "STATE GOVERNMENT-SOCIAL/HUMAN RESOURCES",
      "firstName": "ROXANNA",
      "lastName": "GRASSINI",
      "jobTitle": "director",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1155243,
          "lastContact": "2017-08-28T16:49:24.707",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303633,
      "businessName": "HEALTH SERVICE DEPT",
      "phone": "(530) 895-6711",
      "industry": "STATE GOVERNMENT-PUBLIC HEALTH PROGRAMS",
      "firstName": "Bob ",
      "lastName": "mgee",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1174049,
          "lastContact": "2017-08-28T16:50:19.88",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303634,
      "businessName": "INDEPENDENT LIVING SVC",
      "phone": "(530) 893-8527",
      "industry": "STATE GOVERNMENT-SOCIAL/HUMAN RESOURCES",
      "firstName": "EVAN",
      "lastName": "LE VANG",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1194835,
          "lastContact": "2017-08-28T17:18:51.51",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303635,
      "businessName": "MECHOOPDA INDIAN TRIBE",
      "phone": "(530) 899-8922",
      "industry": "SOCIAL SERVICE & WELFARE ORGANIZATIONS",
      "firstName": "CYNTHIA",
      "lastName": "PHILLIPS",
      "jobTitle": "chief",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 689039,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1160501,
          "lastContact": "2017-08-28T17:22:29.73",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303642,
      "businessName": "CALIFORNIA PARK REHAB HOSPITAL",
      "phone": "(530) 894-1010",
      "industry": "RESIDENTIAL CARE HOMES",
      "firstName": "JERRY",
      "lastName": "HOLLOWAY",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 1,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 689046,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1180944,
          "lastContact": "2017-08-28T16:59:45.19",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303650,
      "businessName": "ROSELEAF GARDEN",
      "phone": "(530) 895-0800",
      "industry": "RESIDENTIAL CARE HOMES",
      "firstName": "FLORIE",
      "lastName": "KAMPBELL",
      "jobTitle": null,
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 1,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 689054,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1167116,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303651,
      "businessName": "WINDCHIME OF CHICO",
      "phone": "(530) 566-1800",
      "industry": "RESIDENTIAL CARE HOMES",
      "firstName": "DONALD",
      "lastName": "PIKE",
      "jobTitle": null,
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 1,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 689055,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1187881,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303692,
      "businessName": "FAR NORTHERN REGIONAL DEVMNT",
      "phone": "(530) 994-3658",
      "industry": "SOCIAL SERVICE & WELFARE ORGANIZATIONS",
      "firstName": "CYNDY",
      "lastName": "MADISON",
      "jobTitle": "director",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1152660,
          "lastContact": "2017-08-28T17:13:40.56",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303694,
      "businessName": "NORTHERN VALLEY CATHOLIC SVC",
      "phone": "(530) 345-1600",
      "industry": "SOCIAL SERVICE & WELFARE ORGANIZATIONS",
      "firstName": "LAURA",
      "lastName": "BEST",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 689098,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1187885,
          "lastContact": "2017-08-28T17:24:09",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303695,
      "businessName": "PRESTIGE ASSISTED LIVING-CHICO",
      "phone": "(530) 899-0814",
      "industry": "RESIDENTIAL CARE HOMES",
      "firstName": "PAT",
      "lastName": "WATTERS",
      "jobTitle": null,
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 1,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 689099,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1152661,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303696,
      "businessName": "SALVATION ARMY THRIFT STORE",
      "phone": "(530) 342-2087",
      "industry": "THRIFT SHOPS",
      "firstName": "Sam",
      "lastName": "shepard",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1174053,
          "lastContact": "2017-08-28T17:26:44.063",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303697,
      "businessName": "SPRINGS OF LIVING WATER",
      "phone": "(530) 893-6750",
      "industry": "NON-PROFIT ORGANIZATIONS",
      "firstName": "DALE",
      "lastName": "HARRISON",
      "jobTitle": "administrator",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1194841,
          "lastContact": "2017-08-18T15:59:50.68",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303698,
      "businessName": "TOWNSEND HOUSE",
      "phone": "(530) 342-4455",
      "industry": "RESIDENTIAL CARE HOMES",
      "firstName": "Susan",
      "lastName": "Hertzfeldt",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 1,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 689102,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1160506,
          "lastContact": "2017-08-28T17:28:13.88",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 303699,
      "businessName": "WALGREENS SPECIALTY PHARMACY",
      "phone": "(530) 000-1111",
      "industry": "PHARMACIES",
      "firstName": "teresa",
      "lastName": "brown",
      "jobTitle": null,
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1174054,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 305916,
      "businessName": "Sycamore Glenn",
      "phone": "(530) 894-0384",
      "industry": "Senior Community",
      "firstName": "Brian",
      "lastName": "Vittitoe",
      "jobTitle": "Ditecotr",
      "isStandardized": true,
      "employmentSize": "500",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 1,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 691340,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1155345,
          "lastContact": "2017-08-28T17:27:33.52",
          "outreachStatus": 3,
          "outreachStatusTitle": "Contract Initiated",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 312381,
      "businessName": "ALMOND GROVE",
      "phone": null,
      "industry": null,
      "firstName": null,
      "lastName": null,
      "jobTitle": null,
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 698313,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 313089,
      "businessName": "CHICO AREA RECREATION & PARKS - SENIOR",
      "phone": null,
      "industry": null,
      "firstName": "Laura",
      "lastName": "Beck",
      "jobTitle": "Recreation Coordinator",
      "isStandardized": true,
      "employmentSize": "",
      "isPreviousClient": true,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 699021,
          "lastContact": "2017-08-28T16:48:54.61",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 467804,
      "businessName": "AMF ORCHARD LANES",
      "phone": "(530) 895-3257",
      "industry": "BOWLING CENTERS",
      "firstName": "",
      "lastName": "",
      "jobTitle": null,
      "isStandardized": true,
      "employmentSize": "39",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 861167,
          "lastContact": "2017-06-21T17:20:58.893",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 467821,
      "businessName": "CHUCK PATTERSON TOWING",
      "phone": "(530) 895-0344",
      "industry": "WRECKER SERVICE",
      "firstName": "JUDY",
      "lastName": "BIRDSEYE",
      "jobTitle": "MANAGER",
      "isStandardized": true,
      "employmentSize": "14",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 861184,
          "lastContact": "2017-08-08T11:30:01.607",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 467822,
      "businessName": "CHUCK PATTERSON TOYOTA SCION",
      "phone": "(530) 895-1771",
      "industry": "AUTOMOBILE DEALERS-USED CARS",
      "firstName": "CHUCK",
      "lastName": "PATTERSON",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "105",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 861185,
          "lastContact": "2017-08-28T17:03:58.797",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 467842,
      "businessName": "HARBOR FREIGHT TOOLS",
      "phone": "(530) 892-8908",
      "industry": "TOOLS-NEW & USED",
      "firstName": "CHARLES",
      "lastName": "WHEELER",
      "jobTitle": "SITE MANAGER",
      "isStandardized": true,
      "employmentSize": "15",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 861205,
          "lastContact": "2017-06-21T17:17:28.333",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 467849,
      "businessName": "LES SCHWAB",
      "phone": "(530) 345-5557",
      "industry": "TIRE-DEALERS-RETAIL",
      "firstName": "ROB",
      "lastName": "ROBERTSON",
      "jobTitle": "SITE MANAGER",
      "isStandardized": true,
      "employmentSize": "20",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 861212,
          "lastContact": "2017-08-28T17:20:57.62",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 467850,
      "businessName": "MICHAEL L STINSON & ASSOC",
      "phone": "(530) 345-7697",
      "industry": "EDUCATIONAL CONSULTANTS",
      "firstName": "MICHAEL L",
      "lastName": "STINSON",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "11",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 861213,
          "lastContact": "2017-08-28T17:23:35.31",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 467870,
      "businessName": "THRIFT STORE CLEARANCE OUTLET",
      "phone": "(530) 893-0522",
      "industry": "STATIONERS-RETAIL",
      "firstName": "bob",
      "lastName": "tolar",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "14",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 861233,
          "lastContact": "2017-08-29T16:09:27.89",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 468109,
      "businessName": "AMPM",
      "phone": "(530) 891-1938",
      "industry": "CONVENIENCE STORES",
      "firstName": "A J",
      "lastName": "JOHL",
      "jobTitle": "CFO",
      "isStandardized": true,
      "employmentSize": "11",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 861472,
          "lastContact": "2017-06-21T17:19:01.487",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 621799,
      "businessName": "Giselle's",
      "phone": "(530) 893-8078",
      "industry": "Assisted Living Facility",
      "firstName": "",
      "lastName": "",
      "jobTitle": null,
      "isStandardized": false,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1016588,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1016589,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 629038,
      "businessName": "Transfer Flow",
      "phone": "(530) 893-5209",
      "industry": "industrial",
      "firstName": "george",
      "lastName": "miller",
      "jobTitle": "directer",
      "isStandardized": false,
      "employmentSize": "20",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1025549,
          "lastContact": "2017-08-08T11:31:55.643",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 629039,
      "businessName": "Sierra Sunrise Apartments",
      "phone": "(530) 894-3220",
      "industry": "senior apartments",
      "firstName": "sierra",
      "lastName": "sunrise",
      "jobTitle": "manager",
      "isStandardized": false,
      "employmentSize": "N/A",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1025550,
          "lastContact": "2017-08-28T17:09:49.233",
          "outreachStatus": 3,
          "outreachStatusTitle": "Contract Initiated",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1249745,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 629042,
      "businessName": "Amber Grove Place",
      "phone": "(530) 487-0751",
      "industry": "alzheimers facility",
      "firstName": "tom ",
      "lastName": "skittle",
      "jobTitle": "manager",
      "isStandardized": false,
      "employmentSize": "N/A",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1025553,
          "lastContact": "2017-08-28T16:53:57.72",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 638790,
      "businessName": "AMBER GROVE PLACE",
      "phone": "(530) 826-3226",
      "industry": null,
      "firstName": "Diania",
      "lastName": "Bingham",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "30",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 1,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1037523,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1195378,
          "lastContact": "2017-08-28T16:52:53.98",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 642170,
      "businessName": "FAR NORTHERN REGL",
      "phone": "(530) 895-8633",
      "industry": null,
      "firstName": "LISA",
      "lastName": "BENARON",
      "jobTitle": "MANAGER",
      "isStandardized": true,
      "employmentSize": "100",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1181916,
          "lastContact": "2017-08-28T17:14:17.047",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 643666,
      "businessName": "HOME HEALTH CARE MANAGEMENT",
      "phone": "(530) 343-0727",
      "industry": null,
      "firstName": "BARBARA",
      "lastName": "HANNA",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "200",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1161538,
          "lastContact": "2017-08-28T17:17:25.853",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 645440,
      "businessName": "MERIT MEDI-TRANS",
      "phone": "(530) 893-8690",
      "industry": null,
      "firstName": "JOE",
      "lastName": "MISILI",
      "jobTitle": "CEO",
      "isStandardized": true,
      "employmentSize": "30",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1202200,
          "lastContact": "2017-08-28T17:22:57.543",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 670309,
      "businessName": "BURGER KING",
      "phone": "(530) 891-6291",
      "industry": null,
      "firstName": "GEORGE",
      "lastName": "SILER",
      "jobTitle": "SITE MANAGER",
      "isStandardized": true,
      "employmentSize": "27",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1069042,
          "lastContact": "2017-06-21T17:20:32.593",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 670313,
      "businessName": "CHICO CREEK CARE & REHAB",
      "phone": "(530) 345-1306",
      "industry": null,
      "firstName": "CARL",
      "lastName": "LEWIS",
      "jobTitle": "ADMINISTRATOR",
      "isStandardized": true,
      "employmentSize": "200",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1069046,
          "lastContact": "2017-08-28T17:01:48.163",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 670316,
      "businessName": "CONVENIENT CARE HEALTH CTR",
      "phone": "(530) 342-2273",
      "industry": null,
      "firstName": "BEV",
      "lastName": "BLAKEMORE",
      "jobTitle": "OFFICE MANAGER",
      "isStandardized": true,
      "employmentSize": "24",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1069049,
          "lastContact": "2017-08-28T17:04:24.017",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 670317,
      "businessName": "KENDAL E CORNELL LAW OFFICES",
      "phone": "(530) 891-6222",
      "industry": null,
      "firstName": "KENDAL",
      "lastName": "CORNELL",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "16",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1069050,
          "lastContact": "2017-08-28T17:20:30.073",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 670318,
      "businessName": "COUNTRY WAFFLES",
      "phone": "(530) 345-1149",
      "industry": null,
      "firstName": "GARRY",
      "lastName": "BROOKS",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "15",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1069051,
          "lastContact": "2017-08-28T17:04:52.19",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 670319,
      "businessName": "BUTTE COUNTY BEHAVIORAL HEALTH",
      "phone": "(530) 891-2784",
      "industry": null,
      "firstName": "ANN",
      "lastName": "ROBIN",
      "jobTitle": "DIRECTOR",
      "isStandardized": true,
      "employmentSize": "50",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1069052,
          "lastContact": "2017-08-28T16:58:03.87",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 735309,
      "businessName": "BIG 5 SPORTING GOODS",
      "phone": "(530) 891-1545",
      "industry": null,
      "firstName": "JIM",
      "lastName": "ARNOLD",
      "jobTitle": "SITE MANAGER",
      "isStandardized": true,
      "employmentSize": "21",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1134042,
          "lastContact": "2017-08-28T16:55:11.41",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 735310,
      "businessName": "COZY DINER",
      "phone": "(530) 895-1195",
      "industry": null,
      "firstName": "JOHN",
      "lastName": "CASTALDO",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "50",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1134043,
          "lastContact": "2017-08-28T17:07:57.463",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 735311,
      "businessName": "ERIC'S CAR WASH",
      "phone": "(530) 893-4400",
      "industry": null,
      "firstName": "ERIC",
      "lastName": "LARSON",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "23",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1134044,
          "lastContact": "2017-08-18T16:00:54.853",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 735312,
      "businessName": "DAHLMEIER INSURANCE INC",
      "phone": "(530) 342-6421",
      "industry": null,
      "firstName": "JOHN",
      "lastName": "DAHLMEIER",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "15",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1134045,
          "lastContact": "2017-08-28T17:12:57.137",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 735314,
      "businessName": "CELESTINO'S PASTA & PIZZA",
      "phone": "(530) 345-7700",
      "industry": null,
      "firstName": "bob",
      "lastName": "ceistina",
      "jobTitle": "maager",
      "isStandardized": true,
      "employmentSize": "20",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1134047,
          "lastContact": "2017-08-28T17:00:57.85",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 735315,
      "businessName": "INNOVATIVE PRESCHOOL INC",
      "phone": "(530) 343-2028",
      "industry": null,
      "firstName": "CATE",
      "lastName": "CAYLOR",
      "jobTitle": "DIRECTOR",
      "isStandardized": true,
      "employmentSize": "15",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1134048,
          "lastContact": "2017-08-28T17:19:27.933",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 765664,
      "businessName": "Les Schwab",
      "phone": "(530) 898-1635",
      "industry": "Automotive",
      "firstName": "Monte",
      "lastName": "Brown",
      "jobTitle": "Asst. Manager",
      "isStandardized": false,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1220357,
          "lastContact": "2017-08-28T17:21:35.06",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 765667,
      "businessName": "WinCo",
      "phone": "(530) 345-7899",
      "industry": "Grocery",
      "firstName": "Gina",
      "lastName": "Mulkey",
      "jobTitle": "",
      "isStandardized": false,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1220362,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 769940,
      "businessName": "Giselle`s Care Home",
      "phone": "(530) 893-1716",
      "industry": "residential care facility",
      "firstName": "",
      "lastName": "",
      "jobTitle": "",
      "isStandardized": false,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1225773,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1225774,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 1968725,
      "businessName": "PAIVA HULLING & SHELLING INC",
      "phone": "(530) 345-8491",
      "industry": "NUTS-EDIBLE-WHOLESALE & PROCESSING",
      "firstName": "JAMES M",
      "lastName": "PAIVA",
      "jobTitle": "CEO",
      "isStandardized": true,
      "employmentSize": "125",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 116345,
          "lastContact": "2017-08-28T17:25:22.923",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 1970664,
      "businessName": "BOYS & GIRLS CLUB OF CHICO",
      "phone": "(530) 899-0335",
      "industry": "YOUTH ORGANIZATIONS & CENTERS",
      "firstName": "RASHELL",
      "lastName": "BROBST",
      "jobTitle": "CEO",
      "isStandardized": true,
      "employmentSize": "43",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 116242,
          "lastContact": "2017-08-28T16:56:24.977",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 1970706,
      "businessName": "CHICO POLICE DEPT",
      "phone": "(530) 899-8613",
      "industry": "POLICE DEPARTMENTS",
      "firstName": "Joe",
      "lastName": "mack",
      "jobTitle": "desk ",
      "isStandardized": true,
      "employmentSize": "10",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 116246,
          "lastContact": "2017-08-28T17:02:30.777",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 1970712,
      "businessName": "CHP CHICO",
      "phone": "(530) 342-4679",
      "industry": "POLICE DEPARTMENTS",
      "firstName": "Unknown",
      "lastName": "unkonw",
      "jobTitle": "unknown ",
      "isStandardized": true,
      "employmentSize": "10",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 116247,
          "lastContact": "2017-08-28T17:03:29.873",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 1970783,
      "businessName": "HOUSING AUTHORITY-THE COUNTY",
      "phone": "(530) 895-4474",
      "industry": "HOUSING AUTHORITIES",
      "firstName": "ED",
      "lastName": "MAYER",
      "jobTitle": "EXEC DIRECTOR",
      "isStandardized": true,
      "employmentSize": "32",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 116253,
          "lastContact": "2017-08-28T17:18:04.247",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 4091155,
      "businessName": "AMVETS DEPT-CA SVC FOUNDATION",
      "phone": "(530) 343-3967",
      "industry": "VETERANS' & MILITARY ORGANIZATIONS",
      "firstName": "amy",
      "lastName": "SHUSTER",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "3",
      "isPreviousClient": false,
      "isMilitaryOpportunity": true,
      "sicPriority": 0,
      "isStoreConfirmed": false,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1289041,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 4105052,
      "businessName": "INSPIRATIONS ADSP INC",
      "phone": "(530) 809-2384",
      "industry": "DAY CARE CENTERS-ADULT",
      "firstName": "George ",
      "lastName": "spensor",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "7",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": false,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1301784,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 4112879,
      "businessName": "HOME INSTEAD SENIOR CARE",
      "phone": "(530) 895-6100",
      "industry": "HOME HEALTH SERVICE",
      "firstName": "NATHAN",
      "lastName": "VAIL",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "9",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": false,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1308865,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 7571216,
      "businessName": "BIDWELL JUNIOR HIGH SCHOOL",
      "phone": "(530) 891-3080",
      "industry": "SCHOOLS",
      "firstName": "JOANNE",
      "lastName": "PARSLEY",
      "jobTitle": "PRINCIPAL",
      "isStandardized": true,
      "employmentSize": "100",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1342730,
          "lastContact": "2017-08-28T16:54:32.237",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 7571766,
      "businessName": "WITTMEIER CHEVROLET",
      "phone": "(530) 895-8181",
      "industry": "AUTOMOBILE DEALERS-NEW CARS",
      "firstName": "ED",
      "lastName": "WITTMEIER",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "243",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1335184,
          "lastContact": "2017-08-28T17:29:52.68",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 7571774,
      "businessName": "WREX PRODUCTS INC",
      "phone": "(530) 895-3838",
      "industry": "PLASTICS-MOLD-MANUFACTURERS",
      "firstName": "JIM",
      "lastName": "BARNETT",
      "jobTitle": "PRESIDENT",
      "isStandardized": true,
      "employmentSize": "100",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1384876,
          "lastContact": "2017-08-28T17:30:25.65",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 7572780,
      "businessName": "COURTESY BUICK GMC OF CHICO",
      "phone": "(559) 824-2026",
      "industry": "AUTOMOBILE DEALERS-NEW CARS",
      "firstName": "RON",
      "lastName": "FARIA",
      "jobTitle": "PRESIDENT",
      "isStandardized": true,
      "employmentSize": "100",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1377329,
          "lastContact": "2017-08-28T17:05:17.987",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 7640077,
      "businessName": "ALMOND GROVE",
      "phone": "(530) 342-6056",
      "industry": "ASSISTED LIVING FACILITY",
      "firstName": "",
      "lastName": "",
      "jobTitle": "",
      "isStandardized": false,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1401747,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 7640220,
      "businessName": "Courtyard Little Chico Creek",
      "phone": "(530) 342-0707",
      "industry": "Senior",
      "firstName": "Jason",
      "lastName": "Stone",
      "jobTitle": "manager",
      "isStandardized": false,
      "employmentSize": "",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1401929,
          "lastContact": "2017-08-28T17:12:28.23",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8347283,
      "businessName": "BOTHIN YOUTH CTR",
      "phone": "(510) 562-8470",
      "industry": "VETERANS' & MILITARY ORGANIZATIONS",
      "firstName": "Bob",
      "lastName": "station",
      "jobTitle": "manager",
      "isStandardized": true,
      "employmentSize": "3",
      "isPreviousClient": false,
      "isMilitaryOpportunity": true,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1436543,
          "lastContact": "2017-08-28T16:55:53.583",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1464520,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8348247,
      "businessName": "CALIFORNIA TRIBAL TANF",
      "phone": "(530) 566-9820",
      "industry": "SOCIAL SERVICE & WELFARE ORGANIZATIONS",
      "firstName": "SALINA",
      "lastName": "ANDERSON",
      "jobTitle": "MANAGER",
      "isStandardized": true,
      "employmentSize": "3",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1436542,
          "lastContact": "2017-08-28T17:00:18.05",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1464519,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8348261,
      "businessName": "GISELLE'S CARE HOME",
      "phone": "(530) 893-8078",
      "industry": "RESIDENTIAL CARE HOMES",
      "firstName": "ELIZABETH",
      "lastName": "MEMORACION",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "2",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 1,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1436548,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1464526,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8348262,
      "businessName": "INDEPENDENT LIVING SVC",
      "phone": "(530) 893-8527",
      "industry": "SOCIAL SERVICE & WELFARE ORGANIZATIONS",
      "firstName": "EVAN",
      "lastName": "LE VANG",
      "jobTitle": "MANAGER",
      "isStandardized": true,
      "employmentSize": "5",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1464521,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8348269,
      "businessName": "BRIDGING THE GAP BY GIVING",
      "phone": "(530) 342-5746",
      "industry": "NON-PROFIT ORGANIZATIONS",
      "firstName": "SHIRLEY J",
      "lastName": "ADAMS",
      "jobTitle": "PRESIDENT",
      "isStandardized": true,
      "employmentSize": "1",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1436540,
          "lastContact": "2017-08-28T16:56:50.853",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1464517,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8348270,
      "businessName": "BUTTE-GLENN MEDICAL SOCIETY",
      "phone": "(530) 342-4296",
      "industry": "NON-PROFIT ORGANIZATIONS",
      "firstName": "PAMELA",
      "lastName": "NELSON",
      "jobTitle": "EXEC DIRECTOR",
      "isStandardized": true,
      "employmentSize": "1",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1436541,
          "lastContact": "2017-08-28T16:59:17.297",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1464518,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8348280,
      "businessName": "INTERIM HEALTH CARE",
      "phone": "(530) 899-9777",
      "industry": "HOME HEALTH SERVICE",
      "firstName": "GREG",
      "lastName": "ASHER",
      "jobTitle": "MANAGER",
      "isStandardized": true,
      "employmentSize": "2",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1436546,
          "lastContact": "2017-08-28T17:19:56.56",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1464524,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8348284,
      "businessName": "ORCHARD VIEW SENIOR CARE",
      "phone": "(530) 518-6640",
      "industry": "RETIREMENT COMMUNITIES & HOMES",
      "firstName": "MICHAEL",
      "lastName": "GRATTIDGE",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "3",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 1,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1436545,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1464523,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8348288,
      "businessName": "VETERANS SERVICE OFFICE",
      "phone": "(530) 891-2759",
      "industry": "VETERANS' & MILITARY ORGANIZATIONS",
      "firstName": "HANNA",
      "lastName": "WILLIAMSON",
      "jobTitle": "MANAGER",
      "isStandardized": true,
      "employmentSize": "3",
      "isPreviousClient": false,
      "isMilitaryOpportunity": true,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1436544,
          "lastContact": "2017-08-28T17:29:08.023",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1464522,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8348290,
      "businessName": "PASSAGES-AREA AGENCY ON AGING",
      "phone": "(530) 283-0891",
      "industry": "SENIOR CITIZENS SERVICE ORGANIZATIONS",
      "firstName": "JOE",
      "lastName": "COBERY",
      "jobTitle": "EXEC DIRECTOR",
      "isStandardized": true,
      "employmentSize": "30",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1436547,
          "lastContact": "2017-08-28T17:26:03.83",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1464525,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 8353805,
      "businessName": "Open  Sky",
      "phone": "(530) 899-0887",
      "industry": "Health",
      "firstName": "Laura",
      "lastName": "Ryan",
      "jobTitle": "Director People",
      "isStandardized": false,
      "employmentSize": "30",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1468888,
          "lastContact": "2017-09-19T15:22:10.8",
          "outreachStatus": 3,
          "outreachStatusTitle": "Contract Initiated",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 9902804,
      "businessName": "CHICO CITY POLICE DEPT",
      "phone": "(530) 897-4900",
      "industry": "POLICE DEPARTMENTS",
      "firstName": "MICHAEL R",
      "lastName": "MALONEY",
      "jobTitle": "MANAGER",
      "isStandardized": true,
      "employmentSize": "150",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1495065,
          "lastContact": "2017-08-28T17:01:22.38",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 9903092,
      "businessName": "BUTTE HOME HEALTH & HOSPICE",
      "phone": "(530) 895-0462",
      "industry": "HOME HEALTH SERVICE",
      "firstName": "BROOKE",
      "lastName": "QUILICI",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "100",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1501532,
          "lastContact": "2017-08-28T16:58:46.717",
          "outreachStatus": 4,
          "outreachStatusTitle": "No Client Interest",
          "isScheduledClinics": false
        }
      ]
    },
    {
      "businessPk": 10513311,
      "businessName": "HOME HEALTH CARE MANAGEMENT",
      "phone": "(530) 343-0727",
      "industry": "HOME HEALTH SERVICE",
      "firstName": "BARBARA",
      "lastName": "HANNA",
      "jobTitle": "OWNER",
      "isStandardized": true,
      "employmentSize": "200",
      "isPreviousClient": false,
      "isMilitaryOpportunity": false,
      "sicPriority": 0,
      "isStoreConfirmed": true,
      "outreachEffortList": [
        {
          "outreachProgram": "SR",
          "outreachBusinessPk": 1514100,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        },
        {
          "outreachProgram": "IP",
          "outreachBusinessPk": 1525328,
          "lastContact": "0001-01-01T00:00:00",
          "outreachStatus": 0,
          "outreachStatusTitle": "No Outreach",
          "isScheduledClinics": false
        }
      ]
    }
  ],
  "workflowMonitorStatusList": [
    {
      "storeTasksStatusId": 0,
      "storeTasksStatus": "Contact Client",
      "statusCountByStore": 0,
      "statusCountByUser": 20
    },
    {
      "storeTasksStatusId": 2,
      "storeTasksStatus": "Complete/Report Clinic",
      "statusCountByStore": 23,
      "statusCountByUser": 7
    },
    {
      "storeTasksStatusId": 3,
      "storeTasksStatus": "Follow-up",
      "statusCountByStore": 44,
      "statusCountByUser": 32
    },
    {
      "storeTasksStatusId": 4,
      "storeTasksStatus": "Perform Clinic",
      "statusCountByStore": 44,
      "statusCountByUser": 7
    }
  ],
  "ScheduledEvents": [
    {
      "storeId": 2362,
      "businessPk": 54782,
      "businessName": "BEDFORD CLARIAN HOME HEALTH - CLINIC A",
      "createDate": "2017-07-20T14:46:25.64",
      "isStandardized": true,
      "accountType": "Charity (HHS Voucher)",
      "contactLogPk": "3314",
      "lastContact": ",07/20/2017",
      "outreachStatus": ",1",
      "clinicTime": "12:00AM - 11:30PM",
      "clinicDate": "10/28/2017",
      "outreachStatusTitle": ",Confirmed",
    },
    {
      "storeId": 2362,
      "businessPk": null,
      "businessName": "BEDFORD PUBLIC LIBRARY",
      "createDate": "2013-01-31T08:54:39.173",
      "isStandardized": true,
      "accountType": "Local",
      "contactLogPk": "3314",
      "lastContact": ",07/20/2017",
      "outreachStatus": ",1",
      "clinicTime": "12:00AM - 11:30PM",
      "clinicDate": null,
      "outreachStatusTitle": ",Contact Client",
    },
    {
      "storeId": 2362,
      "businessPk": 61955,
      "businessName": "FIRST PRESBYTERIAN CHURCH - CLINIC A",
      "createDate": "2018-03-08T15:08:38.803",
      "isStandardized": true,
      "accountType": "Community Outreach",
      "contactLogPk": "3314",
      "lastContact": ",03/08/2018",
      "outreachStatus": ",1",
      "clinicTime": "12:00AM - 11:30PM",
      "clinicDate": "05/26/2018",
      "outreachStatusTitle": ",Contact Client",
    }
  ],
  "OutreachStatuses": [
    {
      "pk": 0,
      "outreachStatus": "No Outreach",
      "category": "SO",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 1,
      "outreachStatus": "Active",
      "category": "SO",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 2,
      "outreachStatus": "Follow-up",
      "category": "SO",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 3,
      "outreachStatus": "Event Scheduled",
      "category": "SO",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 4,
      "outreachStatus": "No Client Interest",
      "category": "SO",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 5,
      "outreachStatus": "Business Closed",
      "category": "SO",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 0,
      "outreachStatus": "Contact Client",
      "category": "AC",
      "isActive": true,
      "businessTypeId": 2
    },
    {
      "pk": 1,
      "outreachStatus": "Confirmed",
      "category": "AC",
      "isActive": true,
      "businessTypeId": 2
    },
    {
      "pk": 2,
      "outreachStatus": "Cancelled",
      "category": "AC",
      "isActive": true,
      "businessTypeId": 2
    },
    {
      "pk": 3,
      "outreachStatus": "Completed",
      "category": "AC",
      "isActive": true,
      "businessTypeId": 2
    },
    {
      "pk": 0,
      "outreachStatus": "No Outreach",
      "category": "IP",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 1,
      "outreachStatus": "Active",
      "category": "IP",
      "isActive": false,
      "businessTypeId": 1
    },
    {
      "pk": 2,
      "outreachStatus": "Follow-up",
      "category": "IP",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 3,
      "outreachStatus": "Contract Initiated",
      "category": "IP",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 4,
      "outreachStatus": "No Client Interest",
      "category": "IP",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 5,
      "outreachStatus": "Business Closed",
      "category": "IP",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 6,
      "outreachStatus": "Undecided",
      "category": "IP",
      "isActive": false,
      "businessTypeId": 1
    },
    {
      "pk": 7,
      "outreachStatus": "Contacted Last Season",
      "category": "IP",
      "isActive": false,
      "businessTypeId": 1
    },
    {
      "pk": 8,
      "outreachStatus": "Charity (HHS Voucher)",
      "category": "IP",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 9,
      "outreachStatus": "DM Contacted",
      "category": "IP",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 10,
      "outreachStatus": "HCS Contacted",
      "category": "IP",
      "isActive": true,
      "businessTypeId": 1
    },
    {
      "pk": 11,
      "outreachStatus": "Vote & Vax",
      "category": "IP",
      "isActive": false,
      "businessTypeId": 1
    },
    {
      "pk": 12,
      "outreachStatus": "Community Outreach",
      "category": "IP",
      "isActive": true,
      "businessTypeId": 1
    }
  ]
}