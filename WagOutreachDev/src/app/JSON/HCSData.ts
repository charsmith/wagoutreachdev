export const HCSData: any = {
    "HCSMetrics": [
      {
        "metricId": 2,
        "metricName": "Review Clinics Pending Signed Contract",
        "metricDesc": "Clinics Pending Signed Contract",
        "metricCount": 259
      },
      {
        "metricId": 3,
        "metricName": "Review Contracts Drafted but Not Sent",
        "metricDesc": "Contracts Drafted but Not Sent",
        "metricCount": 432
      },
      {
        "metricId": 1,
        "metricName": "Review Clinics Executed to Date",
        "metricDesc": "Clinics Executed to Date",
        "metricCount": 57
      },
      {
        "metricId": 4,
        "metricName": "Review Clinics with Signed Contract to be Executed",
        "metricDesc": "Clinics with Signed Contract to be Executed",
        "metricCount": 587
      }
    ],
    "HCSAction": [
      {
        "actionItemId": 1,
        "actionItem": "Corporate Clinics to be Reviewed/Assigned",
        "hcsCount": 297,
        "storeCount": 0
      },
      {
        "actionItemId": 4,
        "actionItem": "Corporate Clinics to be Confirmed",
        "hcsCount": 926,
        "storeCount": 0
      },
      {
        "actionItemId": 5,
        "actionItem": "Corporate Clinics to be Completed",
        "hcsCount": 5,
        "storeCount": 2
      },    
      {
        "actionItemId": 3,
        "actionItem": "Local Leads to be Reviewed/Assigned",
        "hcsCount": 58885,
        "storeCount": null
      }
    ]
  }