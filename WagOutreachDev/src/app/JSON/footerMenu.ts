export const adminFooterMenuItems: any[] = [{
    title: "Home", 
    url: "/communityoutreach/home"
  },
  {
    title: "Dashboard",
    url: "/communityoutreach/home"  
  },  
  {
    title: "Store",
    url: "/communityoutreach/storehome",  
  },
  {
    title: "Resources",    
    url: "/communityoutreach/imzresources"   
  },
  {
    title: "Reports",    
    url: "/communityoutreach/scheduleclinicreport"   
  }
  ]

  export const storeManagerFooterMenuItems: any[] = [{
    title: "Home", 
    url: "/communityoutreach/storehome"
  },   
  {
    title: "Store",
    url: "/communityoutreach/storehome",  
  },
  {
    title: "Reports",    
    url: "/communityoutreach/storehome"   
  }
  ]

  export const districtManagerFooterMenuItems: any[] = [{
    title: "Home", 
    url: "/communityoutreach/storehome"
  },   
  {
    title: "Store",
    url: "/communityoutreach/storehome",  
  },
  {
    title: "Reports",    
    url: "/communityoutreach/storehome"   
  }
  ]

  export const healthcareSupervisorFooterMenuItems: any[] = [{
    title: "Home", 
    url: "/communityoutreach/storehome"
  },   
  {
    title: "Store",
    url: "/communityoutreach/storehome",  
  },
  {
    title: "Reports",    
    url: "/communityoutreach/storehome"   
  }
  ]

  export const regionalVicePresidentFooterMenuItems: any[] = [{
    title: "Home", 
    url: "/communityoutreach/storehome"
  },   
  {
    title: "Store",
    url: "/communityoutreach/storehome",  
  },
  {
    title: "Reports",    
    url: "/communityoutreach/storehome"   
  }
  ]
  export const directorRxRetailFooterMenuItems: any[] = [{
    title: "Home", 
    url: "/communityoutreach/storehome"
  },   
  {
    title: "Store",
    url: "/communityoutreach/storehome",  
  },
  {
    title: "Reports",    
    url: "/communityoutreach/storehome"   
  }
  ]