export const LocalAgreement: any = {
      "ImmunizationsList":[
         {
            "immunizationId":29,
            "immunizationName":"Influenza - Standard/PF Injectable (trivalent)",
            "immunizationSpanishName":"Influenza - Vacuna inyectable trivalente en dosis estándar",
            "paymentOption":0,
            "price":25.99,
            "directBillPrice":25.00,
            "isActive":true,
            "paymentTypeId":6,
            "paymentTypeName":"Corporate to Invoice Employer Directly",
            "paymentTypeSpanishName":"Corporativo al Empleador directamente Factura",
            "paymentGroup":0,
            "isSelected":"False",
            "isPaymentSelected":"False",
            "name":"",
            "address1":"",
            "address2":"",
            "city":"",
            "state":"",
            "zip":"",
            "phone":"",
            "email":"",
            "tax":"",
            "copay":"",
            "copayValue":"",
            "isVoucherNeeded":"",
            "voucherExpirationDate":""
         }
      ],
      "ClinicAgreement":{
         "isApproved":"1",
         "approvedOrRejectedBy":"vishnu.mugdhasani@senecaglobal.com",
         "signature":"MVVR",
         "isEmailSent":"1",
         "Clinics":{
            "clinic":[
               {
                  "clinicLocation":"CLINIC LOCATION A",
                  "localContactName":"Jeanie Barrett",
                  "localContactEmail":"jeanie.samples@testemail.com",
                  "localContactPhone":"562-634-4693",
                  "clinicDate":"02/28/2018 12:00:00 AM",
                  "startTime":"9:00am",
                  "endTime":"11:00am",
                  "address1":"5901 Downey Ave",
                  "city":"Long Beach",
                  "state":"CA",
                  "zipCode":"90805",
                  "isReassign":"0",
                  "isNoClinic":"0",
                  "isRemoved":"0",
                  "fluExpiryDate":"",
                  "routineExpiryDate":"12/04/2018",
                  "naClinicLatitude":"33.86101731",
                  "naClinicLongitude":"-118.1427433",
                  "immunization":[
                     {
                        "pk":"29",
                        "immunizationName":"Influenza - Standard/PF Injectable (trivalent)",
                        "paymentTypeName":"Corporate to Invoice Employer Directly",
                        "paymentTypeId":"6",
                        "estimatedQuantity":"100"
                     }
                  ]
               }
            ]
         },
         "Immunizations":{
            "Immunization":[
               {
                  "pk":"29",
                  "immunizationName":"Influenza - Standard/PF Injectable (trivalent)",
                  "immunizationSpanishName":"Influenza - Vacuna inyectable trivalente en dosis estándar",
                  "price":"25.00",
                  "paymentTypeId":"6",
                  "paymentTypeName":"Corporate to Invoice Employer Directly",
                  "paymentTypeSpanishName":"Corporativo al Empleador directamente Factura",
                  "sendInvoiceTo":"yes",
                  "name":"Jeanie Barrett",
                  "address1":"5901 Downey Ave",
                  "city":"Long Beach",
                  "state":"CA",
                  "zip":"90805",
                  "phone":"562-634-4693",
                  "email":"jeanie.samples@testemail.com",
                  "tax":"Yes",
                  "copay":"Yes",
                  "copayValue":"120.00",
                  "isVoucherNeeded":"Yes",
                  "voucherExpirationDate":"04/15/2018",
                  "showprice":"yes"
               }
            ]
         },
         "Client":[
            {
               "header":"Walgreens",
               "name":"Mathews Samples",
               "title":"Accountant",
               "date":"02/05/2018"
            }
         ],
         "WalgreenCo":[
            {
               "name":"David Samples",
               "title":"Admin",
               "date":"02/05/2018 11:20:07 PM",
               "districtNumber":"254"
            }
         ],
         "LegalNoticeAddress":{
            "-attentionTo":"SenecaGlobalTest",
            "-address1":"1901, North Block, 4th Street",
            "-city":"Whitefield",
            "-state":"IL",
            "-zipCode":"60948"
         }
      },
      "PreviousLocations":[
         {
            "naClinicAddress1":"BENNETT ST",
            "naClinicAddress2":"",
            "naClinicCity":"PARK HILLS",
            "naClinicState":"MO",
            "naClinicZip":"63601",
            "naClinicAddress":"BENNETT ST, PARK HILLS, MO, 63601",
            "isRemoved":0,
            "rowId":1
         }
      ]
   }