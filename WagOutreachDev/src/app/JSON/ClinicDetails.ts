export const ClinicDetailsJSON: any = {
       "LocationDetails":[
          {
            "ImmunizationDetails":[
                {
                   "pk":185482,
                   "clinicPk":54916,
                   "immunizationPk":48,
                   "immunizationName":"MMR (Measles Mumps rubella)",
                   "price":95.00,
                   "estimatedQuantity":30,
                   "totalImmAdministered":25,
                   "vouchersDistributed":25,
                   "paymentTypeID":7,
                   "paymentTypeName":"Cash Paid by Employee",
                   "name":"",
                   "address1":null,
                   "address2":null,
                   "city":null,
                   "state":"  ",
                   "zip":null,
                   "phone":null,
                   "email":null,
                   "tax":"",
                   "isCoPay":"No",
                   "copayValue":null,
                   "isVoucherNeeded":"",
                   "voucherExpirationDate":null
                },
                {
                    "pk":185483,
                    "clinicPk":54917,
                    "immunizationPk":49,
                    "immunizationName":"Influenza   ",
                    "price":111.00,
                    "estimatedQuantity":322,
                    "totalImmAdministered":3223,
                    "vouchersDistributed":233,
                    "paymentTypeID":7,
                    "paymentTypeName":"Cash Paid by Employee",
                    "name":"",
                    "address1":null,
                    "address2":null,
                    "city":null,
                    "state":"  ",
                    "zip":null,
                    "phone":null,
                    "email":null,
                    "tax":"",
                    "isCoPay":"No",
                    "copayValue":null,
                    "isVoucherNeeded":"",
                    "voucherExpirationDate":null
                 }
             ],
             "outreachBusinessPk":639876,
             "contractedStoreId":1234,
             "storeState":"OH",
             "clinicType":"Local",
             "clinicPk":54916,
             "clinicStoreId":1234,
             "clinicAgreementPk":50674,
             "clinicNumber":1,
             "naClinicLocation":"A",
             "naContactFirstName":"John",
             "naContactLastName":"Samples",
             "naContactEmail":"john.samples@test.com",
             "naClinicAddress1":"6749 EASTLAND RD STE A",
             "naClinicAddress2":"",
             "confirmedClientName":null,
             "naClinicCity":"CLEVELAND",
             "naClinicState":"OH",
             "naClinicZip":"44130",
             "naClinicContactPhone":"987-654-3210",
             "clinicDate":"12/11/2018",
             "clinicScheduledOn":"2017-11-15T00:00:00",
             "naClinicStartTime":"9:00 AM",
             "naClinicEndTime":"02:00 PM",
             "naClinicEstimatedVolume":null,
             "isNoClinic":false,
             "naClinicTotalImmAdministered":null,
             "naClinicVouchersDist":null,
             "contactLogPk":940123,
             "isCurrentSeason":false,
             "clinicAgreementXml":"",
             "maxClinicLocationId":2,
             "clinicLatitude":null,
             "clinicLongitude":null,
             "isReassign":0,
             "isCompleted":false,
             "isConfirmed":false,
             "isCancelled":false,
             "coPayFLUGroupId":null,
             "coPayROUTINEGroupId":null,
             "fluExpiryDate":null,
             "routineExpiryDate":null,
             "marketNumber":13,
             "coOutreachTypeId":'4',
             "coOutreachTypeDesc":'Test'
          }
       ],

       "HistoryLog":{
          "HistoryLogItems":[
              {
                  "date":"2018/03/26",
                  "action":"Completed",
                  "updatedField":"Billing & Vaccine Information:..",
                  "update":"Completed Clinic",
                  "user":"CommunityUpdate@WagOutreach.com"
              },
              {
                "date":"2018/03/24",
                "action":"Updated",
                "updatedField":"Pharmacist & Post Clinic Information::..",
                "update":"Confirmed Clinic",
                "user":"CommunityUpdate@WagOutreach.com"
            }

          ]
       },
       "StoreUserDetails":{
          "storeID":"5624",
          "address":"5624 - 600 W KARSCH BLVD, FARMINGTON, MO 63640",
          "storeManagerEmail":"mgr.05624@store.walgreens.com",
          "pharmacyManagerEmail":"rxm.05624@store.walgreens.com",
          "districtManagerEmail":"d384.dm@walgreens.com",
          "healthcareSupervisorEmail":"aaron.small@walgreens.com",
          "directorRetailOpsEmail":"ryan.kruger@walgreens.com"
       },
        "BusinessInformation":{
          "businessName":"BUYERQUEST",
             "businessAddress":"6749 EASTLAND RD STE A",
             "businessAddress2":"",
             "businessCity":"CLEVELAND",
             "businessState":"OH",
             "businessZip":"44130",
             "firstName":"test",
             "lastName":"kumar",
             "jobTitle":"test",
             "businessContactEmail":"test@test.com",
             "phone":"440-000-1111"
       },
       "BillingVaccineInformation":{
            "naClinicAddlComments":null,
             "naClinicSplBillingInstr":null,
             "recipientId":"8-digit patient DOB, 5-digit patient zip code, 5-digit store#",
             "naClinicPlanId":"IMZ",
             "naClinicGroupId":"5547MACZ (cash routine)"            
       },
       "PharmacistPostClinicInformation":{
          "pharmacistName":"Test",
             "pharmacistPhone":"1234567890",
             "totalHours":20,
             "feedbackNotes":"Test"
       }
    }
 