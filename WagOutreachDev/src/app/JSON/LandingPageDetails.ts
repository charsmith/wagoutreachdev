export const LandingPageDetails: any = {
      "LoggedInUserInfo":{
         "email":"CommunityUpdate@walgreens.com",
         "userName":"CommunityUpdate@walgreens.com",
         "userID":41,
         "userRole":"Admin",
         "userType":"Admin",
         "firstName":"Charlie",
         "lastName":"Smith",
         "locationType":"",
         "assignedLocations":""
      },
      "LandingCounts":{
         "loggedContacts":"Outreach Opportunities Contacted",
         "loggedContactsCount":8165,
         "scheduledClinics":"Clinics Scheduled",
         "scheduledClinicsCount":1033,
         "vaccinesAdministered":"Vaccines Administered",
         "vaccinesAdministeredCount":24567,
         "seniorEvents":"Senior Events Scheduled",
         "seniorEventsCount":68
      }
}