import { Observable } from 'rxjs/Observable';
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { Router } from "@angular/router/router";
import { SessionDetails } from './session';
import { RequestOptions, Headers } from '@angular/http';
export class Util {

    public static handleError(error: any, router: Router) {
        ("Error: " + error);
        let errMsg = error.statusText || error.message || 'Server error';
        if (error.statusText == 'Unauthorized' || error.status == 401) {
            //Need to apply toaster
            // swal(Messages.SESSION.SESSION_TIMEOUT, "", "error");
            // error.statusText = Messages.SESSION.SESSION_TIMEOUT;

        }
        else {
            // swal(Messages.SESSION.HTTP_ERROR, "", "error");
        }
        router.navigate(['/unauthorize']);
        return Observable.throw(error);

    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof FormArray) {
                control.controls.forEach(ctrl => {
                    if (ctrl instanceof FormGroup) {
                        this.validateAllFormFields(ctrl);
                    }
                })
            }
            else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    letter(nNum) {
        var a = "A".charCodeAt(0);
        return String.fromCharCode(a + nNum - 1);
    }

    colLetterToNum(val: any) {
        var base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', i, j, result = 0;

        for (i = 0, j = val.length - 1; i < val.length; i += 1, j -= 1) {
            result += Math.pow(base.length, j) * (base.indexOf(val[i]) + 1);
        }

        return result;
    }

    numberToLetters(nNum) {
        var result;
        if (nNum <= 26) {
            result = this.letter(nNum);
        } else {
            var modulo = nNum % 26;
            var quotient = Math.floor(nNum / 26);
            if (modulo === 0) {
                result = this.letter(quotient - 1) + this.letter(26);
            } else {
                result = this.letter(quotient) + this.letter(modulo);
            }
        }

        return result;
    }
    validateEmail(value: string): boolean {        
        let is_valid: boolean = true;
        var emailTokens = value.split(',');
        var regex_pattern = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        var fn = Validators.pattern(regex_pattern);
        emailTokens.forEach(email => {
            if (!regex_pattern.test(email.trim())) {
                is_valid = false;
                return false;
            }

        });

        return is_valid;
    }

    public static telephoneNumberFormat(number: string) {
        var val = number.replace(/\D/g, '');
        number = val;

        if (val.length > 3 && val.length < 7) {
            number = val.substring(0, 3) + '-' + val.substring(3, 6);
        }

        if (val.length >= 7) {
            number = val.substring(0, 3) + '-' + val.substring(3, 6) + '-'
                + val.substring(6, 10);
        }
        return number;
    }
    public static telephoneNumber(number:string) {
       if(number!=null && number!="")
       {
        var val = number.replace(/\D/g, '');
        number = val;
       }
        
        return number;
    }

    public static onlyNumbers(event: any) {

        if (event.key == '.') {
            event.preventDefault();
            return;
        }

        if (event.charCode == 37) { event.preventDefault(); return; }
        var keys = {
            'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'delete': 46, 'arrowleft': 37, 'arrowright': 39,
            '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
        };
        for (var index in keys) {
            if (!keys.hasOwnProperty(index)) continue;
            if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                return; //default event
            }
        }
        event.preventDefault();
    }

    public static onlyNumberDecimals(event: any) {
        let decimalValue = event.target || event.srcElement;//event.srcElement.value;
        var charCode = (event.which) ? event.which : event.keyCode;

        if (decimalValue.value != '' && decimalValue.value.lastIndexOf(".") > 0 &&
            decimalValue.value.split(".").length > 1 &&  event.key == '.') {
            event.preventDefault();
            return;
        }

        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57)) {

            event.preventDefault(); // peserve old value
        }

        return true;

    }

    
    public static getRequestHeaders(): RequestOptions {
        let userData= SessionDetails.GetUserInfo()
        // let token=  window.btoa('pharmacyManager:pharmacyManager123$');
         let token=  window.btoa(userData.userName+':'+userData.password);
        // console.log(token);        
         let headers = new Headers({ 'Content-Type': 'application/json' });
         headers.append('Cache-Control', 'no-cache, no-store, must-revalidate');
         headers.append('Pragma', 'no-cache');
         headers.append('Authorization', 'Basic '+token)
         return new RequestOptions({ headers: headers });;
     }
     compareTwoObjects (x,y) {
        var i, l, leftChain, rightChain;
      
        function compare2Objects (x, y) {
          var p;
      
          // remember that NaN === NaN returns false
          // and isNaN(undefined) returns true
          if (isNaN(x) && isNaN(y) && typeof x === 'number' && typeof y === 'number') {
               return true;
          }
      
          // Compare primitives and functions.     
          // Check if both arguments link to the same object.
          // Especially useful on the step where we compare prototypes
          if (x === y) {
              return true;
          }
      
          // Works in case when functions are created in constructor.
          // Comparing dates is a common scenario. Another built-ins?
          // We can even handle functions passed across iframes
          if ((typeof x === 'function' && typeof y === 'function') ||
             (x instanceof Date && y instanceof Date) ||
             (x instanceof RegExp && y instanceof RegExp) ||
             (x instanceof String && y instanceof String) ||
             (x instanceof Number && y instanceof Number)) {
              return x.toString() === y.toString();
          }
      
          // At last checking prototypes as good as we can
          if (!(x instanceof Object && y instanceof Object)) {
              return false;
          }
      
          if (x.isPrototypeOf(y) || y.isPrototypeOf(x)) {
              return false;
          }
      
          if (x.constructor !== y.constructor) {
              return false;
          }
      
          if (x.prototype !== y.prototype) {
              return false;
          }
      
          // Check for infinitive linking loops
          if (leftChain.indexOf(x) > -1 || rightChain.indexOf(y) > -1) {
               return false;
          }
      
          // Quick checking of one object being a subset of another.
          // todo: cache the structure of arguments[0] for performance
          for (p in y) {
              if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
                  return false;
              }
              else if (typeof y[p] !== typeof x[p]) {
                  return false;
              }
          }
      
          for (p in x) {
              if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
                  return false;
              }
              else if (typeof y[p] !== typeof x[p]) {
                  return false;
              }
      
              switch (typeof (x[p])) {
                  case 'object':
                  case 'function':
      
                      leftChain.push(x);
                      rightChain.push(y);
      
                      if (!compare2Objects (x[p], y[p])) {
                          return false;
                      }
      
                      leftChain.pop();
                      rightChain.pop();
                      break;
      
                  default:
                      if (x[p] !== y[p]) {
                          return false;
                      }
                      break;
              }
          }
      
          return true;
        }
      
        if (arguments.length < 1) {
          return true; //Die silently? Don't know how to handle such case, please help...
          // throw "Need two or more arguments to compare";
        }
      
        for (i = 1, l = arguments.length; i < l; i++) {
      
            leftChain = []; //Todo: this can be cached
            rightChain = [];
      
            if (compare2Objects(arguments[0], arguments[1])) {
                return false;
            }
        }
      
        return true;
      }
}