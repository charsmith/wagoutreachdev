import { UserLoginInfo } from "../models/userLoginInfo";
import { UserInfo } from "../models/userInfo";

export class SessionDetails {
    constructor() {
    }
    public static SetRoleID(roleID: string) {
        sessionStorage["roleID"] = JSON.stringify(roleID);
    }
    public static GetRoleID() {
        if (sessionStorage["roleID"] != null)
            return JSON.parse(sessionStorage["roleID"]);
        return null;
    }
    public static StoreId(storeId: string) {
        sessionStorage["storeId"] = JSON.stringify(storeId);
    }
    public static GetStoreId() {
        if (sessionStorage["storeId"] != null)
            return JSON.parse(sessionStorage["storeId"]);
        return null;
    }
    public static setState(state: string) {
        sessionStorage["state"] = JSON.stringify(state);
    }
    public static GetState() {
        if (sessionStorage["state"] != null)
            return JSON.parse(sessionStorage["state"]);
        return null;
    }
    public static SetActionType(details: any) {
        sessionStorage["actionType"] = JSON.stringify(details);

    }

    public static GetActionType() {
        if (sessionStorage["actionType"] != null)
            return JSON.parse(sessionStorage["actionType"]);
        return 0;
    }
    public static EventDetails(event_details: any) {
        sessionStorage["event_details"] = JSON.stringify(event_details);
    }
    public static GetEventDetails() {
        if (sessionStorage["event_details"] != null)
            return JSON.parse(sessionStorage["event_details"]);
        return null;
    }

    public static opportunitiesData(opportunitie_data: string) {
        sessionStorage["opportunitie_data"] = JSON.stringify(opportunitie_data);
    }
    public static GetopportunitiesData() {
        if (sessionStorage["opportunitie_data"] != null)
            return JSON.parse(sessionStorage["opportunitie_data"]);
        return null;
    }

    public static SetProgramType(details: any) {
        sessionStorage["programType"] = JSON.stringify(details);

    }

    public static GetProgramType() {
        if (sessionStorage["programType"] != null)
            return JSON.parse(sessionStorage["programType"]);
        return 0;
    }
    public static SetLogContact(log_contact: any) {
        sessionStorage["log_contact"] = JSON.stringify(log_contact);

    }

    public static GetLogContact() {
        if (sessionStorage["log_contact"] != null)
            return JSON.parse(sessionStorage["log_contact"]);
        return 0;
    }

    public static SetBusinessType(business_type: any) {
        sessionStorage["business_type"] = JSON.stringify(business_type);

    }

    public static GetBusinessType() {
        if (sessionStorage["business_type"] != null)
            return JSON.parse(sessionStorage["business_type"]);
        return 0;
    }

    public static SetFollowUp(followp_flag: boolean) {
        sessionStorage["followp_flag"] = JSON.stringify(followp_flag);

    }

    public static GetFollowUp() {
        if (sessionStorage["followp_flag"] != null)
            return JSON.parse(sessionStorage["followp_flag"]);
        return false;
    }

    public static SetPageName(page_name: any) {
        sessionStorage["page_name"] = JSON.stringify(page_name);

    }

    public static GetPageName() {
        if (sessionStorage["page_name"] != null)
            return JSON.parse(sessionStorage["page_name"]);
        return "";
    }
    public static SetAgreementPK(agreement_pk: any) {
        sessionStorage["agreement_pk"] = JSON.stringify(agreement_pk);
        console.log('SetAgreementPK', sessionStorage["agreement_pk"]);
    }

    public static GetAgreementPK() {
        if (sessionStorage["agreement_pk"] != null)
            return JSON.parse(sessionStorage["agreement_pk"]);
        console.log('GetAgreementPK', sessionStorage["agreement_pk"]);
        return 0;
    }

    public static SetUserInfo(userData: UserLoginInfo) {
        sessionStorage["userInfo"] = JSON.stringify(userData);
    }
    public static GetUserInfo() {
        if (sessionStorage["userInfo"] != null)
            return JSON.parse(sessionStorage["userInfo"]);
        return null;
    }


    public static SetUserProfile(userData: UserInfo) {
        sessionStorage["userProfile"] = JSON.stringify(userData);
    }
    public static GetUserProfile() {
        if (sessionStorage["userProfile"] != null)
            return JSON.parse(sessionStorage["userProfile"]);
        return null;
    }

    public static fromUserPage(user_page: boolean) {
        sessionStorage["user_page"] = JSON.stringify(user_page);
    }
    public static GetfromUserPage() {
        if (sessionStorage["user_page"] != null)
            return JSON.parse(sessionStorage["user_page"]);
        return false;
    }
}