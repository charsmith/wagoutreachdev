import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  DataTableModule,
  SharedModule, DataListModule, DropdownModule, GMapModule, Accordion, AccordionModule,
  AccordionTab, InputMaskModule, CalendarModule, Calendar, EditorModule, Editor, RadioButtonModule,
  Button, ButtonModule, ConfirmDialogModule, ConfirmationService, AutoCompleteModule, KeyFilterModule, MultiSelectModule
} from 'primeng/primeng';

import { AdminModule } from '../app/modules/admin/admin.module';
import { AuthModule } from '../app/modules/auth/auth.module';
//import { ReportsModule } from '../app/modules/reports/reports.module';
import { StoreModule } from '../app/modules/store/store.module';

import { CommonComponentsModule } from '../app/modules/common/common.module';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AdminRouteGuard } from './guards/admin.route.guard';
import { HomeRouteGuard } from './guards/home.route.gaurd';


//Components
import { AppComponent } from './app.component';

import { HeaderComponent } from '../app/modules/common/components/header/header.component';
//Routes
import { APP_ROUTES } from './routes/app.routes';
import { ReusableModule } from './modules/common/components/reusable.module';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';

import { LandingComponent } from './modules/auth/components/landing/landing.component';
import { AuthenticationService } from './modules/common/services/authentication.service';
import { ClinicDetailsModule } from './modules/clinicdetails/clinicdetails.module';
import { ResourcesModule } from './modules/resources/resources.module';
import { HomeModule } from './modules/home/home.module';
import { ContractagreementModule } from './modules/contractaggreement/contractagreement.module';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LandingComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AccordionModule,
    ResourcesModule,
    AdminModule,
    AuthModule,
    HomeModule,
    //ReportsModule,
    StoreModule,
    InputMaskModule,
    KeyFilterModule,
    CommonComponentsModule,
    AutoCompleteModule,
    ReusableModule,
    FormsModule,
    ButtonModule,
    RouterModule.forRoot(APP_ROUTES),
    BrowserAnimationsModule,
    ClinicDetailsModule,
    ContractagreementModule,
    LoggerModule.forRoot({ serverLoggingUrl: '/api/logs', level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR }),
  ],
  providers: [AuthenticationService, HomeRouteGuard, AdminRouteGuard],
 // providers: [AuthenticationService, HomeRouteGuard, AdminRouteGuard, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
