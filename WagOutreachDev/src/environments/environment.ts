// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
import { BlockedImmunizations } from '../app/config-files/blocked-immunizations';
import { ErrorMessages } from '../app/config-files/error-messages';

export const environment = {
  production: false,
  BlockedImmunizations : [{ BlockedImmunizations,
                            BlockedStartDate : '04/15/2018',
                            BlockedEndDate : '07/15/2018'
  }],
  ErrorMessages :  ErrorMessages,
  date:"01/12/2018",
  clientNumber:"1234" ,
  EmailURL:"http://192.168.2.109:4321/" ,
  EmailLogoImages:"SchedulerLogoImages" ,
  reportStartYear:"2014" ,
  emailSendTo:"harish.shetti@senecaglobal.com" ,
  emailToClinicalContracts:"harish.shetti@senecaglobal.com",
  clientLogosPath:"d:\WebToPrintResources\Walgreens\Development" ,
  outreachStartDateIP:"05/01/2016",
  outreachStartDateSO:"09/01/2015",
  resourcePath:"d:\WebToPrintResources\Walgreens\Production\Resources",
  reportsPath:"d:\WebToPrintResources\Walgreens\Development\Reports",
  voucherExpirationDateMax: "03/15/2019",
  MO_State_From:"03/01/2018",
  MO_State_TO:"12/01/2018",
  restrict_states:"[MO,DC]",
  corporateUploadPath:"d:\WebToPrintResources\Walgreens\Development\CorporateClinicsData",
 //API_URL: "http://tdwagsazureservices20180407022749.azurewebsites.net",
  //API_URL:"https://wagsoutreach.com",
  API_URL:"https://tdwagsazureservices.azurewebsites.net",
  RESOURCES: {
    VERIFY_USER_GET: "/values/GetUserLoginInformation",
    CONFIRM_CLINIC_DETAILS:"/values/confirmClinicDetails",
    GET_NATIONAL_CONTRACT_CLIENTS:"/api/getNationalContractClients",
    GET_STORE_PROFILE:"/api/Store_getStoreProfile",
    GET_ASSIGNED_BUSINESSES:"/api/Store_getAssignedBusinesses",
    GET_OUTREACH_STATUS_LABELS:"/api/ContactLog_getOutreachStatusLabels",
    GET_OUTREACH_CONTACT_LOGS:"/api/ContactLog_getOpportunityContactLogs",
    GET_OPPURTUNITY_DETAILS:"/api/Opportunities_getOpportunityDetails",
    GET_SCHEDULE_EVENT:"/api/ContactLog_getScheduleEvent",
   // CHANGE_PASSWORD_GET: "/User/ChangePassword",
   ADD_OPPORTUNTY:"api/Values/AddStoreBusiness",
   DELETE_NATIONAL_CONTRACT_CLIENTS:"/api/deleteNationalContractClients",
   POST_FOLLOWUP_EVENT : "/api/ContactLog_postFollowUpEvent",
   POST_SCHEDULE_EVENT:"/api/ContactLog_postSrScheduleEvent",
   POST_SIMPLE_EVENT : "/api/ContactLog_postSimpleEvent",
   UPDATE_SCHEDULE_EVENT:"/api/ContactLog_updateScheduleEvent",
   GET_FOLLOWUP_EVENT:"/api/ContactLog_getFollowUpEvent",
   UPDATE_FOLLOWUP_EVENT:"/api/ContactLog_updateFollowUpEvent",
   DELETE_CONTACT_LOG:"/api/ContactLog_deleteContactLog",
   GET_CONTRACT_AGREEMENT:"/api/ContractAgreement_getContractAgreement",
   GET_WAGSOUTREACH_COUNTS:"/api/Main_getWagsOutreachCounts",
   GET_IMMUNIZATION_LIST: "/api/ContractAgreement_getImmunizationList",
   UPDATE_CONTRACT_AGREEMENT: "/api/ContractAgreement_updateContractAgreement",
   UPDATE_CO_AGREEMENT: "/api/ContractAgreement_insertCommOutreachProgram",
   GET_PREVIOUSSEASON_CLINIC_LOCATIONS:"/api/ContractAgreement_getPreviousSeasonClinicLocations",
   POST_ENCRYPTION_DECRYPTION:"/api/Encryption_decrypt",
   GET_USER_PROFILE:"/api/Main_getUserProfile",
   INSERT_CHARITY_PROGRAM:"/api/ContractAgreement_insertCharityVoucherProgram"
  }};
  